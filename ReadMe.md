# The Energem Enigma
© 1987 by Peter Hoar, Catherine Lamb, David Hater, and Sean Boral

Genre: Science Fiction

YUID: 3d1gecnbau3e5yo4

IFDB: https://ifdb.org/viewgame?id=3d1gecnbau3e5yo4

## Info

Tools: UnQuill, The Inker, ngPAWS
- N Game fully tested?
- Y Missing images?
- Y Errors rendering images?
- Y Modern code for ngPAWS?
- 1 Edit txp (1) or sce (2)

## Notes

- This versions misses some graphics
- The graphics here are messed up
- Main loop of the game is different from the original (Scenario should be showed only once per session)

## Test ngPAWS port

https://interactivefiction.gitlab.io/The_Energem_Enigma-1987-Peter_Hoar-Catherine_Lamb-David_Hater-and-Sean_Boral/
