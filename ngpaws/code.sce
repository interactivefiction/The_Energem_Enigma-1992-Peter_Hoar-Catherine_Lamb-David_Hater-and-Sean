






;#define const aLight       = 0
;#define const aWearable    = 1
;#define const aContainer   = 2
;#define const aNPC         = 3
;#define const aConcealed   = 4
;#define const aEdible      = 5
;#define const aDrinkable   = 6
;#define const aEnterable   = 7
;#define const aFemale      = 8
;#define const aLockable    = 9
;#define const aLocked      = 10
;#define const aMale        = 11
;#define const aNeuter      = 12
;#define const aOpenable    = 13
;#define const aOpen        = 14
;#define const aPluralName  = 15
;#define const aTransparent = 16
;#define const aScenery     = 17
;#define const aSupporter   = 18
;#define const aSwitchable  = 19
;#define const aOn          = 20
;#define const aStatic      = 21


;#define const object_0 = 0
;#define const object_1_air = 1
;#define const object_2_air = 2
;#define const object_3_air = 3
;#define const object_4_air = 4
;#define const object_5_crys = 5
;#define const object_6_hand = 6
;#define const object_7_rope = 7
;#define const object_8 = 8
;#define const object_9_red = 9
;#define const object_10_gree = 10
;#define const object_11_blue = 11
;#define const object_12_pink = 12
;#define const object_13_pipe = 13
;#define const object_14 = 14
;#define const object_15_key = 15
;#define const object_16 = 16
;#define const object_17_cage = 17
;#define const object_18_bait = 18
;#define const object_19 = 19
;#define const object_20_mole = 20
;#define const object_21_book = 21
;#define const object_22_hat = 22
;#define const object_23 = 23
;#define const object_24 = 24
;#define const object_25_tin = 25
;#define const object_26 = 26
;#define const object_27_ashe = 27
;#define const object_28_shoe = 28
;#define const object_29 = 29
;#define const object_30 = 30
;#define const object_31 = 31
;#define const object_32 = 32
;#define const object_33 = 33
;#define const object_34_jump = 34
;#define const object_35_witc = 35
;#define const object_36 = 36
;#define const object_37 = 37
;#define const object_38 = 38
;#define const object_39 = 39
;#define const object_40 = 40
;#define const object_41 = 41
;#define const object_42_ball = 42
;#define const object_43_pape = 43
;#define const object_44_fag = 44
;#define const object_45_chai = 45
;#define const object_46 = 46
;#define const object_47 = 47
;#define const object_48 = 48
;#define const object_49 = 49
;#define const object_50 = 50
;#define const object_51_scre = 51
;#define const object_52_magn = 52
;#define const object_53 = 53
;#define const object_54_face = 54
;#define const object_55_magn = 55
;#define const object_56 = 56
;#define const object_57 = 57
;#define const object_58_buck = 58


;#define const room_0 = 0
;#define const room_1 = 1
;#define const room_2 = 2
;#define const room_3 = 3
;#define const room_4 = 4
;#define const room_5 = 5
;#define const room_6 = 6
;#define const room_7 = 7
;#define const room_8 = 8
;#define const room_9 = 9
;#define const room_10 = 10
;#define const room_11 = 11
;#define const room_12 = 12
;#define const room_13 = 13
;#define const room_14 = 14
;#define const room_15 = 15
;#define const room_16 = 16
;#define const room_17 = 17
;#define const room_18 = 18
;#define const room_19 = 19
;#define const room_20 = 20
;#define const room_21 = 21
;#define const room_22 = 22
;#define const room_23 = 23
;#define const room_24 = 24
;#define const room_25 = 25
;#define const room_26 = 26
;#define const room_27 = 27
;#define const room_28 = 28
;#define const room_29 = 29
;#define const room_30 = 30
;#define const room_31 = 31
;#define const room_32 = 32
;#define const room_33 = 33
;#define const room_34 = 34
;#define const room_35 = 35
;#define const room_36 = 36
;#define const room_37 = 37
;#define const room_38 = 38
;#define const room_39 = 39
;#define const room_40 = 40
;#define const room_41 = 41
;#define const room_42 = 42
;#define const room_43 = 43
;#define const room_44 = 44
;#define const room_45 = 45
;#define const room_46 = 46
;#define const room_47 = 47
;#define const room_48 = 48
;#define const room_49 = 49
;#define const room_50 = 50
;#define const room_51 = 51
;#define const room_52 = 52
;#define const room_53 = 53
;#define const room_54 = 54
;#define const room_55 = 55
;#define const room_56 = 56
;#define const room_57 = 57
;#define const room_58 = 58
;#define const room_59 = 59
;#define const room_60 = 60
;#define const room_61 = 61
;#define const room_62 = 62
;#define const room_63 = 63
;#define const room_64 = 64
;#define const room_65 = 65
;#define const room_66 = 66
;#define const room_67 = 67
;#define const room_68 = 68
;#define const room_69 = 69
;#define const room_70 = 70
;#define const room_71 = 71
;#define const room_72 = 72
;#define const room_73 = 73
;#define const room_74 = 74
;#define const room_75 = 75
;#define const room_76 = 76
;#define const room_77 = 77
;#define const room_78 = 78
;#define const room_79 = 79
;#define const room_80 = 80
;#define const room_81 = 81
;#define const room_82 = 82
;#define const room_83 = 83

;#define const room_noncreated = 252 ; destroying an object moves it to this room
;#define const room_worn = 253 ; wearing an object moves it to this room
;#define const room_carried = 254 ; carrying an object moves it to this room
;#define const room_here = 255 ; not used by any Quill condacts










;#define const yesno_is_dark                       =   0
;#define const total_carried                       =   1
;#define const countdown_location_description      =   2
;#define const countdown_location_description_dark =   3
;#define const countdown_object_description_unlit  =   4
;#define const countdown_player_input_1            =   5
;#define const countdown_player_input_2            =   6
;#define const countdown_player_input_3            =   7
;#define const countdown_player_input_4            =   8
;#define const countdown_player_input_dark         =   9
;#define const countdown_player_input_unlit        =  10
;#define const game_flag_11                        = 111 ; ngPAWS assigns a special meaning to flag 11
;#define const game_flag_12                        = 112 ; ngPAWS assigns a special meaning to flag 12
;#define const game_flag_13                        = 113 ; ngPAWS assigns a special meaning to flag 13
;#define const game_flag_14                        = 114 ; ngPAWS assigns a special meaning to flag 14
;#define const game_flag_15                        = 115 ; ngPAWS assigns a special meaning to flag 15
;#define const game_flag_16                        = 116 ; ngPAWS assigns a special meaning to flag 16
;#define const game_flag_17                        =  17
;#define const game_flag_18                        =  18
;#define const game_flag_19                        =  19
;#define const game_flag_20                        =  20
;#define const game_flag_21                        =  21
;#define const game_flag_22                        =  22
;#define const game_flag_23                        =  23
;#define const game_flag_24                        =  24
;#define const game_flag_25                        =  25
;#define const game_flag_26                        =  26
;#define const game_flag_27                        =  27
;#define const pause_parameter                     =  28
;#define const bitset_graphics_status              =  29
;#define const total_player_score                  =  30
;#define const total_turns_lower                   =  31
;#define const total_turns_higher                  =  32
;#define const word_verb                           =  33
;#define const word_noun                           =  34
;#define const room_number                         =  38 ; (35 in The Quill, but 38 in ngPAWS)




;#define const pause_sound_effect_tone_increasing = 1 ; increasing tone (value is the duration)
;#define const pause_sound_effect_telephone       = 2 ; ringing telephone (value is the number of rings)
;#define const pause_sound_effect_tone_decreasing = 3 ; decreasing tone (opposite of effect 1)
;#define const pause_sound_effect_white_noise     = 5 ; white noise (resembles an audience clapping, value is the number of repetitions)
;#define const pause_sound_effect_machine_gun     = 6 ; machine gun noise (value is the duration)

;#define const pause_flash = 4 ; flash the screen and the border (value is the number of flashes)
;#define const pause_box_wipe = 9 ; a series of coloured boxes expands out of the centre of the screen at speed

;#define const pause_font_default = 7 ; switch to the default font
;#define const pause_font_alternate = 8 ; switch to the alternative font.

;#define const pause_change_youcansee = 10 ; Change the "You can also see" message to the message number passed to PAUSE

;#define const pause_restart = 12 ; Restart the game without warning.
;#define const pause_reboot = 13 ; Reboot the Spectrum without warning
;#define const pause_ability = 11 ; Set the maximum number of objects carried at once to the value passed to PAUSE
;#define const pause_ability_plus = 14 ; Increase number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_ability_minus = 15 ; Decrease number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_toggle_graphics = 19 ; switch graphics off (for PAUSE 255) or on (for any other value)
;#define const pause_ram_save_load = 21 ; RAMload (for PAUSE 50) or RAMsave (for any other value)
;#define const pause_set_identity_byte = 22 ; Set the identity in saved games to the value passed to PAUSE

;#define const pause_click_effect_1 = 16
;#define const pause_click_effect_2 = 17
;#define const pause_click_effect_3 = 18


;#define pic location4.svg    =   4
;#define pic location8.svg    =   8
;#define pic location9.svg    =   9
;#define pic location21.svg   =  21
;#define pic location27.svg   =  27
;#define pic location28.svg   =  28
;#define pic location30.svg   =  30
;#define pic location37.svg   =  37
;#define pic location40.svg   =  40
;#define pic location42.svg   =  42
;#define pic location43.svg   =  43
;#define pic location44.svg   =  44
;#define pic location51.svg   =  51
;#define pic location52.svg   =  52
;#define pic location55.svg   =  55
;#define pic location56.svg   =  56
;#define pic location61.svg   =  61
;#define pic location75.svg   =  75







/CTL






/VOC






n         1    verb
nort      1    verb
s         2    verb
sout      2    verb
e         3    verb
east      3    verb
w         4    verb
west      4    verb
acro      9    verb
asce      9    verb
boar      9    verb
clim      9    verb
in        9    verb
u         9    noun
up        9    verb
d        10    noun
desc     10    verb
down     10    verb
out      10    verb
air      11    noun
bott     11    noun
oxyg     11    noun
chec     12    verb
buck     13    noun
pail     13    noun






hand     14    noun
leve     14    noun
rust     14    noun
chan     15    verb
swap     15    verb
well     16    noun
exam     17    verb
read     17    verb
sear     17    verb
rope     18    noun
boul     19    noun
door     19    noun
port     19    noun
rock     19    noun
slab     19    noun
tunn     19    noun
notc     20    noun
ente     21    verb
go       21    verb
inse     21    verb
lowe     22    verb
turn     22    verb
wind     22    verb
crys     23    noun
mete     23    noun
cont     24    noun
pane     24    noun
rece     24    noun
red      25    noun
gree     26    noun
blue     27    noun
pink     28    noun
pres     29    verb
push     29    verb
blac     30    noun
dig      31    verb
lay      32    verb
lie      32    verb
slee     32    verb
musi     33    noun
pipe     33    noun
blow     34    verb
play     34    verb
key      35    noun
ches     36    noun
prin     37    noun
trac     37    noun
open     38    verb
rele     38    verb
unbo     39    verb
unlo     39    verb
bask     41    noun
cage     41    noun
trap     41    noun
bait     42    noun
chee     42    noun
plan     42    noun
set      43    verb
paus     44    verb
wait     44    verb
capt     45    noun
mole     45    noun
drin     46    verb
eat      46    verb
junk     47    noun
pile     47    noun
rubb     47    noun
thin     47    noun
book     48    noun
manu     48    noun
page     48    noun
cabl     49    noun
lead     49    noun






clos     50    verb
drag     50    verb
pull     50    verb
shut     50    verb
slid     50    verb
viso     51    noun
conn     52    verb
join     52    verb
plug     52    verb
godd     53    noun
idol     53    noun
stat     53    noun
alta     54    noun
fix      55    verb
mend     55    verb
patc     55    verb
repa     55    verb
boat     56    noun
foll     57    verb
bush     58    noun
root     58    noun
tree     58    noun
tar      59    noun
tin      60    noun
ashe     63    noun
ashes    63    verb

fire     63    verb
lamp     63    noun

lant     63    noun

smok     63    noun

torc     63    noun

keep     64    noun
ligh     64    noun
stri     64    noun
box      65    noun
boxe     65    noun
matc     65    noun
tool     65    noun
ghos     66    noun
man      66    noun
warr     66    noun
toys     70    noun
off      72    noun

cran     73    noun

jump     74    verb

leap     74    verb
pole     74    noun

vaul     74    noun

ravi     75    noun
rive     75    noun
carp     76    noun
:carp     76    verb
leav     76    noun
leave     76    verb
swim     77    verb
on       78    noun

sand     79    noun
benc     80    noun
desk     80    noun
tabl     80    noun
bed      81    noun
beds     81    noun
bunk     81    noun
matt     81    noun
wall     82    noun
witc     83    noun
labe     84    noun
spec     85    verb
mate     86    noun
bolt     87    noun
undo     88    verb
cabi     89    noun
glas     89    noun
ball     90    noun
beac     90    noun
ledg     91    noun
nest     92    noun
egg      93    noun
pape     94    noun
weig     94    noun
ciga     95    noun
fag      95    noun
fags     95    noun
give     96    verb
pudd     97    noun
wate     97    noun
chai     98    noun
hole     99    noun
coll    100    verb
g       100    verb
get     100    verb
grab    100    verb
pick    100    verb
t       100    verb
take    100    verb
drop    101    verb
put     101    verb
remo    102    verb
wear    103    verb
inven   104    noun
i       104    verb
inve    104    noun

look    105    verb
r       105    verb
rede    105    verb
q       106    verb
quit    106    verb
stop    106    verb

save    107    verb

load    108    verb

rest    108    verb
type    109    noun
new     110    verb
norm    111    verb
insc    113    noun
plaq    113    noun
sign    113    noun
cloc    114    noun
gran    114    noun
face    115    noun
jewe    116    noun
scre    117    noun
magn    118    noun
pend    119    noun
plat    120    noun
butt    121    noun
pict    122    verb
word    123    verb
scor    124    verb
clue    125    verb
help    125    verb
span    126    noun
cog     127    noun
fill    128    verb
disc    129    verb
rams    150    verb
raml    151    verb
boot    249    noun
shoe    249    noun
hat     250    noun
helm    250    noun




AND          2    conjunction
THEN         2    conjunction






/STX





/0
Everything is dark. You can't see.<br>
/1
{TEXTPIC|y.svg|1}ou can also see:-<br>
/2
What do you want to do now?
/3
{EXITS|@38|1000}
/4
Please enter your next order
/5
Please type in your orders
/6
Sorry, I don't understand that. Try some different words.<br>
/7
\nYou can't go in that direction.<br>
/8
\nYou can't.<br>
/9
{TEXTPIC|y.svg|1}ou have with you:-\n
/10
 WORN ▫
/11
▫ Nothing at all. ▫<br>
/12
\nAre you sure you wish to quit?<br>
/13
\nWould you like another game?<br>
/14
Thank you for playing....\n\n{CLASS|center|'THE ENERGEM ENIGMA'}<br>
/15
DONE<br>
/16
{CLASS|center|Press any key to continue}<br>
/17
\nYou have used 
/18
 unit
/19
s
/20
 of air.<br>
/21
You have scored 
/22
%.<br>
/23
You're not wearing it.<br>
/24
You can't. Your hands are full.<br>
/25
You already have it.<br>
/26
You cannot see it here.<br>
/27
You can't carry any more.<br>
/28
You don't have it.<br>
/29
You're already wearing it.<br>
/30
Y<br>
/31
N<br>
/32
More...<br>
/33
><br>
/34
<br>
/35
Time passes...<br>
/36

/37

/38

/39

/40
\nYou can't.<br>
/41
\nYou can't.<br>
/42
You can't. Your hands are full.<br>
/43
You can't carry any more.<br>
/44
You put {OREF} into<br>
/45
{OREF} is not into<br>
/46
<br>
/47
<br>
/48
<br>
/49
You don't have it.<br>
/50
\nYou can't.<br>
/51
.<br>
/52
That is not into<br>
/53
nothing at all<br>
/54
File not found.<br>
/55
File corrupt.<br>
/56
I/O error. File not saved.<br>
/57
Directory full.<br>
/58
Please enter savegame name you used when saving the game status.
/59
Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>
/60
Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.
/61
<br>
/62
Sorry? Please try other words.<br>
/63
Here<br>
/64
you can see<br>
/65
you can see<br>
/66
inside you see<br>
/67
on top you see<br>






/MTX


/1000
Exits: 
/1001
You can't see any exits

/1003
{ACTION|nort|nort}
/1004
{ACTION|sout|sout}
/1005
{ACTION|east|east}
/1006
{ACTION|west|west}
/1011
{ACTION|up|up}
/1012
{ACTION|out|out}






/OTX
/0 ; sust(object_0//0)
▫ A Lantern ▫
/1 ; sust(object_1_air//1)
▫ 4 oxygen bottles ▫
/2 ; sust(object_2_air//2)
▫ 3 Oxygen Bottles ▫
/3 ; sust(object_3_air//3)
▫ 2 Oxygen Bottles ▫
/4 ; sust(object_4_air//4)
▫ 1 Oxygen Bottle ▫
/5 ; sust(object_5_crys//5)
▫ A Crystalometer ▫
/6 ; sust(object_6_hand//6)
▫ A Rusty Handle ▫
/7 ; sust(object_7_rope//7)
▫ A Length of Rope ▫
/8 ; sust(object_8//8)
▫ An Open Portcullis ▫
/9 ; sust(object_9_red//9)
▫ A Red Crystal ▫
/10 ; sust(object_10_gree//10)
▫ A Green Crystal ▫
/11 ; sust(object_11_blue//11)
▫ A Blue Crystal ▫
/12 ; sust(object_12_pink//12)
▫ A Pink Crystal ▫
/13 ; sust(object_13_pipe//13)
▫ A Pipe ▫
/14 ; sust(object_14//14)
▫ A Handle in the Wall ▫
/15 ; sust(object_15_key//15)
▫ A Small Key ▫
/16 ; sust(object_16//16)
▫ A Mole ▫
/17 ; sust(object_17_cage//17)
▫ A Basket ▫
/18 ; sust(object_18_bait//18)
▫ A Cheese Plant ▫
/19 ; sust(object_19//19)
▫ A Mole Trap ▫
/20 ; sust(object_20_mole//20)
▫ A Captive Mole ▫
/21 ; sust(object_21_book//21)
▫ A Book Page ▫
/22 ; sust(object_22_hat//22)
▫ A Helmet ▫
/23 ; sust(object_23//23)
▫ A Handle in the Crank ▫
/24 ; sust(object_24//24)
▫ A Boat ▫
/25 ; sust(object_25_tin//25)
▫ An Empty Tin ▫
/26 ; sust(object_26//26)
▫ A Tin of Tar ▫
/27 ; sust(object_27_ashe//27)
▫ A Lantern ▫
/28 ; sust(object_28_shoe//28)
▫ A Pair of Climbing Boots ▫
/29 ; sust(object_29//29)
▫ A Boulder ▫
/30 ; sust(object_30//30)
▫ A Lantern ▫
/31 ; sust(object_31//31)
▫ A Ghostly Warrior ▫
/32 ; sust(object_32//32)
▫ A Dead Warrior ▫
/33 ; sust(object_33//33)
▫ A Large Sword ▫
/34 ; sust(object_34_jump//34)
▫ A Pole ▫
/35 ; sust(object_35_witc//35)
▫ A Witch ▫
/36 ; sust(object_36//36)
▫ A Pile of Sand ▫
/37 ; sust(object_37//37)
▫ A Large, Smooth Slab of Rock ▫
/38 ; sust(object_38//38)
▫ A Small Lever ▫
/39 ; sust(object_39//39)

/40 ; sust(object_40//40)
▫ A Caved in Exit ▫
/41 ; sust(object_41//41)
▫ An Excavated Tunnel ▫
/42 ; sust(object_42_ball//42)
▫ A Beach Ball ▫
/43 ; sust(object_43_pape//43)
▫ A Paperweight ▫
/44 ; sust(object_44_fag//44)
▫ A packet of cigarettes ▫
/45 ; sust(object_45_chai//45)
▫ A length of chain ▫
/46 ; sust(object_46//46)
▫ A man ▫
/47 ; sust(object_47//47)
▫ A puddle ▫
/48 ; sust(object_48//48)
▫ A trapdoor ▫
/49 ; sust(object_49//49)
▫ An entrance down ▫
/50 ; sust(object_50//50)
▫ A tin of water ▫
/51 ; sust(object_51_scre//51)
▫ A screwdriver ▫
/52 ; sust(object_52_magn//52)
▫ a small magnet ▫
/53 ; sust(object_53//53)
▫ A Grandfather clock ▫
/54 ; sust(object_54_face//54)
▫ a clock face ▫
/55 ; sust(object_55_magn//55)
▫ a plate stuck to a magnet ▫
/56 ; sust(object_56//56)
▫ A key in the clock ▫
/57 ; sust(object_57//57)
▫ A button on the wall ▫
/58 ; sust(object_58_buck//58)
▫ A rusty bucket ▫






/LTX
/0 ; TODO: consider adding relevant links: {ACTION|nort|nort} ; sust(room_0//0)
{CLASS|center|SCENARIO\n}After Professor Roberts's rescue from the planet Arg, he has been able to continue working  on his latest  project.  One problem is that during  his captivation, he was given a truth serum.  So now the  enemy know what he has been working on.\n\nFor his next  project, four rare crystals are needed.  These  are energy  emitting  gems, and  are more commonly called 'ENERGEMS'. There is only one planet, in the Glenbo  system, that  these gems are known to exist.
/1 ; TODO: consider adding relevant links: {ACTION|exam bait|exam bait}, {ACTION|g bait|g bait}, {ACTION|spec mate|spec mate} ; sust(room_1//1)
{TEXTPIC|y.svg|1}ou are in a pleasant clearing surrounded by various types of plant life. There are exits to the {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/2 ; sust(room_2//2)
{TEXTPIC|y.svg|1}ou are at the {ACTION|northern|northern} end of a long winding road. It is very muddy. Exits are to the {ACTION|NORTH|NORTH} and to the {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/3 ; TODO: consider adding relevant links: {ACTION|exam trac|exam trac}, {ACTION|s|s} ; sust(room_3//3)
{TEXTPIC|y.svg|1}ou are walking down a narrow pathway. In the mud are some strange tracks, presumably from the planet's animal life. Exits lead {ACTION|NORTH|NORTH} and SOUTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/4 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|d buck|d buck}, {ACTION|d hand|d hand}, {ACTION|exam buck|exam buck}, {ACTION|exam well|exam well}, {ACTION|g hand|g hand}, {ACTION|go well|go well}, {ACTION|turn hand|turn hand} ; sust(room_4//4)
{TEXTPIC|y.svg|1}ou are walking down a narrow {ACTION|NORTH|NORTH} - {ACTION|SOUTH|SOUTH} pathway. To the {ACTION|south|south} you can see the mountains.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/5 ; TODO: consider adding relevant links: {ACTION|n|n} ; sust(room_5//5)
{TEXTPIC|y.svg|1}ou are in a wide sweeping valley. The only possible exits are NORTH and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/6 ; sust(room_6//6)
{TEXTPIC|y.svg|1}ou are on a narrow ledge. The floor is a little slippery so you must tread carefully. Exits are {ACTION|NORTH|NORTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/7 ; sust(room_7//7)
{TEXTPIC|y.svg|1}ou are at the bottom of a long steep mountain slope. There is little vegetation here. Exits lead {ACTION|EAST|EAST}, {ACTION|NORTH|NORTH} and {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/8 ; sust(room_8//8)
{TEXTPIC|y.svg|1}ou are at the end of a steep sloping path. Towards the {ACTION|west|west} you can see the mountain peak. Exits are {ACTION|SOUTH|SOUTH} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/9 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|s|s}, {ACTION|w|w} ; sust(room_9//9)
{TEXTPIC|y.svg|1}ou are in a crossover of three passages. Choice of exits are DOWN, WEST or SOUTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/10 ; TODO: consider adding relevant links: {ACTION|dig|dig}, {ACTION|n|n}, {ACTION|s|s} ; sust(room_10//10)
{TEXTPIC|y.svg|1}ou are at the edge of an under ground river. The floor is very sandy. Exits are NORTH or SOUTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/11 ; TODO: consider adding relevant links: {ACTION|e|e}, {ACTION|n|n} ; sust(room_11//11)
{TEXTPIC|y.svg|1}ou are now swimming in the underground river. You can go NORTH or EAST.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/12 ; sust(room_12//12)
{TEXTPIC|y.svg|1}ou are at the foot of the mountain. It is a long {ACTION|climb|climb} to the top. The only possible exits lead {ACTION|UP|UP} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/13 ; sust(room_13//13)
{TEXTPIC|y.svg|1}ou are now half-way up the mountain. A narrow ledge leads to the {ACTION|WEST|WEST}. Other exits leads {ACTION|EAST|EAST} or {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/14 ; TODO: consider adding relevant links: {ACTION|e|e}, {ACTION|exam alta|exam alta}, {ACTION|exam insc|exam insc}, {ACTION|exam stat|exam stat}, {ACTION|g hat|g hat}, {ACTION|g stat|g stat} ; sust(room_14//14)
{TEXTPIC|y.svg|1}ou are in what appears to be an ancient place of worship. There is a kind of altar on one wall with an inscription above it. The only exit is EAST.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/15 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|u|u} ; sust(room_15//15)
{TEXTPIC|y.svg|1}ou are in a steep sloping passage with a low ceiling. You can go either UP or DOWN.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/16 ; TODO: consider adding relevant links: {ACTION|exam hole|exam hole}, {ACTION|u|u} ; sust(room_16//16)
{TEXTPIC|y.svg|1}ou are in a low damp cave with a sandy floor. The effect of the light reflecting through the waterfall causes strange dancing shadows to fill one wall. You can see a small hole high above. The only other exit is to the {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/17 ; TODO: consider adding relevant links: {ACTION|d|d} ; sust(room_17//17)
{TEXTPIC|y.svg|1}ou are on a mountain ledge. Looking down you can see a small settlement. You can go {ACTION|WEST|WEST} or {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/18 ; sust(room_18//18)
{TEXTPIC|y.svg|1}ou are on the mountain peak. Exits are {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/19 ; TODO: consider adding relevant links: {ACTION|exam rubb|exam rubb} ; sust(room_19//19)
{TEXTPIC|y.svg|1}ou are {ACTION|in|in} an old mountain hide -away.  It was obviously used as a camp by bandits, as there are still a few of their things left lying around.  You are able to go {ACTION|UP|UP}, {ACTION|SOUTH|SOUTH} or {ACTION|EAST|EAST}.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/20 ; sust(room_20//20)

/21 ; TODO: consider adding relevant links: {ACTION|exam ches|exam ches}, {ACTION|g ches|g ches}, {ACTION|g crys|g crys}, {ACTION|open ches|open ches}, {ACTION|unlo ches|unlo ches} ; sust(room_21//21)
{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small low cave. The only possible exit is {ACTION|UP|UP}\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/22 ; TODO: consider adding relevant links: {ACTION|exam egg|exam egg}, {ACTION|exam ledg|exam ledg}, {ACTION|exam nest|exam nest}, {ACTION|g blue|g blue}, {ACTION|g egg|g egg}, {ACTION|put pape|put pape} ; sust(room_22//22)
{TEXTPIC|i.svg|1}t is very narrow here. Loose rocks fall over the edge as you cautiously make your way along the ledge. Exits lead {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/23 ; sust(room_23//23)
{TEXTPIC|y.svg|1}ou are at the side of a river. You can see a waterfall to the {ACTION|SOUTH|SOUTH}. Other exits lead to the {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/24 ; TODO: consider adding relevant links: {ACTION|exam bush|exam bush}, {ACTION|exam trac|exam trac}, {ACTION|fix boat|fix boat}, {ACTION|foll trac|foll trac}, {ACTION|shut boat|shut boat}, {ACTION|tar boat|tar boat}, {ACTION|u|u} ; sust(room_24//24)
{TEXTPIC|y.svg|1}ou are walking along the {ACTION|west|west} bank of the river. There are some strange tracks in the mud. The path leads {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/25 ; TODO: consider adding relevant links: {ACTION|exam ashe|exam ashe} ; sust(room_25//25)
{TEXTPIC|y.svg|1}ou have entered a clearing. The remains of a small camp fire are in the middle. The only possible exit is {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/26 ; sust(room_26//26)
{TEXTPIC|y.svg|1}ou are floating gently down a river. The river flows from the {ACTION|SOUTH|SOUTH} to the {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/27 ; sust(room_27//27)
{TEXTPIC|y.svg|1}ou are at the edge of the forest. There is a forest path that leads {ACTION|WEST|WEST}. The only other exit is to the {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/28 ; TODO: consider adding relevant links: {ACTION|exam leav|exam leav} ; sust(room_28//28)
{TEXTPIC|y.svg|1}ou are now a little deeper into the forest. It is darker here as the trees are blocking most of the sunlight. The ground is covered with a thick carpet of leaves. The path leads {ACTION|WEST|WEST} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/29 ; TODO: consider adding relevant links: {ACTION|exam rive|exam rive}, {ACTION|jump rive|jump rive}, {ACTION|jump u|jump u}, {ACTION|swim|swim} ; sust(room_29//29)
{TEXTPIC|y.svg|1}ou are on the north bank of the forest river. The only visible exit leads {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/30 ; sust(room_30//30)
{TEXTPIC|t.svg|1}he river is now flowing through the middle of the forest. The trees overhang from each bank to meet each other in the middle. You can go {ACTION|NORTH|NORTH} or {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/31 ; TODO: consider adding relevant links: {ACTION|n|n} ; sust(room_31//31)
{TEXTPIC|t.svg|1}he current is much stronger here. You can see a whirlpool to the NORTH. You can also go {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/32 ; TODO: consider adding relevant links: {ACTION|exam rive|exam rive}, {ACTION|jump rive|jump rive}, {ACTION|jump u|jump u}, {ACTION|swim|swim} ; sust(room_32//32)
{TEXTPIC|y.svg|1}ou are on the south bank of the forest river. The only possible exit is {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/33 ; sust(room_33//33)
{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small clearing that has one large tree standing {ACTION|in|in} the centre. You can go {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/34 ; sust(room_34//34)
{TEXTPIC|y.svg|1}ou are now at the top of the tree. You can see a large hole amongst the branches. Exits are {ACTION|DOWN|DOWN} or {ACTION|IN|IN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/35 ; sust(room_35//35)
{TEXTPIC|y.svg|1}ou are inside the large hollow tree, it is damp and stuffy here. The only possible exits lead {ACTION|OUT|OUT} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/36 ; TODO: consider adding relevant links: {ACTION|exam box|exam box} ; sust(room_36//36)
{TEXTPIC|t.svg|1}his is an ante-chamber and has been used as a store in the past, as there are still a few empty boxes lying around. You can go {ACTION|NORTH|NORTH} or {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/37 ; TODO: consider adding relevant links: {ACTION|exam bed|exam bed}, {ACTION|g red|g red}, {ACTION|lie|lie}, {ACTION|u rope|u rope} ; sust(room_37//37)
{TEXTPIC|y.svg|1}ou are in an old dormitory. Exits are {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and to the {ACTION|EAST|EAST}\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/38 ; TODO: consider adding relevant links: {ACTION|exam bed|exam bed}, {ACTION|exam wall|exam wall} ; sust(room_38//38)
{TEXTPIC|y.svg|1}ou are in a small cave, the walls of which are made of some sort of mineral unknown to you. Exits lead {ACTION|EAST|EAST}, {ACTION|WEST|WEST} and {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/39 ; TODO: consider adding relevant links: {ACTION|exam bush|exam bush}, {ACTION|fill tin|fill tin}, {ACTION|g tar|g tar} ; sust(room_39//39)
{TEXTPIC|y.svg|1}ou are in a small round tunnel that has obviously been made by an intelligent being. The roots of the tree help to keep the walls from caving in. Exits are {ACTION|WEST|WEST} and {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/40 ; sust(room_40//40)
{TEXTPIC|t.svg|1}here are two passages ahead of you. One passage leads to the {ACTION|WEST|WEST}, whilst the other will take you {ACTION|NORTH|NORTH}. You are also able to go {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/41 ; sust(room_41//41)
{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a large earthy cave deep down {ACTION|in|in} the ground. You can go {ACTION|NORTH|NORTH} or {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/42 ; sust(room_42//42)
{TEXTPIC|y.svg|1}ou are in a maze of tunnels. Exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/43 ; sust(room_43//43)
{TEXTPIC|y.svg|1}ou are in a maze of tunnels. Exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/44 ; TODO: consider adding relevant links: {ACTION|go rock|go rock}, {ACTION|open mole|open mole} ; sust(room_44//44)
{TEXTPIC|y.svg|1}ou are in a maze of tunnels. Exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/45 ; TODO: consider adding relevant links: {ACTION|g cage|g cage}, {ACTION|g mole|g mole}, {ACTION|set cage|set cage}, {ACTION|wait|wait} ; sust(room_45//45)
{TEXTPIC|y.svg|1}ou are in an animals lair. The floor is littered with smelly droppings and discarded food. There are exits to the {ACTION|SOUTH|SOUTH} and to the {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/46 ; sust(room_46//46)
{TEXTPIC|y.svg|1}ou are in a slowly widening tunnel. There is a strong odour of cheese here. Exits are {ACTION|NORTH|NORTH} and {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/47 ; sust(room_47//47)
{TEXTPIC|y.svg|1}ou are crawling through a very recently excavated tunnel. It appears to have been dug by some strange animal. You can go {ACTION|DOWN|DOWN} or {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/48 ; sust(room_48//48)
{TEXTPIC|y.svg|1}ou are in a short passage that appears to be the most recently mined part of the system. Exits are {ACTION|NORTH|NORTH} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/49 ; TODO: consider adding relevant links: {ACTION|exam rock|exam rock}, {ACTION|go rock|go rock}, {ACTION|open rock|open rock}, {ACTION|u|u}, {ACTION|undo bolt|undo bolt}, {ACTION|unlo rock|unlo rock} ; sust(room_49//49)
{TEXTPIC|y.svg|1}ou are now at the end of the passage. There is a door at the far end and an exit {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/50 ; TODO: consider adding relevant links: {ACTION|exam man|exam man}, {ACTION|exam tabl|exam tabl}, {ACTION|give fag|give fag} ; sust(room_50//50)
{TEXTPIC|y.svg|1}ou are in a cave almost full of boxes. There is a table in the middle of the room. The only possible exit is {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/51 ; sust(room_51//51)
{TEXTPIC|y.svg|1}ou are in a small crossover of three passages. Exits lead to the {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/52 ; sust(room_52//52)
{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small gallery. You can see a recently dug tunnel leading {ACTION|UP|UP}. There are also exits to the {ACTION|NORTH|NORTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/53 ; sust(room_53//53)
{TEXTPIC|t.svg|1}he walls here are well shored. Timbers at least a foot thick support both walls and ceiling. The only exit is {ACTION|west|west}.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/54 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|e|e}, {ACTION|eat wate|eat wate}, {ACTION|exam rock|exam rock}, {ACTION|exam wate|exam wate}, {ACTION|fill tin|fill tin}, {ACTION|g wate|g wate}, {ACTION|open cage|open cage}, {ACTION|open rock|open rock}, {ACTION|shut cage|shut cage} ; sust(room_54//54)
{TEXTPIC|y.svg|1}ou are in a very well worked section of the mine. You can go {ACTION|WEST|WEST} or EAST.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/55 ; sust(room_55//55)
{TEXTPIC|t.svg|1}here is a choice of passages here. You can go {ACTION|SOUTH|SOUTH}, {ACTION|WEST|WEST} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/56 ; sust(room_56//56)
{TEXTPIC|y.svg|1}ou are at a crossover of three paths. Tunnels lead {ACTION|SOUTH|SOUTH}, {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/57 ; TODO: consider adding relevant links: {ACTION|exam cloc|exam cloc}, {ACTION|exam cog|exam cog}, {ACTION|exam face|exam face}, {ACTION|exam hole|exam hole}, {ACTION|exam insc|exam insc}, {ACTION|exam jewe|exam jewe}, {ACTION|exam pane|exam pane}, {ACTION|exam plat|exam plat}, {ACTION|g key|g key}, {ACTION|g pink|g pink}, {ACTION|go key|go key}, {ACTION|pres pend|pres pend}, {ACTION|remo face|remo face}, {ACTION|remo plat|remo plat}, {ACTION|turn cloc|turn cloc}, {ACTION|turn key|turn key}, {ACTION|undo scre|undo scre} ; sust(room_57//57)
{TEXTPIC|y.svg|1}ou are at the edge of a small shallow pool, looking around you can see that you are in an old mine. Possible exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/58 ; sust(room_58//58)
{TEXTPIC|y.svg|1}ou are knee deep in a small pool of water. There are some pieces of the boat floating around in the water. The only possible exit leads {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/59 ; sust(room_59//59)

/60 ; sust(room_60//60)
{TEXTPIC|y.svg|1}ou are on the main road of a small village. It seems to be quite deserted. Exits are {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/61 ; TODO: consider adding relevant links: {ACTION|n|n}, {ACTION|pres blac|pres blac}, {ACTION|wait|wait} ; sust(room_61//61)
{TEXTPIC|y.svg|1}ou are on a sandy shore.  The tide is out.  Possible exits lead {ACTION|WEST|WEST} and NORTH.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/62 ; TODO: consider adding relevant links: {ACTION|s|s} ; sust(room_62//62)
{TEXTPIC|y.svg|1}ou are further along the road. There is a hut SOUTH and a path {ACTION|EAST|EAST}. The road runs from {ACTION|NORTH|NORTH} to {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/63 ; TODO: consider adding relevant links: {ACTION|exam cran|exam cran}, {ACTION|exam notc|exam notc}, {ACTION|g hand|g hand}, {ACTION|go hand|go hand}, {ACTION|turn hand|turn hand}, {ACTION|w|w} ; sust(room_63//63)
{TEXTPIC|y.svg|1}ou are on the village road. There are huts to the {ACTION|NORTH|NORTH} and {ACTION|SOUTH|SOUTH}. The road runs {ACTION|EAST|EAST}, and there is a bridge leading WEST. You can see a small crank next to the bridge.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/64 ; TODO: consider adding relevant links: {ACTION|u|u} ; sust(room_64//64)
{TEXTPIC|y.svg|1}ou are at the bottom of the cliff face. With a little effort and some equipment it is possible to go UP. The only other exit is to the {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/65 ; TODO: consider adding relevant links: {ACTION|give chai|give chai}, {ACTION|n|n} ; sust(room_65//65)
{TEXTPIC|t.svg|1}his is obviously the home of a warrior as there are various weapons hanging on one wall. The only exit is NORTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/66 ; TODO: consider adding relevant links: {ACTION|exam toys|exam toys} ; sust(room_66//66)
{TEXTPIC|t.svg|1}his appears to be a normal family home. There are lots of toys left lying over the floor, most of them broken. The only way out is {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/67 ; sust(room_67//67)
{TEXTPIC|t.svg|1}his is the dining hut. There is a large table with lots of plates set round it. The only exit is {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/68 ; sust(room_68//68)
{TEXTPIC|y.svg|1}ou are in a small alcove that was once used as an office. You can go {ACTION|EAST|EAST} or {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/69 ; TODO: consider adding relevant links: {ACTION|pres butt|pres butt} ; sust(room_69//69)
{TEXTPIC|y.svg|1}ou are at the bottom of the well. There is no longer any water here, but it is rather muddy. The only exit is {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/70 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|d hand|d hand}, {ACTION|exam notc|exam notc}, {ACTION|exam rock|exam rock}, {ACTION|g hand|g hand}, {ACTION|go hand|go hand}, {ACTION|go rock|go rock}, {ACTION|turn hand|turn hand} ; sust(room_70//70)
{TEXTPIC|y.svg|1}ou are in a small grove. There is a portcullis set into a sheer cliff face.  Exits are to the {ACTION|NORTH|NORTH} and DOWN.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/71 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|u d|u d} ; sust(room_71//71)
{TEXTPIC|y.svg|1}ou are sitting in a boat. You can climb OUT or sail {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/72 ; TODO: consider adding relevant links: {ACTION|e|e}, {ACTION|jump rive|jump rive} ; sust(room_72//72)
{TEXTPIC|y.svg|1}ou are at the edge of a steep ravine. You can see a small village on the other side. There is an entrance to a bridge leading EAST and a path {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/73 ; TODO: consider adding relevant links: {ACTION|dig|dig}, {ACTION|shut hand|shut hand} ; sust(room_73//73)
{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small dank cellar. There is a flight of steps on one wall. The only exit is {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/74 ; sust(room_74//74)

/75 ; TODO: consider adding relevant links: {ACTION|n|n}, {ACTION|wait|wait} ; sust(room_75//75)
{TEXTPIC|y.svg|1}ou are on a sandy shore.  The tide is in.  Possible exits lead {ACTION|WEST|WEST} and NORTH.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/76 ; TODO: consider adding relevant links: {ACTION|s|s} ; sust(room_76//76)
{TEXTPIC|y.svg|1}ou are standing on a small island. A lighthouse towers above your head, it's light flashes far out to sea. There is a sandy shore SOUTH and a door leading {ACTION|IN|IN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/77 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|exam tabl|exam tabl}, {ACTION|give sand|give sand}, {ACTION|give witc|give witc} ; sust(room_77//77)
{TEXTPIC|y.svg|1}ou are on the ground floor of the lighthouse. There is a table and two chairs {ACTION|in|in} the middle of the room. A staircase winds UPwards and a door leads OUT.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/78 ; TODO: consider adding relevant links: {ACTION|exam bed|exam bed}, {ACTION|exam box|exam box}, {ACTION|g scre|g scre} ; sust(room_78//78)
{TEXTPIC|y.svg|1}ou are on the second floor of the lighthouse. You can see a set of bunk beds. The stairs lead {ACTION|UP|UP} and {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/79 ; TODO: consider adding relevant links: {ACTION|exam glas|exam glas}, {ACTION|exam ligh|exam ligh}, {ACTION|g gree|g gree}, {ACTION|leav ashe|leav ashe}, {ACTION|open glas|open glas}, {ACTION|put ashe|put ashe} ; sust(room_79//79)
{TEXTPIC|y.svg|1}ou are in the light room. A glass cabinet takes up most of the room. The light shines brightly in the cabinet, it's beam reflected by a revolving mirror. Stairs lead {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}
/80 ; sust(room_80//80)
{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫\n\n\n C O N G R A T U L A T I O N S\n\n\n ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫\n\n}You have successfully  completed the  'ENERGEM ENIGMA'.  You are now  being  transported  back to the professor's laboratory. When you arrive you  will be sent for de-briefing.   The  four  energy crystals are  to be  used in the professor's  latest  top  secret invention.  Will it  be you  who is sent to try it out?
/81 ; sust(room_81//81)
CONT-\n\nUnfortunately,  the  last  agent sent to collect them  never came back!  You  are now going to the planet EDAM in order to continue the search.  All  the  equipment has been  left  scattered around the planet.  All  you  will have with you is your air bottles. Do not waste air  by swapping  them unnecessarily.
/82 ; sust(room_82//82)
\nExtra commands include:-\n\nRAMSAVE\tRAMLOAD\nNEW TYPEFACE\tNORMAL TYPEFACE\nCHECK AIR\tSWAP BOTTLES\nSCORE\tHELP\nPICTURES\tWORDS\n\n
We hope that you will have many hours of enjoyment playing this game.  If you have any comments or criticisms, then please write to:                             {CLASS|center|The Eighteam            \n
C/O Precision Games   \n
2 Fern Hill         \n
Langdon Hills     \n
Basildon        \n
Essex.}
/83 ; sust(room_83//83)
{CLASS|center|THE 'ENERGEM ENIGMA'}\n
Was written  and designed by the Eighteam  Produced by PrecisionG ames:  Many  thanks  to Gilsoft for The Quill, The Illustrator The Press.  A special  thanks to Phill Wade  for  sorting out our U.D.G problem.\n
The Eighteam:\n\n
{CLASS|center|Peter, Caff, David,  Sean\n}
Additional material: Richard.\n
{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}





/CON
/0 ; sust(room_0//0)
n 1 ; sust(room_1/1)

/1 ; sust(room_1//1)
e 72 ; sust(room_72/72)
n 27 ; sust(room_27/27)
s 2 ; sust(room_2/2)

/2 ; sust(room_2//2)
n 1 ; sust(room_1/1)
s 3 ; sust(room_3/3)

/3 ; sust(room_3//3)
n 2 ; sust(room_2/2)

/4 ; sust(room_4//4)
n 3 ; sust(room_3/3)
s 5 ; sust(room_5/5)

/5 ; sust(room_5//5)
w 7 ; sust(room_7/7)

/6 ; sust(room_6//6)
e 22 ; sust(room_22/22)
n 7 ; sust(room_7/7)

/7 ; sust(room_7//7)
e 5 ; sust(room_5/5)
n 8 ; sust(room_8/8)
s 6 ; sust(room_6/6)

/8 ; sust(room_8//8)
s 7 ; sust(room_7/7)
w 12 ; sust(room_12/12)

/9 ; sust(room_9//9)

/10 ; sust(room_10//10)

/11 ; sust(room_11//11)

/12 ; sust(room_12//12)
e 8 ; sust(room_8/8)
u 13 ; sust(room_13/13)

/13 ; sust(room_13//13)
d 12 ; sust(room_12/12)
e 17 ; sust(room_17/17)
w 19 ; sust(room_19/19)

/14 ; sust(room_14//14)

/15 ; sust(room_15//15)

/16 ; sust(room_16//16)
n 23 ; sust(room_23/23)

/17 ; sust(room_17//17)
u 18 ; sust(room_18/18)
w 13 ; sust(room_13/13)

/18 ; sust(room_18//18)
e 17 ; sust(room_17/17)
w 19 ; sust(room_19/19)

/19 ; sust(room_19//19)
e 13 ; sust(room_13/13)
s 70 ; sust(room_70/70)
u 18 ; sust(room_18/18)

/20 ; sust(room_20//20)

/21 ; sust(room_21//21)
u 70 ; sust(room_70/70)

/22 ; sust(room_22//22)
e 23 ; sust(room_23/23)
w 6 ; sust(room_6/6)

/23 ; sust(room_23//23)
e 24 ; sust(room_24/24)
s 16 ; sust(room_16/16)
w 22 ; sust(room_22/22)

/24 ; sust(room_24//24)
e 25 ; sust(room_25/25)
w 23 ; sust(room_23/23)

/25 ; sust(room_25//25)
w 24 ; sust(room_24/24)

/26 ; sust(room_26//26)
n 30 ; sust(room_30/30)
s 71 ; sust(room_71/71)

/27 ; sust(room_27//27)
s 1 ; sust(room_1/1)
w 28 ; sust(room_28/28)

/28 ; sust(room_28//28)
e 27 ; sust(room_27/27)
w 29 ; sust(room_29/29)

/29 ; sust(room_29//29)
e 28 ; sust(room_28/28)

/30 ; sust(room_30//30)
n 31 ; sust(room_31/31)
s 26 ; sust(room_26/26)

/31 ; sust(room_31//31)
s 30 ; sust(room_30/30)

/32 ; sust(room_32//32)
w 33 ; sust(room_33/33)

/33 ; sust(room_33//33)
e 32 ; sust(room_32/32)
u 34 ; sust(room_34/34)

/34 ; sust(room_34//34)
d 33 ; sust(room_33/33)
u 35 ; sust(room_35/35)

/35 ; sust(room_35//35)
d 34 ; sust(room_34/34)
e 39 ; sust(room_39/39)

/36 ; sust(room_36//36)
n 38 ; sust(room_38/38)
s 37 ; sust(room_37/37)

/37 ; sust(room_37//37)
e 38 ; sust(room_38/38)
n 36 ; sust(room_36/36)
s 40 ; sust(room_40/40)

/38 ; sust(room_38//38)
e 40 ; sust(room_40/40)
s 36 ; sust(room_36/36)
w 37 ; sust(room_37/37)

/39 ; sust(room_39//39)
d 41 ; sust(room_41/41)
w 35 ; sust(room_35/35)

/40 ; sust(room_40//40)
n 37 ; sust(room_37/37)
s 41 ; sust(room_41/41)
w 38 ; sust(room_38/38)

/41 ; sust(room_41//41)
n 40 ; sust(room_40/40)
u 39 ; sust(room_39/39)

/42 ; sust(room_42//42)
e 45 ; sust(room_45/45)
n 42 ; sust(room_42/42)
s 43 ; sust(room_43/43)
w 42 ; sust(room_42/42)

/43 ; sust(room_43//43)
e 43 ; sust(room_43/43)
n 43 ; sust(room_43/43)
s 44 ; sust(room_44/44)
w 43 ; sust(room_43/43)

/44 ; sust(room_44//44)
e 42 ; sust(room_42/42)
n 43 ; sust(room_43/43)
s 43 ; sust(room_43/43)
w 43 ; sust(room_43/43)

/45 ; sust(room_45//45)
n 42 ; sust(room_42/42)
s 46 ; sust(room_46/46)

/46 ; sust(room_46//46)
n 45 ; sust(room_45/45)
s 47 ; sust(room_47/47)

/47 ; sust(room_47//47)
d 52 ; sust(room_52/52)
n 46 ; sust(room_46/46)

/48 ; sust(room_48//48)
e 49 ; sust(room_49/49)
n 51 ; sust(room_51/51)

/49 ; sust(room_49//49)
w 48 ; sust(room_48/48)

/50 ; sust(room_50//50)
s 49 ; sust(room_49/49)

/51 ; sust(room_51//51)
e 68 ; sust(room_68/68)
n 57 ; sust(room_57/57)
s 48 ; sust(room_48/48)

/52 ; sust(room_52//52)
e 56 ; sust(room_56/56)
n 55 ; sust(room_55/55)
u 47 ; sust(room_47/47)

/53 ; sust(room_53//53)
w 54 ; sust(room_54/54)

/54 ; sust(room_54//54)
w 55 ; sust(room_55/55)

/55 ; sust(room_55//55)
e 54 ; sust(room_54/54)
s 52 ; sust(room_52/52)
w 56 ; sust(room_56/56)

/56 ; sust(room_56//56)
e 55 ; sust(room_55/55)
s 57 ; sust(room_57/57)
w 52 ; sust(room_52/52)

/57 ; sust(room_57//57)
e 58 ; sust(room_58/58)
n 56 ; sust(room_56/56)
s 51 ; sust(room_51/51)
w 68 ; sust(room_68/68)

/58 ; sust(room_58//58)
w 57 ; sust(room_57/57)

/59 ; sust(room_59//59)

/60 ; sust(room_60//60)
n 44 ; sust(room_44/44)
s 62 ; sust(room_62/62)
w 64 ; sust(room_64/64)

/61 ; sust(room_61//61)
w 62 ; sust(room_62/62)

/62 ; sust(room_62//62)
e 61 ; sust(room_61/61)
n 60 ; sust(room_60/60)
w 63 ; sust(room_63/63)

/63 ; sust(room_63//63)
e 62 ; sust(room_62/62)
n 67 ; sust(room_67/67)
s 66 ; sust(room_66/66)

/64 ; sust(room_64//64)
e 60 ; sust(room_60/60)

/65 ; sust(room_65//65)

/66 ; sust(room_66//66)
n 63 ; sust(room_63/63)

/67 ; sust(room_67//67)
s 63 ; sust(room_63/63)

/68 ; sust(room_68//68)
e 57 ; sust(room_57/57)
w 51 ; sust(room_51/51)

/69 ; sust(room_69//69)
u 4 ; sust(room_4/4)

/70 ; sust(room_70//70)
n 19 ; sust(room_19/19)

/71 ; sust(room_71//71)
n 26 ; sust(room_26/26)

/72 ; sust(room_72//72)
w 1 ; sust(room_1/1)

/73 ; sust(room_73//73)
u 54 ; sust(room_54/54)

/74 ; sust(room_74//74)

/75 ; sust(room_75//75)
w 62 ; sust(room_62/62)

/76 ; sust(room_76//76)
u 77 ; sust(room_77/77)

/77 ; sust(room_77//77)
u 78 ; sust(room_78/78)

/78 ; sust(room_78//78)
d 77 ; sust(room_77/77)
u 79 ; sust(room_79/79)

/79 ; sust(room_79//79)
d 78 ; sust(room_78/78)

/80 ; sust(room_80//80)

/81 ; sust(room_81//81)

/82 ; sust(room_82//82)

/83 ; sust(room_83//83)







/OBJ

/0       252      1    _    _       10000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_0//0) ; sust(room_noncreated/252) ; sust(ght/ATTR)
/1      254      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_1_air//1) ; sust(room_carried/254) ; sust(ATTR/ATTR)
/2   252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_2_air//2) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/3   252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_3_air//3) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/4   252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_4_air//4) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/5  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_5_crys//5) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/6  252      1 hand _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_6_hand//6) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/7  252      1 rope _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_7_rope//7) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/8       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_8//8) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/9   252      1 red  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_9_red//9) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/10 252      1 gree _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_10_gree//10) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/11 252      1 blue _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_11_blue//11) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/12 252      1 pink _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_12_pink//12) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/13 252      1 pipe _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_13_pipe//13) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/14      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_14//14) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/15  252      1 key  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_15_key//15) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/16      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_16//16) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/17         53      1 cage _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_17_cage//17) ; sust(room_53/53) ; sust(ATTR/ATTR)
/18 252      1 bait _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_18_bait//18) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/19      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_19//19) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/20 252      1 mole _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_20_mole//20) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/21 252      1 book _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_21_book//21) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/22  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_22_hat//22) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/23      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_23//23) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/24      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_24//24) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/25  252      1 tin  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_25_tin//25) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/26      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_26//26) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/27 252      1 ashe _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_27_ashe//27) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/28 252      1 shoe _       01000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_28_shoe//28) ; sust(room_noncreated/252) ; sust(arable/ATTR)
/29      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_29//29) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/30      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_30//30) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/31      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_31//31) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/32      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_32//32) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/33      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_33//33) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/34 252      1 pole _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_34_jump//34) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/35 252      1 witc _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_35_witc//35) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/36              73      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_36//36) ; sust(room_73/73) ; sust(ATTR/ATTR)
/37              54      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_37//37) ; sust(room_54/54) ; sust(ATTR/ATTR)
/38      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_38//38) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/39      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_39//39) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/40              44      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_40//40) ; sust(room_44/44) ; sust(ATTR/ATTR)
/41      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_41//41) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/42 252      1 ball _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_42_ball//42) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/43 252      1 pape _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_43_pape//43) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/44  252      1 fag  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_44_fag//44) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/45 252      1 chai _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_45_chai//45) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/46              50      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_46//46) ; sust(room_50/50) ; sust(ATTR/ATTR)
/47              54      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_47//47) ; sust(room_54/54) ; sust(ATTR/ATTR)
/48      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_48//48) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/49      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_49//49) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/50      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_50//50) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/51 252      1 scre _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_51_scre//51) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/52 252      1 magn _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_52_magn//52) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/53              57      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_53//53) ; sust(room_57/57) ; sust(ATTR/ATTR)
/54 252      1 face _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_54_face//54) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/55 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_55_magn//55) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/56      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_56//56) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/57              69      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_57//57) ; sust(room_69/69) ; sust(ATTR/ATTR)
/58 252      1 buck _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_58_buck//58) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)






/PRO 0








_ _
 hook    "RESPONSE_START"


_ _
 hook    "RESPONSE_USER"

n _
 at      65 ; sust(room_65/65)
 present 31 ; sust(object_31/31)
 writeln "\nThe warrior blocks your path."
 done

n _
 at      65 ; sust(room_65/65)
 absent  31 ; sust(object_31/31)
 goto    62 ; sust(room_62/62)
 desc

n _
 at      10 ; sust(room_10/10)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    9 ; sust(room_9/9)
 desc

n _
 at      11 ; sust(room_11/11)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    10 ; sust(room_10/10)
 desc

n _
 at      31 ; sust(room_31/31)
 writeln "\nThe boat spins round and round getting nearer to the centre of the whirlpool by the second..... All of a sudden you are thrown clear of the boat and........."
 destroy 24 ; sust(object_24/24)
 pause   250
 goto    58 ; sust(room_58/58)
 desc

n _
 at      61 ; sust(room_61/61)
 writeln "\nYou are caught by a sludge monster and eaten."
 score
 turns
 end

n _
 at      75 ; sust(room_75/75)
 notzero 7 ; sust(countdown_player_input_3/7)
 carried 42 ; sust(object_42_ball/42)
 eq      19 20 ; sust(game_flag_19/19)
 goto    76 ; sust(room_76/76)
 desc

n _
 at      75 ; sust(room_75/75)
 writeln "\nIt's too far to swim unaided, your arms grow tired and you sink to the bottom. You fall victim to the sludge monster."
 score
 turns
 end

n _
 at      5 ; sust(room_5/5)
 let     28 19 ; sust(pause_parameter/28) ; sust(pause_toggle_graphics/19)
 pause   10
 goto    4 ; sust(room_4/4)
 desc

s _
 at      62 ; sust(room_62/62)
 eq      115 30 ; sust(game_flag_15/115)
 let     7 2 ; sust(countdown_player_input_3/7)
 goto    65 ; sust(room_65/65)
 desc

s _
 at      62 ; sust(room_62/62)
 gt      115 30 ; sust(game_flag_15/115)
 goto    65 ; sust(room_65/65)
 desc

s _
 at      9 ; sust(room_9/9)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    10 ; sust(room_10/10)
 desc

s _
 at      10 ; sust(room_10/10)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    11 ; sust(room_11/11)
 desc

s _
 at      76 ; sust(room_76/76)
 notzero 7 ; sust(countdown_player_input_3/7)
 carried 42 ; sust(object_42_ball/42)
 eq      19 20 ; sust(game_flag_19/19)
 goto    75 ; sust(room_75/75)
 desc

s _
 at      76 ; sust(room_76/76)
 writeln "\nUnfortunately the ball has a slow puncture and has deflated."
 writeln "\nIt's too far to swim unaided, your arms grow tired and you sink to the bottom. You fall victim to the sludge monster."
 score
 turns
 end

s _
 at      3 ; sust(room_3/3)
 let     28 19 ; sust(pause_parameter/28) ; sust(pause_toggle_graphics/19)
 pause   10
 goto    4 ; sust(room_4/4)
 desc

e _
 at      11 ; sust(room_11/11)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 writeln "\nYou are swept over the top of the waterfall and fall to your death."
 score
 turns
 end

e _
 at      72 ; sust(room_72/72)
 lt      111 40 ; sust(game_flag_11/111)
 writeln "\nThe bridge is raised at the opposite side of the ravine."
 done

e _
 at      72 ; sust(room_72/72)
 eq      111 40 ; sust(game_flag_11/111)
 goto    63 ; sust(room_63/63)
 desc

e _
 at      14 ; sust(room_14/14)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    9 ; sust(room_9/9)
 desc

e _
 at      54 ; sust(room_54/54)
 present 37 ; sust(object_37/37)
 writeln "\nYou bang your nose on a slab of rock."
 done

e _
 at      54 ; sust(room_54/54)
 absent  37 ; sust(object_37/37)
 goto    53 ; sust(room_53/53)
 desc

w _
 at      63 ; sust(room_63/63)
 lt      111 40 ; sust(game_flag_11/111)
 writeln "\nThe bridge is up."
 done

w _
 at      63 ; sust(room_63/63)
 eq      111 40 ; sust(game_flag_11/111)
 goto    72 ; sust(room_72/72)
 desc

w _
 at      9 ; sust(room_9/9)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    14 ; sust(room_14/14)
 desc

u d
 at      71 ; sust(room_71/71)
 goto    24 ; sust(room_24/24)
 create  24 ; sust(object_24/24)
 desc

u rope
 notzero 5 ; sust(countdown_player_input_1/5)
 present 7 ; sust(object_7_rope/7)
 notat   37 ; sust(room_37/37)
 writeln "\nThere is a time and place for everything..and this is neither."
 done

u rope
 eq      114 10 ; sust(game_flag_14/114)
 notzero 5 ; sust(countdown_player_input_1/5)
 zero    113 ; sust(game_flag_13/113)
 present 7 ; sust(object_7_rope/7)
 at      37 ; sust(room_37/37)
 writeln "\nYou climb the rope and are now able to reach the crystal."
 let     6 4 ; sust(countdown_player_input_2/6)
 done

u rope
 zero    5 ; sust(countdown_player_input_1/5)
 present 7 ; sust(object_7_rope/7)
 writeln "\nIt's a bit too wobbly to climb."
 done

u rope
 at      37 ; sust(room_37/37)
 zero    113 ; sust(game_flag_13/113)
 zero    114 ; sust(game_flag_14/114)
 present 7 ; sust(object_7_rope/7)
 notzero 5 ; sust(countdown_player_input_1/5)
 writeln "\nYou climb up the rope but seeing nothing of interest, you climb back down."
 done

u rope
 at      37 ; sust(room_37/37)
 eq      113 10 ; sust(game_flag_13/113)
 present 7 ; sust(object_7_rope/7)
 notzero 5 ; sust(countdown_player_input_1/5)
 writeln "\nYou climb up the rope but seeing nothing of interest, you climb back down."
 done

u _
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 eq      18 30 ; sust(game_flag_18/18)
 goto    71 ; sust(room_71/71)
 desc

u _
 at      64 ; sust(room_64/64)
 notworn 28 ; sust(object_28_shoe/28)
 writeln "\nYou get half-way up the cliff but your feet slip on the rocks and you fall to your death."
 score
 turns
 end

u _
 at      64 ; sust(room_64/64)
 worn    28 ; sust(object_28_shoe/28)
 goto    17 ; sust(room_17/17)
 desc

u _
 at      15 ; sust(room_15/15)
 zero    0 ; sust(yesno_is_dark/0)
 goto    9 ; sust(room_9/9)
 desc

u _
 at      16 ; sust(room_16/16)
 absent  0 ; sust(object_0/0)
 set     0 ; sust(yesno_is_dark/0)
 goto    15 ; sust(room_15/15)
 desc

u _
 at      16 ; sust(room_16/16)
 present 0 ; sust(object_0/0)
 clear   0 ; sust(yesno_is_dark/0)
 goto    15 ; sust(room_15/15)
 desc

u _
 at      49 ; sust(room_49/49)
 eq      22 10 ; sust(game_flag_22/22)
 writeln "\nIt's locked."
 done

u _
 at      49 ; sust(room_49/49)
 gt      22 15 ; sust(game_flag_22/22)
 goto    50 ; sust(room_50/50)
 desc

u _
 at      49 ; sust(room_49/49)
 eq      22 15 ; sust(game_flag_22/22)
 writeln "\nIt's shut!"
 done

d buck
 at      4 ; sust(room_4/4)
 zero    111 ; sust(game_flag_11/111)
 create  6 ; sust(object_6_hand/6)
 get     6 ; sust(object_6_hand/6)
 writeln "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy."
 let     111 10 ; sust(game_flag_11/111)
 done

d buck
 at      4 ; sust(room_4/4)
 notzero 111 ; sust(game_flag_11/111)
 writeln "\nYou have already done that."
 done

d hand
 at      4 ; sust(room_4/4)
 zero    111 ; sust(game_flag_11/111)
 create  6 ; sust(object_6_hand/6)
 get     6 ; sust(object_6_hand/6)
 create  7 ; sust(object_7_rope/7)
 writeln "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy."
 let     111 10 ; sust(game_flag_11/111)
 done

d hand
 at      4 ; sust(room_4/4)
 notzero 111 ; sust(game_flag_11/111)
 writeln "\nYou have already done that."
 done

d hand
 at      70 ; sust(room_70/70)
 eq      111 20 ; sust(game_flag_11/111)
 create  8 ; sust(object_8/8)
 let     111 30 ; sust(game_flag_11/111)
 writeln "\nAs you turn the handle the door slowly rises upwards."
 done

d hand
 at      70 ; sust(room_70/70)
 lt      111 30 ; sust(game_flag_11/111)
 writeln "\nNothing happens."
 done

d _
 at      70 ; sust(room_70/70)
 present 8 ; sust(object_8/8)
 goto    21 ; sust(room_21/21)
 desc
 done

d _
 at      70 ; sust(room_70/70)
 absent  8 ; sust(object_8/8)
 writeln "\nYou cannot pass through a closed portcullis."
 done

d _
 at      71 ; sust(room_71/71)
 goto    24 ; sust(room_24/24)
 desc

d _
 at      17 ; sust(room_17/17)
 notworn 28 ; sust(object_28_shoe/28)
 writeln "\nYou start to climb down to the village below but your feet slip on the rocks and you fall to your death."
 score
 turns
 end

d _
 at      17 ; sust(room_17/17)
 worn    28 ; sust(object_28_shoe/28)
 goto    64 ; sust(room_64/64)
 desc

d _
 at      15 ; sust(room_15/15)
 goto    16 ; sust(room_16/16)
 clear   0 ; sust(yesno_is_dark/0)
 desc

d _
 at      9 ; sust(room_9/9)
 carried 0 ; sust(object_0/0)
 zero    0 ; sust(yesno_is_dark/0)
 goto    15 ; sust(room_15/15)
 desc

d _
 at      77 ; sust(room_77/77)
 carried 10 ; sust(object_10_gree/10)
 eq      116 20 ; sust(game_flag_16/116)
 destroy 10 ; sust(object_10_gree/10)
 let     116 10 ; sust(game_flag_16/116)
 let     113 10 ; sust(game_flag_13/113)
 let     114 15 ; sust(game_flag_14/114)
 writeln "\nThe lighthouse keeper snatches the crystal from your hand and disappears up the stairs."
 done

d _
 at      77 ; sust(room_77/77)
 notcarr 10 ; sust(object_10_gree/10)
 goto    76 ; sust(room_76/76)
 desc

d _
 at      77 ; sust(room_77/77)
 eq      116 30 ; sust(game_flag_16/116)
 goto    76 ; sust(room_76/76)
 desc

d _
 at      54 ; sust(room_54/54)
 present 49 ; sust(object_49/49)
 goto    73 ; sust(room_73/73)
 desc

d _
 at      4 ; sust(room_4/4)
 notworn 28 ; sust(object_28_shoe/28)
 writeln "\nAs you climb into the well you slip on the moss covered walls and fall to your death."
 score
 turns
 end

d _
 at      4 ; sust(room_4/4)
 worn    28 ; sust(object_28_shoe/28)
 gt      111 9 ; sust(game_flag_11/111)
 zero    29 ; sust(bitset_graphics_status/29)
 let     29 10 ; sust(bitset_graphics_status/29)
 goto    69 ; sust(room_69/69)
 create  58 ; sust(object_58_buck/58)
 desc

d _
 at      4 ; sust(room_4/4)
 worn    28 ; sust(object_28_shoe/28)
 goto    69 ; sust(room_69/69)
 desc

chec air
 plus    31 1 ; sust(total_turns_lower/31)
 turns

swap air
 carried 1 ; sust(object_1_air/1)
 gt      31 140 ; sust(total_turns_lower/31)
 swap    1 2 ; sust(object_1_air/1) ; sust(object_2_air/2)
 writeln "\nAs you change bottle's the old one disintegrates."
 clear   31 ; sust(total_turns_lower/31)
 done

swap air
 carried 2 ; sust(object_2_air/2)
 gt      31 140 ; sust(total_turns_lower/31)
 swap    2 3 ; sust(object_2_air/2) ; sust(object_3_air/3)
 writeln "\nAs you change bottle's the old one disintegrates."
 clear   31 ; sust(total_turns_lower/31)
 done

swap air
 carried 3 ; sust(object_3_air/3)
 gt      31 140 ; sust(total_turns_lower/31)
 swap    3 4 ; sust(object_3_air/3) ; sust(object_4_air/4)
 writeln "\nAs you change bottle's the old one disintegrates."
 clear   31 ; sust(total_turns_lower/31)
 done

swap air
 carried 4 ; sust(object_4_air/4)
 destroy 4 ; sust(object_4_air/4)
 writeln "\nAs you change bottle's the old one disintegrates."
 writeln "\nYou are now on your last bottle of oxygen."
 clear   31 ; sust(total_turns_lower/31)
 done

swap air
 absent  1 ; sust(object_1_air/1)
 absent  2 ; sust(object_2_air/2)
 absent  3 ; sust(object_3_air/3)
 absent  4 ; sust(object_4_air/4)
 writeln "\nSorry....you are not carrying any."
 done

swap air
 carried 1 ; sust(object_1_air/1)
 lt      31 140 ; sust(total_turns_lower/31)
 writeln "As you remove the bottle the remaining air escapes and the bottle disintegrates....you have wasted valuable air."
 swap    1 2 ; sust(object_1_air/1) ; sust(object_2_air/2)
 clear   31 ; sust(total_turns_lower/31)
 done

swap air
 carried 2 ; sust(object_2_air/2)
 lt      31 140 ; sust(total_turns_lower/31)
 swap    2 3 ; sust(object_2_air/2) ; sust(object_3_air/3)
 writeln "As you remove the bottle the remaining air escapes and the bottle disintegrates....you have wasted valuable air."
 clear   31 ; sust(total_turns_lower/31)
 done

swap air
 carried 3 ; sust(object_3_air/3)
 lt      31 140 ; sust(total_turns_lower/31)
 swap    3 4 ; sust(object_3_air/3) ; sust(object_4_air/4)
 writeln "As you remove the bottle the remaining air escapes and the bottle disintegrates....you have wasted valuable air."
 clear   31 ; sust(total_turns_lower/31)
 done

exam buck
 at      4 ; sust(room_4/4)
 zero    111 ; sust(game_flag_11/111)
 eq      27 200 ; sust(game_flag_27/27)
 writeln "\nYou find a packet of cigarettes."
 create  44 ; sust(object_44_fag/44)
 plus    30 2 ; sust(total_player_score/30)
 clear   27 ; sust(game_flag_27/27)
 let     111 5 ; sust(game_flag_11/111)
 done

exam buck
 at      4 ; sust(room_4/4)
 notzero 111 ; sust(game_flag_11/111)
 lt      111 10 ; sust(game_flag_11/111)
 writeln "\nYou have already done that."
 done

exam buck
 at      4 ; sust(room_4/4)
 gt      111 9 ; sust(game_flag_11/111)
 writeln "\nThe bucket is at the bottom of the well."
 done

exam buck
 present 58 ; sust(object_58_buck/58)
 eq      27 200 ; sust(game_flag_27/27)
 clear   27 ; sust(game_flag_27/27)
 create  44 ; sust(object_44_fag/44)
 writeln "\nYou find a packet of cigarettes."
 done

exam buck
 present 58 ; sust(object_58_buck/58)
 zero    27 ; sust(game_flag_27/27)
 writeln "\nYou have already done that."
 done

exam hand
 carried 6 ; sust(object_6_hand/6)
 notat   73 ; sust(room_73/73)
 writeln "\nA rusty handle with a square end."
 done

exam hand
 notcarr 6 ; sust(object_6_hand/6)
 notat   73 ; sust(room_73/73)
 writeln "\nMost objects may only be examined when carried."
 done

exam well
 at      4 ; sust(room_4/4)
 zero    111 ; sust(game_flag_11/111)
 writeln "\nYou can see a rusty handle on the side of the well and a bucket hanging above it."
 done

exam well
 at      4 ; sust(room_4/4)
 notzero 111 ; sust(game_flag_11/111)
 writeln "\nYou have already done that."
 done

exam rope
 carried 7 ; sust(object_7_rope/7)
 writeln "\nAt one end of the rope are the words 'MADE IN INDIA'."
 done

exam rock
 at      70 ; sust(room_70/70)
 absent  8 ; sust(object_8/8)
 writeln "\nIt is a metal door. There is a small square notch to one side."
 done

exam rock
 at      54 ; sust(room_54/54)
 present 37 ; sust(object_37/37)
 writeln "\nIt is set into the east wall and appears to be some kind of door, although there is no handle or lock."
 done

exam rock
 at      49 ; sust(room_49/49)
 writeln "\nThere is a bolt at the top."
 done

exam rock
 at      70 ; sust(room_70/70)
 present 8 ; sust(object_8/8)
 writeln "\nAn open portcullis."
 done

exam notc
 at      70 ; sust(room_70/70)
 writeln "\nA means of opening the door."
 done

exam notc
 at      63 ; sust(room_63/63)
 writeln "\nA small square notch."
 done

exam crys
 carried 5 ; sust(object_5_crys/5)
 writeln "\nA complicated instrument for detecting the presence of rare minerals. It has a small control panel and a plug socket on the side."
 done

exam crys
 notcarr 5 ; sust(object_5_crys/5)
 writeln "\nMost objects may only be examined when carried."
 done

exam pane
 carried 5 ; sust(object_5_crys/5)
 notat   57 ; sust(room_57/57)
 writeln "\nIt has five small buttons on it, RED, GREEN, BLUE and PINK, with the BLACK button on the side."
 done

exam pane
 notcarr 5 ; sust(object_5_crys/5)
 notat   57 ; sust(room_57/57)
 writeln "\nMost objects may only be examined when carried."
 done

exam pane
 at      57 ; sust(room_57/57)
 writeln "\nVery spacious, with 9 cogs and a pendulum (seen them somewhere else)."
 done

exam pipe
 carried 13 ; sust(object_13_pipe/13)
 writeln "\nA small intricately carved pipe."
 done

exam key
 carried 15 ; sust(object_15_key/15)
 writeln "\nA small key."
 done

exam ches
 at      21 ; sust(room_21/21)
 eq      112 6 ; sust(game_flag_12/112)
 writeln "\nInside the chest you can see a crystalometer."
 done

exam ches
 at      21 ; sust(room_21/21)
 gt      112 6 ; sust(game_flag_12/112)
 writeln "\nRather like Samantha Fox's...... BIG."
 done

exam ches
 at      21 ; sust(room_21/21)
 lt      112 6 ; sust(game_flag_12/112)
 writeln "\nYou have already done that."
 done

exam trac
 at      3 ; sust(room_3/3)
 eq      112 20 ; sust(game_flag_12/112)
 writeln "\nYou can see a small key that has been trodden into the mud by an animal."
 create  15 ; sust(object_15_key/15)
 plus    30 2 ; sust(total_player_score/30)
 let     112 10 ; sust(game_flag_12/112)
 done

exam trac
 at      3 ; sust(room_3/3)
 lt      112 20 ; sust(game_flag_12/112)
 writeln "\nYou have already done that."
 done

exam trac
 at      24 ; sust(room_24/24)
 writeln "\nNot made by any animal."
 done

exam cage
 carried 17 ; sust(object_17_cage/17)
 writeln "\nAn old washing basket with a flapping lid."
 done

exam bait
 present 18 ; sust(object_18_bait/18)
 writeln "\nAn edible, green leafy plant."
 done

exam bait
 at      1 ; sust(room_1/1)
 eq      115 200 ; sust(game_flag_15/115)
 writeln "\nThe only recognizable species is a cheese plant."
 done

exam bait
 at      1 ; sust(room_1/1)
 lt      115 200 ; sust(game_flag_15/115)
 writeln "\nYou have already done that."
 done

exam rubb
 at      19 ; sust(room_19/19)
 zero    116 ; sust(game_flag_16/116)
 writeln "\nAmongst all the rubbish you find a pole."
 create  34 ; sust(object_34_jump/34)
 plus    30 2 ; sust(total_player_score/30)
 let     116 10 ; sust(game_flag_16/116)
 done

exam rubb
 at      19 ; sust(room_19/19)
 notzero 116 ; sust(game_flag_16/116)
 writeln "\nYou have already done that."
 done

exam book
 carried 21 ; sust(object_21_book/21)
 writeln "{CLASS|center|\nCRYSTALOMETER MANUAL\n}THIS INSTRUMENT WILL ENABLE YOU TO DETECT A CRYSTAL OF A GIVEN COLOUR, WITHIN AN AREA OF SIX LOCATIONS. IT HAS ALREADY BEEN PRE-PROGRAMED TO COLLECT FOUR COLOUR CRYSTALS IN THE FOLLOWING ORDER:-"
 writeln "\nUnfortunately the rest of the book has been burnt."
 done

exam cabl
 carried 23 ; sust(object_23/23)
 writeln "An electrical cable attached to the helmet, with a connector at one end."
 done

exam stat
 at      14 ; sust(room_14/14)
 zero    17 ; sust(game_flag_17/17)
 writeln "\nWOW! It's got six pairs of arms. It's also wearing a helmet."
 done

exam stat
 at      14 ; sust(room_14/14)
 notzero 17 ; sust(game_flag_17/17)
 writeln "\nYou have already done that."
 done

exam alta
 at      14 ; sust(room_14/14)
 writeln "\nA stone bench encrusted with jewels. It has a large statue at one end."
 done

exam boat
 present 24 ; sust(object_24/24)
 eq      18 10 ; sust(game_flag_18/18)
 writeln "\nIt is a small boat, made from reads and waterproofed with a thick layer of a black sticky substance."
 writeln "There is a bare patch in the bottom that needs repair."
 done

exam boat
 present 24 ; sust(object_24/24)
 gt      18 10 ; sust(game_flag_18/18)
 writeln "\nIt is a small boat, made from reads and waterproofed with a thick layer of a black sticky substance."
 done

exam bush
 at      24 ; sust(room_24/24)
 zero    18 ; sust(game_flag_18/18)
 create  24 ; sust(object_24/24)
 let     18 10 ; sust(game_flag_18/18)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nHidden amongst the bushes is an old boat."
 done

exam bush
 at      24 ; sust(room_24/24)
 notzero 18 ; sust(game_flag_18/18)
 writeln "\nYou have already done that."
 done

exam bush
 at      39 ; sust(room_39/39)
 writeln "\nFrom the roots oozes a sticky tar like substance."
 done

exam tin
 carried 25 ; sust(object_25_tin/25)
 writeln "\nFull of air.....what did you expect?"
 done

exam tin
 carried 50 ; sust(object_50/50)
 writeln "\nFull of water."
 done

exam tin
 carried 26 ; sust(object_26/26)
 writeln "\nFull of tar."
 done

exam ashe
 at      25 ; sust(room_25/25)
 eq      19 210 ; sust(game_flag_19/19)
 create  21 ; sust(object_21_book/21)
 let     19 10 ; sust(game_flag_19/19)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nAmongst all the ashes you find a book page."
 done

exam ashe
 at      25 ; sust(room_25/25)
 lt      19 210 ; sust(game_flag_19/19)
 writeln "\nYou have already done that."
 done

exam ashe
 carried 27 ; sust(object_27_ashe/27)
 writeln "\nAn electric lantern with a small switch marked on/off."
 done

exam ashe
 carried 0 ; sust(object_0/0)
 writeln "\nAn electric lantern with a small switch marked on/off."
 done

exam ligh
 at      79 ; sust(room_79/79)
 eq      112 1 ; sust(game_flag_12/112)
 eq      113 10 ; sust(game_flag_13/113)
 eq      17 30 ; sust(game_flag_17/17)
 lt      114 20 ; sust(game_flag_14/114)
 let     114 15 ; sust(game_flag_14/114)
 writeln "\nYou notice that the source of light is a green crystal."
 done

exam ligh
 at      79 ; sust(room_79/79)
 lt      17 30 ; sust(game_flag_17/17)
 writeln "\nA very bright light."
 done

exam box
 at      36 ; sust(room_36/36)
 zero    25 ; sust(game_flag_25/25)
 create  52 ; sust(object_52_magn/52)
 plus    30 2 ; sust(total_player_score/30)
 let     25 10 ; sust(game_flag_25/25)
 writeln "\nNeatly stacked boxes. In one of them you find a magnet."
 done

exam box
 at      78 ; sust(room_78/78)
 zero    23 ; sust(game_flag_23/23)
 writeln "\nThere's just one screwdriver in it."
 done

exam box
 at      78 ; sust(room_78/78)
 notzero 23 ; sust(game_flag_23/23)
 writeln "\nYou have already done that."
 done

exam box
 at      36 ; sust(room_36/36)
 notzero 25 ; sust(game_flag_25/25)
 writeln "\nYou have already done that."
 done

exam man
 at      50 ; sust(room_50/50)
 present 46 ; sust(object_46/46)
 writeln "\nThere is a cloud of smoke above him caused by his compulsive chain smoking."
 done

exam toys
 at      66 ; sust(room_66/66)
 zero    20 ; sust(game_flag_20/20)
 create  13 ; sust(object_13_pipe/13)
 let     20 10 ; sust(game_flag_20/20)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nThe only toy that is not broken is a small pipe."
 done

exam toys
 at      66 ; sust(room_66/66)
 notzero 20 ; sust(game_flag_20/20)
 writeln "\nYou have already done that."
 done

exam cran
 at      63 ; sust(room_63/63)
 writeln "\nIt is a means of lowering and raising the bridge. There is a small square notch on the side."
 done

exam pole ; It was jump (noun)
 carried 34 ; sust(object_34_jump/34)
 writeln "\nA very long, flexible pole."
 done

exam rive
 at      29 ; sust(room_29/29)
 writeln "\nA fast flowing river."
 done

exam rive
 at      32 ; sust(room_32/32)
 writeln "\nA fast flowing river."
 done

exam leav
 at      28 ; sust(room_28/28)
 eq      17 200 ; sust(game_flag_17/17)
 create  27 ; sust(object_27_ashe/27)
 clear   17 ; sust(game_flag_17/17)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nAmongst a pile of leaves you uncover a lantern."
 done

exam leav
 at      28 ; sust(room_28/28)
 lt      17 200 ; sust(game_flag_17/17)
 writeln "\nYou have already done that."
 done

exam tabl
 at      50 ; sust(room_50/50)
 eq      22 20 ; sust(game_flag_22/22)
 writeln "\nInside one of the drawers is a pair of boots."
 let     22 30 ; sust(game_flag_22/22)
 plus    30 2 ; sust(total_player_score/30)
 create  28 ; sust(object_28_shoe/28)
 done

exam tabl
 at      50 ; sust(room_50/50)
 gt      22 20 ; sust(game_flag_22/22)
 writeln "\nYou have already done that."
 done

exam tabl
 at      77 ; sust(room_77/77)
 gt      115 40 ; sust(game_flag_15/115)
 writeln "\nYou have already done that."
 done

exam tabl
 at      77 ; sust(room_77/77)
 eq      115 40 ; sust(game_flag_15/115)
 writeln "\nBehind the desk sits a hungry lighthouse keeper."
 done

exam bed
 at      38 ; sust(room_38/38)
 writeln "\nOld and dirty."
 done

exam bed
 at      37 ; sust(room_37/37)
 writeln "\nA smelly looking mattress."
 done

exam bed
 at      78 ; sust(room_78/78)
 writeln "\nVery untidy... In fact there's a tool box on it!"
 done

exam wall
 at      38 ; sust(room_38/38)
 writeln "\nNot really that important."
 done

exam witc
 present 35 ; sust(object_35_witc/35)
 writeln "\nIt has a small label wrapped around it's middle."
 done

exam labe
 present 35 ; sust(object_35_witc/35)
 writeln "\nIt says 'CHEESE  PICKLE'..Must b e a sandwitch."
 done

exam glas
 at      79 ; sust(room_79/79)
 writeln "\nA glass cabinet, it opens for easy access to the light."
 done

exam ball
 carried 42 ; sust(object_42_ball/42)
 eq      19 10 ; sust(game_flag_19/19)
 writeln "\nDeflated."
 done

exam ball
 carried 42 ; sust(object_42_ball/42)
 eq      19 20 ; sust(game_flag_19/19)
 writeln "\nInflated."
 done

exam ledg
 at      22 ; sust(room_22/22)
 writeln "\nYou can see a large nest."
 done

exam nest
 at      22 ; sust(room_22/22)
 writeln "\nA large untidy nest containing one egg."
 done

exam egg
 at      22 ; sust(room_22/22)
 lt      17 30 ; sust(game_flag_17/17)
 writeln "\nA large smooth egg."
 done

exam egg
 at      22 ; sust(room_22/22)
 eq      112 2 ; sust(game_flag_12/112)
 lt      114 30 ; sust(game_flag_14/114)
 eq      17 30 ; sust(game_flag_17/17)
 let     114 25 ; sust(game_flag_14/114)
 writeln "\nAn egg shaped blue crystal."
 done

exam egg
 at      22 ; sust(room_22/22)
 eq      17 30 ; sust(game_flag_17/17)
 lt      113 30 ; sust(game_flag_13/113)
 lt      112 2 ; sust(game_flag_12/112)
 writeln "\nA large smooth egg."
 done

exam egg
 at      22 ; sust(room_22/22)
 eq      17 30 ; sust(game_flag_17/17)
 lt      113 30 ; sust(game_flag_13/113)
 gt      112 2 ; sust(game_flag_12/112)
 writeln "\nA large smooth egg."
 done

exam egg
 at      22 ; sust(room_22/22)
 absent  11 ; sust(object_11_blue/11)
 writeln "\nYou have already done that."
 done

exam pape
 carried 43 ; sust(object_43_pape/43)
 writeln "\nAn oval, glass paperweight."
 done

exam fag
 carried 44 ; sust(object_44_fag/44)
 writeln "\nA packet of 19 (someone's smoked one)."
 done

exam wate
 at      54 ; sust(room_54/54)
 present 47 ; sust(object_47/47)
 writeln "\nA murky puddle."
 done

exam chai
 carried 45 ; sust(object_45_chai/45)
 writeln "\nA length of chain."
 done

exam hole
 at      16 ; sust(room_16/16)
 writeln "\nAn easily accessible entrance."
 done

exam hole
 at      57 ; sust(room_57/57)
 eq      23 30 ; sust(game_flag_23/23)
 writeln "\nThere is a sheet of metal that has a small plate of a similar material set snuggly in it's centre."
 done

exam hole
 at      57 ; sust(room_57/57)
 eq      23 40 ; sust(game_flag_23/23)
 zero    24 ; sust(game_flag_24/24)
 writeln "\nBehind the metal plate there is a large cog."
 done

exam hole
 at      57 ; sust(room_57/57)
 eq      23 40 ; sust(game_flag_23/23)
 eq      24 10 ; sust(game_flag_24/24)
 writeln "\nThere is a large cog with 17 jewels on."
 done

exam insc
 at      14 ; sust(room_14/14)
 writeln "\nThe 'EIGHTEAM' woz 'ere. Aug '87"
 done

exam insc
 at      57 ; sust(room_57/57)
 lt      23 30 ; sust(game_flag_23/23)
 writeln "\n 17 Jewels: MADE IN HONG KONG "
 done

exam insc
 present 54 ; sust(object_54_face/54)
 writeln "\n 17 Jewels: MADE IN HONG KONG "
 done

exam cloc
 at      57 ; sust(room_57/57)
 lt      23 30 ; sust(game_flag_23/23)
 writeln "\nIt is a very large Grandfather clock. It has a large face, and a recess on the front. There is a keyhole on the side."
 done

exam cloc
 at      57 ; sust(room_57/57)
 gt      23 29 ; sust(game_flag_23/23)
 writeln "\nYou have already done that."
 done

exam face
 at      57 ; sust(room_57/57)
 lt      23 30 ; sust(game_flag_23/23)
 writeln "\nA normal sort of clock face. It has a small inscription in the centre, and three small screws around the outside. It is set to 12:15."
 done

exam face
 present 54 ; sust(object_54_face/54)
 writeln "\nA normal sort of clock face. It has a small inscription in the centre, and three small screws around the outside. It is set to 12:15."
 done

exam jewe
 at      57 ; sust(room_57/57)
 eq      23 40 ; sust(game_flag_23/23)
 eq      24 10 ; sust(game_flag_24/24)
 lt      112 3 ; sust(game_flag_12/112)
 writeln "\nVery rare jewels."
 done

exam jewe
 at      57 ; sust(room_57/57)
 eq      23 40 ; sust(game_flag_23/23)
 eq      24 10 ; sust(game_flag_24/24)
 eq      112 3 ; sust(game_flag_12/112)
 eq      113 30 ; sust(game_flag_13/113)
 eq      17 30 ; sust(game_flag_17/17)
 lt      114 40 ; sust(game_flag_14/114)
 writeln "\n16 red and one pink."
 let     114 35 ; sust(game_flag_14/114)
 done

exam jewe
 at      57 ; sust(room_57/57)
 eq      24 10 ; sust(game_flag_24/24)
 eq      23 40 ; sust(game_flag_23/23)
 eq      112 3 ; sust(game_flag_12/112)
 lt      17 30 ; sust(game_flag_17/17)
 writeln "\nVery rare jewels."
 done

exam scre
 carried 51 ; sust(object_51_scre/51)
 writeln "\nA small flat ended screwdriver."
 done

exam magn
 carried 52 ; sust(object_52_magn/52)
 writeln "\nIt would attract metal (like most other magnets)."
 done

exam plat
 at      57 ; sust(room_57/57)
 gt      23 29 ; sust(game_flag_23/23)
 lt      23 40 ; sust(game_flag_23/23)
 writeln "\nIt is obviously removable, but there is no visible means of doing so."
 done

exam cog
 at      57 ; sust(room_57/57)
 eq      23 40 ; sust(game_flag_23/23)
 zero    24 ; sust(game_flag_24/24)
 writeln "\nA large cog."
 done

exam cog
 at      57 ; sust(room_57/57)
 eq      23 40 ; sust(game_flag_23/23)
 eq      24 10 ; sust(game_flag_24/24)
 writeln "\nA large jewelled cog."
 done

exam shoe
 carried 28 ; sust(object_28_shoe/28)
 writeln "\nA pair of climbing shoes."
 done

exam hat
 carried 22 ; sust(object_22_hat/22)
 writeln "\nA tin helmet with a slide down visor. There is also a cable attached to the side with a\nplug at the end."
 done

exam hat
 worn    22 ; sust(object_22_hat/22)
 writeln "\nMost objects may only be examined when carried."
 done

exam _
 writeln "\nYou see nothing of interest."
 done

go hand
 at      70 ; sust(room_70/70)
 carried 6 ; sust(object_6_hand/6)
 lt      111 20 ; sust(game_flag_11/111)
 swap    14 6 ; sust(object_14/14) ; sust(object_6_hand/6)
 drop    14 ; sust(object_14/14)
 writeln "\nIt fits perfectly."
 let     111 20 ; sust(game_flag_11/111)
 done

go hand
 at      63 ; sust(room_63/63)
 carried 6 ; sust(object_6_hand/6)
 lt      111 20 ; sust(game_flag_11/111)
 let     111 20 ; sust(game_flag_11/111)
 writeln "\nIt fits perfectly."
 swap    23 6 ; sust(object_23/23) ; sust(object_6_hand/6)
 drop    23 ; sust(object_23/23)
 done

go well
 at      4 ; sust(room_4/4)
 notworn 28 ; sust(object_28_shoe/28)
 writeln "\nAs you climb into the well you slip on the moss covered walls and fall to your death."
 score
 turns
 end

go well
 at      4 ; sust(room_4/4)
 worn    28 ; sust(object_28_shoe/28)
 goto    69 ; sust(room_69/69)
 desc

go rock
 at      70 ; sust(room_70/70)
 present 8 ; sust(object_8/8)
 goto    21 ; sust(room_21/21)
 desc
 done

go rock
 at      70 ; sust(room_70/70)
 absent  8 ; sust(object_8/8)
 writeln "\nYou cannot pass through a closed portcullis."
 done

go rock
 at      44 ; sust(room_44/44)
 present 41 ; sust(object_41/41)
 goto    60 ; sust(room_60/60)
 desc

go rock
 at      49 ; sust(room_49/49)
 eq      22 10 ; sust(game_flag_22/22)
 writeln "\nIt's locked."
 done

go rock
 at      49 ; sust(room_49/49)
 gt      22 15 ; sust(game_flag_22/22)
 goto    50 ; sust(room_50/50)
 desc

go rock
 at      49 ; sust(room_49/49)
 eq      22 15 ; sust(game_flag_22/22)
 writeln "\nIt's shut!"
 done

go key
 at      57 ; sust(room_57/57)
 carried 15 ; sust(object_15_key/15)
 swap    15 56 ; sust(object_15_key/15) ; sust(object_56/56)
 drop    56 ; sust(object_56/56)
 ok

turn hand
 at      4 ; sust(room_4/4)
 lt      111 9 ; sust(game_flag_11/111)
 create  6 ; sust(object_6_hand/6)
 writeln "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy."
 let     111 10 ; sust(game_flag_11/111)
 plus    30 2 ; sust(total_player_score/30)
 create  7 ; sust(object_7_rope/7)
 get     6 ; sust(object_6_hand/6)
 done

turn hand
 at      4 ; sust(room_4/4)
 gt      111 9 ; sust(game_flag_11/111)
 writeln "\nYou have already done that."
 done

turn hand
 at      70 ; sust(room_70/70)
 eq      111 20 ; sust(game_flag_11/111)
 create  8 ; sust(object_8/8)
 writeln "\nAs you turn the handle the door slowly rises upwards."
 let     111 30 ; sust(game_flag_11/111)
 done

turn hand
 at      70 ; sust(room_70/70)
 present 6 ; sust(object_6_hand/6)
 lt      111 20 ; sust(game_flag_11/111)
 writeln "\nNothing happens."
 done

turn hand
 at      70 ; sust(room_70/70)
 eq      111 30 ; sust(game_flag_11/111)
 present 14 ; sust(object_14/14)
 let     111 20 ; sust(game_flag_11/111)
 destroy 8 ; sust(object_8/8)
 writeln "\nThe door slowly falls down into place."
 done

turn hand
 at      63 ; sust(room_63/63)
 eq      111 20 ; sust(game_flag_11/111)
 let     111 40 ; sust(game_flag_11/111)
 writeln "\nAs you turn the handle it puts the cogs into motion and the bridge slowly lowers into place."
 done

turn hand
 at      63 ; sust(room_63/63)
 eq      111 40 ; sust(game_flag_11/111)
 let     111 20 ; sust(game_flag_11/111)
 writeln "\nThe bridge slowly raises into place."
 done

turn hand
 at      63 ; sust(room_63/63)
 carried 6 ; sust(object_6_hand/6)
 lt      111 20 ; sust(game_flag_11/111)
 writeln "\nNothing happens."
 done

turn key
 at      57 ; sust(room_57/57)
 present 56 ; sust(object_56/56)
 zero    24 ; sust(game_flag_24/24)
 let     24 10 ; sust(game_flag_24/24)
 writeln "\nThe clock starts ticking softly."
 done

turn cloc
 at      57 ; sust(room_57/57)
 present 56 ; sust(object_56/56)
 zero    24 ; sust(game_flag_24/24)
 let     24 10 ; sust(game_flag_24/24)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nThe clock starts ticking softly."
 done

pres red
 carried 5 ; sust(object_5_crys/5)
 clear   112 ; sust(game_flag_12/112)
 let     28 2 ; sust(pause_parameter/28) ; sust(pause_sound_effect_telephone/2)
 pause   1
 writeln "\nThe crystalometer gives a small beep as a program is selected."
 writeln "It is in 'RED' mode."
 done

pres gree
 carried 5 ; sust(object_5_crys/5)
 let     112 1 ; sust(game_flag_12/112)
 let     28 2 ; sust(pause_parameter/28) ; sust(pause_sound_effect_telephone/2)
 pause   1
 writeln "\nThe crystalometer gives a small beep as a program is selected."
 writeln "It is in 'GREEN' mode."
 done

pres blue
 carried 5 ; sust(object_5_crys/5)
 let     112 2 ; sust(game_flag_12/112)
 let     28 2 ; sust(pause_parameter/28) ; sust(pause_sound_effect_telephone/2)
 pause   1
 writeln "\nThe crystalometer gives a small beep as a program is selected."
 writeln "It is in 'BLUE' mode."
 done

pres pink
 carried 5 ; sust(object_5_crys/5)
 let     112 3 ; sust(game_flag_12/112)
 let     28 2 ; sust(pause_parameter/28) ; sust(pause_sound_effect_telephone/2)
 pause   1
 writeln "\nThe crystalometer gives a small beep as a program is selected."
 writeln "It is in 'PINK' mode."
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 atgt    35 ; sust(room_35/35)
 atlt    42 ; sust(room_42/42)
 zero    112 ; sust(game_flag_12/112)
 zero    113 ; sust(game_flag_13/113)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 atlt    80 ; sust(room_80/80)
 atgt    74 ; sust(room_74/74)
 eq      112 1 ; sust(game_flag_12/112)
 eq      113 10 ; sust(game_flag_13/113)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 atgt    14 ; sust(room_14/14)
 atlt    26 ; sust(room_26/26)
 notat   17 ; sust(room_17/17)
 notat   18 ; sust(room_18/18)
 notat   19 ; sust(room_19/19)
 notat   20 ; sust(room_20/20)
 notat   21 ; sust(room_21/21)
 eq      112 2 ; sust(game_flag_12/112)
 eq      113 20 ; sust(game_flag_13/113)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 atlt    58 ; sust(room_58/58)
 atgt    51 ; sust(room_51/51)
 eq      112 3 ; sust(game_flag_12/112)
 eq      113 30 ; sust(game_flag_13/113)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 zero    112 ; sust(game_flag_12/112)
 notcarr 9 ; sust(object_9_red/9)
 writeln "{CLASS|center|\nNOTHING DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 at      61 ; sust(room_61/61)
 eq      112 1 ; sust(game_flag_12/112)
 eq      113 10 ; sust(game_flag_13/113)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 carried 9 ; sust(object_9_red/9)
 zero    112 ; sust(game_flag_12/112)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"

pres blac
 carried 5 ; sust(object_5_crys/5)
 carried 10 ; sust(object_10_gree/10)
 eq      112 1 ; sust(game_flag_12/112)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 carried 11 ; sust(object_11_blue/11)
 eq      112 2 ; sust(game_flag_12/112)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 carried 12 ; sust(object_12_pink/12)
 eq      112 3 ; sust(game_flag_12/112)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "{CLASS|center|\nCRYSTAL DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 eq      112 1 ; sust(game_flag_12/112)
 absent  10 ; sust(object_10_gree/10)
 writeln "{CLASS|center|\nNOTHING DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 eq      112 2 ; sust(game_flag_12/112)
 absent  11 ; sust(object_11_blue/11)
 writeln "{CLASS|center|\nNOTHING DETECTED}"
 done

pres blac
 carried 5 ; sust(object_5_crys/5)
 eq      112 3 ; sust(game_flag_12/112)
 absent  12 ; sust(object_12_pink/12)
 writeln "{CLASS|center|\nNOTHING DETECTED}"
 done

pres pend
 at      57 ; sust(room_57/57)
 writeln "\nWrong game!"
 done

pres butt
 at      69 ; sust(room_69/69)
 absent  9 ; sust(object_9_red/9)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 let     28 6 ; sust(pause_parameter/28) ; sust(pause_sound_effect_machine_gun/6)
 pause   50
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 writeln "\nA bright pulsating light scans your body, then a digitized message says:"
 writeln "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN."
 done

pres butt
 at      69 ; sust(room_69/69)
 absent  10 ; sust(object_10_gree/10)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 let     28 6 ; sust(pause_parameter/28) ; sust(pause_sound_effect_machine_gun/6)
 pause   50
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 writeln "\nA bright pulsating light scans your body, then a digitized message says:"
 writeln "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN."
 done

pres butt
 at      69 ; sust(room_69/69)
 absent  11 ; sust(object_11_blue/11)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 let     28 6 ; sust(pause_parameter/28) ; sust(pause_sound_effect_machine_gun/6)
 pause   50
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 writeln "\nA bright pulsating light scans your body, then a digitized message says:"
 writeln "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN."
 done

pres butt
 at      69 ; sust(room_69/69)
 absent  12 ; sust(object_12_pink/12)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 let     28 6 ; sust(pause_parameter/28) ; sust(pause_sound_effect_machine_gun/6)
 pause   50
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 writeln "\nA bright pulsating light scans your body, then a digitized message says:"
 writeln "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN."
 done

pres butt
 at      69 ; sust(room_69/69)
 present 9 ; sust(object_9_red/9)
 present 10 ; sust(object_10_gree/10)
 present 11 ; sust(object_11_blue/11)
 present 12 ; sust(object_12_pink/12)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 let     28 6 ; sust(pause_parameter/28) ; sust(pause_sound_effect_machine_gun/6)
 pause   50
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   1
 writeln "\nA bright pulsating light scans your body, then a digitized message says:"
 writeln "ALL FOUR CRYSTALS DETECTED: PREPARE FOR TRANSPORTATION."
 plus    30 4 ; sust(total_player_score/30)
 pause   250
 pause   100
 goto    80 ; sust(room_80/80)
 desc

dig _
 at      10 ; sust(room_10/10)
 zero    22 ; sust(game_flag_22/22)
 writeln "\nYou uncover an old tin."
 create  25 ; sust(object_25_tin/25)
 let     22 10 ; sust(game_flag_22/22)
 plus    30 2 ; sust(total_player_score/30)
 done

dig _
 at      10 ; sust(room_10/10)
 notzero 22 ; sust(game_flag_22/22)
 writeln "\nYou have already done that."
 done

dig _
 at      73 ; sust(room_73/73)
 eq      18 30 ; sust(game_flag_18/18)
 create  35 ; sust(object_35_witc/35)
 let     18 40 ; sust(game_flag_18/18)
 plus    30 1 ; sust(total_player_score/30)
 writeln "\nYou disturb some of the sand and it rises into the air forming itself into a witch."
 done

dig _
 at      73 ; sust(room_73/73)
 eq      18 40 ; sust(game_flag_18/18)
 create  38 ; sust(object_38/38)
 let     18 50 ; sust(game_flag_18/18)
 plus    30 1 ; sust(total_player_score/30)
 writeln "\nYou uncover a small lever."
 done

lie _
 at      37 ; sust(room_37/37)
 zero    113 ; sust(game_flag_13/113)
 zero    112 ; sust(game_flag_12/112)
 eq      17 30 ; sust(game_flag_17/17)
 let     114 10 ; sust(game_flag_14/114)
 writeln "\nAs you lie down on the mattress you notice the red crystal hanging from the ceiling."
 done

lie _
 at      37 ; sust(room_37/37)
 lt      17 30 ; sust(game_flag_17/17)
 writeln "\nComfortable isn't it."
 done

lie _
 at      37 ; sust(room_37/37)
 eq      17 30 ; sust(game_flag_17/17)
 notzero 112 ; sust(game_flag_12/112)
 writeln "\nComfortable isn't it."
 done

play u
 carried 42 ; sust(object_42_ball/42)
 eq      19 10 ; sust(game_flag_19/19)
 let     7 10 ; sust(countdown_player_input_3/7)
 let     19 20 ; sust(game_flag_19/19)
 ok

play u
 carried 42 ; sust(object_42_ball/42)
 gt      19 10 ; sust(game_flag_19/19)
 writeln "\nYou have already done that."
 done

play pipe
 zero    5 ; sust(countdown_player_input_1/5)
 carried 13 ; sust(object_13_pipe/13)
 present 7 ; sust(object_7_rope/7)
 let     5 4 ; sust(countdown_player_input_1/5)
 writeln "\nYou blow gently into the pipe, mystical Indian music fills your ears and the rope rises upwards."
 done

play pipe
 notzero 5 ; sust(countdown_player_input_1/5)
 carried 13 ; sust(object_13_pipe/13)
 present 7 ; sust(object_7_rope/7)
 writeln "\nThe rope is already taut."
 done

play pipe
 carried 13 ; sust(object_13_pipe/13)
 absent  7 ; sust(object_7_rope/7)
 writeln "\nThe air is filled with mystical Indian music."
 done

play ball
 carried 42 ; sust(object_42_ball/42)
 eq      19 10 ; sust(game_flag_19/19)
 let     7 10 ; sust(countdown_player_input_3/7)
 let     19 20 ; sust(game_flag_19/19)
 ok

play ball
 carried 42 ; sust(object_42_ball/42)
 gt      19 10 ; sust(game_flag_19/19)
 writeln "\nYou have already done that."
 done

open rock
 at      54 ; sust(room_54/54)
 present 37 ; sust(object_37/37)
 writeln "\nThere is no means of opening the door here."
 done

open rock
 at      49 ; sust(room_49/49)
 eq      22 15 ; sust(game_flag_22/22)
 let     22 20 ; sust(game_flag_22/22)
 ok

open rock
 at      49 ; sust(room_49/49)
 gt      22 15 ; sust(game_flag_22/22)
 writeln "\nYou have already done that."
 done

open ches
 at      21 ; sust(room_21/21)
 eq      112 10 ; sust(game_flag_12/112)
 writeln "\nIt's locked."
 done

open ches
 at      21 ; sust(room_21/21)
 eq      112 8 ; sust(game_flag_12/112)
 let     112 6 ; sust(game_flag_12/112)
 ok

open cage
 at      54 ; sust(room_54/54)
 present 48 ; sust(object_48/48)
 swap    48 49 ; sust(object_48/48) ; sust(object_49/49)
 ok

open mole
 at      44 ; sust(room_44/44)
 present 20 ; sust(object_20_mole/20)
 swap    40 41 ; sust(object_40/40) ; sust(object_41/41)
 swap    20 17 ; sust(object_20_mole/20) ; sust(object_17_cage/17)
 plus    30 5 ; sust(total_player_score/30)
 writeln "\nNow the mole has been EXTRICATED it FRANTICALLY burrows through the cave in creating a new exit."
 done

open mole
 notat   44 ; sust(room_44/44)
 present 20 ; sust(object_20_mole/20)
 destroy 20 ; sust(object_20_mole/20)
 create  19 ; sust(object_19/19)
 let     115 10 ; sust(game_flag_15/115)
 writeln "\nThe mole is scared, adds to the droppings, and scurries away."
 done

open glas
 at      79 ; sust(room_79/79)
 eq      116 10 ; sust(game_flag_16/116)
 let     116 20 ; sust(game_flag_16/116)
 ok

unlo rock
 at      49 ; sust(room_49/49)
 eq      22 10 ; sust(game_flag_22/22)
 let     22 15 ; sust(game_flag_22/22)
 ok

unlo ches
 at      21 ; sust(room_21/21)
 eq      112 10 ; sust(game_flag_12/112)
 absent  15 ; sust(object_15_key/15)
 writeln "\nYou try to unlock the chest but the absence of a key makes it impossible."
 done

unlo ches
 at      21 ; sust(room_21/21)
 eq      112 10 ; sust(game_flag_12/112)
 carried 15 ; sust(object_15_key/15)
 let     112 8 ; sust(game_flag_12/112)
 writeln "\nThe chest is now unlocked."
 done

set cage
 at      45 ; sust(room_45/45)
 present 17 ; sust(object_17_cage/17)
 present 18 ; sust(object_18_bait/18)
 let     115 10 ; sust(game_flag_15/115)
 destroy 17 ; sust(object_17_cage/17)
 destroy 18 ; sust(object_18_bait/18)
 create  19 ; sust(object_19/19)
 desc

set cage
 notat   45 ; sust(room_45/45)
 writeln "\nYou can't do that.....Yet."
 done

wait _
 at      45 ; sust(room_45/45)
 eq      115 10 ; sust(game_flag_15/115)
 zero    7 ; sust(countdown_player_input_3/7)
 let     7 4 ; sust(countdown_player_input_3/7)
 writeln "\nTime passes....."
 pause   100
 plus    31 10 ; sust(total_turns_lower/31)
 done

wait _
 at      45 ; sust(room_45/45)
 eq      115 10 ; sust(game_flag_15/115)
 notzero 7 ; sust(countdown_player_input_3/7)
 let     115 20 ; sust(game_flag_15/115)
 writeln "\nTime passes....."
 pause   100
 plus    31 10 ; sust(total_turns_lower/31)
 create  16 ; sust(object_16/16)
 desc

wait _
 at      45 ; sust(room_45/45)
 eq      115 20 ; sust(game_flag_15/115)
 present 19 ; sust(object_19/19)
 present 16 ; sust(object_16/16)
 writeln "\nTime passes....."
 pause   100
 plus    31 10 ; sust(total_turns_lower/31)
 writeln "\nThe mole is tempted by the bait and wanders into the trap..... It is trapped!"
 destroy 19 ; sust(object_19/19)
 destroy 16 ; sust(object_16/16)
 create  20 ; sust(object_20_mole/20)
 let     115 30 ; sust(game_flag_15/115)
 done

wait _
 notat   45 ; sust(room_45/45)
 notat   61 ; sust(room_61/61)
 notat   75 ; sust(room_75/75)
 writeln "\nTime passes....."
 pause   100
 plus    31 10 ; sust(total_turns_lower/31)
 done

wait _
 at      45 ; sust(room_45/45)
 lt      115 10 ; sust(game_flag_15/115)
 writeln "\nTime passes....."
 pause   100
 plus    31 10 ; sust(total_turns_lower/31)
 done

wait _
 at      45 ; sust(room_45/45)
 gt      115 29 ; sust(game_flag_15/115)
 writeln "\nTime passes....."
 pause   100
 plus    31 10 ; sust(total_turns_lower/31)
 done

wait _
 at      61 ; sust(room_61/61)
 writeln "\nTime passes....."
 pause   100
 goto    75 ; sust(room_75/75)
 desc

wait _
 at      75 ; sust(room_75/75)
 writeln "\nTime passes....."
 pause   100
 goto    61 ; sust(room_61/61)
 desc

eat bait
 carried 18 ; sust(object_18_bait/18)
 writeln "\nYUK!!"
 destroy 18 ; sust(object_18_bait/18)
 done

eat wate
 at      54 ; sust(room_54/54)
 present 47 ; sust(object_47/47)
 writeln "\nThe water is highly neurotoxic, that is to say it attacks the central nervous system, causing intense pain, profuse sweating, difficulty in breathing, violent convulsions, unconciousness and finally......DEATH!"
 score
 turns
 end

eat wate
 carried 50 ; sust(object_50/50)
 writeln "\nThe water is highly neurotoxic, that is to say it attacks the central nervous system, causing intense pain, profuse sweating, difficulty in breathing, violent convulsions, unconciousness and finally......DEATH!"
 score
 turns
 end

shut hand
 at      73 ; sust(room_73/73)
 eq      18 50 ; sust(game_flag_18/18)
 destroy 36 ; sust(object_36/36)
 create  37 ; sust(object_37/37)
 let     18 60 ; sust(game_flag_18/18)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nThere is a loud rumbling noise and the pile of sand gradually seeps away.....a large smooth slab of rock slides down from the ceiling. "
 done

shut cage
 at      54 ; sust(room_54/54)
 present 49 ; sust(object_49/49)
 swap    49 48 ; sust(object_49/49) ; sust(object_48/48)
 ok

shut viso
 worn    22 ; sust(object_22_hat/22)
 eq      17 20 ; sust(game_flag_17/17)
 writeln "\nThe visor on the helmet slides down."
 let     17 30 ; sust(game_flag_17/17)
 let     8 6 ; sust(countdown_player_input_4/8)
 done

shut viso
 worn    22 ; sust(object_22_hat/22)
 eq      17 30 ; sust(game_flag_17/17)
 writeln "\nThe visor is already shut."
 done

shut viso
 notworn 22 ; sust(object_22_hat/22)
 writeln "\nYou can't do that.....Yet."
 done

shut viso
 lt      17 20 ; sust(game_flag_17/17)
 writeln "\nYou can't do that.....Yet."
 done

shut boat
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 eq      18 10 ; sust(game_flag_18/18)
 destroy 24 ; sust(object_24/24)
 writeln "\nYou give the boat a hard tug, it slides down the river bank, hits the water with a splash......... then sinks."
 done

shut boat
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 eq      18 20 ; sust(game_flag_18/18)
 writeln "\nYou give the boat a hard tug and it slides down the bank into the water."
 let     18 30 ; sust(game_flag_18/18)
 done

conn cabl
 carried 5 ; sust(object_5_crys/5)
 worn    22 ; sust(object_22_hat/22)
 eq      17 10 ; sust(game_flag_17/17)
 let     17 20 ; sust(game_flag_17/17)
 writeln "\nIt fits perfectly."
 done

conn cabl
 carried 22 ; sust(object_22_hat/22)
 notworn 5 ; sust(object_5_crys/5)
 writeln "\nYou can't do that.....Yet."
 done

conn cabl
 gt      17 10 ; sust(game_flag_17/17)
 writeln "\nYou have already done that."
 done

conn cabl
 notcarr 5 ; sust(object_5_crys/5)
 writeln "\nIt will not plug into any of the objects you are carrying."
 done

fix boat
 at      24 ; sust(room_24/24)
 carried 26 ; sust(object_26/26)
 present 21 ; sust(object_21_book/21)
 present 24 ; sust(object_24/24)
 eq      18 10 ; sust(game_flag_18/18)
 writeln "\nThe pages from the book and the sticky tar make the boat water- proof."
 destroy 21 ; sust(object_21_book/21)
 let     18 20 ; sust(game_flag_18/18)
 swap    26 25 ; sust(object_26/26) ; sust(object_25_tin/25)
 plus    30 5 ; sust(total_player_score/30)
 done

fix boat
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 carried 26 ; sust(object_26/26)
 absent  21 ; sust(object_21_book/21)
 writeln "\nYou haven't got everything You need to repair the boat."
 done

fix boat
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 absent  26 ; sust(object_26/26)
 carried 21 ; sust(object_21_book/21)
 writeln "\nYou haven't got everything You need to repair the boat."
 done

fix boat
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 absent  21 ; sust(object_21_book/21)
 absent  26 ; sust(object_26/26)
 writeln "\nYou can't do that.....Yet."
 done

fix boat
 at      24 ; sust(room_24/24)
 gt      18 19 ; sust(game_flag_18/18)
 writeln "\nYou have already done that."
 done

foll trac
 at      24 ; sust(room_24/24)
 writeln "\nThe tracks disappear into some overgrown bushes."
 done

put tar
 NOUN2   boat
 at      24 ; sust(room_24/24)
 present 24 ; sust(object_24/24)
 present 26 ; sust(object_26/26)
 eq      18 10 ; sust(game_flag_18/18)
 let     18 20 ; sust(game_flag_18/18)
 swap    26 25 ; sust(object_26/26) ; sust(object_25_tin/25)
 ok

ashes off
 atgt    8 ; sust(room_8/8)
 atlt    16 ; sust(room_16/16)
 notat   12 ; sust(room_12/12)
 notat   13 ; sust(room_13/13)
 carried 0 ; sust(object_0/0)
 set     0 ; sust(yesno_is_dark/0)
 swap    0 27 ; sust(object_0/0) ; sust(object_27_ashe/27)
 desc

ashes off
 carried 27 ; sust(object_27_ashe/27)
 writeln "\nIt's already off....stupid."
 done

ashes off
 carried 0 ; sust(object_0/0)
 swap    0 27 ; sust(object_0/0) ; sust(object_27_ashe/27)
 ok

ashes on
 carried 27 ; sust(object_27_ashe/27)
 swap    27 0 ; sust(object_27_ashe/27) ; sust(object_0/0)
 clear   0 ; sust(yesno_is_dark/0)
 desc

ashes on
 carried 0 ; sust(object_0/0)
 writeln "\nIt's already on...stupid!"
 done

ashes fag
 carried 44 ; sust(object_44_fag/44)
 writeln "\nWarning: SMOKING CAN CAUSE HEART DISEASE."
 done

turn off 
 NOUN2   ashe
 atgt    8 ; sust(room_8/8)
 atlt    16 ; sust(room_16/16)
 notat   12 ; sust(room_12/12)
 notat   13 ; sust(room_13/13)
 carried 0 ; sust(object_0/0)
 swap    0 27 ; sust(object_0/0) ; sust(object_27_ashe/27)
 set     0 ; sust(yesno_is_dark/0)
 desc

turn off 
 NOUN2   ashe
 carried 27 ; sust(object_27_ashe/27)
 writeln "\nIt's already off....stupid."
 done

turn off
 NOUN2   ashe
 carried 0 ; sust(object_0/0)
 swap    0 27 ; sust(object_0/0) ; sust(object_27_ashe/27)
 ok

jump u
 at      29 ; sust(room_29/29)
 notcarr 34 ; sust(object_34_jump/34)
 writeln "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed."
 score
 turns
 end

jump u
 at      32 ; sust(room_32/32)
 notcarr 34 ; sust(object_34_jump/34)
 writeln "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed."
 score
 turns
 end

jump rive
 at      29 ; sust(room_29/29)
 notcarr 34 ; sust(object_34_jump/34)
 writeln "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed."
 score
 turns
 end

jump rive
 at      32 ; sust(room_32/32)
 notcarr 34 ; sust(object_34_jump/34)
 writeln "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed."
 score
 turns
 end

jump rive
 at      72 ; sust(room_72/72)
 writeln "\nThe ravine is much to wide and you fall to your death."
 score
 turns
 end

jump rive
 at      29 ; sust(room_29/29)
 carried 34 ; sust(object_34_jump/34)
 writeln "\nThe pole catches on the edge of the river, you sail high in the air and........."
 pause   150
 goto    32 ; sust(room_32/32)
 desc

jump rive
 at      32 ; sust(room_32/32)
 carried 34 ; sust(object_34_jump/34)
 writeln "\nThe pole catches on the edge of the river, you sail high in the air and........."
 pause   150
 goto    29 ; sust(room_29/29)
 desc

jump _
 writeln "\nWheeee..."
 done

leave ashe
 at      79 ; sust(room_79/79)
 eq      116 20 ; sust(game_flag_16/116)
 eq      113 20 ; sust(game_flag_13/113)
 carried 0 ; sust(object_0/0)
 destroy 0 ; sust(object_0/0)
 writeln "\nThe lamp shines brightly in the cabinet."
 let     116 30 ; sust(game_flag_16/116)
 done

swim _
 at      29 ; sust(room_29/29)
 writeln "\nThe current is too strong and sweeps you against the rocks.... you are dead."
 score
 turns
 end

swim _
 at      32 ; sust(room_32/32)
 writeln "\nThe current is too strong and sweeps you against the rocks.... you are dead."
 score
 turns
 end

turn on
 NOUN2   ashe
 carried 27 ; sust(object_27_ashe/27)
 swap    27 0 ; sust(object_27_ashe/27) ; sust(object_0/0)
 clear   0 ; sust(yesno_is_dark/0)
 desc

turn on
 NOUN2   ashe
 carried 0 ; sust(object_0/0)
 writeln "\nIt's already on...stupid!"
 done

spec mate
 at      1 ; sust(room_1/1)
 goto    74 ; sust(room_74/74)
 desc
 done

undo bolt
 at      49 ; sust(room_49/49)
 eq      22 10 ; sust(game_flag_22/22)
 let     22 15 ; sust(game_flag_22/22)
 ok

undo bolt
 at      49 ; sust(room_49/49)
 gt      22 10 ; sust(game_flag_22/22)
 writeln "\nYou have already done that."
 done

undo scre
 at      57 ; sust(room_57/57)
 carried 51 ; sust(object_51_scre/51)
 lt      23 20 ; sust(game_flag_23/23)
 let     23 20 ; sust(game_flag_23/23)
 plus    30 2 ; sust(total_player_score/30)
 ok

undo scre
 present 51 ; sust(object_51_scre/51)
 gt      23 19 ; sust(game_flag_23/23)
 writeln "\nYou have already done that."
 done

give sand
 at      77 ; sust(room_77/77)
 eq      115 40 ; sust(game_flag_15/115)
 carried 35 ; sust(object_35_witc/35)
 swap    35 43 ; sust(object_35_witc/35) ; sust(object_43_pape/43)
 let     115 50 ; sust(game_flag_15/115)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nThe lighthouse keeper takes the sandwich, and waffles it down as though he hasn't eaten for a week, he then looks around his desk for something to give you in return. He gives you a glass paperweight."
 done

give witc
 at      77 ; sust(room_77/77)
 eq      115 40 ; sust(game_flag_15/115)
 carried 35 ; sust(object_35_witc/35)
 swap    35 43 ; sust(object_35_witc/35) ; sust(object_43_pape/43)
 let     115 50 ; sust(game_flag_15/115)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nThe lighthouse keeper takes the sandwich, and waffles it down as though he hasn't eaten for a week, he then looks around his desk for something to give you in return. He gives you a glass paperweight."
 done

give fag
 at      50 ; sust(room_50/50)
 carried 44 ; sust(object_44_fag/44)
 present 46 ; sust(object_46/46)
 swap    44 45 ; sust(object_44_fag/44) ; sust(object_45_chai/45)
 destroy 46 ; sust(object_46/46)
 plus    30 2 ; sust(total_player_score/30)
 writeln "\nGrateful, for at last getting some real cigarettes, he gives you the chain he's been smoking, and leaves."
 done

give chai
 at      65 ; sust(room_65/65)
 carried 45 ; sust(object_45_chai/45)
 present 31 ; sust(object_31/31)
 destroy 31 ; sust(object_31/31)
 swap    45 42 ; sust(object_45_chai/45) ; sust(object_42_ball/42)
 let     115 40 ; sust(game_flag_15/115)
 writeln "\nThe ghost takes the chain, then rattles it a few times, he is so pleased that he gives a beach ball to you before disapearing."
 plus    30 3 ; sust(total_player_score/30)
 done

g air
 present 1 ; sust(object_1_air/1)
 get     1 ; sust(object_1_air/1)
 ok

g air
 present 2 ; sust(object_2_air/2)
 get     2 ; sust(object_2_air/2)
 ok

g air
 present 3 ; sust(object_3_air/3)
 get     3 ; sust(object_3_air/3)
 ok

g air
 present 4 ; sust(object_4_air/4)
 get     4 ; sust(object_4_air/4)
 ok

g hand
 at      4 ; sust(room_4/4)
 lt      111 9 ; sust(game_flag_11/111)
 create  6 ; sust(object_6_hand/6)
 let     111 10 ; sust(game_flag_11/111)
 create  7 ; sust(object_7_rope/7)
 writeln "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy."
 get     6 ; sust(object_6_hand/6)
 done

g hand
 at      70 ; sust(room_70/70)
 eq      111 30 ; sust(game_flag_11/111)
 present 8 ; sust(object_8/8)
 writeln "\nIt's stuck fast."
 done

g hand
 at      70 ; sust(room_70/70)
 eq      111 20 ; sust(game_flag_11/111)
 swap    6 14 ; sust(object_6_hand/6) ; sust(object_14/14)
 get     6 ; sust(object_6_hand/6)
 let     111 18 ; sust(game_flag_11/111)
 ok

g hand
 at      63 ; sust(room_63/63)
 eq      111 20 ; sust(game_flag_11/111)
 let     111 18 ; sust(game_flag_11/111)
 swap    6 23 ; sust(object_6_hand/6) ; sust(object_23/23)
 get     6 ; sust(object_6_hand/6)
 ok

g hand
 at      63 ; sust(room_63/63)
 eq      111 40 ; sust(game_flag_11/111)
 writeln "\nIt's stuck fast."
 done

g crys
 carried 5 ; sust(object_5_crys/5)
 writeln "\nYou are already carrying the crystalometer."
 done

g crys
 at      21 ; sust(room_21/21)
 eq      112 6 ; sust(game_flag_12/112)
 create  5 ; sust(object_5_crys/5)
 plus    30 4 ; sust(total_player_score/30)
 let     112 1 ; sust(game_flag_12/112)
 get     5 ; sust(object_5_crys/5)
 ok

g crys
 present 5 ; sust(object_5_crys/5)
 get     5 ; sust(object_5_crys/5)
 ok

g red
 at      37 ; sust(room_37/37)
 notzero 5 ; sust(countdown_player_input_1/5)
 notzero 6 ; sust(countdown_player_input_2/6)
 zero    113 ; sust(game_flag_13/113)
 eq      114 10 ; sust(game_flag_14/114)
 let     113 10 ; sust(game_flag_13/113)
 plus    30 8 ; sust(total_player_score/30)
 create  9 ; sust(object_9_red/9)
 get     9 ; sust(object_9_red/9)
 ok

g red
 at      37 ; sust(room_37/37)
 notzero 114 ; sust(game_flag_14/114)
 zero    113 ; sust(game_flag_13/113)
 zero    6 ; sust(countdown_player_input_2/6)
 writeln "\nYou can't reach it."
 done

g gree
 at      79 ; sust(room_79/79)
 eq      116 20 ; sust(game_flag_16/116)
 lt      113 20 ; sust(game_flag_13/113)
 eq      114 15 ; sust(game_flag_14/114)
 create  10 ; sust(object_10_gree/10)
 plus    30 8 ; sust(total_player_score/30)
 let     113 20 ; sust(game_flag_13/113)
 let     114 20 ; sust(game_flag_14/114)
 let     28 14 ; sust(pause_parameter/28) ; sust(pause_ability_plus/14)
 pause   1
 get     10 ; sust(object_10_gree/10)
 ok

g gree
 at      79 ; sust(room_79/79)
 absent  10 ; sust(object_10_gree/10)
 eq      113 20 ; sust(game_flag_13/113)
 writeln "\nYou have already done that."
 done

g gree
 at      79 ; sust(room_79/79)
 eq      116 10 ; sust(game_flag_16/116)
 writeln "\nThe cabinet is closed."
 done

g blue
 at      22 ; sust(room_22/22)
 present 43 ; sust(object_43_pape/43)
 notcarr 43 ; sust(object_43_pape/43)
 eq      114 25 ; sust(game_flag_14/114)
 create  11 ; sust(object_11_blue/11)
 plus    30 8 ; sust(total_player_score/30)
 destroy 43 ; sust(object_43_pape/43)
 let     114 30 ; sust(game_flag_14/114)
 let     113 30 ; sust(game_flag_13/113)
 get     11 ; sust(object_11_blue/11)
 ok

g blue
 at      22 ; sust(room_22/22)
 eq      114 25 ; sust(game_flag_14/114)
 writeln "\nAnnoyed at you for stealing her egg, an eagle swoops down from the sky snatching it from your hand and replacing it in the nest."
 done

g pink
 at      57 ; sust(room_57/57)
 eq      113 30 ; sust(game_flag_13/113)
 eq      114 35 ; sust(game_flag_14/114)
 let     113 40 ; sust(game_flag_13/113)
 let     114 40 ; sust(game_flag_14/114)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   6
 let     28 14 ; sust(pause_parameter/28) ; sust(pause_ability_plus/14)
 pause   1
 create  12 ; sust(object_12_pink/12)
 plus    30 8 ; sust(total_player_score/30)
 writeln "\nThe Crystalometer bleep's and a recorded message is played:\n&#34;All Crystals found: Now take them to the underground trans- porter tube, and prepare for transport.&#34;"
 get     12 ; sust(object_12_pink/12)
 done

g key
 at      57 ; sust(room_57/57)
 present 56 ; sust(object_56/56)
 writeln "\nIt's stuck fast."
 done

g ches
 at      21 ; sust(room_21/21)
 writeln "\nIt is much too large for just one mere mortal to lift."
 done

g cage
 at      45 ; sust(room_45/45)
 present 19 ; sust(object_19/19)
 destroy 19 ; sust(object_19/19)
 clear   115 ; sust(game_flag_15/115)
 create  17 ; sust(object_17_cage/17)
 create  18 ; sust(object_18_bait/18)
 get     17 ; sust(object_17_cage/17)
 get     18 ; sust(object_18_bait/18)
 ok

g cage
 present 20 ; sust(object_20_mole/20)
 get     20 ; sust(object_20_mole/20)
 ok

g bait
 at      1 ; sust(room_1/1)
 eq      115 200 ; sust(game_flag_15/115)
 plus    30 2 ; sust(total_player_score/30)
 create  18 ; sust(object_18_bait/18)
 clear   115 ; sust(game_flag_15/115)
 get     18 ; sust(object_18_bait/18)
 ok

g mole
 at      45 ; sust(room_45/45)
 present 16 ; sust(object_16/16)
 writeln "\nThe mole is scared, adds to the droppings, and scurries away."
 destroy 16 ; sust(object_16/16)
 let     115 10 ; sust(game_flag_15/115)
 create  19 ; sust(object_19/19)
 done

g mole
 present 20 ; sust(object_20_mole/20)
 get     20 ; sust(object_20_mole/20)
 ok

g stat
 at      14 ; sust(room_14/14)
 writeln "\nAs you attempt to remove this sacred object from it's resting place, a bolt of plasma strikes you down.....you are a bit dead."
 score
 turns
 end

g boat
 present 24 ; sust(object_24/24)
 writeln "\nThe boat is too heavy to carry."
 done

g boat
 absent  24 ; sust(object_24/24)
 writeln "\nYou cannot see a boat."
 done

g tar
 at      39 ; sust(room_39/39)
 absent  25 ; sust(object_25_tin/25)
 absent  26 ; sust(object_26/26)
 writeln "\nAnd what do you suggest you should use to collect it in."
 done

g tar
 at      39 ; sust(room_39/39)
 carried 25 ; sust(object_25_tin/25)
 swap    25 26 ; sust(object_25_tin/25) ; sust(object_26/26)
 ok

g tar
 at      39 ; sust(room_39/39)
 present 26 ; sust(object_26/26)
 writeln "\nYou have already done that."
 done

g egg
 at      22 ; sust(room_22/22)
 absent  43 ; sust(object_43_pape/43)
 writeln "\nAnnoyed at you for stealing her egg, an eagle swoops down from the sky snatching it from your hand and replacing it in the nest."
 done

g egg
 at      22 ; sust(room_22/22)
 carried 43 ; sust(object_43_pape/43)
 writeln "\nAnnoyed at you for stealing her egg, an eagle swoops down from the sky snatching it from your hand and replacing it in the nest."
 done

g egg
 at      22 ; sust(room_22/22)
 present 43 ; sust(object_43_pape/43)
 notcarr 43 ; sust(object_43_pape/43)
 get     43 ; sust(object_43_pape/43)
 writeln "\nYou take the paperweight in mistake for the egg."
 done

g wate
 at      54 ; sust(room_54/54)
 carried 25 ; sust(object_25_tin/25)
 present 47 ; sust(object_47/47)
 swap    25 50 ; sust(object_25_tin/25) ; sust(object_50/50)
 swap    47 48 ; sust(object_47/47) ; sust(object_48/48)
 plus    30 1 ; sust(total_player_score/30)
 writeln "\nAfter collecting all the water, you discover a trapdoor."
 done

g wate
 at      54 ; sust(room_54/54)
 absent  47 ; sust(object_47/47)
 writeln "\nYou have already done that."
 done

i _
 inven

get scre
 at      78 ; sust(room_78/78)
 zero    23 ; sust(game_flag_23/23)
 create  51 ; sust(object_51_scre/51)
 plus    30 2 ; sust(total_player_score/30)
 let     23 10 ; sust(game_flag_23/23)
 get     51 ; sust(object_51_scre/51)
 ok

get magn
 present 55 ; sust(object_55_magn/55)
 get     55 ; sust(object_55_magn/55)
 ok

g hat
 at      14 ; sust(room_14/14)
 zero    17 ; sust(game_flag_17/17)
 create  22 ; sust(object_22_hat/22)
 plus    30 2 ; sust(total_player_score/30)
 let     17 10 ; sust(game_flag_17/17)
 get     22 ; sust(object_22_hat/22)
 ok

g hat
 present 22 ; sust(object_22_hat/22)
 get     22 ; sust(object_22_hat/22)
 ok

g _
 autog
 ok

put air
 carried 1 ; sust(object_1_air/1)
 drop    1 ; sust(object_1_air/1)
 ok

put air
 carried 2 ; sust(object_2_air/2)
 drop    2 ; sust(object_2_air/2)
 ok

put air
 carried 3 ; sust(object_3_air/3)
 drop    3 ; sust(object_3_air/3)
 ok

put air
 carried 4 ; sust(object_4_air/4)
 drop    4 ; sust(object_4_air/4)
 ok

put crys
 carried 5 ; sust(object_5_crys/5)
 eq      17 10 ; sust(game_flag_17/17)
 drop    5 ; sust(object_5_crys/5)
 ok

put crys
 carried 5 ; sust(object_5_crys/5)
 gt      17 10 ; sust(game_flag_17/17)
 lt      17 200 ; sust(game_flag_17/17)
 notzero 8 ; sust(countdown_player_input_4/8)
 drop    5 ; sust(object_5_crys/5)
 let     17 10 ; sust(game_flag_17/17)
 clear   8 ; sust(countdown_player_input_4/8)
 writeln "\nAs you drop the Crystalometer the cable disconnects."
 writeln "\nThe visor slowly slides up."
 done

put crys
 carried 5 ; sust(object_5_crys/5)
 gt      17 10 ; sust(game_flag_17/17)
 lt      17 200 ; sust(game_flag_17/17)
 zero    8 ; sust(countdown_player_input_4/8)
 drop    5 ; sust(object_5_crys/5)
 let     17 10 ; sust(game_flag_17/17)
 writeln "\nAs you drop the Crystalometer the cable disconnects."
 done

put crys
 carried 5 ; sust(object_5_crys/5)
 eq      17 200 ; sust(game_flag_17/17)
 drop    5 ; sust(object_5_crys/5)
 ok

put cage
 carried 20 ; sust(object_20_mole/20)
 drop    20 ; sust(object_20_mole/20)
 ok

put mole
 carried 20 ; sust(object_20_mole/20)
 drop    20 ; sust(object_20_mole/20)
 ok

put tin
 carried 26 ; sust(object_26/26)
 swap    26 25 ; sust(object_26/26) ; sust(object_25_tin/25)
 drop    25 ; sust(object_25_tin/25)
 writeln "\nAs the tin hits the floor the tar spills out and seeps into the floor."
 done

put tin
 carried 50 ; sust(object_50/50)
 swap    50 25 ; sust(object_50/50) ; sust(object_25_tin/25)
 drop    25 ; sust(object_25_tin/25)
 writeln "\nAs the tin hits the floor, the water seeps away."
 done

put ashe
 carried 0 ; sust(object_0/0)
 atlt    16 ; sust(room_16/16)
 atgt    8 ; sust(room_8/8)
 notat   12 ; sust(room_12/12)
 notat   13 ; sust(room_13/13)
 swap    0 27 ; sust(object_0/0) ; sust(object_27_ashe/27)
 drop    27 ; sust(object_27_ashe/27)
 set     0 ; sust(yesno_is_dark/0)
 writeln "\nAs the lantern hits the floor it goes out."
 pause   100
 desc

put ashe
 carried 0 ; sust(object_0/0)
 notat   11 ; sust(room_11/11)
 notat   10 ; sust(room_10/10)
 notat   14 ; sust(room_14/14)
 notat   9 ; sust(room_9/9)
 notat   15 ; sust(room_15/15)
 lt      113 20 ; sust(game_flag_13/113)
 swap    0 27 ; sust(object_0/0) ; sust(object_27_ashe/27)
 drop    27 ; sust(object_27_ashe/27)
 writeln "\nAs the lantern hits the floor it goes out."
 done

put ashe
 at      79 ; sust(room_79/79)
 eq      116 20 ; sust(game_flag_16/116)
 eq      113 20 ; sust(game_flag_13/113)
 carried 0 ; sust(object_0/0)
 destroy 0 ; sust(object_0/0)
 let     116 30 ; sust(game_flag_16/116)
 writeln "\nThe lamp shines brightly in the cabinet."
 done

put pape
 at      22 ; sust(room_22/22)
 carried 43 ; sust(object_43_pape/43)
 drop    43 ; sust(object_43_pape/43)
 writeln "\nThe paperweight settles nicely next to the egg in the nest."
 done

put magn
 carried 55 ; sust(object_55_magn/55)
 drop    55 ; sust(object_55_magn/55)
 ok

put hat
 carried 22 ; sust(object_22_hat/22)
 drop    22 ; sust(object_22_hat/22)
 ok

put hat
 worn    22 ; sust(object_22_hat/22)
 writeln "\nYou can't do that.....Yet."
 done

put _
 autod
 ok

remo face
 at      57 ; sust(room_57/57)
 eq      23 20 ; sust(game_flag_23/23)
 let     23 30 ; sust(game_flag_23/23)
 create  54 ; sust(object_54_face/54)
 writeln "\nYou remove the face leaving a large hole."
 done

remo face
 at      57 ; sust(room_57/57)
 gt      23 20 ; sust(game_flag_23/23)
 writeln "\nYou have already done that."
 done

remo plat
 at      57 ; sust(room_57/57)
 eq      23 30 ; sust(game_flag_23/23)
 carried 52 ; sust(object_52_magn/52)
 swap    52 55 ; sust(object_52_magn/52) ; sust(object_55_magn/55)
 let     23 40 ; sust(game_flag_23/23)
 plus    30 2 ; sust(total_player_score/30)
 ok

remo hat
 gt      17 10 ; sust(game_flag_17/17)
 worn    22 ; sust(object_22_hat/22)
 notzero 8 ; sust(countdown_player_input_4/8)
 remove  22 ; sust(object_22_hat/22)
 clear   8 ; sust(countdown_player_input_4/8)
 writeln "\nAs you remove the helmet the cable disconnects."
 writeln "\nThe visor slowly slides up."
 let     17 10 ; sust(game_flag_17/17)
 done

remo hat
 worn    22 ; sust(object_22_hat/22)
 lt      17 20 ; sust(game_flag_17/17)
 remove  22 ; sust(object_22_hat/22)
 ok

remo hat
 notworn 22 ; sust(object_22_hat/22)
 writeln "\nIt's already off....stupid."
 done

remo hat
 gt      17 10 ; sust(game_flag_17/17)
 zero    8 ; sust(countdown_player_input_4/8)
 worn    22 ; sust(object_22_hat/22)
 remove  22 ; sust(object_22_hat/22)
 writeln "\nAs you remove the helmet the cable disconnects."
 let     17 10 ; sust(game_flag_17/17)
 done

remo _
 autor
 ok

wear hat
 carried 22 ; sust(object_22_hat/22)
 wear    22 ; sust(object_22_hat/22)
 ok

wear _
 autow
 ok

i _
 inven

r _
 desc

q _
 quit
 score
 turns
 end

save _
 save

rest _
 load

new type
 let     28 8 ; sust(pause_parameter/28) ; sust(pause_font_alternate/8)
 pause   1
 desc
 done

norm type
 let     28 7 ; sust(pause_parameter/28) ; sust(pause_font_default/7)
 pause   1
 desc
 done

pict _
 let     28 19 ; sust(pause_parameter/28) ; sust(pause_toggle_graphics/19)
 pause   10
 desc

word _
 let     28 19 ; sust(pause_parameter/28) ; sust(pause_toggle_graphics/19)
 pause   255
 desc

scor _
 score
 done

help crys
 zero    113 ; sust(game_flag_13/113)
 writeln "\nRed first."
 done

help crys
 eq      113 10 ; sust(game_flag_13/113)
 writeln "\nGreen next."
 done

help crys
 eq      113 20 ; sust(game_flag_13/113)
 writeln "\nNow the blue."
 done

help crys
 eq      113 20 ; sust(game_flag_13/113)
 writeln "\nThere's only one left..Berk."
 done

help cage
 writeln "\nThis has all the trappings of a useful object."
 done

help bait
 writeln "\nThis will cause you to wait with baited breath."
 done

help book
 writeln "\nYou may have thought it was a spelling error!"
 done

help tin
 writeln "\nA sticky situation is at the root of this problem."
 done

help witc
 writeln "\nBe generous."
 done

help fag
 writeln "\nBe generous."
 done

help chai
 writeln "\nBe generous."
 done

help span
 writeln "\nNo, you plonker, there is not a spanner in this game, it was an example.  GIVE ME STRENGTH."
 done

help _
 zero    26 ; sust(game_flag_26/26)
 let     26 10 ; sust(game_flag_26/26)
 writeln "\nIf you are in real need of help, then use HELP &#34;OBJECT&#34;.\neg. HELP SPANNER will give you a clue about the spanner."
 done

help _
 notzero 26 ; sust(game_flag_26/26)
 writeln "\nWe're not giving too much away!"
 done

fill tin
 at      39 ; sust(room_39/39)
 absent  25 ; sust(object_25_tin/25)
 absent  26 ; sust(object_26/26)
 writeln "\nAnd what do you suggest you should use to collect it in."
 done

fill tin
 at      39 ; sust(room_39/39)
 carried 25 ; sust(object_25_tin/25)
 swap    25 26 ; sust(object_25_tin/25) ; sust(object_26/26)
 ok

fill tin
 at      39 ; sust(room_39/39)
 present 26 ; sust(object_26/26)
 writeln "\nYou have already done that."
 done

fill tin
 at      54 ; sust(room_54/54)
 carried 25 ; sust(object_25_tin/25)
 present 47 ; sust(object_47/47)
 swap    25 50 ; sust(object_25_tin/25) ; sust(object_50/50)
 swap    47 48 ; sust(object_47/47) ; sust(object_48/48)
 plus    30 1 ; sust(total_player_score/30)
 writeln "\nAfter collecting all the water, you discover a trapdoor."
 done

fill tin
 at      54 ; sust(room_54/54)
 absent  47 ; sust(object_47/47)
 writeln "\nYou have already done that."
 done

fill tin
 carried 25 ; sust(object_25_tin/25)
 notat   39 ; sust(room_39/39)
 notat   54 ; sust(room_54/54)
 writeln "\nYou can't do that.....Yet."
 done

save _

 RAMSAVE
 pause   1

rest _

 LOAD
 pause   1


_ _
 hook    "RESPONSE_DEFAULT_START"

rams _
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   100
 desc
 done

raml _
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   50
 desc
 done


_ _
 hook    "RESPONSE_DEFAULT_END"







/PRO 1


_ _
 hook "PRO1"

_ _
 at     0
 bclear 12 5                      ; Set language to English

_ _
 islight
 listobj                        ; Lists present objects
 listnpc @38                    ; Lists present NPCs







/PRO 2


_ _
 eq      31 0 ; sust(total_turns_lower/31)
 eq      32 0 ; sust(total_turns_higher/32)
 ability 8 8


_ _
 hook    "PRO2"

_ _
 gt      31 139 ; sust(total_turns_lower/31)
 lt      31 150 ; sust(total_turns_lower/31)
 writeln "\nYou are getting low on air......"









 done

_ _
 gt      31 149 ; sust(total_turns_lower/31)
 writeln "\nSorry you have run out of air..."
 turns
 score
 end

_ _
 at      80 ; sust(room_80/80)
 anykey
 cls
 score
 turns
 end

_ _
 at      0 ; sust(room_0/0)
 anykey
 let     115 200 ; sust(game_flag_15/115)
 let     112 20 ; sust(game_flag_12/112)
 let     17 200 ; sust(game_flag_17/17)
 let     27 200 ; sust(game_flag_27/27)
 let     19 210 ; sust(game_flag_19/19)
 goto    81 ; sust(room_81/81)
 desc

_ _
 eq      8 1 ; sust(countdown_player_input_4/8)
 writeln "\nThe visor slowly slides up."
 let     17 20 ; sust(game_flag_17/17)
 done

_ _
 at      65 ; sust(room_65/65)
 eq      7 1 ; sust(countdown_player_input_3/7)
 create  31 ; sust(object_31/31)
 writeln "\nThe spirit of a brave warrior (called Stephen) appears."
 done

_ _
 at      74 ; sust(room_74/74)
 anykey
 pause   100
 let     28 8 ; sust(pause_parameter/28) ; sust(pause_font_alternate/8)
 pause   10
 goto    81 ; sust(room_81/81)
 desc

_ _
 at      81 ; sust(room_81/81)
 anykey
 pause   10
 goto    82 ; sust(room_82/82)
 desc

_ _
 at      82 ; sust(room_82/82)
 anykey
 pause   10
 goto    83 ; sust(room_83/83)
 desc

_ _
 at      83 ; sust(room_83/83)
 anykey

 pause   10
 goto    1 ; sust(room_1/1)
 desc

_ _
 lt      19 200 ; sust(game_flag_19/19)
 lt      7 1 ; sust(countdown_player_input_3/7)
 let     19 10 ; sust(game_flag_19/19)
 done
