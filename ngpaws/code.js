// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
var VOCABULARY_ID = 0;
var VOCABULARY_WORD = 1;
var VOCABULARY_TYPE = 2;

var WORDTYPE_VERB = 0;
var WORDTYPE_NOUN = 1
var WORDTYPE_ADJECT = 2;
var WORDTYPE_ADVERB = 3;
var WORDTYPE_PRONOUN = 4;
var WORDTYPE_CONJUNCTION = 5;
var WORDTYPE_PREPOSITION = 6;

var TIMER_MILLISECONDS  = 40;

var RESOURCE_TYPE_IMG = 1;
var RESOURCE_TYPE_SND = 2;

var PROCESS_RESPONSE = 0;
var PROCESS_DESCRIPTION = 1;
var PROCESS_TURN = 2;

var DIV_TEXT_SCROLL_STEP = 40;


// Aux
var SET_VALUE = 255; // Value assigned by SET condact
var EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
var MAX_WORD_LENGHT = 10;  // Number of characters considered per word
var FLAG_COUNT = 256;  // Number of flags
var NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
var NUM_CONVERTIBLE_NOUNS = 20;
var NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
var EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
var NO_EXIT = 255;  // If an exit does not exist, its value is this value
var MAX_CHANNELS = 17; // Number of SFX channels
var RESOURCES_DIR='dat/';


//Attributes
var ATTR_LIGHT=0;			// Object produces light
var ATTR_WEARABLE=1;		// Object is wearable
var ATTR_CONTAINER=2;       // Object is a container
var ATTR_NPC=3;             // Object is actually an NPC
var ATTR_CONCEALED = 4; /// Present but not visible
var ATTR_EDIBLE = 5;   /// Can be eaten
var ATTR_DRINKABLE=6;
var ATTR_ENTERABLE = 7;
var ATTR_FEMALE = 8;
var ATTR_LOCKABLE = 9;
var ATTR_LOCKED = 10;
var ATTR_MALE = 11;
var ATTR_NEUTER=12;
var ATTR_OPENABLE =13;
var ATTR_OPEN=14;
var ATTR_PLURALNAME = 15;
var ATTR_TRANSPARENT=16;
var ATTR_SCENERY=17;
var ATTR_SUPPORTER = 18;
var ATTR_SWITCHABLE=19;
var ATTR_ON  =20;
var ATTR_STATIC  =21;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
var STR_NEWLINE = '<br />';
var STR_PROMPT_START = '<span class="feedback">&gt; ';
var STR_PROMPT_END = '</span>';
var STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
var STR_WRONG_SYSMESS = 'WARNING: System message requested does not exist.'; 
var STR_WRONG_LOCATION = 'WARNING: Location requested does not exist.'; 
var STR_WRONG_MESSAGE = 'WARNING: Message requested does not exist.'; 
var STR_WRONG_PROCESS = 'WARNING: Process requested does not exist.' 
var STR_RAMLOAD_ERROR= 'WARNING: You can\'t restore game as it has not yet been saved.'; 
var STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez.  Released under {URL|http://www.opensource.org/licenses/MIT| MIT license}.\nBuzz sound libray (C) Jay Salvat. Released under the {URL|http://www.opensource.org/licenses/MIT| MIT license} \n jQuery (C) jQuery Foundation. Released under the {URL|https://jquery.org/license/| MIT license}.';
var STR_TRANSCRIPT = 'To copy the transcript to your clipboard, press Ctrl+C, then press Enter';

var STR_INVALID_TAG_SEQUENCE = 'Invalid tag sequence: ';
var STR_INVALID_TAG_SEQUENCE_EMPTY = 'Invalid tag sequence.';
var STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Invalid tag sequence: bad parameters.';
var STR_INVALID_TAG_SEQUENCE_BADTAG = 'Invalid tag sequence: unknown tag.';
var STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
var STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var FLAG_LIGHT = 0;
var FLAG_OBJECTS_CARRIED_COUNT = 1;
var FLAG_AUTODEC2 = 2; 
var FLAG_AUTODEC3 = 3;
var FLAG_AUTODEC4 = 4;
var FLAG_AUTODEC5 = 5;
var FLAG_AUTODEC6 = 6;
var FLAG_AUTODEC7 = 7;
var FLAG_AUTODEC8 = 8;
var FLAG_AUTODEC9 = 9;
var FLAG_AUTODEC10 = 10;
var FLAG_ESCAPE = 11;
var FLAG_PARSER_SETTINGS = 12;
var FLAG_PICTURE_SETTINGS = 29
var FLAG_SCORE = 30;
var FLAG_TURNS_LOW = 31;
var FLAG_TURNS_HIGH = 32;
var FLAG_VERB = 33;
var FLAG_NOUN1 =34;
var FLAG_ADJECT1 = 35;
var FLAG_ADVERB = 36;
var FLAG_MAXOBJECTS_CARRIED = 37;
var FLAG_LOCATION = 38;
var FLAG_TOPLINE = 39;   // deprecated
var FLAG_MODE = 40;  // deprecated
var FLAG_PROTECT = 41;   // deprecated
var FLAG_PROMPT = 42; 
var FLAG_PREP = 43;
var FLAG_NOUN2 = 44;
var FLAG_ADJECT2 = 45;
var FLAG_PRONOUN = 46;
var FLAG_PRONOUN_ADJECT = 47;
var FLAG_TIMEOUT_LENGTH = 48;
var FLAG_TIMEOUT_SETTINGS = 49; 
var FLAG_DOALL_LOC = 50;
var FLAG_REFERRED_OBJECT = 51;
var FLAG_MAXWEIGHT_CARRIED = 52;
var FLAG_OBJECT_LIST_FORMAT = 53;
var FLAG_REFERRED_OBJECT_LOCATION = 54;
var FLAG_REFERRED_OBJECT_WEIGHT = 55;
var FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
var FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
var FLAG_EXPANSION1 = 58;
var FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var LOCATION_WORN = 253;
var LOCATION_CARRIED = 254;
var LOCATION_NONCREATED = 252;
var LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



var SYSMESS_ISDARK = 0;
var SYSMESS_YOUCANSEE = 1;
var SYSMESS_PROMPT0 = 2;
var SYSMESS_PROMPT1 = 3;
var SYSMESS_PROMPT2 = 4
var SYSMESS_PROMPT3= 5;
var SYSMESS_IDONTUNDERSTAND = 6;
var SYSMESS_WRONGDIRECTION = 7
var SYSMESS_CANTDOTHAT = 8;
var SYSMESS_YOUARECARRYING = 9;
var SYSMESS_WORN = 10;
var SYSMESS_CARRYING_NOTHING = 11;
var SYSMESS_AREYOUSURE = 12;
var SYSMESS_PLAYAGAIN = 13;
var SYSMESS_FAREWELL = 14;
var SYSMESS_OK = 15;
var SYSMESS_PRESSANYKEY = 16;
var SYSMESS_TURNS_START = 17;
var SYSMESS_TURNS_CONTINUE = 18;
var SYSMESS_TURNS_PLURAL = 19;
var SYSMESS_TURNS_END = 20;
var SYSMESS_SCORE_START= 21;
var SYSMESS_SCORE_END =22;
var SYSMESS_YOURENOTWEARINGTHAT = 23;
var SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
var SYSMESS_YOUALREADYHAVEOBJECT = 25;
var SYSMESS_CANTSEETHAT = 26;
var SYSMESS_CANTCARRYANYMORE = 27;
var SYSMESS_YOUDONTHAVETHAT = 28;
var SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
var SYSMESS_YES = 30;
var SYSMESS_NO = 31;
var SYSMESS_MORE = 32;
var SYSMESS_CARET = 33;
var SYSMESS_TIMEOUT=35;
var SYSMESS_YOUTAKEOBJECT = 36;
var SYSMESS_YOUWEAROBJECT = 37;
var SYSMESS_YOUREMOVEOBJECT = 38;
var SYSMESS_YOUDROPOBJECT = 39;
var SYSMESS_YOUCANTWEAROBJECT = 40;
var SYSMESS_YOUCANTREMOVEOBJECT = 41;
var SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
var SYSMESS_WEIGHSTOOMUCH = 43;
var SYSMESS_YOUPUTOBJECTIN = 44;
var SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
var SYSMESS_LISTSEPARATOR = 46;
var SYSMESS_LISTLASTSEPARATOR = 47;
var SYSMESS_LISTEND = 48;
var SYSMESS_YOUDONTHAVEOBJECT = 49;
var SYSMESS_YOUARENOTWEARINGOBJECT = 50;
var SYSMESS_PUTINTAKEOUTTERMINATION = 51;
var SYSMESS_THATISNOTIN = 52;
var SYSMESS_EMPTYOBJECTLIST = 53;
var SYSMESS_FILENOTFOUND = 54;
var SYSMESS_CORRUPTFILE = 55;
var SYSMESS_IOFAILURE = 56;
var SYSMESS_DIRECTORYFULL = 57;
var SYSMESS_LOADFILE = 58;
var SYSMESS_FILENOTFOUND = 59;
var SYSMESS_SAVEFILE = 60;
var SYSMESS_SORRY = 61;
var SYSMESS_NONSENSE_SENTENCE = 62;
var SYSMESS_NPCLISTSTART = 63;
var SYSMESS_NPCLISTCONTINUE = 64;
var SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
var SYSMESS_INSIDE_YOUCANSEE = 66;
var SYSMESS_OVER_YOUCANSEE = 67;
var SYSMESS_YOUPUTOBJECTON = 68;
var SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		ACChook(0);
		if (done_flag) break pro000_restart;
		{}

	}

	// _ _
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		ACChook(1);
		if (done_flag) break pro000_restart;
		{}

	}

	// N _
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0002;
 		}
		if (!CNDat(65)) break p000e0002;
		if (!CNDpresent(31)) break p000e0002;
 		ACCwriteln(2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0003;
 		}
		if (!CNDat(65)) break p000e0003;
		if (!CNDabsent(31)) break p000e0003;
 		ACCgoto(62);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0004;
 		}
		if (!CNDat(10)) break p000e0004;
		if (!CNDcarried(0)) break p000e0004;
		if (!CNDzero(0)) break p000e0004;
 		ACCgoto(9);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0005;
 		}
		if (!CNDat(11)) break p000e0005;
		if (!CNDcarried(0)) break p000e0005;
		if (!CNDzero(0)) break p000e0005;
 		ACCgoto(10);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0006;
 		}
		if (!CNDat(31)) break p000e0006;
 		ACCwriteln(3);
 		ACCdestroy(24);
 		ACCpause(250);
 		function anykey00000() 
		{
 		ACCgoto(58);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// N _
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0007;
 		}
		if (!CNDat(61)) break p000e0007;
 		ACCwriteln(4);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// N _
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0008;
 		}
		if (!CNDat(75)) break p000e0008;
		if (!CNDnotzero(7)) break p000e0008;
		if (!CNDcarried(42)) break p000e0008;
		if (!CNDeq(19,20)) break p000e0008;
 		ACCgoto(76);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0009;
 		}
		if (!CNDat(75)) break p000e0009;
 		ACCwriteln(5);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// N _
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0010;
 		}
		if (!CNDat(5)) break p000e0010;
 		ACClet(28,19);
 		ACCpause(10);
 		function anykey00001() 
		{
 		ACCgoto(4);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00001);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// S _
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0011;
 		}
		if (!CNDat(62)) break p000e0011;
		if (!CNDeq(115,30)) break p000e0011;
 		ACClet(7,2);
 		ACCgoto(65);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0012;
 		}
		if (!CNDat(62)) break p000e0012;
		if (!CNDgt(115,30)) break p000e0012;
 		ACCgoto(65);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0013;
 		}
		if (!CNDat(9)) break p000e0013;
		if (!CNDcarried(0)) break p000e0013;
		if (!CNDzero(0)) break p000e0013;
 		ACCgoto(10);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0014;
 		}
		if (!CNDat(10)) break p000e0014;
		if (!CNDcarried(0)) break p000e0014;
		if (!CNDzero(0)) break p000e0014;
 		ACCgoto(11);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0015;
 		}
		if (!CNDat(76)) break p000e0015;
		if (!CNDnotzero(7)) break p000e0015;
		if (!CNDcarried(42)) break p000e0015;
		if (!CNDeq(19,20)) break p000e0015;
 		ACCgoto(75);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0016;
 		}
		if (!CNDat(76)) break p000e0016;
 		ACCwriteln(6);
 		ACCwriteln(7);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// S _
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0017;
 		}
		if (!CNDat(3)) break p000e0017;
 		ACClet(28,19);
 		ACCpause(10);
 		function anykey00002() 
		{
 		ACCgoto(4);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// E _
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0018;
 		}
		if (!CNDat(11)) break p000e0018;
		if (!CNDcarried(0)) break p000e0018;
		if (!CNDzero(0)) break p000e0018;
 		ACCwriteln(8);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// E _
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0019;
 		}
		if (!CNDat(72)) break p000e0019;
		if (!CNDlt(111,40)) break p000e0019;
 		ACCwriteln(9);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// E _
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0020;
 		}
		if (!CNDat(72)) break p000e0020;
		if (!CNDeq(111,40)) break p000e0020;
 		ACCgoto(63);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0021;
 		}
		if (!CNDat(14)) break p000e0021;
		if (!CNDcarried(0)) break p000e0021;
		if (!CNDzero(0)) break p000e0021;
 		ACCgoto(9);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0022;
 		}
		if (!CNDat(54)) break p000e0022;
		if (!CNDpresent(37)) break p000e0022;
 		ACCwriteln(10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// E _
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0023;
 		}
		if (!CNDat(54)) break p000e0023;
		if (!CNDabsent(37)) break p000e0023;
 		ACCgoto(53);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0024;
 		}
		if (!CNDat(63)) break p000e0024;
		if (!CNDlt(111,40)) break p000e0024;
 		ACCwriteln(11);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// W _
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0025;
 		}
		if (!CNDat(63)) break p000e0025;
		if (!CNDeq(111,40)) break p000e0025;
 		ACCgoto(72);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0026;
 		}
		if (!CNDat(9)) break p000e0026;
		if (!CNDcarried(0)) break p000e0026;
		if (!CNDzero(0)) break p000e0026;
 		ACCgoto(14);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO D
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0027;
			if (!CNDnoun1(10)) break p000e0027;
 		}
		if (!CNDat(71)) break p000e0027;
 		ACCgoto(24);
 		ACCcreate(24);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO ROPE
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0028;
			if (!CNDnoun1(18)) break p000e0028;
 		}
		if (!CNDnotzero(5)) break p000e0028;
		if (!CNDpresent(7)) break p000e0028;
		if (!CNDnotat(37)) break p000e0028;
 		ACCwriteln(12);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACRO ROPE
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0029;
			if (!CNDnoun1(18)) break p000e0029;
 		}
		if (!CNDeq(114,10)) break p000e0029;
		if (!CNDnotzero(5)) break p000e0029;
		if (!CNDzero(113)) break p000e0029;
		if (!CNDpresent(7)) break p000e0029;
		if (!CNDat(37)) break p000e0029;
 		ACCwriteln(13);
 		ACClet(6,4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACRO ROPE
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0030;
			if (!CNDnoun1(18)) break p000e0030;
 		}
		if (!CNDzero(5)) break p000e0030;
		if (!CNDpresent(7)) break p000e0030;
 		ACCwriteln(14);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACRO ROPE
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0031;
			if (!CNDnoun1(18)) break p000e0031;
 		}
		if (!CNDat(37)) break p000e0031;
		if (!CNDzero(113)) break p000e0031;
		if (!CNDzero(114)) break p000e0031;
		if (!CNDpresent(7)) break p000e0031;
		if (!CNDnotzero(5)) break p000e0031;
 		ACCwriteln(15);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACRO ROPE
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0032;
			if (!CNDnoun1(18)) break p000e0032;
 		}
		if (!CNDat(37)) break p000e0032;
		if (!CNDeq(113,10)) break p000e0032;
		if (!CNDpresent(7)) break p000e0032;
		if (!CNDnotzero(5)) break p000e0032;
 		ACCwriteln(16);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0033;
 		}
		if (!CNDat(24)) break p000e0033;
		if (!CNDpresent(24)) break p000e0033;
		if (!CNDeq(18,30)) break p000e0033;
 		ACCgoto(71);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0034;
 		}
		if (!CNDat(64)) break p000e0034;
		if (!CNDnotworn(28)) break p000e0034;
 		ACCwriteln(17);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0035;
 		}
		if (!CNDat(64)) break p000e0035;
		if (!CNDworn(28)) break p000e0035;
 		ACCgoto(17);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0036;
 		}
		if (!CNDat(15)) break p000e0036;
		if (!CNDzero(0)) break p000e0036;
 		ACCgoto(9);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0037;
 		}
		if (!CNDat(16)) break p000e0037;
		if (!CNDabsent(0)) break p000e0037;
 		ACCset(0);
 		ACCgoto(15);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0038;
 		}
		if (!CNDat(16)) break p000e0038;
		if (!CNDpresent(0)) break p000e0038;
 		ACCclear(0);
 		ACCgoto(15);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0039;
 		}
		if (!CNDat(49)) break p000e0039;
		if (!CNDeq(22,10)) break p000e0039;
 		ACCwriteln(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0040;
 		}
		if (!CNDat(49)) break p000e0040;
		if (!CNDgt(22,15)) break p000e0040;
 		ACCgoto(50);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ACRO _
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0041;
 		}
		if (!CNDat(49)) break p000e0041;
		if (!CNDeq(22,15)) break p000e0041;
 		ACCwriteln(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC BUCK
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0042;
			if (!CNDnoun1(13)) break p000e0042;
 		}
		if (!CNDat(4)) break p000e0042;
		if (!CNDzero(111)) break p000e0042;
 		ACCcreate(6);
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCwriteln(20);
 		ACClet(111,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC BUCK
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0043;
			if (!CNDnoun1(13)) break p000e0043;
 		}
		if (!CNDat(4)) break p000e0043;
		if (!CNDnotzero(111)) break p000e0043;
 		ACCwriteln(21);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC HAND
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0044;
			if (!CNDnoun1(14)) break p000e0044;
 		}
		if (!CNDat(4)) break p000e0044;
		if (!CNDzero(111)) break p000e0044;
 		ACCcreate(6);
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCcreate(7);
 		ACCwriteln(22);
 		ACClet(111,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC HAND
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0045;
			if (!CNDnoun1(14)) break p000e0045;
 		}
		if (!CNDat(4)) break p000e0045;
		if (!CNDnotzero(111)) break p000e0045;
 		ACCwriteln(23);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC HAND
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0046;
			if (!CNDnoun1(14)) break p000e0046;
 		}
		if (!CNDat(70)) break p000e0046;
		if (!CNDeq(111,20)) break p000e0046;
 		ACCcreate(8);
 		ACClet(111,30);
 		ACCwriteln(24);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC HAND
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0047;
			if (!CNDnoun1(14)) break p000e0047;
 		}
		if (!CNDat(70)) break p000e0047;
		if (!CNDlt(111,30)) break p000e0047;
 		ACCwriteln(25);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0048;
 		}
		if (!CNDat(70)) break p000e0048;
		if (!CNDpresent(8)) break p000e0048;
 		ACCgoto(21);
 		ACCdesc();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0049;
 		}
		if (!CNDat(70)) break p000e0049;
		if (!CNDabsent(8)) break p000e0049;
 		ACCwriteln(26);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0050;
 		}
		if (!CNDat(71)) break p000e0050;
 		ACCgoto(24);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0051;
 		}
		if (!CNDat(17)) break p000e0051;
		if (!CNDnotworn(28)) break p000e0051;
 		ACCwriteln(27);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0052;
 		}
		if (!CNDat(17)) break p000e0052;
		if (!CNDworn(28)) break p000e0052;
 		ACCgoto(64);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0053;
 		}
		if (!CNDat(15)) break p000e0053;
 		ACCgoto(16);
 		ACCclear(0);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0054;
 		}
		if (!CNDat(9)) break p000e0054;
		if (!CNDcarried(0)) break p000e0054;
		if (!CNDzero(0)) break p000e0054;
 		ACCgoto(15);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0055;
 		}
		if (!CNDat(77)) break p000e0055;
		if (!CNDcarried(10)) break p000e0055;
		if (!CNDeq(116,20)) break p000e0055;
 		ACCdestroy(10);
 		ACClet(116,10);
 		ACClet(113,10);
 		ACClet(114,15);
 		ACCwriteln(28);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0056;
 		}
		if (!CNDat(77)) break p000e0056;
		if (!CNDnotcarr(10)) break p000e0056;
 		ACCgoto(76);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0057;
 		}
		if (!CNDat(77)) break p000e0057;
		if (!CNDeq(116,30)) break p000e0057;
 		ACCgoto(76);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0058;
 		}
		if (!CNDat(54)) break p000e0058;
		if (!CNDpresent(49)) break p000e0058;
 		ACCgoto(73);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0059;
 		}
		if (!CNDat(4)) break p000e0059;
		if (!CNDnotworn(28)) break p000e0059;
 		ACCwriteln(29);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0060;
 		}
		if (!CNDat(4)) break p000e0060;
		if (!CNDworn(28)) break p000e0060;
		if (!CNDgt(111,9)) break p000e0060;
		if (!CNDzero(29)) break p000e0060;
 		ACClet(29,10);
 		ACCgoto(69);
 		ACCcreate(58);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0061;
 		}
		if (!CNDat(4)) break p000e0061;
		if (!CNDworn(28)) break p000e0061;
 		ACCgoto(69);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// CHEC AIR
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0062;
			if (!CNDnoun1(11)) break p000e0062;
 		}
 		ACCplus(31,1);
 		ACCturns();
		{}

	}

	// CHAN AIR
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0063;
			if (!CNDnoun1(11)) break p000e0063;
 		}
		if (!CNDcarried(1)) break p000e0063;
		if (!CNDgt(31,140)) break p000e0063;
 		ACCswap(1,2);
 		ACCwriteln(30);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0064;
			if (!CNDnoun1(11)) break p000e0064;
 		}
		if (!CNDcarried(2)) break p000e0064;
		if (!CNDgt(31,140)) break p000e0064;
 		ACCswap(2,3);
 		ACCwriteln(31);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0065;
			if (!CNDnoun1(11)) break p000e0065;
 		}
		if (!CNDcarried(3)) break p000e0065;
		if (!CNDgt(31,140)) break p000e0065;
 		ACCswap(3,4);
 		ACCwriteln(32);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0066;
			if (!CNDnoun1(11)) break p000e0066;
 		}
		if (!CNDcarried(4)) break p000e0066;
 		ACCdestroy(4);
 		ACCwriteln(33);
 		ACCwriteln(34);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0067;
			if (!CNDnoun1(11)) break p000e0067;
 		}
		if (!CNDabsent(1)) break p000e0067;
		if (!CNDabsent(2)) break p000e0067;
		if (!CNDabsent(3)) break p000e0067;
		if (!CNDabsent(4)) break p000e0067;
 		ACCwriteln(35);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0068;
			if (!CNDnoun1(11)) break p000e0068;
 		}
		if (!CNDcarried(1)) break p000e0068;
		if (!CNDlt(31,140)) break p000e0068;
 		ACCwriteln(36);
 		ACCswap(1,2);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0069;
			if (!CNDnoun1(11)) break p000e0069;
 		}
		if (!CNDcarried(2)) break p000e0069;
		if (!CNDlt(31,140)) break p000e0069;
 		ACCswap(2,3);
 		ACCwriteln(37);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAN AIR
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0070;
			if (!CNDnoun1(11)) break p000e0070;
 		}
		if (!CNDcarried(3)) break p000e0070;
		if (!CNDlt(31,140)) break p000e0070;
 		ACCswap(3,4);
 		ACCwriteln(38);
 		ACCclear(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUCK
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0071;
			if (!CNDnoun1(13)) break p000e0071;
 		}
		if (!CNDat(4)) break p000e0071;
		if (!CNDzero(111)) break p000e0071;
		if (!CNDeq(27,200)) break p000e0071;
 		ACCwriteln(39);
 		ACCcreate(44);
 		ACCplus(30,2);
 		ACCclear(27);
 		ACClet(111,5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUCK
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0072;
			if (!CNDnoun1(13)) break p000e0072;
 		}
		if (!CNDat(4)) break p000e0072;
		if (!CNDnotzero(111)) break p000e0072;
		if (!CNDlt(111,10)) break p000e0072;
 		ACCwriteln(40);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUCK
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0073;
			if (!CNDnoun1(13)) break p000e0073;
 		}
		if (!CNDat(4)) break p000e0073;
		if (!CNDgt(111,9)) break p000e0073;
 		ACCwriteln(41);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUCK
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0074;
			if (!CNDnoun1(13)) break p000e0074;
 		}
		if (!CNDpresent(58)) break p000e0074;
		if (!CNDeq(27,200)) break p000e0074;
 		ACCclear(27);
 		ACCcreate(44);
 		ACCwriteln(42);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUCK
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0075;
			if (!CNDnoun1(13)) break p000e0075;
 		}
		if (!CNDpresent(58)) break p000e0075;
		if (!CNDzero(27)) break p000e0075;
 		ACCwriteln(43);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HAND
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0076;
			if (!CNDnoun1(14)) break p000e0076;
 		}
		if (!CNDcarried(6)) break p000e0076;
		if (!CNDnotat(73)) break p000e0076;
 		ACCwriteln(44);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HAND
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0077;
			if (!CNDnoun1(14)) break p000e0077;
 		}
		if (!CNDnotcarr(6)) break p000e0077;
		if (!CNDnotat(73)) break p000e0077;
 		ACCwriteln(45);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WELL
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0078;
			if (!CNDnoun1(16)) break p000e0078;
 		}
		if (!CNDat(4)) break p000e0078;
		if (!CNDzero(111)) break p000e0078;
 		ACCwriteln(46);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WELL
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0079;
			if (!CNDnoun1(16)) break p000e0079;
 		}
		if (!CNDat(4)) break p000e0079;
		if (!CNDnotzero(111)) break p000e0079;
 		ACCwriteln(47);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ROPE
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0080;
			if (!CNDnoun1(18)) break p000e0080;
 		}
		if (!CNDcarried(7)) break p000e0080;
 		ACCwriteln(48);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOUL
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0081;
			if (!CNDnoun1(19)) break p000e0081;
 		}
		if (!CNDat(70)) break p000e0081;
		if (!CNDabsent(8)) break p000e0081;
 		ACCwriteln(49);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOUL
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0082;
			if (!CNDnoun1(19)) break p000e0082;
 		}
		if (!CNDat(54)) break p000e0082;
		if (!CNDpresent(37)) break p000e0082;
 		ACCwriteln(50);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOUL
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0083;
			if (!CNDnoun1(19)) break p000e0083;
 		}
		if (!CNDat(49)) break p000e0083;
 		ACCwriteln(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOUL
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0084;
			if (!CNDnoun1(19)) break p000e0084;
 		}
		if (!CNDat(70)) break p000e0084;
		if (!CNDpresent(8)) break p000e0084;
 		ACCwriteln(52);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NOTC
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0085;
			if (!CNDnoun1(20)) break p000e0085;
 		}
		if (!CNDat(70)) break p000e0085;
 		ACCwriteln(53);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NOTC
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0086;
			if (!CNDnoun1(20)) break p000e0086;
 		}
		if (!CNDat(63)) break p000e0086;
 		ACCwriteln(54);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CRYS
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0087;
			if (!CNDnoun1(23)) break p000e0087;
 		}
		if (!CNDcarried(5)) break p000e0087;
 		ACCwriteln(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CRYS
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0088;
			if (!CNDnoun1(23)) break p000e0088;
 		}
		if (!CNDnotcarr(5)) break p000e0088;
 		ACCwriteln(56);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CONT
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0089;
			if (!CNDnoun1(24)) break p000e0089;
 		}
		if (!CNDcarried(5)) break p000e0089;
		if (!CNDnotat(57)) break p000e0089;
 		ACCwriteln(57);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CONT
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0090;
			if (!CNDnoun1(24)) break p000e0090;
 		}
		if (!CNDnotcarr(5)) break p000e0090;
		if (!CNDnotat(57)) break p000e0090;
 		ACCwriteln(58);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CONT
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0091;
			if (!CNDnoun1(24)) break p000e0091;
 		}
		if (!CNDat(57)) break p000e0091;
 		ACCwriteln(59);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM MUSI
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0092;
			if (!CNDnoun1(33)) break p000e0092;
 		}
		if (!CNDcarried(13)) break p000e0092;
 		ACCwriteln(60);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM KEY
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0093;
			if (!CNDnoun1(35)) break p000e0093;
 		}
		if (!CNDcarried(15)) break p000e0093;
 		ACCwriteln(61);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CHES
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0094;
			if (!CNDnoun1(36)) break p000e0094;
 		}
		if (!CNDat(21)) break p000e0094;
		if (!CNDeq(112,6)) break p000e0094;
 		ACCwriteln(62);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CHES
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0095;
			if (!CNDnoun1(36)) break p000e0095;
 		}
		if (!CNDat(21)) break p000e0095;
		if (!CNDgt(112,6)) break p000e0095;
 		ACCwriteln(63);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CHES
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0096;
			if (!CNDnoun1(36)) break p000e0096;
 		}
		if (!CNDat(21)) break p000e0096;
		if (!CNDlt(112,6)) break p000e0096;
 		ACCwriteln(64);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PRIN
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0097;
			if (!CNDnoun1(37)) break p000e0097;
 		}
		if (!CNDat(3)) break p000e0097;
		if (!CNDeq(112,20)) break p000e0097;
 		ACCwriteln(65);
 		ACCcreate(15);
 		ACCplus(30,2);
 		ACClet(112,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PRIN
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0098;
			if (!CNDnoun1(37)) break p000e0098;
 		}
		if (!CNDat(3)) break p000e0098;
		if (!CNDlt(112,20)) break p000e0098;
 		ACCwriteln(66);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PRIN
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0099;
			if (!CNDnoun1(37)) break p000e0099;
 		}
		if (!CNDat(24)) break p000e0099;
 		ACCwriteln(67);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BASK
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0100;
			if (!CNDnoun1(41)) break p000e0100;
 		}
		if (!CNDcarried(17)) break p000e0100;
 		ACCwriteln(68);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BAIT
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0101;
			if (!CNDnoun1(42)) break p000e0101;
 		}
		if (!CNDpresent(18)) break p000e0101;
 		ACCwriteln(69);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BAIT
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0102;
			if (!CNDnoun1(42)) break p000e0102;
 		}
		if (!CNDat(1)) break p000e0102;
		if (!CNDeq(115,200)) break p000e0102;
 		ACCwriteln(70);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BAIT
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0103;
			if (!CNDnoun1(42)) break p000e0103;
 		}
		if (!CNDat(1)) break p000e0103;
		if (!CNDlt(115,200)) break p000e0103;
 		ACCwriteln(71);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM JUNK
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0104;
			if (!CNDnoun1(47)) break p000e0104;
 		}
		if (!CNDat(19)) break p000e0104;
		if (!CNDzero(116)) break p000e0104;
 		ACCwriteln(72);
 		ACCcreate(34);
 		ACCplus(30,2);
 		ACClet(116,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM JUNK
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0105;
			if (!CNDnoun1(47)) break p000e0105;
 		}
		if (!CNDat(19)) break p000e0105;
		if (!CNDnotzero(116)) break p000e0105;
 		ACCwriteln(73);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOOK
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0106;
			if (!CNDnoun1(48)) break p000e0106;
 		}
		if (!CNDcarried(21)) break p000e0106;
 		ACCwriteln(74);
 		ACCwriteln(75);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CABL
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0107;
			if (!CNDnoun1(49)) break p000e0107;
 		}
		if (!CNDcarried(23)) break p000e0107;
 		ACCwriteln(76);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GODD
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0108;
			if (!CNDnoun1(53)) break p000e0108;
 		}
		if (!CNDat(14)) break p000e0108;
		if (!CNDzero(17)) break p000e0108;
 		ACCwriteln(77);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GODD
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0109;
			if (!CNDnoun1(53)) break p000e0109;
 		}
		if (!CNDat(14)) break p000e0109;
		if (!CNDnotzero(17)) break p000e0109;
 		ACCwriteln(78);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ALTA
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0110;
			if (!CNDnoun1(54)) break p000e0110;
 		}
		if (!CNDat(14)) break p000e0110;
 		ACCwriteln(79);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOAT
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0111;
			if (!CNDnoun1(56)) break p000e0111;
 		}
		if (!CNDpresent(24)) break p000e0111;
		if (!CNDeq(18,10)) break p000e0111;
 		ACCwriteln(80);
 		ACCwriteln(81);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOAT
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0112;
			if (!CNDnoun1(56)) break p000e0112;
 		}
		if (!CNDpresent(24)) break p000e0112;
		if (!CNDgt(18,10)) break p000e0112;
 		ACCwriteln(82);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUSH
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0113;
			if (!CNDnoun1(58)) break p000e0113;
 		}
		if (!CNDat(24)) break p000e0113;
		if (!CNDzero(18)) break p000e0113;
 		ACCcreate(24);
 		ACClet(18,10);
 		ACCplus(30,2);
 		ACCwriteln(83);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUSH
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0114;
			if (!CNDnoun1(58)) break p000e0114;
 		}
		if (!CNDat(24)) break p000e0114;
		if (!CNDnotzero(18)) break p000e0114;
 		ACCwriteln(84);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BUSH
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0115;
			if (!CNDnoun1(58)) break p000e0115;
 		}
		if (!CNDat(39)) break p000e0115;
 		ACCwriteln(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TIN
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0116;
			if (!CNDnoun1(60)) break p000e0116;
 		}
		if (!CNDcarried(25)) break p000e0116;
 		ACCwriteln(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TIN
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0117;
			if (!CNDnoun1(60)) break p000e0117;
 		}
		if (!CNDcarried(50)) break p000e0117;
 		ACCwriteln(87);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TIN
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0118;
			if (!CNDnoun1(60)) break p000e0118;
 		}
		if (!CNDcarried(26)) break p000e0118;
 		ACCwriteln(88);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ASHE
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0119;
			if (!CNDnoun1(63)) break p000e0119;
 		}
		if (!CNDat(25)) break p000e0119;
		if (!CNDeq(19,210)) break p000e0119;
 		ACCcreate(21);
 		ACClet(19,10);
 		ACCplus(30,2);
 		ACCwriteln(89);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ASHE
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0120;
			if (!CNDnoun1(63)) break p000e0120;
 		}
		if (!CNDat(25)) break p000e0120;
		if (!CNDlt(19,210)) break p000e0120;
 		ACCwriteln(90);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ASHE
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0121;
			if (!CNDnoun1(63)) break p000e0121;
 		}
		if (!CNDcarried(27)) break p000e0121;
 		ACCwriteln(91);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ASHE
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0122;
			if (!CNDnoun1(63)) break p000e0122;
 		}
		if (!CNDcarried(0)) break p000e0122;
 		ACCwriteln(92);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM KEEP
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0123;
			if (!CNDnoun1(64)) break p000e0123;
 		}
		if (!CNDat(79)) break p000e0123;
		if (!CNDeq(112,1)) break p000e0123;
		if (!CNDeq(113,10)) break p000e0123;
		if (!CNDeq(17,30)) break p000e0123;
		if (!CNDlt(114,20)) break p000e0123;
 		ACClet(114,15);
 		ACCwriteln(93);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM KEEP
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0124;
			if (!CNDnoun1(64)) break p000e0124;
 		}
		if (!CNDat(79)) break p000e0124;
		if (!CNDlt(17,30)) break p000e0124;
 		ACCwriteln(94);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0125;
			if (!CNDnoun1(65)) break p000e0125;
 		}
		if (!CNDat(36)) break p000e0125;
		if (!CNDzero(25)) break p000e0125;
 		ACCcreate(52);
 		ACCplus(30,2);
 		ACClet(25,10);
 		ACCwriteln(95);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0126;
			if (!CNDnoun1(65)) break p000e0126;
 		}
		if (!CNDat(78)) break p000e0126;
		if (!CNDzero(23)) break p000e0126;
 		ACCwriteln(96);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0127;
			if (!CNDnoun1(65)) break p000e0127;
 		}
		if (!CNDat(78)) break p000e0127;
		if (!CNDnotzero(23)) break p000e0127;
 		ACCwriteln(97);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0128;
			if (!CNDnoun1(65)) break p000e0128;
 		}
		if (!CNDat(36)) break p000e0128;
		if (!CNDnotzero(25)) break p000e0128;
 		ACCwriteln(98);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GHOS
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0129;
			if (!CNDnoun1(66)) break p000e0129;
 		}
		if (!CNDat(50)) break p000e0129;
		if (!CNDpresent(46)) break p000e0129;
 		ACCwriteln(99);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TOYS
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0130;
			if (!CNDnoun1(70)) break p000e0130;
 		}
		if (!CNDat(66)) break p000e0130;
		if (!CNDzero(20)) break p000e0130;
 		ACCcreate(13);
 		ACClet(20,10);
 		ACCplus(30,2);
 		ACCwriteln(100);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TOYS
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0131;
			if (!CNDnoun1(70)) break p000e0131;
 		}
		if (!CNDat(66)) break p000e0131;
		if (!CNDnotzero(20)) break p000e0131;
 		ACCwriteln(101);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CRAN
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0132;
			if (!CNDnoun1(73)) break p000e0132;
 		}
		if (!CNDat(63)) break p000e0132;
 		ACCwriteln(102);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM POLE
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0133;
			if (!CNDnoun1(74)) break p000e0133;
 		}
		if (!CNDcarried(34)) break p000e0133;
 		ACCwriteln(103);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM RAVI
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0134;
			if (!CNDnoun1(75)) break p000e0134;
 		}
		if (!CNDat(29)) break p000e0134;
 		ACCwriteln(104);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM RAVI
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0135;
			if (!CNDnoun1(75)) break p000e0135;
 		}
		if (!CNDat(32)) break p000e0135;
 		ACCwriteln(105);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CARP
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0136;
			if (!CNDnoun1(76)) break p000e0136;
 		}
		if (!CNDat(28)) break p000e0136;
		if (!CNDeq(17,200)) break p000e0136;
 		ACCcreate(27);
 		ACCclear(17);
 		ACCplus(30,2);
 		ACCwriteln(106);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CARP
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0137;
			if (!CNDnoun1(76)) break p000e0137;
 		}
		if (!CNDat(28)) break p000e0137;
		if (!CNDlt(17,200)) break p000e0137;
 		ACCwriteln(107);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BENC
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0138;
			if (!CNDnoun1(80)) break p000e0138;
 		}
		if (!CNDat(50)) break p000e0138;
		if (!CNDeq(22,20)) break p000e0138;
 		ACCwriteln(108);
 		ACClet(22,30);
 		ACCplus(30,2);
 		ACCcreate(28);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BENC
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0139;
			if (!CNDnoun1(80)) break p000e0139;
 		}
		if (!CNDat(50)) break p000e0139;
		if (!CNDgt(22,20)) break p000e0139;
 		ACCwriteln(109);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BENC
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0140;
			if (!CNDnoun1(80)) break p000e0140;
 		}
		if (!CNDat(77)) break p000e0140;
		if (!CNDgt(115,40)) break p000e0140;
 		ACCwriteln(110);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BENC
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0141;
			if (!CNDnoun1(80)) break p000e0141;
 		}
		if (!CNDat(77)) break p000e0141;
		if (!CNDeq(115,40)) break p000e0141;
 		ACCwriteln(111);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BED
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0142;
			if (!CNDnoun1(81)) break p000e0142;
 		}
		if (!CNDat(38)) break p000e0142;
 		ACCwriteln(112);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BED
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0143;
			if (!CNDnoun1(81)) break p000e0143;
 		}
		if (!CNDat(37)) break p000e0143;
 		ACCwriteln(113);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BED
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0144;
			if (!CNDnoun1(81)) break p000e0144;
 		}
		if (!CNDat(78)) break p000e0144;
 		ACCwriteln(114);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WALL
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0145;
			if (!CNDnoun1(82)) break p000e0145;
 		}
		if (!CNDat(38)) break p000e0145;
 		ACCwriteln(115);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WITC
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0146;
			if (!CNDnoun1(83)) break p000e0146;
 		}
		if (!CNDpresent(35)) break p000e0146;
 		ACCwriteln(116);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LABE
	p000e0147:
	{
 		if (skipdoall('p000e0147')) break p000e0147;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0147;
			if (!CNDnoun1(84)) break p000e0147;
 		}
		if (!CNDpresent(35)) break p000e0147;
 		ACCwriteln(117);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CABI
	p000e0148:
	{
 		if (skipdoall('p000e0148')) break p000e0148;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0148;
			if (!CNDnoun1(89)) break p000e0148;
 		}
		if (!CNDat(79)) break p000e0148;
 		ACCwriteln(118);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BALL
	p000e0149:
	{
 		if (skipdoall('p000e0149')) break p000e0149;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0149;
			if (!CNDnoun1(90)) break p000e0149;
 		}
		if (!CNDcarried(42)) break p000e0149;
		if (!CNDeq(19,10)) break p000e0149;
 		ACCwriteln(119);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BALL
	p000e0150:
	{
 		if (skipdoall('p000e0150')) break p000e0150;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0150;
			if (!CNDnoun1(90)) break p000e0150;
 		}
		if (!CNDcarried(42)) break p000e0150;
		if (!CNDeq(19,20)) break p000e0150;
 		ACCwriteln(120);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LEDG
	p000e0151:
	{
 		if (skipdoall('p000e0151')) break p000e0151;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0151;
			if (!CNDnoun1(91)) break p000e0151;
 		}
		if (!CNDat(22)) break p000e0151;
 		ACCwriteln(121);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NEST
	p000e0152:
	{
 		if (skipdoall('p000e0152')) break p000e0152;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0152;
			if (!CNDnoun1(92)) break p000e0152;
 		}
		if (!CNDat(22)) break p000e0152;
 		ACCwriteln(122);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM EGG
	p000e0153:
	{
 		if (skipdoall('p000e0153')) break p000e0153;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0153;
			if (!CNDnoun1(93)) break p000e0153;
 		}
		if (!CNDat(22)) break p000e0153;
		if (!CNDlt(17,30)) break p000e0153;
 		ACCwriteln(123);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM EGG
	p000e0154:
	{
 		if (skipdoall('p000e0154')) break p000e0154;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0154;
			if (!CNDnoun1(93)) break p000e0154;
 		}
		if (!CNDat(22)) break p000e0154;
		if (!CNDeq(112,2)) break p000e0154;
		if (!CNDlt(114,30)) break p000e0154;
		if (!CNDeq(17,30)) break p000e0154;
 		ACClet(114,25);
 		ACCwriteln(124);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM EGG
	p000e0155:
	{
 		if (skipdoall('p000e0155')) break p000e0155;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0155;
			if (!CNDnoun1(93)) break p000e0155;
 		}
		if (!CNDat(22)) break p000e0155;
		if (!CNDeq(17,30)) break p000e0155;
		if (!CNDlt(113,30)) break p000e0155;
		if (!CNDlt(112,2)) break p000e0155;
 		ACCwriteln(125);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM EGG
	p000e0156:
	{
 		if (skipdoall('p000e0156')) break p000e0156;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0156;
			if (!CNDnoun1(93)) break p000e0156;
 		}
		if (!CNDat(22)) break p000e0156;
		if (!CNDeq(17,30)) break p000e0156;
		if (!CNDlt(113,30)) break p000e0156;
		if (!CNDgt(112,2)) break p000e0156;
 		ACCwriteln(126);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM EGG
	p000e0157:
	{
 		if (skipdoall('p000e0157')) break p000e0157;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0157;
			if (!CNDnoun1(93)) break p000e0157;
 		}
		if (!CNDat(22)) break p000e0157;
		if (!CNDabsent(11)) break p000e0157;
 		ACCwriteln(127);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PAPE
	p000e0158:
	{
 		if (skipdoall('p000e0158')) break p000e0158;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0158;
			if (!CNDnoun1(94)) break p000e0158;
 		}
		if (!CNDcarried(43)) break p000e0158;
 		ACCwriteln(128);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CIGA
	p000e0159:
	{
 		if (skipdoall('p000e0159')) break p000e0159;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0159;
			if (!CNDnoun1(95)) break p000e0159;
 		}
		if (!CNDcarried(44)) break p000e0159;
 		ACCwriteln(129);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PUDD
	p000e0160:
	{
 		if (skipdoall('p000e0160')) break p000e0160;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0160;
			if (!CNDnoun1(97)) break p000e0160;
 		}
		if (!CNDat(54)) break p000e0160;
		if (!CNDpresent(47)) break p000e0160;
 		ACCwriteln(130);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CHAI
	p000e0161:
	{
 		if (skipdoall('p000e0161')) break p000e0161;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0161;
			if (!CNDnoun1(98)) break p000e0161;
 		}
		if (!CNDcarried(45)) break p000e0161;
 		ACCwriteln(131);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HOLE
	p000e0162:
	{
 		if (skipdoall('p000e0162')) break p000e0162;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0162;
			if (!CNDnoun1(99)) break p000e0162;
 		}
		if (!CNDat(16)) break p000e0162;
 		ACCwriteln(132);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HOLE
	p000e0163:
	{
 		if (skipdoall('p000e0163')) break p000e0163;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0163;
			if (!CNDnoun1(99)) break p000e0163;
 		}
		if (!CNDat(57)) break p000e0163;
		if (!CNDeq(23,30)) break p000e0163;
 		ACCwriteln(133);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HOLE
	p000e0164:
	{
 		if (skipdoall('p000e0164')) break p000e0164;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0164;
			if (!CNDnoun1(99)) break p000e0164;
 		}
		if (!CNDat(57)) break p000e0164;
		if (!CNDeq(23,40)) break p000e0164;
		if (!CNDzero(24)) break p000e0164;
 		ACCwriteln(134);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HOLE
	p000e0165:
	{
 		if (skipdoall('p000e0165')) break p000e0165;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0165;
			if (!CNDnoun1(99)) break p000e0165;
 		}
		if (!CNDat(57)) break p000e0165;
		if (!CNDeq(23,40)) break p000e0165;
		if (!CNDeq(24,10)) break p000e0165;
 		ACCwriteln(135);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM INSC
	p000e0166:
	{
 		if (skipdoall('p000e0166')) break p000e0166;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0166;
			if (!CNDnoun1(113)) break p000e0166;
 		}
		if (!CNDat(14)) break p000e0166;
 		ACCwriteln(136);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM INSC
	p000e0167:
	{
 		if (skipdoall('p000e0167')) break p000e0167;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0167;
			if (!CNDnoun1(113)) break p000e0167;
 		}
		if (!CNDat(57)) break p000e0167;
		if (!CNDlt(23,30)) break p000e0167;
 		ACCwriteln(137);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM INSC
	p000e0168:
	{
 		if (skipdoall('p000e0168')) break p000e0168;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0168;
			if (!CNDnoun1(113)) break p000e0168;
 		}
		if (!CNDpresent(54)) break p000e0168;
 		ACCwriteln(138);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CLOC
	p000e0169:
	{
 		if (skipdoall('p000e0169')) break p000e0169;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0169;
			if (!CNDnoun1(114)) break p000e0169;
 		}
		if (!CNDat(57)) break p000e0169;
		if (!CNDlt(23,30)) break p000e0169;
 		ACCwriteln(139);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CLOC
	p000e0170:
	{
 		if (skipdoall('p000e0170')) break p000e0170;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0170;
			if (!CNDnoun1(114)) break p000e0170;
 		}
		if (!CNDat(57)) break p000e0170;
		if (!CNDgt(23,29)) break p000e0170;
 		ACCwriteln(140);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FACE
	p000e0171:
	{
 		if (skipdoall('p000e0171')) break p000e0171;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0171;
			if (!CNDnoun1(115)) break p000e0171;
 		}
		if (!CNDat(57)) break p000e0171;
		if (!CNDlt(23,30)) break p000e0171;
 		ACCwriteln(141);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FACE
	p000e0172:
	{
 		if (skipdoall('p000e0172')) break p000e0172;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0172;
			if (!CNDnoun1(115)) break p000e0172;
 		}
		if (!CNDpresent(54)) break p000e0172;
 		ACCwriteln(142);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM JEWE
	p000e0173:
	{
 		if (skipdoall('p000e0173')) break p000e0173;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0173;
			if (!CNDnoun1(116)) break p000e0173;
 		}
		if (!CNDat(57)) break p000e0173;
		if (!CNDeq(23,40)) break p000e0173;
		if (!CNDeq(24,10)) break p000e0173;
		if (!CNDlt(112,3)) break p000e0173;
 		ACCwriteln(143);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM JEWE
	p000e0174:
	{
 		if (skipdoall('p000e0174')) break p000e0174;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0174;
			if (!CNDnoun1(116)) break p000e0174;
 		}
		if (!CNDat(57)) break p000e0174;
		if (!CNDeq(23,40)) break p000e0174;
		if (!CNDeq(24,10)) break p000e0174;
		if (!CNDeq(112,3)) break p000e0174;
		if (!CNDeq(113,30)) break p000e0174;
		if (!CNDeq(17,30)) break p000e0174;
		if (!CNDlt(114,40)) break p000e0174;
 		ACCwriteln(144);
 		ACClet(114,35);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM JEWE
	p000e0175:
	{
 		if (skipdoall('p000e0175')) break p000e0175;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0175;
			if (!CNDnoun1(116)) break p000e0175;
 		}
		if (!CNDat(57)) break p000e0175;
		if (!CNDeq(24,10)) break p000e0175;
		if (!CNDeq(23,40)) break p000e0175;
		if (!CNDeq(112,3)) break p000e0175;
		if (!CNDlt(17,30)) break p000e0175;
 		ACCwriteln(145);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0176:
	{
 		if (skipdoall('p000e0176')) break p000e0176;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0176;
			if (!CNDnoun1(117)) break p000e0176;
 		}
		if (!CNDcarried(51)) break p000e0176;
 		ACCwriteln(146);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM MAGN
	p000e0177:
	{
 		if (skipdoall('p000e0177')) break p000e0177;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0177;
			if (!CNDnoun1(118)) break p000e0177;
 		}
		if (!CNDcarried(52)) break p000e0177;
 		ACCwriteln(147);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PLAT
	p000e0178:
	{
 		if (skipdoall('p000e0178')) break p000e0178;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0178;
			if (!CNDnoun1(120)) break p000e0178;
 		}
		if (!CNDat(57)) break p000e0178;
		if (!CNDgt(23,29)) break p000e0178;
		if (!CNDlt(23,40)) break p000e0178;
 		ACCwriteln(148);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COG
	p000e0179:
	{
 		if (skipdoall('p000e0179')) break p000e0179;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0179;
			if (!CNDnoun1(127)) break p000e0179;
 		}
		if (!CNDat(57)) break p000e0179;
		if (!CNDeq(23,40)) break p000e0179;
		if (!CNDzero(24)) break p000e0179;
 		ACCwriteln(149);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COG
	p000e0180:
	{
 		if (skipdoall('p000e0180')) break p000e0180;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0180;
			if (!CNDnoun1(127)) break p000e0180;
 		}
		if (!CNDat(57)) break p000e0180;
		if (!CNDeq(23,40)) break p000e0180;
		if (!CNDeq(24,10)) break p000e0180;
 		ACCwriteln(150);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOOT
	p000e0181:
	{
 		if (skipdoall('p000e0181')) break p000e0181;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0181;
			if (!CNDnoun1(249)) break p000e0181;
 		}
		if (!CNDcarried(28)) break p000e0181;
 		ACCwriteln(151);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HAT
	p000e0182:
	{
 		if (skipdoall('p000e0182')) break p000e0182;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0182;
			if (!CNDnoun1(250)) break p000e0182;
 		}
		if (!CNDcarried(22)) break p000e0182;
 		ACCwriteln(152);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HAT
	p000e0183:
	{
 		if (skipdoall('p000e0183')) break p000e0183;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0183;
			if (!CNDnoun1(250)) break p000e0183;
 		}
		if (!CNDworn(22)) break p000e0183;
 		ACCwriteln(153);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM _
	p000e0184:
	{
 		if (skipdoall('p000e0184')) break p000e0184;
 		if (in_response)
		{
			if (!CNDverb(17)) break p000e0184;
 		}
 		ACCwriteln(154);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE HAND
	p000e0185:
	{
 		if (skipdoall('p000e0185')) break p000e0185;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0185;
			if (!CNDnoun1(14)) break p000e0185;
 		}
		if (!CNDat(70)) break p000e0185;
		if (!CNDcarried(6)) break p000e0185;
		if (!CNDlt(111,20)) break p000e0185;
 		ACCswap(14,6);
 		ACCdrop(14);
		if (!success) break pro000_restart;
 		ACCwriteln(155);
 		ACClet(111,20);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE HAND
	p000e0186:
	{
 		if (skipdoall('p000e0186')) break p000e0186;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0186;
			if (!CNDnoun1(14)) break p000e0186;
 		}
		if (!CNDat(63)) break p000e0186;
		if (!CNDcarried(6)) break p000e0186;
		if (!CNDlt(111,20)) break p000e0186;
 		ACClet(111,20);
 		ACCwriteln(156);
 		ACCswap(23,6);
 		ACCdrop(23);
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE WELL
	p000e0187:
	{
 		if (skipdoall('p000e0187')) break p000e0187;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0187;
			if (!CNDnoun1(16)) break p000e0187;
 		}
		if (!CNDat(4)) break p000e0187;
		if (!CNDnotworn(28)) break p000e0187;
 		ACCwriteln(157);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// ENTE WELL
	p000e0188:
	{
 		if (skipdoall('p000e0188')) break p000e0188;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0188;
			if (!CNDnoun1(16)) break p000e0188;
 		}
		if (!CNDat(4)) break p000e0188;
		if (!CNDworn(28)) break p000e0188;
 		ACCgoto(69);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE BOUL
	p000e0189:
	{
 		if (skipdoall('p000e0189')) break p000e0189;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0189;
			if (!CNDnoun1(19)) break p000e0189;
 		}
		if (!CNDat(70)) break p000e0189;
		if (!CNDpresent(8)) break p000e0189;
 		ACCgoto(21);
 		ACCdesc();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE BOUL
	p000e0190:
	{
 		if (skipdoall('p000e0190')) break p000e0190;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0190;
			if (!CNDnoun1(19)) break p000e0190;
 		}
		if (!CNDat(70)) break p000e0190;
		if (!CNDabsent(8)) break p000e0190;
 		ACCwriteln(158);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE BOUL
	p000e0191:
	{
 		if (skipdoall('p000e0191')) break p000e0191;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0191;
			if (!CNDnoun1(19)) break p000e0191;
 		}
		if (!CNDat(44)) break p000e0191;
		if (!CNDpresent(41)) break p000e0191;
 		ACCgoto(60);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE BOUL
	p000e0192:
	{
 		if (skipdoall('p000e0192')) break p000e0192;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0192;
			if (!CNDnoun1(19)) break p000e0192;
 		}
		if (!CNDat(49)) break p000e0192;
		if (!CNDeq(22,10)) break p000e0192;
 		ACCwriteln(159);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE BOUL
	p000e0193:
	{
 		if (skipdoall('p000e0193')) break p000e0193;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0193;
			if (!CNDnoun1(19)) break p000e0193;
 		}
		if (!CNDat(49)) break p000e0193;
		if (!CNDgt(22,15)) break p000e0193;
 		ACCgoto(50);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE BOUL
	p000e0194:
	{
 		if (skipdoall('p000e0194')) break p000e0194;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0194;
			if (!CNDnoun1(19)) break p000e0194;
 		}
		if (!CNDat(49)) break p000e0194;
		if (!CNDeq(22,15)) break p000e0194;
 		ACCwriteln(160);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTE KEY
	p000e0195:
	{
 		if (skipdoall('p000e0195')) break p000e0195;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0195;
			if (!CNDnoun1(35)) break p000e0195;
 		}
		if (!CNDat(57)) break p000e0195;
		if (!CNDcarried(15)) break p000e0195;
 		ACCswap(15,56);
 		ACCdrop(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0196:
	{
 		if (skipdoall('p000e0196')) break p000e0196;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0196;
			if (!CNDnoun1(14)) break p000e0196;
 		}
		if (!CNDat(4)) break p000e0196;
		if (!CNDlt(111,9)) break p000e0196;
 		ACCcreate(6);
 		ACCwriteln(161);
 		ACClet(111,10);
 		ACCplus(30,2);
 		ACCcreate(7);
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0197:
	{
 		if (skipdoall('p000e0197')) break p000e0197;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0197;
			if (!CNDnoun1(14)) break p000e0197;
 		}
		if (!CNDat(4)) break p000e0197;
		if (!CNDgt(111,9)) break p000e0197;
 		ACCwriteln(162);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0198:
	{
 		if (skipdoall('p000e0198')) break p000e0198;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0198;
			if (!CNDnoun1(14)) break p000e0198;
 		}
		if (!CNDat(70)) break p000e0198;
		if (!CNDeq(111,20)) break p000e0198;
 		ACCcreate(8);
 		ACCwriteln(163);
 		ACClet(111,30);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0199:
	{
 		if (skipdoall('p000e0199')) break p000e0199;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0199;
			if (!CNDnoun1(14)) break p000e0199;
 		}
		if (!CNDat(70)) break p000e0199;
		if (!CNDpresent(6)) break p000e0199;
		if (!CNDlt(111,20)) break p000e0199;
 		ACCwriteln(164);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0200:
	{
 		if (skipdoall('p000e0200')) break p000e0200;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0200;
			if (!CNDnoun1(14)) break p000e0200;
 		}
		if (!CNDat(70)) break p000e0200;
		if (!CNDeq(111,30)) break p000e0200;
		if (!CNDpresent(14)) break p000e0200;
 		ACClet(111,20);
 		ACCdestroy(8);
 		ACCwriteln(165);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0201:
	{
 		if (skipdoall('p000e0201')) break p000e0201;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0201;
			if (!CNDnoun1(14)) break p000e0201;
 		}
		if (!CNDat(63)) break p000e0201;
		if (!CNDeq(111,20)) break p000e0201;
 		ACClet(111,40);
 		ACCwriteln(166);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0202:
	{
 		if (skipdoall('p000e0202')) break p000e0202;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0202;
			if (!CNDnoun1(14)) break p000e0202;
 		}
		if (!CNDat(63)) break p000e0202;
		if (!CNDeq(111,40)) break p000e0202;
 		ACClet(111,20);
 		ACCwriteln(167);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE HAND
	p000e0203:
	{
 		if (skipdoall('p000e0203')) break p000e0203;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0203;
			if (!CNDnoun1(14)) break p000e0203;
 		}
		if (!CNDat(63)) break p000e0203;
		if (!CNDcarried(6)) break p000e0203;
		if (!CNDlt(111,20)) break p000e0203;
 		ACCwriteln(168);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE KEY
	p000e0204:
	{
 		if (skipdoall('p000e0204')) break p000e0204;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0204;
			if (!CNDnoun1(35)) break p000e0204;
 		}
		if (!CNDat(57)) break p000e0204;
		if (!CNDpresent(56)) break p000e0204;
		if (!CNDzero(24)) break p000e0204;
 		ACClet(24,10);
 		ACCwriteln(169);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE CLOC
	p000e0205:
	{
 		if (skipdoall('p000e0205')) break p000e0205;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0205;
			if (!CNDnoun1(114)) break p000e0205;
 		}
		if (!CNDat(57)) break p000e0205;
		if (!CNDpresent(56)) break p000e0205;
		if (!CNDzero(24)) break p000e0205;
 		ACClet(24,10);
 		ACCplus(30,2);
 		ACCwriteln(170);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES RED
	p000e0206:
	{
 		if (skipdoall('p000e0206')) break p000e0206;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0206;
			if (!CNDnoun1(25)) break p000e0206;
 		}
		if (!CNDcarried(5)) break p000e0206;
 		ACCclear(112);
 		ACClet(28,2);
 		ACCpause(1);
 		function anykey00003() 
		{
 		ACCwriteln(171);
 		ACCwriteln(172);
 		ACCdone();
		return;
		}
 		waitKey(anykey00003);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0207:
	{
 		if (skipdoall('p000e0207')) break p000e0207;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0207;
			if (!CNDnoun1(26)) break p000e0207;
 		}
		if (!CNDcarried(5)) break p000e0207;
 		ACClet(112,1);
 		ACClet(28,2);
 		ACCpause(1);
 		function anykey00004() 
		{
 		ACCwriteln(173);
 		ACCwriteln(174);
 		ACCdone();
		return;
		}
 		waitKey(anykey00004);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLUE
	p000e0208:
	{
 		if (skipdoall('p000e0208')) break p000e0208;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0208;
			if (!CNDnoun1(27)) break p000e0208;
 		}
		if (!CNDcarried(5)) break p000e0208;
 		ACClet(112,2);
 		ACClet(28,2);
 		ACCpause(1);
 		function anykey00005() 
		{
 		ACCwriteln(175);
 		ACCwriteln(176);
 		ACCdone();
		return;
		}
 		waitKey(anykey00005);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES PINK
	p000e0209:
	{
 		if (skipdoall('p000e0209')) break p000e0209;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0209;
			if (!CNDnoun1(28)) break p000e0209;
 		}
		if (!CNDcarried(5)) break p000e0209;
 		ACClet(112,3);
 		ACClet(28,2);
 		ACCpause(1);
 		function anykey00006() 
		{
 		ACCwriteln(177);
 		ACCwriteln(178);
 		ACCdone();
		return;
		}
 		waitKey(anykey00006);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0210:
	{
 		if (skipdoall('p000e0210')) break p000e0210;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0210;
			if (!CNDnoun1(30)) break p000e0210;
 		}
		if (!CNDcarried(5)) break p000e0210;
		if (!CNDatgt(35)) break p000e0210;
		if (!CNDatlt(42)) break p000e0210;
		if (!CNDzero(112)) break p000e0210;
		if (!CNDzero(113)) break p000e0210;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00007() 
		{
 		ACCwriteln(179);
 		ACCdone();
		return;
		}
 		waitKey(anykey00007);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0211:
	{
 		if (skipdoall('p000e0211')) break p000e0211;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0211;
			if (!CNDnoun1(30)) break p000e0211;
 		}
		if (!CNDcarried(5)) break p000e0211;
		if (!CNDatlt(80)) break p000e0211;
		if (!CNDatgt(74)) break p000e0211;
		if (!CNDeq(112,1)) break p000e0211;
		if (!CNDeq(113,10)) break p000e0211;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00008() 
		{
 		ACCwriteln(180);
 		ACCdone();
		return;
		}
 		waitKey(anykey00008);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0212:
	{
 		if (skipdoall('p000e0212')) break p000e0212;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0212;
			if (!CNDnoun1(30)) break p000e0212;
 		}
		if (!CNDcarried(5)) break p000e0212;
		if (!CNDatgt(14)) break p000e0212;
		if (!CNDatlt(26)) break p000e0212;
		if (!CNDnotat(17)) break p000e0212;
		if (!CNDnotat(18)) break p000e0212;
		if (!CNDnotat(19)) break p000e0212;
		if (!CNDnotat(20)) break p000e0212;
		if (!CNDnotat(21)) break p000e0212;
		if (!CNDeq(112,2)) break p000e0212;
		if (!CNDeq(113,20)) break p000e0212;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00009() 
		{
 		ACCwriteln(181);
 		ACCdone();
		return;
		}
 		waitKey(anykey00009);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0213:
	{
 		if (skipdoall('p000e0213')) break p000e0213;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0213;
			if (!CNDnoun1(30)) break p000e0213;
 		}
		if (!CNDcarried(5)) break p000e0213;
		if (!CNDatlt(58)) break p000e0213;
		if (!CNDatgt(51)) break p000e0213;
		if (!CNDeq(112,3)) break p000e0213;
		if (!CNDeq(113,30)) break p000e0213;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00010() 
		{
 		ACCwriteln(182);
 		ACCdone();
		return;
		}
 		waitKey(anykey00010);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0214:
	{
 		if (skipdoall('p000e0214')) break p000e0214;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0214;
			if (!CNDnoun1(30)) break p000e0214;
 		}
		if (!CNDcarried(5)) break p000e0214;
		if (!CNDzero(112)) break p000e0214;
		if (!CNDnotcarr(9)) break p000e0214;
 		ACCwriteln(183);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0215:
	{
 		if (skipdoall('p000e0215')) break p000e0215;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0215;
			if (!CNDnoun1(30)) break p000e0215;
 		}
		if (!CNDcarried(5)) break p000e0215;
		if (!CNDat(61)) break p000e0215;
		if (!CNDeq(112,1)) break p000e0215;
		if (!CNDeq(113,10)) break p000e0215;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00011() 
		{
 		ACCwriteln(184);
 		ACCdone();
		return;
		}
 		waitKey(anykey00011);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0216:
	{
 		if (skipdoall('p000e0216')) break p000e0216;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0216;
			if (!CNDnoun1(30)) break p000e0216;
 		}
		if (!CNDcarried(5)) break p000e0216;
		if (!CNDcarried(9)) break p000e0216;
		if (!CNDzero(112)) break p000e0216;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00012() 
		{
 		ACCwriteln(185);
		}
 		waitKey(anykey00012);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0217:
	{
 		if (skipdoall('p000e0217')) break p000e0217;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0217;
			if (!CNDnoun1(30)) break p000e0217;
 		}
		if (!CNDcarried(5)) break p000e0217;
		if (!CNDcarried(10)) break p000e0217;
		if (!CNDeq(112,1)) break p000e0217;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00013() 
		{
 		ACCwriteln(186);
 		ACCdone();
		return;
		}
 		waitKey(anykey00013);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0218:
	{
 		if (skipdoall('p000e0218')) break p000e0218;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0218;
			if (!CNDnoun1(30)) break p000e0218;
 		}
		if (!CNDcarried(5)) break p000e0218;
		if (!CNDcarried(11)) break p000e0218;
		if (!CNDeq(112,2)) break p000e0218;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00014() 
		{
 		ACCwriteln(187);
 		ACCdone();
		return;
		}
 		waitKey(anykey00014);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0219:
	{
 		if (skipdoall('p000e0219')) break p000e0219;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0219;
			if (!CNDnoun1(30)) break p000e0219;
 		}
		if (!CNDcarried(5)) break p000e0219;
		if (!CNDcarried(12)) break p000e0219;
		if (!CNDeq(112,3)) break p000e0219;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00015() 
		{
 		ACCwriteln(188);
 		ACCdone();
		return;
		}
 		waitKey(anykey00015);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0220:
	{
 		if (skipdoall('p000e0220')) break p000e0220;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0220;
			if (!CNDnoun1(30)) break p000e0220;
 		}
		if (!CNDcarried(5)) break p000e0220;
		if (!CNDeq(112,1)) break p000e0220;
		if (!CNDabsent(10)) break p000e0220;
 		ACCwriteln(189);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0221:
	{
 		if (skipdoall('p000e0221')) break p000e0221;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0221;
			if (!CNDnoun1(30)) break p000e0221;
 		}
		if (!CNDcarried(5)) break p000e0221;
		if (!CNDeq(112,2)) break p000e0221;
		if (!CNDabsent(11)) break p000e0221;
 		ACCwriteln(190);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BLAC
	p000e0222:
	{
 		if (skipdoall('p000e0222')) break p000e0222;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0222;
			if (!CNDnoun1(30)) break p000e0222;
 		}
		if (!CNDcarried(5)) break p000e0222;
		if (!CNDeq(112,3)) break p000e0222;
		if (!CNDabsent(12)) break p000e0222;
 		ACCwriteln(191);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES PEND
	p000e0223:
	{
 		if (skipdoall('p000e0223')) break p000e0223;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0223;
			if (!CNDnoun1(119)) break p000e0223;
 		}
		if (!CNDat(57)) break p000e0223;
 		ACCwriteln(192);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0224:
	{
 		if (skipdoall('p000e0224')) break p000e0224;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0224;
			if (!CNDnoun1(121)) break p000e0224;
 		}
		if (!CNDat(69)) break p000e0224;
		if (!CNDabsent(9)) break p000e0224;
 		ACClet(28,4);
 		ACCpause(1);
 		function anykey00018() 
		{
 		ACCwriteln(193);
 		ACCwriteln(194);
 		ACCdone();
		return;
		}
 		function anykey00017() 
		{
 		ACClet(28,4);
 		ACCpause(1);
 		waitKey(anykey00018);
		}
 		function anykey00016() 
		{
 		ACClet(28,6);
 		ACCpause(50);
 		waitKey(anykey00017);
		}
 		waitKey(anykey00016);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0225:
	{
 		if (skipdoall('p000e0225')) break p000e0225;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0225;
			if (!CNDnoun1(121)) break p000e0225;
 		}
		if (!CNDat(69)) break p000e0225;
		if (!CNDabsent(10)) break p000e0225;
 		ACClet(28,4);
 		ACCpause(1);
 		function anykey00021() 
		{
 		ACCwriteln(195);
 		ACCwriteln(196);
 		ACCdone();
		return;
		}
 		function anykey00020() 
		{
 		ACClet(28,4);
 		ACCpause(1);
 		waitKey(anykey00021);
		}
 		function anykey00019() 
		{
 		ACClet(28,6);
 		ACCpause(50);
 		waitKey(anykey00020);
		}
 		waitKey(anykey00019);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0226:
	{
 		if (skipdoall('p000e0226')) break p000e0226;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0226;
			if (!CNDnoun1(121)) break p000e0226;
 		}
		if (!CNDat(69)) break p000e0226;
		if (!CNDabsent(11)) break p000e0226;
 		ACClet(28,4);
 		ACCpause(1);
 		function anykey00024() 
		{
 		ACCwriteln(197);
 		ACCwriteln(198);
 		ACCdone();
		return;
		}
 		function anykey00023() 
		{
 		ACClet(28,4);
 		ACCpause(1);
 		waitKey(anykey00024);
		}
 		function anykey00022() 
		{
 		ACClet(28,6);
 		ACCpause(50);
 		waitKey(anykey00023);
		}
 		waitKey(anykey00022);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0227:
	{
 		if (skipdoall('p000e0227')) break p000e0227;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0227;
			if (!CNDnoun1(121)) break p000e0227;
 		}
		if (!CNDat(69)) break p000e0227;
		if (!CNDabsent(12)) break p000e0227;
 		ACClet(28,4);
 		ACCpause(1);
 		function anykey00027() 
		{
 		ACCwriteln(199);
 		ACCwriteln(200);
 		ACCdone();
		return;
		}
 		function anykey00026() 
		{
 		ACClet(28,4);
 		ACCpause(1);
 		waitKey(anykey00027);
		}
 		function anykey00025() 
		{
 		ACClet(28,6);
 		ACCpause(50);
 		waitKey(anykey00026);
		}
 		waitKey(anykey00025);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0228:
	{
 		if (skipdoall('p000e0228')) break p000e0228;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0228;
			if (!CNDnoun1(121)) break p000e0228;
 		}
		if (!CNDat(69)) break p000e0228;
		if (!CNDpresent(9)) break p000e0228;
		if (!CNDpresent(10)) break p000e0228;
		if (!CNDpresent(11)) break p000e0228;
		if (!CNDpresent(12)) break p000e0228;
 		ACClet(28,4);
 		ACCpause(1);
 		function anykey00032() 
		{
 		ACCgoto(80);
 		ACCdesc();
		return;
		}
 		function anykey00031() 
		{
 		ACCpause(100);
 		waitKey(anykey00032);
		}
 		function anykey00030() 
		{
 		ACCwriteln(201);
 		ACCwriteln(202);
 		ACCplus(30,4);
 		ACCpause(250);
 		waitKey(anykey00031);
		}
 		function anykey00029() 
		{
 		ACClet(28,4);
 		ACCpause(1);
 		waitKey(anykey00030);
		}
 		function anykey00028() 
		{
 		ACClet(28,6);
 		ACCpause(50);
 		waitKey(anykey00029);
		}
 		waitKey(anykey00028);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0229:
	{
 		if (skipdoall('p000e0229')) break p000e0229;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0229;
 		}
		if (!CNDat(10)) break p000e0229;
		if (!CNDzero(22)) break p000e0229;
 		ACCwriteln(203);
 		ACCcreate(25);
 		ACClet(22,10);
 		ACCplus(30,2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0230:
	{
 		if (skipdoall('p000e0230')) break p000e0230;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0230;
 		}
		if (!CNDat(10)) break p000e0230;
		if (!CNDnotzero(22)) break p000e0230;
 		ACCwriteln(204);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0231:
	{
 		if (skipdoall('p000e0231')) break p000e0231;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0231;
 		}
		if (!CNDat(73)) break p000e0231;
		if (!CNDeq(18,30)) break p000e0231;
 		ACCcreate(35);
 		ACClet(18,40);
 		ACCplus(30,1);
 		ACCwriteln(205);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0232:
	{
 		if (skipdoall('p000e0232')) break p000e0232;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0232;
 		}
		if (!CNDat(73)) break p000e0232;
		if (!CNDeq(18,40)) break p000e0232;
 		ACCcreate(38);
 		ACClet(18,50);
 		ACCplus(30,1);
 		ACCwriteln(206);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LAY _
	p000e0233:
	{
 		if (skipdoall('p000e0233')) break p000e0233;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0233;
 		}
		if (!CNDat(37)) break p000e0233;
		if (!CNDzero(113)) break p000e0233;
		if (!CNDzero(112)) break p000e0233;
		if (!CNDeq(17,30)) break p000e0233;
 		ACClet(114,10);
 		ACCwriteln(207);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LAY _
	p000e0234:
	{
 		if (skipdoall('p000e0234')) break p000e0234;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0234;
 		}
		if (!CNDat(37)) break p000e0234;
		if (!CNDlt(17,30)) break p000e0234;
 		ACCwriteln(208);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LAY _
	p000e0235:
	{
 		if (skipdoall('p000e0235')) break p000e0235;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0235;
 		}
		if (!CNDat(37)) break p000e0235;
		if (!CNDeq(17,30)) break p000e0235;
		if (!CNDnotzero(112)) break p000e0235;
 		ACCwriteln(209);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOW U
	p000e0236:
	{
 		if (skipdoall('p000e0236')) break p000e0236;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0236;
			if (!CNDnoun1(9)) break p000e0236;
 		}
		if (!CNDcarried(42)) break p000e0236;
		if (!CNDeq(19,10)) break p000e0236;
 		ACClet(7,10);
 		ACClet(19,20);
 		ACCok();
		break pro000_restart;
		{}

	}

	// BLOW U
	p000e0237:
	{
 		if (skipdoall('p000e0237')) break p000e0237;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0237;
			if (!CNDnoun1(9)) break p000e0237;
 		}
		if (!CNDcarried(42)) break p000e0237;
		if (!CNDgt(19,10)) break p000e0237;
 		ACCwriteln(210);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOW MUSI
	p000e0238:
	{
 		if (skipdoall('p000e0238')) break p000e0238;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0238;
			if (!CNDnoun1(33)) break p000e0238;
 		}
		if (!CNDzero(5)) break p000e0238;
		if (!CNDcarried(13)) break p000e0238;
		if (!CNDpresent(7)) break p000e0238;
 		ACClet(5,4);
 		ACCwriteln(211);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOW MUSI
	p000e0239:
	{
 		if (skipdoall('p000e0239')) break p000e0239;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0239;
			if (!CNDnoun1(33)) break p000e0239;
 		}
		if (!CNDnotzero(5)) break p000e0239;
		if (!CNDcarried(13)) break p000e0239;
		if (!CNDpresent(7)) break p000e0239;
 		ACCwriteln(212);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOW MUSI
	p000e0240:
	{
 		if (skipdoall('p000e0240')) break p000e0240;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0240;
			if (!CNDnoun1(33)) break p000e0240;
 		}
		if (!CNDcarried(13)) break p000e0240;
		if (!CNDabsent(7)) break p000e0240;
 		ACCwriteln(213);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOW BALL
	p000e0241:
	{
 		if (skipdoall('p000e0241')) break p000e0241;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0241;
			if (!CNDnoun1(90)) break p000e0241;
 		}
		if (!CNDcarried(42)) break p000e0241;
		if (!CNDeq(19,10)) break p000e0241;
 		ACClet(7,10);
 		ACClet(19,20);
 		ACCok();
		break pro000_restart;
		{}

	}

	// BLOW BALL
	p000e0242:
	{
 		if (skipdoall('p000e0242')) break p000e0242;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0242;
			if (!CNDnoun1(90)) break p000e0242;
 		}
		if (!CNDcarried(42)) break p000e0242;
		if (!CNDgt(19,10)) break p000e0242;
 		ACCwriteln(214);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN BOUL
	p000e0243:
	{
 		if (skipdoall('p000e0243')) break p000e0243;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0243;
			if (!CNDnoun1(19)) break p000e0243;
 		}
		if (!CNDat(54)) break p000e0243;
		if (!CNDpresent(37)) break p000e0243;
 		ACCwriteln(215);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN BOUL
	p000e0244:
	{
 		if (skipdoall('p000e0244')) break p000e0244;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0244;
			if (!CNDnoun1(19)) break p000e0244;
 		}
		if (!CNDat(49)) break p000e0244;
		if (!CNDeq(22,15)) break p000e0244;
 		ACClet(22,20);
 		ACCok();
		break pro000_restart;
		{}

	}

	// OPEN BOUL
	p000e0245:
	{
 		if (skipdoall('p000e0245')) break p000e0245;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0245;
			if (!CNDnoun1(19)) break p000e0245;
 		}
		if (!CNDat(49)) break p000e0245;
		if (!CNDgt(22,15)) break p000e0245;
 		ACCwriteln(216);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN CHES
	p000e0246:
	{
 		if (skipdoall('p000e0246')) break p000e0246;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0246;
			if (!CNDnoun1(36)) break p000e0246;
 		}
		if (!CNDat(21)) break p000e0246;
		if (!CNDeq(112,10)) break p000e0246;
 		ACCwriteln(217);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN CHES
	p000e0247:
	{
 		if (skipdoall('p000e0247')) break p000e0247;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0247;
			if (!CNDnoun1(36)) break p000e0247;
 		}
		if (!CNDat(21)) break p000e0247;
		if (!CNDeq(112,8)) break p000e0247;
 		ACClet(112,6);
 		ACCok();
		break pro000_restart;
		{}

	}

	// OPEN BASK
	p000e0248:
	{
 		if (skipdoall('p000e0248')) break p000e0248;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0248;
			if (!CNDnoun1(41)) break p000e0248;
 		}
		if (!CNDat(54)) break p000e0248;
		if (!CNDpresent(48)) break p000e0248;
 		ACCswap(48,49);
 		ACCok();
		break pro000_restart;
		{}

	}

	// OPEN CAPT
	p000e0249:
	{
 		if (skipdoall('p000e0249')) break p000e0249;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0249;
			if (!CNDnoun1(45)) break p000e0249;
 		}
		if (!CNDat(44)) break p000e0249;
		if (!CNDpresent(20)) break p000e0249;
 		ACCswap(40,41);
 		ACCswap(20,17);
 		ACCplus(30,5);
 		ACCwriteln(218);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN CAPT
	p000e0250:
	{
 		if (skipdoall('p000e0250')) break p000e0250;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0250;
			if (!CNDnoun1(45)) break p000e0250;
 		}
		if (!CNDnotat(44)) break p000e0250;
		if (!CNDpresent(20)) break p000e0250;
 		ACCdestroy(20);
 		ACCcreate(19);
 		ACClet(115,10);
 		ACCwriteln(219);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN CABI
	p000e0251:
	{
 		if (skipdoall('p000e0251')) break p000e0251;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0251;
			if (!CNDnoun1(89)) break p000e0251;
 		}
		if (!CNDat(79)) break p000e0251;
		if (!CNDeq(116,10)) break p000e0251;
 		ACClet(116,20);
 		ACCok();
		break pro000_restart;
		{}

	}

	// UNBO BOUL
	p000e0252:
	{
 		if (skipdoall('p000e0252')) break p000e0252;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0252;
			if (!CNDnoun1(19)) break p000e0252;
 		}
		if (!CNDat(49)) break p000e0252;
		if (!CNDeq(22,10)) break p000e0252;
 		ACClet(22,15);
 		ACCok();
		break pro000_restart;
		{}

	}

	// UNBO CHES
	p000e0253:
	{
 		if (skipdoall('p000e0253')) break p000e0253;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0253;
			if (!CNDnoun1(36)) break p000e0253;
 		}
		if (!CNDat(21)) break p000e0253;
		if (!CNDeq(112,10)) break p000e0253;
		if (!CNDabsent(15)) break p000e0253;
 		ACCwriteln(220);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNBO CHES
	p000e0254:
	{
 		if (skipdoall('p000e0254')) break p000e0254;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0254;
			if (!CNDnoun1(36)) break p000e0254;
 		}
		if (!CNDat(21)) break p000e0254;
		if (!CNDeq(112,10)) break p000e0254;
		if (!CNDcarried(15)) break p000e0254;
 		ACClet(112,8);
 		ACCwriteln(221);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SET BASK
	p000e0255:
	{
 		if (skipdoall('p000e0255')) break p000e0255;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0255;
			if (!CNDnoun1(41)) break p000e0255;
 		}
		if (!CNDat(45)) break p000e0255;
		if (!CNDpresent(17)) break p000e0255;
		if (!CNDpresent(18)) break p000e0255;
 		ACClet(115,10);
 		ACCdestroy(17);
 		ACCdestroy(18);
 		ACCcreate(19);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SET BASK
	p000e0256:
	{
 		if (skipdoall('p000e0256')) break p000e0256;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0256;
			if (!CNDnoun1(41)) break p000e0256;
 		}
		if (!CNDnotat(45)) break p000e0256;
 		ACCwriteln(222);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0257:
	{
 		if (skipdoall('p000e0257')) break p000e0257;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0257;
 		}
		if (!CNDat(45)) break p000e0257;
		if (!CNDeq(115,10)) break p000e0257;
		if (!CNDzero(7)) break p000e0257;
 		ACClet(7,4);
 		ACCwriteln(223);
 		ACCpause(100);
 		function anykey00033() 
		{
 		ACCplus(31,10);
 		ACCdone();
		return;
		}
 		waitKey(anykey00033);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0258:
	{
 		if (skipdoall('p000e0258')) break p000e0258;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0258;
 		}
		if (!CNDat(45)) break p000e0258;
		if (!CNDeq(115,10)) break p000e0258;
		if (!CNDnotzero(7)) break p000e0258;
 		ACClet(115,20);
 		ACCwriteln(224);
 		ACCpause(100);
 		function anykey00034() 
		{
 		ACCplus(31,10);
 		ACCcreate(16);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00034);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0259:
	{
 		if (skipdoall('p000e0259')) break p000e0259;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0259;
 		}
		if (!CNDat(45)) break p000e0259;
		if (!CNDeq(115,20)) break p000e0259;
		if (!CNDpresent(19)) break p000e0259;
		if (!CNDpresent(16)) break p000e0259;
 		ACCwriteln(225);
 		ACCpause(100);
 		function anykey00035() 
		{
 		ACCplus(31,10);
 		ACCwriteln(226);
 		ACCdestroy(19);
 		ACCdestroy(16);
 		ACCcreate(20);
 		ACClet(115,30);
 		ACCdone();
		return;
		}
 		waitKey(anykey00035);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0260:
	{
 		if (skipdoall('p000e0260')) break p000e0260;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0260;
 		}
		if (!CNDnotat(45)) break p000e0260;
		if (!CNDnotat(61)) break p000e0260;
		if (!CNDnotat(75)) break p000e0260;
 		ACCwriteln(227);
 		ACCpause(100);
 		function anykey00036() 
		{
 		ACCplus(31,10);
 		ACCdone();
		return;
		}
 		waitKey(anykey00036);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0261:
	{
 		if (skipdoall('p000e0261')) break p000e0261;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0261;
 		}
		if (!CNDat(45)) break p000e0261;
		if (!CNDlt(115,10)) break p000e0261;
 		ACCwriteln(228);
 		ACCpause(100);
 		function anykey00037() 
		{
 		ACCplus(31,10);
 		ACCdone();
		return;
		}
 		waitKey(anykey00037);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0262:
	{
 		if (skipdoall('p000e0262')) break p000e0262;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0262;
 		}
		if (!CNDat(45)) break p000e0262;
		if (!CNDgt(115,29)) break p000e0262;
 		ACCwriteln(229);
 		ACCpause(100);
 		function anykey00038() 
		{
 		ACCplus(31,10);
 		ACCdone();
		return;
		}
 		waitKey(anykey00038);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0263:
	{
 		if (skipdoall('p000e0263')) break p000e0263;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0263;
 		}
		if (!CNDat(61)) break p000e0263;
 		ACCwriteln(230);
 		ACCpause(100);
 		function anykey00039() 
		{
 		ACCgoto(75);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00039);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PAUS _
	p000e0264:
	{
 		if (skipdoall('p000e0264')) break p000e0264;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0264;
 		}
		if (!CNDat(75)) break p000e0264;
 		ACCwriteln(231);
 		ACCpause(100);
 		function anykey00040() 
		{
 		ACCgoto(61);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00040);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DRIN BAIT
	p000e0265:
	{
 		if (skipdoall('p000e0265')) break p000e0265;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0265;
			if (!CNDnoun1(42)) break p000e0265;
 		}
		if (!CNDcarried(18)) break p000e0265;
 		ACCwriteln(232);
 		ACCdestroy(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DRIN PUDD
	p000e0266:
	{
 		if (skipdoall('p000e0266')) break p000e0266;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0266;
			if (!CNDnoun1(97)) break p000e0266;
 		}
		if (!CNDat(54)) break p000e0266;
		if (!CNDpresent(47)) break p000e0266;
 		ACCwriteln(233);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DRIN PUDD
	p000e0267:
	{
 		if (skipdoall('p000e0267')) break p000e0267;
 		if (in_response)
		{
			if (!CNDverb(46)) break p000e0267;
			if (!CNDnoun1(97)) break p000e0267;
 		}
		if (!CNDcarried(50)) break p000e0267;
 		ACCwriteln(234);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// CLOS HAND
	p000e0268:
	{
 		if (skipdoall('p000e0268')) break p000e0268;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0268;
			if (!CNDnoun1(14)) break p000e0268;
 		}
		if (!CNDat(73)) break p000e0268;
		if (!CNDeq(18,50)) break p000e0268;
 		ACCdestroy(36);
 		ACCcreate(37);
 		ACClet(18,60);
 		ACCplus(30,2);
 		ACCwriteln(235);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLOS BASK
	p000e0269:
	{
 		if (skipdoall('p000e0269')) break p000e0269;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0269;
			if (!CNDnoun1(41)) break p000e0269;
 		}
		if (!CNDat(54)) break p000e0269;
		if (!CNDpresent(49)) break p000e0269;
 		ACCswap(49,48);
 		ACCok();
		break pro000_restart;
		{}

	}

	// CLOS VISO
	p000e0270:
	{
 		if (skipdoall('p000e0270')) break p000e0270;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0270;
			if (!CNDnoun1(51)) break p000e0270;
 		}
		if (!CNDworn(22)) break p000e0270;
		if (!CNDeq(17,20)) break p000e0270;
 		ACCwriteln(236);
 		ACClet(17,30);
 		ACClet(8,6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLOS VISO
	p000e0271:
	{
 		if (skipdoall('p000e0271')) break p000e0271;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0271;
			if (!CNDnoun1(51)) break p000e0271;
 		}
		if (!CNDworn(22)) break p000e0271;
		if (!CNDeq(17,30)) break p000e0271;
 		ACCwriteln(237);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLOS VISO
	p000e0272:
	{
 		if (skipdoall('p000e0272')) break p000e0272;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0272;
			if (!CNDnoun1(51)) break p000e0272;
 		}
		if (!CNDnotworn(22)) break p000e0272;
 		ACCwriteln(238);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLOS VISO
	p000e0273:
	{
 		if (skipdoall('p000e0273')) break p000e0273;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0273;
			if (!CNDnoun1(51)) break p000e0273;
 		}
		if (!CNDlt(17,20)) break p000e0273;
 		ACCwriteln(239);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLOS BOAT
	p000e0274:
	{
 		if (skipdoall('p000e0274')) break p000e0274;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0274;
			if (!CNDnoun1(56)) break p000e0274;
 		}
		if (!CNDat(24)) break p000e0274;
		if (!CNDpresent(24)) break p000e0274;
		if (!CNDeq(18,10)) break p000e0274;
 		ACCdestroy(24);
 		ACCwriteln(240);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLOS BOAT
	p000e0275:
	{
 		if (skipdoall('p000e0275')) break p000e0275;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0275;
			if (!CNDnoun1(56)) break p000e0275;
 		}
		if (!CNDat(24)) break p000e0275;
		if (!CNDpresent(24)) break p000e0275;
		if (!CNDeq(18,20)) break p000e0275;
 		ACCwriteln(241);
 		ACClet(18,30);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN CABL
	p000e0276:
	{
 		if (skipdoall('p000e0276')) break p000e0276;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0276;
			if (!CNDnoun1(49)) break p000e0276;
 		}
		if (!CNDcarried(5)) break p000e0276;
		if (!CNDworn(22)) break p000e0276;
		if (!CNDeq(17,10)) break p000e0276;
 		ACClet(17,20);
 		ACCwriteln(242);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN CABL
	p000e0277:
	{
 		if (skipdoall('p000e0277')) break p000e0277;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0277;
			if (!CNDnoun1(49)) break p000e0277;
 		}
		if (!CNDcarried(22)) break p000e0277;
		if (!CNDnotworn(5)) break p000e0277;
 		ACCwriteln(243);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN CABL
	p000e0278:
	{
 		if (skipdoall('p000e0278')) break p000e0278;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0278;
			if (!CNDnoun1(49)) break p000e0278;
 		}
		if (!CNDgt(17,10)) break p000e0278;
 		ACCwriteln(244);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN CABL
	p000e0279:
	{
 		if (skipdoall('p000e0279')) break p000e0279;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0279;
			if (!CNDnoun1(49)) break p000e0279;
 		}
		if (!CNDnotcarr(5)) break p000e0279;
 		ACCwriteln(245);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX BOAT
	p000e0280:
	{
 		if (skipdoall('p000e0280')) break p000e0280;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0280;
			if (!CNDnoun1(56)) break p000e0280;
 		}
		if (!CNDat(24)) break p000e0280;
		if (!CNDcarried(26)) break p000e0280;
		if (!CNDpresent(21)) break p000e0280;
		if (!CNDpresent(24)) break p000e0280;
		if (!CNDeq(18,10)) break p000e0280;
 		ACCwriteln(246);
 		ACCdestroy(21);
 		ACClet(18,20);
 		ACCswap(26,25);
 		ACCplus(30,5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX BOAT
	p000e0281:
	{
 		if (skipdoall('p000e0281')) break p000e0281;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0281;
			if (!CNDnoun1(56)) break p000e0281;
 		}
		if (!CNDat(24)) break p000e0281;
		if (!CNDpresent(24)) break p000e0281;
		if (!CNDcarried(26)) break p000e0281;
		if (!CNDabsent(21)) break p000e0281;
 		ACCwriteln(247);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX BOAT
	p000e0282:
	{
 		if (skipdoall('p000e0282')) break p000e0282;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0282;
			if (!CNDnoun1(56)) break p000e0282;
 		}
		if (!CNDat(24)) break p000e0282;
		if (!CNDpresent(24)) break p000e0282;
		if (!CNDabsent(26)) break p000e0282;
		if (!CNDcarried(21)) break p000e0282;
 		ACCwriteln(248);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX BOAT
	p000e0283:
	{
 		if (skipdoall('p000e0283')) break p000e0283;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0283;
			if (!CNDnoun1(56)) break p000e0283;
 		}
		if (!CNDat(24)) break p000e0283;
		if (!CNDpresent(24)) break p000e0283;
		if (!CNDabsent(21)) break p000e0283;
		if (!CNDabsent(26)) break p000e0283;
 		ACCwriteln(249);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX BOAT
	p000e0284:
	{
 		if (skipdoall('p000e0284')) break p000e0284;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0284;
			if (!CNDnoun1(56)) break p000e0284;
 		}
		if (!CNDat(24)) break p000e0284;
		if (!CNDgt(18,19)) break p000e0284;
 		ACCwriteln(250);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FOLL PRIN
	p000e0285:
	{
 		if (skipdoall('p000e0285')) break p000e0285;
 		if (in_response)
		{
			if (!CNDverb(57)) break p000e0285;
			if (!CNDnoun1(37)) break p000e0285;
 		}
		if (!CNDat(24)) break p000e0285;
 		ACCwriteln(251);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP TAR
	p000e0286:
	{
 		if (skipdoall('p000e0286')) break p000e0286;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0286;
			if (!CNDnoun1(59)) break p000e0286;
 		}
		if (!CNDnoun2(56)) break p000e0286;
		if (!CNDat(24)) break p000e0286;
		if (!CNDpresent(24)) break p000e0286;
		if (!CNDpresent(26)) break p000e0286;
		if (!CNDeq(18,10)) break p000e0286;
 		ACClet(18,20);
 		ACCswap(26,25);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ASHES OFF
	p000e0287:
	{
 		if (skipdoall('p000e0287')) break p000e0287;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0287;
			if (!CNDnoun1(72)) break p000e0287;
 		}
		if (!CNDatgt(8)) break p000e0287;
		if (!CNDatlt(16)) break p000e0287;
		if (!CNDnotat(12)) break p000e0287;
		if (!CNDnotat(13)) break p000e0287;
		if (!CNDcarried(0)) break p000e0287;
 		ACCset(0);
 		ACCswap(0,27);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ASHES OFF
	p000e0288:
	{
 		if (skipdoall('p000e0288')) break p000e0288;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0288;
			if (!CNDnoun1(72)) break p000e0288;
 		}
		if (!CNDcarried(27)) break p000e0288;
 		ACCwriteln(252);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASHES OFF
	p000e0289:
	{
 		if (skipdoall('p000e0289')) break p000e0289;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0289;
			if (!CNDnoun1(72)) break p000e0289;
 		}
		if (!CNDcarried(0)) break p000e0289;
 		ACCswap(0,27);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ASHES ON
	p000e0290:
	{
 		if (skipdoall('p000e0290')) break p000e0290;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0290;
			if (!CNDnoun1(78)) break p000e0290;
 		}
		if (!CNDcarried(27)) break p000e0290;
 		ACCswap(27,0);
 		ACCclear(0);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ASHES ON
	p000e0291:
	{
 		if (skipdoall('p000e0291')) break p000e0291;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0291;
			if (!CNDnoun1(78)) break p000e0291;
 		}
		if (!CNDcarried(0)) break p000e0291;
 		ACCwriteln(253);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASHES CIGA
	p000e0292:
	{
 		if (skipdoall('p000e0292')) break p000e0292;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0292;
			if (!CNDnoun1(95)) break p000e0292;
 		}
		if (!CNDcarried(44)) break p000e0292;
 		ACCwriteln(254);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE OFF
	p000e0293:
	{
 		if (skipdoall('p000e0293')) break p000e0293;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0293;
			if (!CNDnoun1(72)) break p000e0293;
 		}
		if (!CNDnoun2(63)) break p000e0293;
		if (!CNDatgt(8)) break p000e0293;
		if (!CNDatlt(16)) break p000e0293;
		if (!CNDnotat(12)) break p000e0293;
		if (!CNDnotat(13)) break p000e0293;
		if (!CNDcarried(0)) break p000e0293;
 		ACCswap(0,27);
 		ACCset(0);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// LOWE OFF
	p000e0294:
	{
 		if (skipdoall('p000e0294')) break p000e0294;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0294;
			if (!CNDnoun1(72)) break p000e0294;
 		}
		if (!CNDnoun2(63)) break p000e0294;
		if (!CNDcarried(27)) break p000e0294;
 		ACCwriteln(255);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LOWE OFF
	p000e0295:
	{
 		if (skipdoall('p000e0295')) break p000e0295;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0295;
			if (!CNDnoun1(72)) break p000e0295;
 		}
		if (!CNDnoun2(63)) break p000e0295;
		if (!CNDcarried(0)) break p000e0295;
 		ACCswap(0,27);
 		ACCok();
		break pro000_restart;
		{}

	}

	// JUMP U
	p000e0296:
	{
 		if (skipdoall('p000e0296')) break p000e0296;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0296;
			if (!CNDnoun1(9)) break p000e0296;
 		}
		if (!CNDat(29)) break p000e0296;
		if (!CNDnotcarr(34)) break p000e0296;
 		ACCwriteln(256);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// JUMP U
	p000e0297:
	{
 		if (skipdoall('p000e0297')) break p000e0297;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0297;
			if (!CNDnoun1(9)) break p000e0297;
 		}
		if (!CNDat(32)) break p000e0297;
		if (!CNDnotcarr(34)) break p000e0297;
 		ACCwriteln(257);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// JUMP RAVI
	p000e0298:
	{
 		if (skipdoall('p000e0298')) break p000e0298;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0298;
			if (!CNDnoun1(75)) break p000e0298;
 		}
		if (!CNDat(29)) break p000e0298;
		if (!CNDnotcarr(34)) break p000e0298;
 		ACCwriteln(258);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// JUMP RAVI
	p000e0299:
	{
 		if (skipdoall('p000e0299')) break p000e0299;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0299;
			if (!CNDnoun1(75)) break p000e0299;
 		}
		if (!CNDat(32)) break p000e0299;
		if (!CNDnotcarr(34)) break p000e0299;
 		ACCwriteln(259);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// JUMP RAVI
	p000e0300:
	{
 		if (skipdoall('p000e0300')) break p000e0300;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0300;
			if (!CNDnoun1(75)) break p000e0300;
 		}
		if (!CNDat(72)) break p000e0300;
 		ACCwriteln(260);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// JUMP RAVI
	p000e0301:
	{
 		if (skipdoall('p000e0301')) break p000e0301;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0301;
			if (!CNDnoun1(75)) break p000e0301;
 		}
		if (!CNDat(29)) break p000e0301;
		if (!CNDcarried(34)) break p000e0301;
 		ACCwriteln(261);
 		ACCpause(150);
 		function anykey00041() 
		{
 		ACCgoto(32);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00041);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// JUMP RAVI
	p000e0302:
	{
 		if (skipdoall('p000e0302')) break p000e0302;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0302;
			if (!CNDnoun1(75)) break p000e0302;
 		}
		if (!CNDat(32)) break p000e0302;
		if (!CNDcarried(34)) break p000e0302;
 		ACCwriteln(262);
 		ACCpause(150);
 		function anykey00042() 
		{
 		ACCgoto(29);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00042);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0303:
	{
 		if (skipdoall('p000e0303')) break p000e0303;
 		if (in_response)
		{
			if (!CNDverb(74)) break p000e0303;
 		}
 		ACCwriteln(263);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// :CARP ASHE
	p000e0304:
	{
 		if (skipdoall('p000e0304')) break p000e0304;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0304;
			if (!CNDnoun1(63)) break p000e0304;
 		}
		if (!CNDat(79)) break p000e0304;
		if (!CNDeq(116,20)) break p000e0304;
		if (!CNDeq(113,20)) break p000e0304;
		if (!CNDcarried(0)) break p000e0304;
 		ACCdestroy(0);
 		ACCwriteln(264);
 		ACClet(116,30);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SWIM _
	p000e0305:
	{
 		if (skipdoall('p000e0305')) break p000e0305;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0305;
 		}
		if (!CNDat(29)) break p000e0305;
 		ACCwriteln(265);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// SWIM _
	p000e0306:
	{
 		if (skipdoall('p000e0306')) break p000e0306;
 		if (in_response)
		{
			if (!CNDverb(77)) break p000e0306;
 		}
		if (!CNDat(32)) break p000e0306;
 		ACCwriteln(266);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// LOWE ON
	p000e0307:
	{
 		if (skipdoall('p000e0307')) break p000e0307;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0307;
			if (!CNDnoun1(78)) break p000e0307;
 		}
		if (!CNDnoun2(63)) break p000e0307;
		if (!CNDcarried(27)) break p000e0307;
 		ACCswap(27,0);
 		ACCclear(0);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// LOWE ON
	p000e0308:
	{
 		if (skipdoall('p000e0308')) break p000e0308;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0308;
			if (!CNDnoun1(78)) break p000e0308;
 		}
		if (!CNDnoun2(63)) break p000e0308;
		if (!CNDcarried(0)) break p000e0308;
 		ACCwriteln(267);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SPEC MATE
	p000e0309:
	{
 		if (skipdoall('p000e0309')) break p000e0309;
 		if (in_response)
		{
			if (!CNDverb(85)) break p000e0309;
			if (!CNDnoun1(86)) break p000e0309;
 		}
		if (!CNDat(1)) break p000e0309;
 		ACCgoto(74);
 		ACCdesc();
		break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNDO BOLT
	p000e0310:
	{
 		if (skipdoall('p000e0310')) break p000e0310;
 		if (in_response)
		{
			if (!CNDverb(88)) break p000e0310;
			if (!CNDnoun1(87)) break p000e0310;
 		}
		if (!CNDat(49)) break p000e0310;
		if (!CNDeq(22,10)) break p000e0310;
 		ACClet(22,15);
 		ACCok();
		break pro000_restart;
		{}

	}

	// UNDO BOLT
	p000e0311:
	{
 		if (skipdoall('p000e0311')) break p000e0311;
 		if (in_response)
		{
			if (!CNDverb(88)) break p000e0311;
			if (!CNDnoun1(87)) break p000e0311;
 		}
		if (!CNDat(49)) break p000e0311;
		if (!CNDgt(22,10)) break p000e0311;
 		ACCwriteln(268);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNDO SCRE
	p000e0312:
	{
 		if (skipdoall('p000e0312')) break p000e0312;
 		if (in_response)
		{
			if (!CNDverb(88)) break p000e0312;
			if (!CNDnoun1(117)) break p000e0312;
 		}
		if (!CNDat(57)) break p000e0312;
		if (!CNDcarried(51)) break p000e0312;
		if (!CNDlt(23,20)) break p000e0312;
 		ACClet(23,20);
 		ACCplus(30,2);
 		ACCok();
		break pro000_restart;
		{}

	}

	// UNDO SCRE
	p000e0313:
	{
 		if (skipdoall('p000e0313')) break p000e0313;
 		if (in_response)
		{
			if (!CNDverb(88)) break p000e0313;
			if (!CNDnoun1(117)) break p000e0313;
 		}
		if (!CNDpresent(51)) break p000e0313;
		if (!CNDgt(23,19)) break p000e0313;
 		ACCwriteln(269);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE SAND
	p000e0314:
	{
 		if (skipdoall('p000e0314')) break p000e0314;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0314;
			if (!CNDnoun1(79)) break p000e0314;
 		}
		if (!CNDat(77)) break p000e0314;
		if (!CNDeq(115,40)) break p000e0314;
		if (!CNDcarried(35)) break p000e0314;
 		ACCswap(35,43);
 		ACClet(115,50);
 		ACCplus(30,2);
 		ACCwriteln(270);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE WITC
	p000e0315:
	{
 		if (skipdoall('p000e0315')) break p000e0315;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0315;
			if (!CNDnoun1(83)) break p000e0315;
 		}
		if (!CNDat(77)) break p000e0315;
		if (!CNDeq(115,40)) break p000e0315;
		if (!CNDcarried(35)) break p000e0315;
 		ACCswap(35,43);
 		ACClet(115,50);
 		ACCplus(30,2);
 		ACCwriteln(271);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE CIGA
	p000e0316:
	{
 		if (skipdoall('p000e0316')) break p000e0316;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0316;
			if (!CNDnoun1(95)) break p000e0316;
 		}
		if (!CNDat(50)) break p000e0316;
		if (!CNDcarried(44)) break p000e0316;
		if (!CNDpresent(46)) break p000e0316;
 		ACCswap(44,45);
 		ACCdestroy(46);
 		ACCplus(30,2);
 		ACCwriteln(272);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE CHAI
	p000e0317:
	{
 		if (skipdoall('p000e0317')) break p000e0317;
 		if (in_response)
		{
			if (!CNDverb(96)) break p000e0317;
			if (!CNDnoun1(98)) break p000e0317;
 		}
		if (!CNDat(65)) break p000e0317;
		if (!CNDcarried(45)) break p000e0317;
		if (!CNDpresent(31)) break p000e0317;
 		ACCdestroy(31);
 		ACCswap(45,42);
 		ACClet(115,40);
 		ACCwriteln(273);
 		ACCplus(30,3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL AIR
	p000e0318:
	{
 		if (skipdoall('p000e0318')) break p000e0318;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0318;
			if (!CNDnoun1(11)) break p000e0318;
 		}
		if (!CNDpresent(1)) break p000e0318;
 		ACCget(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL AIR
	p000e0319:
	{
 		if (skipdoall('p000e0319')) break p000e0319;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0319;
			if (!CNDnoun1(11)) break p000e0319;
 		}
		if (!CNDpresent(2)) break p000e0319;
 		ACCget(2);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL AIR
	p000e0320:
	{
 		if (skipdoall('p000e0320')) break p000e0320;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0320;
			if (!CNDnoun1(11)) break p000e0320;
 		}
		if (!CNDpresent(3)) break p000e0320;
 		ACCget(3);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL AIR
	p000e0321:
	{
 		if (skipdoall('p000e0321')) break p000e0321;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0321;
			if (!CNDnoun1(11)) break p000e0321;
 		}
		if (!CNDpresent(4)) break p000e0321;
 		ACCget(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL HAND
	p000e0322:
	{
 		if (skipdoall('p000e0322')) break p000e0322;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0322;
			if (!CNDnoun1(14)) break p000e0322;
 		}
		if (!CNDat(4)) break p000e0322;
		if (!CNDlt(111,9)) break p000e0322;
 		ACCcreate(6);
 		ACClet(111,10);
 		ACCcreate(7);
 		ACCwriteln(274);
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL HAND
	p000e0323:
	{
 		if (skipdoall('p000e0323')) break p000e0323;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0323;
			if (!CNDnoun1(14)) break p000e0323;
 		}
		if (!CNDat(70)) break p000e0323;
		if (!CNDeq(111,30)) break p000e0323;
		if (!CNDpresent(8)) break p000e0323;
 		ACCwriteln(275);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL HAND
	p000e0324:
	{
 		if (skipdoall('p000e0324')) break p000e0324;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0324;
			if (!CNDnoun1(14)) break p000e0324;
 		}
		if (!CNDat(70)) break p000e0324;
		if (!CNDeq(111,20)) break p000e0324;
 		ACCswap(6,14);
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACClet(111,18);
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL HAND
	p000e0325:
	{
 		if (skipdoall('p000e0325')) break p000e0325;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0325;
			if (!CNDnoun1(14)) break p000e0325;
 		}
		if (!CNDat(63)) break p000e0325;
		if (!CNDeq(111,20)) break p000e0325;
 		ACClet(111,18);
 		ACCswap(6,23);
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL HAND
	p000e0326:
	{
 		if (skipdoall('p000e0326')) break p000e0326;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0326;
			if (!CNDnoun1(14)) break p000e0326;
 		}
		if (!CNDat(63)) break p000e0326;
		if (!CNDeq(111,40)) break p000e0326;
 		ACCwriteln(276);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL CRYS
	p000e0327:
	{
 		if (skipdoall('p000e0327')) break p000e0327;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0327;
			if (!CNDnoun1(23)) break p000e0327;
 		}
		if (!CNDcarried(5)) break p000e0327;
 		ACCwriteln(277);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL CRYS
	p000e0328:
	{
 		if (skipdoall('p000e0328')) break p000e0328;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0328;
			if (!CNDnoun1(23)) break p000e0328;
 		}
		if (!CNDat(21)) break p000e0328;
		if (!CNDeq(112,6)) break p000e0328;
 		ACCcreate(5);
 		ACCplus(30,4);
 		ACClet(112,1);
 		ACCget(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL CRYS
	p000e0329:
	{
 		if (skipdoall('p000e0329')) break p000e0329;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0329;
			if (!CNDnoun1(23)) break p000e0329;
 		}
		if (!CNDpresent(5)) break p000e0329;
 		ACCget(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL RED
	p000e0330:
	{
 		if (skipdoall('p000e0330')) break p000e0330;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0330;
			if (!CNDnoun1(25)) break p000e0330;
 		}
		if (!CNDat(37)) break p000e0330;
		if (!CNDnotzero(5)) break p000e0330;
		if (!CNDnotzero(6)) break p000e0330;
		if (!CNDzero(113)) break p000e0330;
		if (!CNDeq(114,10)) break p000e0330;
 		ACClet(113,10);
 		ACCplus(30,8);
 		ACCcreate(9);
 		ACCget(9);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL RED
	p000e0331:
	{
 		if (skipdoall('p000e0331')) break p000e0331;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0331;
			if (!CNDnoun1(25)) break p000e0331;
 		}
		if (!CNDat(37)) break p000e0331;
		if (!CNDnotzero(114)) break p000e0331;
		if (!CNDzero(113)) break p000e0331;
		if (!CNDzero(6)) break p000e0331;
 		ACCwriteln(278);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL GREE
	p000e0332:
	{
 		if (skipdoall('p000e0332')) break p000e0332;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0332;
			if (!CNDnoun1(26)) break p000e0332;
 		}
		if (!CNDat(79)) break p000e0332;
		if (!CNDeq(116,20)) break p000e0332;
		if (!CNDlt(113,20)) break p000e0332;
		if (!CNDeq(114,15)) break p000e0332;
 		ACCcreate(10);
 		ACCplus(30,8);
 		ACClet(113,20);
 		ACClet(114,20);
 		ACClet(28,14);
 		ACCpause(1);
 		function anykey00043() 
		{
 		ACCget(10);
		return;
 		ACCok();
		return;
		}
 		waitKey(anykey00043);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// COLL GREE
	p000e0333:
	{
 		if (skipdoall('p000e0333')) break p000e0333;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0333;
			if (!CNDnoun1(26)) break p000e0333;
 		}
		if (!CNDat(79)) break p000e0333;
		if (!CNDabsent(10)) break p000e0333;
		if (!CNDeq(113,20)) break p000e0333;
 		ACCwriteln(279);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL GREE
	p000e0334:
	{
 		if (skipdoall('p000e0334')) break p000e0334;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0334;
			if (!CNDnoun1(26)) break p000e0334;
 		}
		if (!CNDat(79)) break p000e0334;
		if (!CNDeq(116,10)) break p000e0334;
 		ACCwriteln(280);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL BLUE
	p000e0335:
	{
 		if (skipdoall('p000e0335')) break p000e0335;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0335;
			if (!CNDnoun1(27)) break p000e0335;
 		}
		if (!CNDat(22)) break p000e0335;
		if (!CNDpresent(43)) break p000e0335;
		if (!CNDnotcarr(43)) break p000e0335;
		if (!CNDeq(114,25)) break p000e0335;
 		ACCcreate(11);
 		ACCplus(30,8);
 		ACCdestroy(43);
 		ACClet(114,30);
 		ACClet(113,30);
 		ACCget(11);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL BLUE
	p000e0336:
	{
 		if (skipdoall('p000e0336')) break p000e0336;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0336;
			if (!CNDnoun1(27)) break p000e0336;
 		}
		if (!CNDat(22)) break p000e0336;
		if (!CNDeq(114,25)) break p000e0336;
 		ACCwriteln(281);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL PINK
	p000e0337:
	{
 		if (skipdoall('p000e0337')) break p000e0337;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0337;
			if (!CNDnoun1(28)) break p000e0337;
 		}
		if (!CNDat(57)) break p000e0337;
		if (!CNDeq(113,30)) break p000e0337;
		if (!CNDeq(114,35)) break p000e0337;
 		ACClet(113,40);
 		ACClet(114,40);
 		ACClet(28,4);
 		ACCpause(6);
 		function anykey00045() 
		{
 		ACCcreate(12);
 		ACCplus(30,8);
 		ACCwriteln(282);
 		ACCget(12);
		return;
 		ACCdone();
		return;
		}
 		function anykey00044() 
		{
 		ACClet(28,14);
 		ACCpause(1);
 		waitKey(anykey00045);
		}
 		waitKey(anykey00044);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// COLL KEY
	p000e0338:
	{
 		if (skipdoall('p000e0338')) break p000e0338;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0338;
			if (!CNDnoun1(35)) break p000e0338;
 		}
		if (!CNDat(57)) break p000e0338;
		if (!CNDpresent(56)) break p000e0338;
 		ACCwriteln(283);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL CHES
	p000e0339:
	{
 		if (skipdoall('p000e0339')) break p000e0339;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0339;
			if (!CNDnoun1(36)) break p000e0339;
 		}
		if (!CNDat(21)) break p000e0339;
 		ACCwriteln(284);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL BASK
	p000e0340:
	{
 		if (skipdoall('p000e0340')) break p000e0340;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0340;
			if (!CNDnoun1(41)) break p000e0340;
 		}
		if (!CNDat(45)) break p000e0340;
		if (!CNDpresent(19)) break p000e0340;
 		ACCdestroy(19);
 		ACCclear(115);
 		ACCcreate(17);
 		ACCcreate(18);
 		ACCget(17);
		if (!success) break pro000_restart;
 		ACCget(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL BASK
	p000e0341:
	{
 		if (skipdoall('p000e0341')) break p000e0341;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0341;
			if (!CNDnoun1(41)) break p000e0341;
 		}
		if (!CNDpresent(20)) break p000e0341;
 		ACCget(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL BAIT
	p000e0342:
	{
 		if (skipdoall('p000e0342')) break p000e0342;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0342;
			if (!CNDnoun1(42)) break p000e0342;
 		}
		if (!CNDat(1)) break p000e0342;
		if (!CNDeq(115,200)) break p000e0342;
 		ACCplus(30,2);
 		ACCcreate(18);
 		ACCclear(115);
 		ACCget(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL CAPT
	p000e0343:
	{
 		if (skipdoall('p000e0343')) break p000e0343;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0343;
			if (!CNDnoun1(45)) break p000e0343;
 		}
		if (!CNDat(45)) break p000e0343;
		if (!CNDpresent(16)) break p000e0343;
 		ACCwriteln(285);
 		ACCdestroy(16);
 		ACClet(115,10);
 		ACCcreate(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL CAPT
	p000e0344:
	{
 		if (skipdoall('p000e0344')) break p000e0344;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0344;
			if (!CNDnoun1(45)) break p000e0344;
 		}
		if (!CNDpresent(20)) break p000e0344;
 		ACCget(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL GODD
	p000e0345:
	{
 		if (skipdoall('p000e0345')) break p000e0345;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0345;
			if (!CNDnoun1(53)) break p000e0345;
 		}
		if (!CNDat(14)) break p000e0345;
 		ACCwriteln(286);
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// COLL BOAT
	p000e0346:
	{
 		if (skipdoall('p000e0346')) break p000e0346;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0346;
			if (!CNDnoun1(56)) break p000e0346;
 		}
		if (!CNDpresent(24)) break p000e0346;
 		ACCwriteln(287);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL BOAT
	p000e0347:
	{
 		if (skipdoall('p000e0347')) break p000e0347;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0347;
			if (!CNDnoun1(56)) break p000e0347;
 		}
		if (!CNDabsent(24)) break p000e0347;
 		ACCwriteln(288);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL TAR
	p000e0348:
	{
 		if (skipdoall('p000e0348')) break p000e0348;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0348;
			if (!CNDnoun1(59)) break p000e0348;
 		}
		if (!CNDat(39)) break p000e0348;
		if (!CNDabsent(25)) break p000e0348;
		if (!CNDabsent(26)) break p000e0348;
 		ACCwriteln(289);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL TAR
	p000e0349:
	{
 		if (skipdoall('p000e0349')) break p000e0349;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0349;
			if (!CNDnoun1(59)) break p000e0349;
 		}
		if (!CNDat(39)) break p000e0349;
		if (!CNDcarried(25)) break p000e0349;
 		ACCswap(25,26);
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL TAR
	p000e0350:
	{
 		if (skipdoall('p000e0350')) break p000e0350;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0350;
			if (!CNDnoun1(59)) break p000e0350;
 		}
		if (!CNDat(39)) break p000e0350;
		if (!CNDpresent(26)) break p000e0350;
 		ACCwriteln(290);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL EGG
	p000e0351:
	{
 		if (skipdoall('p000e0351')) break p000e0351;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0351;
			if (!CNDnoun1(93)) break p000e0351;
 		}
		if (!CNDat(22)) break p000e0351;
		if (!CNDabsent(43)) break p000e0351;
 		ACCwriteln(291);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL EGG
	p000e0352:
	{
 		if (skipdoall('p000e0352')) break p000e0352;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0352;
			if (!CNDnoun1(93)) break p000e0352;
 		}
		if (!CNDat(22)) break p000e0352;
		if (!CNDcarried(43)) break p000e0352;
 		ACCwriteln(292);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL EGG
	p000e0353:
	{
 		if (skipdoall('p000e0353')) break p000e0353;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0353;
			if (!CNDnoun1(93)) break p000e0353;
 		}
		if (!CNDat(22)) break p000e0353;
		if (!CNDpresent(43)) break p000e0353;
		if (!CNDnotcarr(43)) break p000e0353;
 		ACCget(43);
		if (!success) break pro000_restart;
 		ACCwriteln(293);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL PUDD
	p000e0354:
	{
 		if (skipdoall('p000e0354')) break p000e0354;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0354;
			if (!CNDnoun1(97)) break p000e0354;
 		}
		if (!CNDat(54)) break p000e0354;
		if (!CNDcarried(25)) break p000e0354;
		if (!CNDpresent(47)) break p000e0354;
 		ACCswap(25,50);
 		ACCswap(47,48);
 		ACCplus(30,1);
 		ACCwriteln(294);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLL PUDD
	p000e0355:
	{
 		if (skipdoall('p000e0355')) break p000e0355;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0355;
			if (!CNDnoun1(97)) break p000e0355;
 		}
		if (!CNDat(54)) break p000e0355;
		if (!CNDabsent(47)) break p000e0355;
 		ACCwriteln(295);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// I _
	p000e0356:
	{
 		if (skipdoall('p000e0356')) break p000e0356;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0356;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// COLL SCRE
	p000e0357:
	{
 		if (skipdoall('p000e0357')) break p000e0357;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0357;
			if (!CNDnoun1(117)) break p000e0357;
 		}
		if (!CNDat(78)) break p000e0357;
		if (!CNDzero(23)) break p000e0357;
 		ACCcreate(51);
 		ACCplus(30,2);
 		ACClet(23,10);
 		ACCget(51);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL MAGN
	p000e0358:
	{
 		if (skipdoall('p000e0358')) break p000e0358;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0358;
			if (!CNDnoun1(118)) break p000e0358;
 		}
		if (!CNDpresent(55)) break p000e0358;
 		ACCget(55);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL HAT
	p000e0359:
	{
 		if (skipdoall('p000e0359')) break p000e0359;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0359;
			if (!CNDnoun1(250)) break p000e0359;
 		}
		if (!CNDat(14)) break p000e0359;
		if (!CNDzero(17)) break p000e0359;
 		ACCcreate(22);
 		ACCplus(30,2);
 		ACClet(17,10);
 		ACCget(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL HAT
	p000e0360:
	{
 		if (skipdoall('p000e0360')) break p000e0360;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0360;
			if (!CNDnoun1(250)) break p000e0360;
 		}
		if (!CNDpresent(22)) break p000e0360;
 		ACCget(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// COLL _
	p000e0361:
	{
 		if (skipdoall('p000e0361')) break p000e0361;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0361;
 		}
 		ACCautog();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP AIR
	p000e0362:
	{
 		if (skipdoall('p000e0362')) break p000e0362;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0362;
			if (!CNDnoun1(11)) break p000e0362;
 		}
		if (!CNDcarried(1)) break p000e0362;
 		ACCdrop(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP AIR
	p000e0363:
	{
 		if (skipdoall('p000e0363')) break p000e0363;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0363;
			if (!CNDnoun1(11)) break p000e0363;
 		}
		if (!CNDcarried(2)) break p000e0363;
 		ACCdrop(2);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP AIR
	p000e0364:
	{
 		if (skipdoall('p000e0364')) break p000e0364;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0364;
			if (!CNDnoun1(11)) break p000e0364;
 		}
		if (!CNDcarried(3)) break p000e0364;
 		ACCdrop(3);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP AIR
	p000e0365:
	{
 		if (skipdoall('p000e0365')) break p000e0365;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0365;
			if (!CNDnoun1(11)) break p000e0365;
 		}
		if (!CNDcarried(4)) break p000e0365;
 		ACCdrop(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CRYS
	p000e0366:
	{
 		if (skipdoall('p000e0366')) break p000e0366;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0366;
			if (!CNDnoun1(23)) break p000e0366;
 		}
		if (!CNDcarried(5)) break p000e0366;
		if (!CNDeq(17,10)) break p000e0366;
 		ACCdrop(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CRYS
	p000e0367:
	{
 		if (skipdoall('p000e0367')) break p000e0367;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0367;
			if (!CNDnoun1(23)) break p000e0367;
 		}
		if (!CNDcarried(5)) break p000e0367;
		if (!CNDgt(17,10)) break p000e0367;
		if (!CNDlt(17,200)) break p000e0367;
		if (!CNDnotzero(8)) break p000e0367;
 		ACCdrop(5);
		if (!success) break pro000_restart;
 		ACClet(17,10);
 		ACCclear(8);
 		ACCwriteln(296);
 		ACCwriteln(297);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP CRYS
	p000e0368:
	{
 		if (skipdoall('p000e0368')) break p000e0368;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0368;
			if (!CNDnoun1(23)) break p000e0368;
 		}
		if (!CNDcarried(5)) break p000e0368;
		if (!CNDgt(17,10)) break p000e0368;
		if (!CNDlt(17,200)) break p000e0368;
		if (!CNDzero(8)) break p000e0368;
 		ACCdrop(5);
		if (!success) break pro000_restart;
 		ACClet(17,10);
 		ACCwriteln(298);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP CRYS
	p000e0369:
	{
 		if (skipdoall('p000e0369')) break p000e0369;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0369;
			if (!CNDnoun1(23)) break p000e0369;
 		}
		if (!CNDcarried(5)) break p000e0369;
		if (!CNDeq(17,200)) break p000e0369;
 		ACCdrop(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BASK
	p000e0370:
	{
 		if (skipdoall('p000e0370')) break p000e0370;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0370;
			if (!CNDnoun1(41)) break p000e0370;
 		}
		if (!CNDcarried(20)) break p000e0370;
 		ACCdrop(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP CAPT
	p000e0371:
	{
 		if (skipdoall('p000e0371')) break p000e0371;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0371;
			if (!CNDnoun1(45)) break p000e0371;
 		}
		if (!CNDcarried(20)) break p000e0371;
 		ACCdrop(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP TIN
	p000e0372:
	{
 		if (skipdoall('p000e0372')) break p000e0372;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0372;
			if (!CNDnoun1(60)) break p000e0372;
 		}
		if (!CNDcarried(26)) break p000e0372;
 		ACCswap(26,25);
 		ACCdrop(25);
		if (!success) break pro000_restart;
 		ACCwriteln(299);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP TIN
	p000e0373:
	{
 		if (skipdoall('p000e0373')) break p000e0373;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0373;
			if (!CNDnoun1(60)) break p000e0373;
 		}
		if (!CNDcarried(50)) break p000e0373;
 		ACCswap(50,25);
 		ACCdrop(25);
		if (!success) break pro000_restart;
 		ACCwriteln(300);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP ASHE
	p000e0374:
	{
 		if (skipdoall('p000e0374')) break p000e0374;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0374;
			if (!CNDnoun1(63)) break p000e0374;
 		}
		if (!CNDcarried(0)) break p000e0374;
		if (!CNDatlt(16)) break p000e0374;
		if (!CNDatgt(8)) break p000e0374;
		if (!CNDnotat(12)) break p000e0374;
		if (!CNDnotat(13)) break p000e0374;
 		ACCswap(0,27);
 		ACCdrop(27);
		if (!success) break pro000_restart;
 		ACCset(0);
 		ACCwriteln(301);
 		ACCpause(100);
 		function anykey00046() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00046);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP ASHE
	p000e0375:
	{
 		if (skipdoall('p000e0375')) break p000e0375;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0375;
			if (!CNDnoun1(63)) break p000e0375;
 		}
		if (!CNDcarried(0)) break p000e0375;
		if (!CNDnotat(11)) break p000e0375;
		if (!CNDnotat(10)) break p000e0375;
		if (!CNDnotat(14)) break p000e0375;
		if (!CNDnotat(9)) break p000e0375;
		if (!CNDnotat(15)) break p000e0375;
		if (!CNDlt(113,20)) break p000e0375;
 		ACCswap(0,27);
 		ACCdrop(27);
		if (!success) break pro000_restart;
 		ACCwriteln(302);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP ASHE
	p000e0376:
	{
 		if (skipdoall('p000e0376')) break p000e0376;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0376;
			if (!CNDnoun1(63)) break p000e0376;
 		}
		if (!CNDat(79)) break p000e0376;
		if (!CNDeq(116,20)) break p000e0376;
		if (!CNDeq(113,20)) break p000e0376;
		if (!CNDcarried(0)) break p000e0376;
 		ACCdestroy(0);
 		ACClet(116,30);
 		ACCwriteln(303);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP PAPE
	p000e0377:
	{
 		if (skipdoall('p000e0377')) break p000e0377;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0377;
			if (!CNDnoun1(94)) break p000e0377;
 		}
		if (!CNDat(22)) break p000e0377;
		if (!CNDcarried(43)) break p000e0377;
 		ACCdrop(43);
		if (!success) break pro000_restart;
 		ACCwriteln(304);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP MAGN
	p000e0378:
	{
 		if (skipdoall('p000e0378')) break p000e0378;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0378;
			if (!CNDnoun1(118)) break p000e0378;
 		}
		if (!CNDcarried(55)) break p000e0378;
 		ACCdrop(55);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP HAT
	p000e0379:
	{
 		if (skipdoall('p000e0379')) break p000e0379;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0379;
			if (!CNDnoun1(250)) break p000e0379;
 		}
		if (!CNDcarried(22)) break p000e0379;
 		ACCdrop(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP HAT
	p000e0380:
	{
 		if (skipdoall('p000e0380')) break p000e0380;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0380;
			if (!CNDnoun1(250)) break p000e0380;
 		}
		if (!CNDworn(22)) break p000e0380;
 		ACCwriteln(305);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP _
	p000e0381:
	{
 		if (skipdoall('p000e0381')) break p000e0381;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0381;
 		}
 		ACCautod();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO FACE
	p000e0382:
	{
 		if (skipdoall('p000e0382')) break p000e0382;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0382;
			if (!CNDnoun1(115)) break p000e0382;
 		}
		if (!CNDat(57)) break p000e0382;
		if (!CNDeq(23,20)) break p000e0382;
 		ACClet(23,30);
 		ACCcreate(54);
 		ACCwriteln(306);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO FACE
	p000e0383:
	{
 		if (skipdoall('p000e0383')) break p000e0383;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0383;
			if (!CNDnoun1(115)) break p000e0383;
 		}
		if (!CNDat(57)) break p000e0383;
		if (!CNDgt(23,20)) break p000e0383;
 		ACCwriteln(307);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO PLAT
	p000e0384:
	{
 		if (skipdoall('p000e0384')) break p000e0384;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0384;
			if (!CNDnoun1(120)) break p000e0384;
 		}
		if (!CNDat(57)) break p000e0384;
		if (!CNDeq(23,30)) break p000e0384;
		if (!CNDcarried(52)) break p000e0384;
 		ACCswap(52,55);
 		ACClet(23,40);
 		ACCplus(30,2);
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO HAT
	p000e0385:
	{
 		if (skipdoall('p000e0385')) break p000e0385;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0385;
			if (!CNDnoun1(250)) break p000e0385;
 		}
		if (!CNDgt(17,10)) break p000e0385;
		if (!CNDworn(22)) break p000e0385;
		if (!CNDnotzero(8)) break p000e0385;
 		ACCremove(22);
		if (!success) break pro000_restart;
 		ACCclear(8);
 		ACCwriteln(308);
 		ACCwriteln(309);
 		ACClet(17,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO HAT
	p000e0386:
	{
 		if (skipdoall('p000e0386')) break p000e0386;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0386;
			if (!CNDnoun1(250)) break p000e0386;
 		}
		if (!CNDworn(22)) break p000e0386;
		if (!CNDlt(17,20)) break p000e0386;
 		ACCremove(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO HAT
	p000e0387:
	{
 		if (skipdoall('p000e0387')) break p000e0387;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0387;
			if (!CNDnoun1(250)) break p000e0387;
 		}
		if (!CNDnotworn(22)) break p000e0387;
 		ACCwriteln(310);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO HAT
	p000e0388:
	{
 		if (skipdoall('p000e0388')) break p000e0388;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0388;
			if (!CNDnoun1(250)) break p000e0388;
 		}
		if (!CNDgt(17,10)) break p000e0388;
		if (!CNDzero(8)) break p000e0388;
		if (!CNDworn(22)) break p000e0388;
 		ACCremove(22);
		if (!success) break pro000_restart;
 		ACCwriteln(311);
 		ACClet(17,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO _
	p000e0389:
	{
 		if (skipdoall('p000e0389')) break p000e0389;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0389;
 		}
 		ACCautor();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR HAT
	p000e0390:
	{
 		if (skipdoall('p000e0390')) break p000e0390;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0390;
			if (!CNDnoun1(250)) break p000e0390;
 		}
		if (!CNDcarried(22)) break p000e0390;
 		ACCwear(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR _
	p000e0391:
	{
 		if (skipdoall('p000e0391')) break p000e0391;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0391;
 		}
 		ACCautow();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// I _
	p000e0392:
	{
 		if (skipdoall('p000e0392')) break p000e0392;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0392;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// LOOK _
	p000e0393:
	{
 		if (skipdoall('p000e0393')) break p000e0393;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0393;
 		}
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// Q _
	p000e0394:
	{
 		if (skipdoall('p000e0394')) break p000e0394;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0394;
 		}
 		ACCquit();
 		function anykey00047() 
		{
 		ACCscore();
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00047);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SAVE _
	p000e0395:
	{
 		if (skipdoall('p000e0395')) break p000e0395;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0395;
 		}
 		ACCsave();
		break pro000_restart;
		{}

	}

	// LOAD _
	p000e0396:
	{
 		if (skipdoall('p000e0396')) break p000e0396;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0396;
 		}
 		ACCload();
		break pro000_restart;
		{}

	}

	// NEW TYPE
	p000e0397:
	{
 		if (skipdoall('p000e0397')) break p000e0397;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0397;
			if (!CNDnoun1(109)) break p000e0397;
 		}
 		ACClet(28,8);
 		ACCpause(1);
 		function anykey00048() 
		{
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00048);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NORM TYPE
	p000e0398:
	{
 		if (skipdoall('p000e0398')) break p000e0398;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0398;
			if (!CNDnoun1(109)) break p000e0398;
 		}
 		ACClet(28,7);
 		ACCpause(1);
 		function anykey00049() 
		{
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00049);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PICT _
	p000e0399:
	{
 		if (skipdoall('p000e0399')) break p000e0399;
 		if (in_response)
		{
			if (!CNDverb(122)) break p000e0399;
 		}
 		ACClet(28,19);
 		ACCpause(10);
 		function anykey00050() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00050);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// WORD _
	p000e0400:
	{
 		if (skipdoall('p000e0400')) break p000e0400;
 		if (in_response)
		{
			if (!CNDverb(123)) break p000e0400;
 		}
 		ACClet(28,19);
 		ACCpause(255);
 		function anykey00051() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00051);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SCOR _
	p000e0401:
	{
 		if (skipdoall('p000e0401')) break p000e0401;
 		if (in_response)
		{
			if (!CNDverb(124)) break p000e0401;
 		}
 		ACCscore();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE CRYS
	p000e0402:
	{
 		if (skipdoall('p000e0402')) break p000e0402;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0402;
			if (!CNDnoun1(23)) break p000e0402;
 		}
		if (!CNDzero(113)) break p000e0402;
 		ACCwriteln(312);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE CRYS
	p000e0403:
	{
 		if (skipdoall('p000e0403')) break p000e0403;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0403;
			if (!CNDnoun1(23)) break p000e0403;
 		}
		if (!CNDeq(113,10)) break p000e0403;
 		ACCwriteln(313);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE CRYS
	p000e0404:
	{
 		if (skipdoall('p000e0404')) break p000e0404;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0404;
			if (!CNDnoun1(23)) break p000e0404;
 		}
		if (!CNDeq(113,20)) break p000e0404;
 		ACCwriteln(314);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE CRYS
	p000e0405:
	{
 		if (skipdoall('p000e0405')) break p000e0405;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0405;
			if (!CNDnoun1(23)) break p000e0405;
 		}
		if (!CNDeq(113,20)) break p000e0405;
 		ACCwriteln(315);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE BASK
	p000e0406:
	{
 		if (skipdoall('p000e0406')) break p000e0406;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0406;
			if (!CNDnoun1(41)) break p000e0406;
 		}
 		ACCwriteln(316);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE BAIT
	p000e0407:
	{
 		if (skipdoall('p000e0407')) break p000e0407;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0407;
			if (!CNDnoun1(42)) break p000e0407;
 		}
 		ACCwriteln(317);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE BOOK
	p000e0408:
	{
 		if (skipdoall('p000e0408')) break p000e0408;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0408;
			if (!CNDnoun1(48)) break p000e0408;
 		}
 		ACCwriteln(318);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE TIN
	p000e0409:
	{
 		if (skipdoall('p000e0409')) break p000e0409;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0409;
			if (!CNDnoun1(60)) break p000e0409;
 		}
 		ACCwriteln(319);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE WITC
	p000e0410:
	{
 		if (skipdoall('p000e0410')) break p000e0410;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0410;
			if (!CNDnoun1(83)) break p000e0410;
 		}
 		ACCwriteln(320);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE CIGA
	p000e0411:
	{
 		if (skipdoall('p000e0411')) break p000e0411;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0411;
			if (!CNDnoun1(95)) break p000e0411;
 		}
 		ACCwriteln(321);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE CHAI
	p000e0412:
	{
 		if (skipdoall('p000e0412')) break p000e0412;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0412;
			if (!CNDnoun1(98)) break p000e0412;
 		}
 		ACCwriteln(322);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE SPAN
	p000e0413:
	{
 		if (skipdoall('p000e0413')) break p000e0413;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0413;
			if (!CNDnoun1(126)) break p000e0413;
 		}
 		ACCwriteln(323);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0414:
	{
 		if (skipdoall('p000e0414')) break p000e0414;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0414;
 		}
		if (!CNDzero(26)) break p000e0414;
 		ACClet(26,10);
 		ACCwriteln(324);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0415:
	{
 		if (skipdoall('p000e0415')) break p000e0415;
 		if (in_response)
		{
			if (!CNDverb(125)) break p000e0415;
 		}
		if (!CNDnotzero(26)) break p000e0415;
 		ACCwriteln(325);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL TIN
	p000e0416:
	{
 		if (skipdoall('p000e0416')) break p000e0416;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0416;
			if (!CNDnoun1(60)) break p000e0416;
 		}
		if (!CNDat(39)) break p000e0416;
		if (!CNDabsent(25)) break p000e0416;
		if (!CNDabsent(26)) break p000e0416;
 		ACCwriteln(326);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL TIN
	p000e0417:
	{
 		if (skipdoall('p000e0417')) break p000e0417;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0417;
			if (!CNDnoun1(60)) break p000e0417;
 		}
		if (!CNDat(39)) break p000e0417;
		if (!CNDcarried(25)) break p000e0417;
 		ACCswap(25,26);
 		ACCok();
		break pro000_restart;
		{}

	}

	// FILL TIN
	p000e0418:
	{
 		if (skipdoall('p000e0418')) break p000e0418;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0418;
			if (!CNDnoun1(60)) break p000e0418;
 		}
		if (!CNDat(39)) break p000e0418;
		if (!CNDpresent(26)) break p000e0418;
 		ACCwriteln(327);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL TIN
	p000e0419:
	{
 		if (skipdoall('p000e0419')) break p000e0419;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0419;
			if (!CNDnoun1(60)) break p000e0419;
 		}
		if (!CNDat(54)) break p000e0419;
		if (!CNDcarried(25)) break p000e0419;
		if (!CNDpresent(47)) break p000e0419;
 		ACCswap(25,50);
 		ACCswap(47,48);
 		ACCplus(30,1);
 		ACCwriteln(328);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL TIN
	p000e0420:
	{
 		if (skipdoall('p000e0420')) break p000e0420;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0420;
			if (!CNDnoun1(60)) break p000e0420;
 		}
		if (!CNDat(54)) break p000e0420;
		if (!CNDabsent(47)) break p000e0420;
 		ACCwriteln(329);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL TIN
	p000e0421:
	{
 		if (skipdoall('p000e0421')) break p000e0421;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0421;
			if (!CNDnoun1(60)) break p000e0421;
 		}
		if (!CNDcarried(25)) break p000e0421;
		if (!CNDnotat(39)) break p000e0421;
		if (!CNDnotat(54)) break p000e0421;
 		ACCwriteln(330);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SAVE _
	p000e0422:
	{
 		if (skipdoall('p000e0422')) break p000e0422;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0422;
 		}
 		ACCramsave();
 		ACCpause(1);
 		function anykey00052() 
		{
		}
 		waitKey(anykey00052);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LOAD _
	p000e0423:
	{
 		if (skipdoall('p000e0423')) break p000e0423;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0423;
 		}
 		ACCload();
		break pro000_restart;
 		ACCpause(1);
 		function anykey00053() 
		{
		}
 		waitKey(anykey00053);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// _ _
	p000e0424:
	{
 		if (skipdoall('p000e0424')) break p000e0424;
 		ACChook(331);
		if (done_flag) break pro000_restart;
		{}

	}

	// RAMS _
	p000e0425:
	{
 		if (skipdoall('p000e0425')) break p000e0425;
 		if (in_response)
		{
			if (!CNDverb(150)) break p000e0425;
 		}
 		ACClet(28,21);
 		ACCpause(100);
 		function anykey00054() 
		{
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00054);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// RAML _
	p000e0426:
	{
 		if (skipdoall('p000e0426')) break p000e0426;
 		if (in_response)
		{
			if (!CNDverb(151)) break p000e0426;
 		}
 		ACClet(28,21);
 		ACCpause(50);
 		function anykey00055() 
		{
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		waitKey(anykey00055);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// _ _
	p000e0427:
	{
 		if (skipdoall('p000e0427')) break p000e0427;
 		ACChook(332);
		if (done_flag) break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
 		ACChook(333);
		if (done_flag) break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
		if (!CNDat(0)) break p001e0001;
 		ACCbclear(12,5);
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDislight()) break p001e0002;
 		ACClistobj();
 		ACClistnpc(getFlag(38));
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
		if (!CNDeq(31,0)) break p002e0000;
		if (!CNDeq(32,0)) break p002e0000;
 		ACCability(8,8);
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
 		ACChook(334);
		if (done_flag) break pro002_restart;
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
		if (!CNDgt(31,139)) break p002e0002;
		if (!CNDlt(31,150)) break p002e0002;
 		ACCwriteln(335);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0003:
	{
 		if (skipdoall('p002e0003')) break p002e0003;
		if (!CNDgt(31,149)) break p002e0003;
 		ACCwriteln(336);
 		ACCturns();
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0004:
	{
 		if (skipdoall('p002e0004')) break p002e0004;
		if (!CNDat(80)) break p002e0004;
 		ACCanykey();
 		function anykey00056() 
		{
 		ACCcls();
 		ACCscore();
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00056);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0005:
	{
 		if (skipdoall('p002e0005')) break p002e0005;
		if (!CNDat(0)) break p002e0005;
 		ACCanykey();
 		function anykey00057() 
		{
 		ACClet(115,200);
 		ACClet(112,20);
 		ACClet(17,200);
 		ACClet(27,200);
 		ACClet(19,210);
 		ACCgoto(81);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00057);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0006:
	{
 		if (skipdoall('p002e0006')) break p002e0006;
		if (!CNDeq(8,1)) break p002e0006;
 		ACCwriteln(337);
 		ACClet(17,20);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0007:
	{
 		if (skipdoall('p002e0007')) break p002e0007;
		if (!CNDat(65)) break p002e0007;
		if (!CNDeq(7,1)) break p002e0007;
 		ACCcreate(31);
 		ACCwriteln(338);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0008:
	{
 		if (skipdoall('p002e0008')) break p002e0008;
		if (!CNDat(74)) break p002e0008;
 		ACCanykey();
 		function anykey00060() 
		{
 		ACCgoto(81);
 		ACCdesc();
		return;
		}
 		function anykey00059() 
		{
 		ACClet(28,8);
 		ACCpause(10);
 		waitKey(anykey00060);
		}
 		function anykey00058() 
		{
 		ACCpause(100);
 		waitKey(anykey00059);
		}
 		waitKey(anykey00058);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0009:
	{
 		if (skipdoall('p002e0009')) break p002e0009;
		if (!CNDat(81)) break p002e0009;
 		ACCanykey();
 		function anykey00062() 
		{
 		ACCgoto(82);
 		ACCdesc();
		return;
		}
 		function anykey00061() 
		{
 		ACCpause(10);
 		waitKey(anykey00062);
		}
 		waitKey(anykey00061);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0010:
	{
 		if (skipdoall('p002e0010')) break p002e0010;
		if (!CNDat(82)) break p002e0010;
 		ACCanykey();
 		function anykey00064() 
		{
 		ACCgoto(83);
 		ACCdesc();
		return;
		}
 		function anykey00063() 
		{
 		ACCpause(10);
 		waitKey(anykey00064);
		}
 		waitKey(anykey00063);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0011:
	{
 		if (skipdoall('p002e0011')) break p002e0011;
		if (!CNDat(83)) break p002e0011;
 		ACCanykey();
 		function anykey00066() 
		{
 		ACCgoto(1);
 		ACCdesc();
		return;
		}
 		function anykey00065() 
		{
 		ACCpause(10);
 		waitKey(anykey00066);
		}
 		waitKey(anykey00065);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0012:
	{
 		if (skipdoall('p002e0012')) break p002e0012;
		if (!CNDlt(19,200)) break p002e0012;
		if (!CNDlt(7,1)) break p002e0012;
 		ACClet(19,10);
 		ACCdone();
		break pro002_restart;
		{}

	}


}
}

last_process = 2;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
    describe_location_flag = true;
    ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
    done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
     return (loc_here()!=locno);
}


function CNDatgt(locno)
{
     return (loc_here()>locno);
}


function CNDatlt(locno)
{
     return (loc_here()<locno);
}

function CNDpresent(objno)
{
    var loc = getObjectLocation(objno);
    if (loc == loc_here()) return true;
    if (loc == LOCATION_WORN) return true;
    if (loc == LOCATION_CARRIED) return true;
    if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
    {
        if (objectIsSupporter(loc)) return true;  // On supporter
        if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
        if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
    }
    return false;
}

function CNDabsent(objno)
{
    return !CNDpresent(objno);
}

function CNDworn(objno)
{
    return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
    return !CNDworn(objno);
}

function CNDcarried(objno)
{
    return (getObjectLocation(objno) == LOCATION_CARRIED);
}

function CNDnotcarr(objno)
{
    return !CNDcarried(objno);
}


function CNDchance(percent)
{
     var val = Math.floor((Math.random()*101));
     return (val<=percent);
}

function CNDzero(flagno)
{
    return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
     return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
    return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
    return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
    return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
    return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
    return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
    return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
     return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
    return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
    return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
    return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
    return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
    return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
    return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
    return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
    var count = 0;
    writeSysMessage(SYSMESS_YOUARECARRYING);
    ACCnewline();
    var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
    var i;
    for (i=0;i<num_objects;i++)
    {
        if ((getObjectLocation(i)) == LOCATION_CARRIED)
        {

            if ((listnpcs_with_objects) || (!objectIsNPC(i)))
            {
                writeObject(i);
                if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
                ACCnewline();
                count++;
            }
        }
        if (getObjectLocation(i) == LOCATION_WORN)
        {
            if (listnpcs_with_objects || (!objectIsNPC(i)))
            {
                writeObject(i);
                writeSysMessage(SYSMESS_WORN);
                count++;
                ACCnewline();
            }
        }
    }
    if (!count)
    {
         writeSysMessage(SYSMESS_CARRYING_NOTHING);
         ACCnewline();
    }

    if (!listnpcs_with_objects)
    {
        var numNPC = getNPCCountAt(LOCATION_CARRIED);
        if (numNPC) ACClistnpc(LOCATION_CARRIED);
    }
    done_flag = true;
}

function desc()
{
    describe_location_flag = true;
}


function ACCquit()
{
    inQUIT = true;
    writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
    $('.input').hide();
    inEND = true;
    writeSysMessage(SYSMESS_PLAYAGAIN);
    done_flag = true;
}


function done()
{
    done_flag = true;
}

function ACCok()
{
    writeSysMessage(SYSMESS_OK);
    done_flag = true;
}



function ACCramsave()
{
    ramsave_value = getSaveGameObject();
    var savegame_object = getSaveGameObject();
    savegame =   JSON.stringify(savegame_object);
    localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
    if (ramsave_value==null)
    {
        var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
        if (json_str)
        {
            savegame_object = JSON.parse(json_str.trim());
            restoreSaveGameObject(savegame_object);
            ACCdesc();
            focusInput();
            return;
        }
        else
        {
            writeText (STR_RAMLOAD_ERROR);
            ACCnewline();
            done_flag = true;
            return;
        }
    }
    restoreSaveGameObject(ramsave_value);
    ACCdesc();
}

function ACCsave()
{
    var savegame_object = getSaveGameObject();
    savegame =   JSON.stringify(savegame_object);
    filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
    if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
    ACCok();
}


function ACCload()
{
    var json_str;
    filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
    if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
    if (json_str)
    {
        savegame_object = JSON.parse(json_str.trim());
        restoreSaveGameObject(savegame_object);
    }
    else
    {
        writeSysMessage(SYSMESS_FILENOTFOUND);
        ACCnewline();
        done_flag = true; return;
    }
    ACCdesc();
    focusInput();
}



function ACCturns()
{
    var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
    writeSysMessage(SYSMESS_TURNS_START);
    writeText(turns + '');
    writeSysMessage(SYSMESS_TURNS_CONTINUE);
    if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
    writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
    var score = getFlag(FLAG_SCORE);
    writeSysMessage(SYSMESS_SCORE_START);
    writeText(score + '');
    writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
    clearScreen();
}

function ACCdropall()
{
    // Done in two different loops cause PAW did it like that, just a question of retro compatibility
    var i;
    for (i=0;i<num_objects;i++) if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
    for (i=0;i<num_objects;i++) if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
    objno = findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
    objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
    if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
    for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
    {
        if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
        {
            objno =findMatchingObject(i);
            if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
        }
    }
    success = false;
    writeSysMessage(SYSMESS_CANTSEETHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautod()
{
    var objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
    objno =findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
    success = false;
    writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautow()
{
    var objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
    objno =findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
    success = false;
    writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautor()
{
    var objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
    objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
    objno =findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
    success = false;
    writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
    ACCnewtext();
    ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);
 inPause = true;
 showAnykeyLayer();
}

function ACCgoto(locno)
{
    setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
    writeMessage(mesno);
    ACCnewline();
}


function ACCremove(objno)
{
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_CARRIED:
        case loc_here():
            writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case LOCATION_WORN:
            if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
            {
                writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
                ACCnewtext();
                ACCdone();
                return;
            }
            setObjectLocation(objno, LOCATION_CARRIED);
            writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
            success = true;
            break;

        default:
            writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;
    }
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
    if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
    {
        writeSysMessage(SYSMESS_CANTCARRYANYMORE);
        ACCnewtext();
        ACCdone();
        doall_flag = false;
        return;
    }
    var weight = 0;
    weight += getObjectWeight(objno);
    weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
    weight +=  getLocationObjectsWeight(LOCATION_WORN);
    if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
    {
        writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
        ACCnewtext();
        ACCdone();
        return;
    }
    setObjectLocation(objno, LOCATION_CARRIED);
    writeSysMessage(SYSMESS_YOUTAKEOBJECT);
    success = true;
}


 function ACCget(objno)
 {
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_CARRIED:
        case LOCATION_WORN:
            writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case loc_here():
            trytoGet(objno);
            break;

        default:
            if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
            {
                trytoGet(objno);
            }
            else
            {
                writeSysMessage(SYSMESS_CANTSEETHAT);
                ACCnewtext();
                ACCdone();
                return;
                break;
            }
    }
 }

function ACCdrop(objno)
{
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_WORN:
            writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case loc_here():
            writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;


        case LOCATION_CARRIED:
            setObjectLocation(objno, loc_here());
            writeSysMessage(SYSMESS_YOUDROPOBJECT);
            success = true;
            break;

        default:
            writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;
    }
}

function ACCwear(objno)
{
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_WORN:
            writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case loc_here():
            writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;


        case LOCATION_CARRIED:
            if (!objectIsWearable(objno))
            {
                writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
                ACCnewtext();
                ACCdone();
                return;
            }
            setObjectLocation(objno, LOCATION_WORN);
            writeSysMessage(SYSMESS_YOUWEAROBJECT);
            success = true;
            break;

        default:
            writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;
    }
}



function ACCdestroy(objno)
{
    setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
    setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
    var locno1 = getObjectLocation (objno1);
    var locno2 = getObjectLocation (objno2);
    ACCplace (objno1,locno2);
    ACCplace (objno2,locno1);
    setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
    setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
    setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
    setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
    var newval = getFlag(flagno) + value;
    setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
    setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
    setFlag(flagno,value);
}

function ACCnewline()
{
    writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
    writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
    writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
    setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
    setObjectLocation(objno2,getObjectLocation(objno1));
    setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
    setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
    setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
    var newval = getFlag(flagno1) + getFlag(flagno2);
    setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
    var newval = getFlag(flagno2) - getFlag(flagno1);
    if (newval < 0) newval = 0;
    setFlag(flagno2, newval);
}


function CNDparse()
{
    return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7));
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer)
    {
        writeText(' (');
        if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
        continouslisting = true;  // listing contents of container always continuous
    }

  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7));
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
    if (getObjectLocation(i) == locno)
        if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
          {
             writeText(getObjectText(i));
             if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
             progresscount++
             if (continouslisting)
             {
                    if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
                    if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
                    if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
             } else ACCnewline();
          };
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5));
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5));
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
    if (getObjectLocation(i) == locno)
        if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
          {
             writeText(getObjectText(i));
             progresscount++
             if (continouslisting)
             {
                if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
                if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
                if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
             } else ACCnewline();
          };
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
      writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
    if (procno > last_process)
    {
        writeText(STR_WRONG_PROCESS);
        ACCnewtext();
        ACCdone();
    }
    callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
    writeMessage(mesno);
}

function ACCmode(mode)
{
    setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
    setFlag(FLAG_TIMEOUT_LENGTH, length);
    setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
    doall_flag = true;
    if (locno == LOCATION_HERE) locno = loc_here();
    // Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
    setFlag(FLAG_DOALL_LOC, locno);
    var doall_obj;
    doall_loop:
    for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)
    {
        if (getObjectLocation(doall_obj) == locno)
            if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))
             if (!objectIsAttr(doall_obj, ATTR_CONCEALED))
              if (!objectIsAttr(doall_obj, ATTR_SCENERY))
                if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
                {
                    setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
                    setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
                    setReferredObject(doall_obj);
                    callProcess(process_in_doall);
                    if (describe_location_flag)
                        {
                            doall_flag = false;
                            entry_for_doall = '';
                            break doall_loop;
                        }
                }
    }
    doall_flag = false;
    entry_for_doall = '';
    if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
    setFlag(FLAG_PROMPT, value);
    setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
    var weight = getObjectWeight(objno);
    setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
    success = false;
    setReferredObject(objno);
    if (getObjectLocation(objno) == LOCATION_WORN)
    {
        writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectLocation(objno) == loc_here())
    {
        writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectLocation(objno) == LOCATION_CARRIED)
    {
        setObjectLocation(objno, locno);
        if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
        writeText(getObjectFixArticles(locno));
        writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
        success = true;
        return;
    }

    writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
    ACCnewtext();
    ACCdone();
}


function ACCtakeout(objno, locno)
{
    success = false;
    setReferredObject(objno);
    if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
    {
        writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectLocation(objno) == loc_here())
    {
        if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
        writeText(getObjectFixArticles(locno));
        writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
    {
        writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
    {
        writeSysMessage(SYSMESS_CANTCARRYANYMORE);
        ACCnewtext();
        ACCdone();
        return;
    }

    setObjectLocation(objno, LOCATION_CARRIED);
    writeSysMessage(SYSMESS_YOUTAKEOBJECT);
    success = true;


}
function ACCnewtext()
{
    player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
    setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
    setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
    var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
    var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
    var total_weight = weight_worn + weight_carried;
    setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
     setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
    var whatofound = getReferredObject();
    if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
    setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
    done_flag = false;
}

function ACCautop(locno)
{
    var objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
    objno = findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
    objno = findMatchingObject(null); // anywhere
    if (objno != EMPTY_OBJECT)
        {
            writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
            ACCnewtext();
            ACCdone();
            return;
        };

    success = false;
    writeSysMessage(SYSMESS_CANTDOTHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautot(locno)
{

    var objno =findMatchingObject(locno);
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
    objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
    objno = findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

    objno = findMatchingObject(null); // anywhere
    if (objno != EMPTY_OBJECT)
        {
            if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
            writeText(getObjectFixArticles(locno));
            writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
            ACCnewtext();
            ACCdone();
            return;
        };

    success = false;
    writeSysMessage(SYSMESS_CANTDOTHAT);
    ACCnewtext();
    ACCdone();

}


function CNDmove(flagno)
{
    var locno = getFlag(flagno);
    var dirno = getFlag(FLAG_VERB);
    var destination = getConnection( locno,  dirno);
    if (destination != -1)
        {
             setFlag(flagno, destination);
             return true;
        }
    return false;
}


function ACCextern(writeno)
{
    eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
    drawPicture(picno);
}



function ACCgraphic(option)
{
    graphicsON = (option==1);
    if (!graphicsON) hideGraphicsWindow();
}

function ACCbeep(sfxno, channelno, times)
{
    if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
    sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
    soundsON = (value==1);
    if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
    if (attrno > 63) return false;
    return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
    return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
    if (attrno > 63) return;
    if (attrno <= 31)
    {
        attrs = getObjectLowAttributes(objno);
        var attrs = bitset(attrs, attrno);
        setObjectLowAttributes(objno, attrs);
        return;
    }
    var attrs = getObjectHighAttributes(objno);
    attrno = attrno - 32;
    attrs = bitset(attrs, attrno);
    setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
    if (attrno > 63) return;
    if (attrno <= 31)
    {
        var attrs = getObjectLowAttributes(objno);
        attrs = bitclear(attrs, attrno);
        setObjectLowAttributes(objno, attrs);
        return;
    }
    var attrs = getObjectHighAttributes(objno);
    attrno = attrno - 32;
    attrs = bitclear(attrs, attrno);
    setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
    if (!isDarkHere()) return true;
    return lightObjectsPresent();
}



function CNDisnotlight()
{
    return ! CNDislight();
}

function ACCversion()
{
    writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
    writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
    writeWriteMessage(writeno);
    ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
    $('#transcript_area').html(transcript);
    $('.transcript_layer').show();
    inTranscript = true;
}

function ACCanykey()
{
    writeSysMessage(SYSMESS_PRESSANYKEY);
    inAnykey = true;
}

function ACCgetkey(flagno)
{
    getkey_return_flag = flagno;
    inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
    // Empty, PAW PC legacy, just does nothing
}


// From PAW Spectrum
function ACCreset()
{
    // Legacy condact, does nothing now
}


function ACCpaper(color)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
    // Legacy condact, does nothing now
}

function ACCsaveat()
{
    // Legacy condact, does nothing now
}

function ACCbackat()
{
    // Legacy condact, does nothing now
}

function ACCprintat()
{
    // Legacy condact, does nothing now
}

function ACCprotect()
{
    // Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
    // Legacy condact, does nothing now
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
    return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
    return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
    $('.block_layer').show();

}
//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp(); else SpanishHelp();
}	

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SAVE/RESTORE MY GAME?');
	writeText(STR_NEWLINE);
	writeText('Use SAVE/LOAD commands.');
	writeText(STR_NEWLINE + STR_NEWLINE);

}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en qué dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuáles hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX": EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿CÓMO LO SACO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿CÓMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO GRABO Y CARGO LA PARTIDA?');
	writeText(STR_NEWLINE);
	writeText('Usa las órdenes SAVE y LOAD, o GRABAR y CARGAR.');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  writeText(getExitsText(locno,mesno));
}

//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}
//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(gameName);
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("No saved games found."); else writelnText("No hay ninguna partida guardada.");
  }
}


// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights= function(n){
    if (n <= 0)
       return "";
    else if (n > String(this).length)
       return this;
    else {
       var iLen = String(this).length;
       return String(this).substring(iLen, iLen - n);
    }
}


String.prototype.firstToLower= function()
{
    return  this.charAt(0).toLowerCase() + this.slice(1);
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE () {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.indexOf('msie') == -1) return false;
  ieversion =  parseInt(myNav.split('msie')[1]);
  return (ieversion<10);
}


function runningLocal()
{
    return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance (a, b)
{
  if(a.length == 0) return b.length;
  if(b.length == 0) return a.length;

  var matrix = [];

  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }

  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }

  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }

  return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction)
{
    waitkey_callback_function.push(callbackFunction);
    showAnykeyLayer();
}

function waitKeyCallback()
{
    var callback = waitkey_callback_function.pop();
    if ( callback ) callback();
    if (describe_location_flag) descriptionLoop();
}


// Check DOALL entry

function skipdoall(entry)
{
    return  ((doall_flag==true) && (entry_for_doall!='') && (current_process==process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute()
{
    var value = nextFreeAttr;
    nextFreeAttr++;
    return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
    isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
    if (isPlural) return "P";
    isFemale = objectIsAttr(objno, ATTR_FEMALE);
    if (isFemale) return "F";
    isMale = objectIsAttr(objno, ATTR_MALE);
    if (isMale) return "M";
    return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
    var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
    var isFemale = objectIsAttr(objno, ATTR_FEMALE);
    var isMale = objectIsAttr(objno, ATTR_MALE);

    if (!isPlural)
    {
        if (isFemale) return "F";
        if (isMale) return "M";
        return "N"; // Neuter
    }
    else
    {
        if (isFemale) return "PF";
        if (isMale) return "PM";
        return "PN"; // Neuter plural
    }

}

function getLang()
{
    var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
    if (value) return "ES"; else return "EN";
}

function getObjectFixArticles(objno)
{
    var object_text = getObjectText(objno);
    var object_words = object_text.split(' ');
    if (object_words.length == 1) return object_text;
    var candidate = object_words[0];
    object_words.splice(0, 1);
    if (getLang()=='EN')
    {
        if ((candidate!='an') && (candidate!='a') && (candidate!='some')) return object_text;
        return 'the ' + object_words.join(' ');
    }
    else
    {
        if ( (candidate!='un') && (candidate!='una') && (candidate!='unos') && (candidate!='unas') && (candidate!='alguna') && (candidate!='algunos') && (candidate!='algunas') && (candidate!='algun')) return object_text;
        var gender = getAdvancedGender(objno);
        if (gender == 'F') return 'la ' + object_words.join(' ');
        if (gender == 'M') return 'el ' + object_words.join(' ');
        if (gender == 'N') return 'el ' + object_words.join(' ');
        if (gender == 'PF') return 'las ' + object_words.join(' ');
        if (gender == 'PM') return 'los ' + object_words.join(' ');
        if (gender == 'PN') return 'los ' + object_words.join(' ');
    }


}



// JS level log functions
function console_log(string)
{
    if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id)
{
    for (var i=0;i<resources.length;i++)
     if ((resources[i][0] == resource_type) && (resources[i][1]==id)) return resources[i][2];
    return false;
}

// Flag read/write functions
function getFlag(flagno)
{
     return flags[flagno];
}

function setFlag(flagno, value)
{
     flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
     return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2)
{
    connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno)
{
    return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno)
{
    return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno)
{
    return filterText(messages[mesno]);
}

function getSysMessageText(sysno)
{
    return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno)
{
    return filterText(writemessages[writeno]);
}

function getExitsText(locno,mesno)
{
  if ( locno === undefined ) return ''; // game hasn't fully initialised yet
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
        var exitcount = 0;
        for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
            var message = getMessageText(mesno);
            var exitcountprogress = 0;
            for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
            {
                exitcountprogress++;
                message += getMessageText(mesno + 2 + i);
                if (exitcountprogress == exitcount) message += getSysMessageText(SYSMESS_LISTEND);
                if (exitcountprogress == exitcount-1) message += getSysMessageText(SYSMESS_LISTLASTSEPARATOR);
                if (exitcountprogress <= exitcount-2) message += getSysMessageText(SYSMESS_LISTSEPARATOR);
          }
          return message;
      } else return getMessageText(mesno + 1);
  } else return getMessageText(mesno + 1);
}


// Location text functions
function getLocationText(locno)
{
    return  filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag)
{
    tagparams = tag.split('|');
    for (var tagindex=0;tagindex<tagparams.length-1;tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
    if (tagparams.length == 0) {writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return ''}

    var resolved_hook_value = h_sequencetag(tagparams);
    if (resolved_hook_value!='') return resolved_hook_value;

    switch(tagparams[0].toUpperCase())
    {
        case 'URL': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                    return '<a target="newWindow" href="' + tagparams[1]+ '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
                    break;
        case 'CLASS': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span class="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'STYLE': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span style="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'INK': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span style="color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'PAPER': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span style="background-color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'OBJECT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
                       break;
        case 'WEIGHT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
                       break;
        case 'OLOCATION': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                          if(objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
                          break;
        case 'MESSAGE':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
                       break;
        case 'SYSMESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
                       break;
        case 'LOCATION':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
        case 'EXITS':if (tagparams.length != 3 ) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       return getExitsText(/^@\d+/.test(tagparams[1]) ? getFlag(tagparams[1].substr(1)) : tagparams[1],parseInt(tagparams[2],10));
                       break;
        case 'PROCESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       callProcess(tagparams[1]);
                       return "";
                       break;
        case 'ACTION': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       return '<a href="type: ' + tagparams[1] + '" onmouseup="orderEnteredLoop(\'' + tagparams[1]+ '\');return false;">' + tagparams[2] + '</a>';
                       break;
        case 'RESTART': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return '<a href="javascript: void(0)" onmouseup="restart()">' + tagparams[1] + '</a>';
                        break;
        case 'EXTERN': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return '<a href="javascript: void(0)" onmouseup="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
                        break;
        case 'TEXTPIC': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        var style = '';
                        var post = '';
                        var pre = '';
                        align = tagparams[2];
                        switch(align)
                        {
                            case 1: style = 'float:left'; break;
                            case 2: style = 'float:right'; break;
                            case 3: post = '<br />';
                            case 4: pre='<center>';post='</center>';break;
                        }
                        return pre + "<img class='textpic' style='"+style+"' src='"+ RESOURCES_DIR + tagparams[1]+"' />" + post;
                        break;
        case 'HTML': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return tagparams[1];
                        break;
        case 'FLAG': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return getFlag(tagparams[1]);
                        break;
        case 'OREF': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                    if(objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
                    break;
        case 'TT':
        case 'TOOLTIP':
                    if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                    var title = $('<span>'+tagparams[1]+'</span>').text().replace(/'/g,"&apos;").replace(/\n/g, "&#10;");
                    var text = tagparams[2];
                    return "<span title='"+title+"'>"+text+"</span>";
                    break;
        case 'OPRO': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};  // returns the pronoun for a given object, used for english start database
                     switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT)))
                     {
                        case 'M' : return "him";
                        case "F" : return "her";
                        case "N" : return "it";
                        case "P" : return "them";  // plural returns them
                     }
                    break;

        default : return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
    }
}

function processTags(text)
{
    //Apply the {} tags filtering
    var pre, post, innerTag;
    tagfilter:
    while (text.indexOf('{') != -1)
    {
        if (( text.indexOf('}') == -1 ) || ((text.indexOf('}') < text.indexOf('{'))))
        {
            writeWarning(STR_INVALID_TAG_SEQUENCE + text);
            break tagfilter;
        }
        pre = text.substring(0,text.indexOf('{'));
        var openbracketcont = 1;
        pointer = text.indexOf('{') + 1;
        innerTag = ''
        while (openbracketcont>0)
        {
            if (text.charAt(pointer) == '{') openbracketcont++;
            if (text.charAt(pointer) == '}') openbracketcont--;
            if ( text.length <= pointer )
            {
                writeWarning(STR_INVALID_TAG_SEQUENCE + text);
                break tagfilter;
            }
            innerTag = innerTag + text.charAt(pointer);
            pointer++;
        }
        innerTag = innerTag.substring(0,innerTag.length - 1);
        post = text.substring(pointer);
        if (innerTag.indexOf('{') != -1 ) innerTag = processTags(innerTag);
        innerTag = implementTag(innerTag);
        text = pre + innerTag + post;
    }
    return text;
}

function filterText(text)
{
    // ngPAWS sequences
    text = processTags(text);


    // Superglus sequences (only \n remains)
    text = text.replace(/\n/g, STR_NEWLINE);

    // PAWS sequences (only underscore)
    objno = getFlag(FLAG_REFERRED_OBJECT);
    if ((objno != EMPTY_OBJECT) && (objects[objno]))    text = text.replace(/_/g,objects[objno].firstToLower()); else text = text.replace(/_/g,'');
    text = text.replace(/¬/g,' ');

    return text;
}


// Text Output functions
function writeText(text, skipAutoComplete)
{
    if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
    text = h_writeText(text); // hook
    $('.text').append(text);
    $('.text').scrollTop($('.text')[0].scrollHeight);
    addToTranscript(text);
    if (!skipAutoComplete) addToAutoComplete(text);
    focusInput();
}

function writeWarning(text)
{
    if (showWarnings) writeText(text)
}

function addToTranscript(text)
{
    transcript = transcript + text;
}

function writelnText(text, skipAutoComplete)
{
    if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
    writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno)
{
    if (messages[mesno]!=null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno)
{
        if (sysmessages[sysno]!=null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
        $(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno)
{
        writeText(getWriteMessageText(writeno));
}

function writeObject(objno)
{
    writeText(getObjectText(objno));
}

function clearTextWindow()
{
    $('.text').empty();
}


function clearInputWindow()
{
    $('.prompt').val('');
}


function writeLocation(locno)
{
    if (locations[locno]!=null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow()
{
    $('.graphics').empty();
}


function clearScreen()
{
    clearInputWindow();
    clearTextWindow();
    clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order)
{

    last_player_orders.push(player_order);
    last_player_orders_pointer = 0;
    clearInputWindow();
    writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order()
{
    if (!last_player_orders.length) return '';
    var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
    if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
    return last;
}

function get_next_player_order()
{
    if (!last_player_orders.length || last_player_orders_pointer == 0) return '';
    last_player_orders_pointer--;
    return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow()
{
        $('.text').removeClass('half_text');
        $('.text').addClass('all_text');
        $('.graphics').removeClass('half_graphics');
        $('.graphics').addClass('hidden');
        if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno)
{
    var pictureDraw = false;
    if (graphicsON)
    {
        if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
        var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
        if (filename)
        {
            $('.graphics').removeClass('hidden');
            $('.graphics').addClass('half_graphics');
            $('.text').removeClass('all_text');
            $('.text').addClass('half_text');
            $('.graphics').html('<img alt="" class="location_picture" src="' +  filename + '" />');
            $('.location_picture').css('height','100%');
            pictureDraw = true;
        }
    }

    if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
    $.each($('.graphics img'), function () {
        if ($(this)[0].className!= 'location_picture') $(this).remove();
    });

}

// Turns functions

function incTurns()
{
    turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH)  + 1;
    setFlag(FLAG_TURNS_LOW, turns % 256);
    setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput()
{
    $(".input").prop('disabled', true);
}

function enableInput()
{
    $(".input").prop('disabled', false);
}

function focusInput()
{
    $(".prompt").focus();
    timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno)
{
    if (attrno > 63) return false;
    var attrs = getObjectLowAttributes(objno);
    if (attrno > 31)
    {
        attrs = getObjectHighAttributes(objno);
        attrno = attrno - 32;
    }
    return bittest(attrs, attrno);
}

function isAccesibleContainer(objno)
{
    if (objectIsSupporter(objno)) return true;   // supporter
    if ( objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE) ) return true;  // No openable container
    if ( objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)  )  return true;  // No openable & open container
    return false;
}

//Objects and NPC functions

function findMatchingObject(locno)
{
    for (var i=0;i<num_objects;i++)
        if ((locno==-1) || (getObjectLocation(i) == locno))
         if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1))))  return i;
    return EMPTY_OBJECT;
}

function getReferredObject()
{
    var objectfound = EMPTY_OBJECT;
    refobject_search:
    {
        object_id = findMatchingObject(LOCATION_CARRIED);
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}

        object_id = findMatchingObject(LOCATION_WORN);
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}

        object_id = findMatchingObject(loc_here());
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}

        object_id = findMatchingObject(-1);
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}
    }
    return objectfound;
}


function getObjectLowAttributes(objno)
{
    return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno)
{
    return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs)
{
    objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs)
{
    objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno)
{
    if (objno > last_object_number)
        writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
    return objectsLocation[objno];
}

function setObjectLocation(objno, locno)
{
    if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
    objectsLocation[objno] = locno;
    if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS
function setReferredObject(objno)
{
    if (objno == EMPTY_OBJECT)
    {
        setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
        setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
        setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
        setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
        setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
        return;
    }
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
    setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
    setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
    setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno)
{
    var weight = objectsWeight[objno];
    if ( ((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight!=0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
        weight = weight + getLocationObjectsWeight(objno);
    return weight;
}


function getLocationObjectsWeight(locno)
{
    var weight = 0;
    for (var i=0;i<num_objects;i++)
    {
        if (getObjectLocation(i) == locno)
        {
            objweight = objectsWeight[i];
            weight += objweight;
            if (objweight > 0)
            {
                if (  (objectIsContainer(i)) || (objectIsSupporter(i)) )
                {
                    weight += getLocationObjectsWeight(i);
                }
            }
        }
    }
    return weight;
}

function getObjectCountAt(locno)
{
    var count = 0;
    for (i=0;i<num_objects;i++)
    {
        if (getObjectLocation(i) == locno)
        {
            attr = getObjectLowAttributes(i);
            if (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)) count ++;  // Parser settings say we should include NPCs as objects
             else if (!objectIsNPC(i)) count++;     // or object is not an NPC
        }
    }
    return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray)
{
    var count = 0;
    for (var i=0;i<num_objects;i++)
        if (getObjectLocation(i) == locno)
            for (var j=0;j<attrnoArray.length;j++)
                if (objectIsAttr(i, attrnoArray[j])) count++;
    return count;
}


function getNPCCountAt(locno)
{
    var count = 0;
    for (i=0;i<num_objects;i++)
        if ((getObjectLocation(i) == locno) &&  (objectIsNPC(i))) count++;
    return count;
}


// Location light function

function lightObjectsAt(locno)
{
    return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent()
{
  if (lightObjectsAt(LOCATION_CARRIED)) return true;
  if (lightObjectsAt(LOCATION_WORN)) return true;
  if (lightObjectsAt(loc_here())) return true;
  return false;
}


function isDarkHere()
{
    return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx()
{
    for (var i=0;i<resources.length;i++)
        if (resources[i][0] == 'RESOURCE_TYPE_SND')
        {
            var fileparts = resources[i][2].split('.');
            var basename = fileparts[0];
            var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] , preload: true} );
        }
}

function sfxplay(sfxno, channelno, times, method)
{

    if (!soundsON) return;
    if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
    if (times == 0) times = -1; // more than 4000 million times
    var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
    if (filename)
    {
        var fileparts = filename.split('.');
        var basename = fileparts[0];
        var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] });
        if (soundChannels[channelno]) soundChannels[channelno].stop();
        soundLoopCount[channelno] = times;
        mySound.bind("ended", function(e) {
            for (sndloop=0;sndloop<MAX_CHANNELS;sndloop++)
                if (soundChannels[sndloop] == this)
                {
                    if (soundLoopCount[sndloop]==-1) {this.play(); return }
                    soundLoopCount[sndloop]--;
                    if (soundLoopCount[sndloop] > 0) {this.play(); return }
                    sfxstop(sndloop);
                    return;
                }
        });
        soundChannels[channelno] = mySound;
        if (method=='play') mySound.play(); else mySound.fadeIn(2000);
    }
}

function playLocationMusic(locno)
{
    if (soundsON)
        {
            sfxstop(0);
            sfxplay(locno, 0, 0, 'play');
        }
}

function musicplay(musicno, times)
{
    sfxplay(musicno, 0, times);
}

function channelActive(channelno)
{
    if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall()
{
    for (channelno=0;channelno<MAX_CHANNELS;channelno++) sfxstop(channelno);

}


function sfxstop(channelno)
{
    if (soundChannels[channelno])
        {
            soundChannels[channelno].unbind('ended');
            soundChannels[channelno].stop();
            soundChannels[channelno] = null;
        }
}

function sfxvolume(channelno, value)
{
    if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor( value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno)
{
    if (!soundChannels[channelno]) return false;
    return true;
}


function sfxfadeout(channelno, value)
{
    if (!soundChannels[channelno]) return;
    soundChannels[channelno].fadeOut(value, function() { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno)
{
    if (inEND) return;
    current_process = procno;
    var prostr = procno.toString();
    while (prostr.length < 3) prostr = "0" + prostr;
    if (procno==0) in_response = true;
    if (doall_flag && in_response) done_flag = false;
    if (!in_response) done_flag = false;
    h_preProcess(procno);
    eval("pro" + prostr + "()");
    h_postProcess(procno);
    if (procno==0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno)
{
    mask = 1 << bitno;
    return ((value & mask) != 0);
}

function bitset(value, bitno)
{

    mask = 1 << bitno;
    return value | mask;
}

function bitclear(value, bitno)
{
    mask = 1 << bitno;
    return value & (~mask);
}


function bitneg(value, bitno)
{
    mask = 1 << bitno;
    return value ^ mask;

}

// Savegame functions
function getSaveGameObject()
{
    var savegame_object = new Object();
    // Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
    savegame_object.flags = flags.slice();
    savegame_object.objectsLocation = objectsLocation.slice();
    savegame_object.objectsWeight = objectsWeight.slice();
    savegame_object.objectsAttrLO = objectsAttrLO.slice();
    savegame_object.objectsAttrHI = objectsAttrHI.slice();
    savegame_object.connections = connections.slice();
    savegame_object.last_player_orders = last_player_orders.slice();
    savegame_object.last_player_orders_pointer = last_player_orders_pointer;
    savegame_object.transcript = transcript;
    savegame_object = h_saveGame(savegame_object);
    return savegame_object;
}

function restoreSaveGameObject(savegame_object)
{
    flags = savegame_object.flags;
    // Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
    objectsLocation = savegame_object.objectsLocation.slice();
    objectsWeight = savegame_object.objectsWeight.slice();
    objectsAttrLO = savegame_object.objectsAttrLO.slice();
    objectsAttrHI = savegame_object.objectsAttrHI.slice();
    connections = savegame_object.connections.slice();
    last_player_orders = savegame_object.last_player_orders.slice();
    last_player_orders_pointer = savegame_object.last_player_orders_pointer;
    transcript = savegame_object.transcript;
    h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes()
{

    var swapped;

    for (var j=0;j<vocabulary.length;j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
             pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
    // Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
    do {
        swapped = false;
        for (var i=0; i < pronoun_suffixes.length-1; i++)
        {
            if (pronoun_suffixes[i].length < pronoun_suffixes[i+1].length)
            {
                var temp = pronoun_suffixes[i];
                pronoun_suffixes[i] = pronoun_suffixes[i+1];
                pronoun_suffixes[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein)
{
    // Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
    //          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
    //          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

    // Search word in vocabulary
    for (var j=0;j<vocabulary.length;j++)
        if (vocabulary[j][VOCABULARY_WORD] == word)
             return vocabulary[j];

    if (forceDisableLevenshtein) return null;

    if (word.length <=4) return null; // Don't try to fix typo for words with less than 5 length

    if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

    // Search words in vocabulary with a Levenshtein distance of 1
    var distance2_match = null;
    for (var k=0;k<vocabulary.length;k++)
    {
        if ([WORDTYPE_VERB,WORDTYPE_NOUN,WORDTYPE_ADJECT,WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE])  != -1 )
        {
            var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
            if ((!distance2_match) && (distance==2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
            if (distance <= 1) return vocabulary[k];
        }
    }

    // If we found any word with distance 2, return it, only if word was at least 7 characters long
    if ((distance2_match) &&  (word.length >6)) return distance2_match;

    // Word not found
    return null;
}

function normalize(player_order)
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
    var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
    var i;
    var output = '';
    var pos;

    for (i=0;i<player_order.length;i++)
    {
        pos = originalchars.indexOf(player_order.charAt(i));
        if (pos!=-1) output = output + "aeiou".charAt(pos % 5); else
        {
            ch = player_order.charAt(i);
                if ((ch=='.') || (ch==',') || (ch==';') || (ch=='"') || (ch=='\'') || (ch=='«') || (ch=='»')) output = output + ' , '; else output = output + player_order.charAt(i);
        }

    }
    return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
     player_order = normalize(player_order);
     player_order = player_order.toUpperCase();

     var words = player_order.split(' ');
     for (var q=0;q<words.length;q++)
     {
        words[q] = words[q].trim();
        if  (words[q]!=',')
        {
            words[q] = words[q].trim();
            foundWord = findVocabulary(words[q], false);
            if (foundWord)
            {
                if (foundWord[VOCABULARY_TYPE]==WORDTYPE_CONJUNCTION)
                {
                words[q] = ','; // Replace conjunctions with commas
                }
            }
        }
     }

     var output = '';
     for (q=0;q<words.length;q++)
     {
        if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
     }
     output = output.replace(/ ,/g,',');
     output = output.trim();
     player_order_buffer = output;
}

function getSentencefromBuffer()
{
    var sentences = player_order_buffer.split(',');
    var result = sentences[0];
    sentences.splice(0,1);
    player_order_buffer = sentences.join();
    return result;
}

function processPronounSufixes(words)
{
    // This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
    // it's only for spanish so if lang is english then it makes no changes
    if (getLang() == 'EN') return words;
    var verbFound = false;
    if (!bittest(getFlag(FLAG_PARSER_SETTINGS),0)) return words;  // If pronoun sufixes inactive, just do nothing
    // First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
    var filtered_words = [];
    for (var q=0;q < words.length;q++)
    {
        foundWord = findVocabulary(words[q], false);
        if (foundWord)
            {
                if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
            }
            else filtered_words[filtered_words.length] = words[q];
    }
    words = filtered_words;

    // Now let's start trying to get sufixes
    new_words = [];
    for (var k=0;k < words.length;k++)
    {
        words[k] = words[k].trim();
        foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
        if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
        suffixFound = false;
        pronunsufix_search:
        for (var l=0;(l<pronoun_suffixes.length) && (!suffixFound) && (!verbFound);l++)
        {

            if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length))
            {
                var verb_part = words[k].substring(0,words[k].length - pronoun_suffixes[l].length);
                var checkWord = findVocabulary(verb_part, false);
                if ((!checkWord)  || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
                {
                    new_words.push(words[k]);
                    continue pronunsufix_search;
                }
                new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
                new_words.push(pronoun_suffixes[l]);
                suffixFound = true;
                verbFound = true;
            }
        }
        if (!suffixFound) new_words.push(words[k]);
    }
    return new_words;
}

function getLogicSentence()
{
    parser_word_found = false; ;
    aux_verb = -1;
    aux_noun1 = -1;
    aux_adject1 = -1;
    aux_adverb = -1;
    aux_pronoun = -1
    aux_pronoun_adject = -1
    aux_preposition = -1;
    aux_noun2 = -1;
    aux_adject2 = -1;
    initializeLSWords();
    SL_found = false;

    var order = getSentencefromBuffer();
    setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS),1)); // Initialize flag that says an unknown word was found in the sentence


    words = order.split(" ");
    words = processPronounSufixes(words);
    wordsearch_loop:
    for (var i=0;i<words.length;i++)
    {
        original_word = currentword = words[i];
        if (currentword.length>10) currentword = currentword.substring(0,MAX_WORD_LENGHT);
        foundWord = findVocabulary(currentword, false);
        if (foundWord)
        {
            wordtype = foundWord[VOCABULARY_TYPE];
            word_id = foundWord[VOCABULARY_ID];

            switch (wordtype)
            {
                case WORDTYPE_VERB: if (aux_verb == -1)  aux_verb = word_id;
                                    break;

                case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
                                    break;

                case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
                                      break;

                case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
                                      break;

                case WORDTYPE_PRONOUN: if (aux_pronoun == -1)
                                            {
                                                aux_pronoun = word_id;
                                                if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1))
                                                {
                                                    aux_noun1 = previous_noun;
                                                    if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
                                                }
                                            }

                                       break;

                case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security

                case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
                                           if (aux_noun1!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),2));  // Set bit that determines that a preposition word was found after first noun
                                           break;
            }

            // Nouns that can be converted to verbs
            if ((aux_noun1!=-1) && (aux_verb==-1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS))
            {
                aux_verb = aux_noun1;
                aux_noun1 = -1;
            }

            if ((aux_verb==-1) && (aux_noun1!=-1) && (previous_verb!=EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

            if ((aux_verb!=-1) || (aux_noun1!=-1) || (aux_adject1!=-1 || (aux_preposition!=-1) || (aux_adverb!=-1))) SL_found = true;



        } else if (aux_verb!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),1));  // Set bit that determines that an unknown word was found after the verb
    }

    if (SL_found)
    {
        if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
        if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
        if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
        if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
        if (aux_pronoun != -1)
            {
                setFlag(FLAG_PRONOUN, aux_noun1);
                setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
            }
            else
            {
                setFlag(FLAG_PRONOUN, EMPTY_WORD);
                setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
            }
        if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
        if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
        if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
        setReferredObject(getReferredObject());
        previous_verb = aux_verb;
        if ((aux_noun1!=-1) && (aux_noun1>=NUM_PROPER_NOUNS))
        {
            previous_noun = aux_noun1;
            if (aux_adject1!=-1) previous_adject = aux_adject1;
        }

    }
    if ((aux_verb + aux_noun1+ aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

    return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt()
{
    interruptDisabled = false;
}

function disableInterrupt()
{
    interruptDisabled = true;
}

function timer()
{
    // Timeout control
    timeout_progress=  timeout_progress + 1/32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
    timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
    if ((timeout_length) && (timeout_progress> timeout_length))  // time for timeout
    {
        timeout_progress = 0;
        if (($('.prompt').val() == '')  || (($('.prompt').val()!='') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS),0))) )  // but first check there is no text type, or is allowed to timeout when text typed already
        {
            setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
            writeSysMessage(SYSMESS_TIMEOUT);
            callProcess(PROCESS_TURN);
        }
    }

    // PAUSE condact control
    if (inPause)
    {
        pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
        if (pauseRemainingTime<=0)
        {
            inPause = false;
            hideAnykeyLayer();
            waitKeyCallback()
        }
    }

    // Interrupt process control
    if (!interruptDisabled)
    if (interruptProcessExists)
    {
        callProcess(interrupt_proc);
        setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened
    }

}

// Initialize and finalize functions

function farewell()
{
    writeSysMessage(SYSMESS_FAREWELL);
    ACCnewline();
}


function initializeConnections()
{
  connections = [].concat(connections_start);
}

function initializeObjects()
{
  for (i=0;i<objects.length;i++)
  {
    objectsAttrLO = [].concat(objectsAttrLO_start);
    objectsAttrHI = [].concat(objectsAttrHI_start);
    objectsLocation = [].concat(objectsLocation_start);
    objectsWeight = [].concat(objectsWeight_start);
  }
}

function  initializeLSWords()
{
  setFlag(FLAG_PREP,EMPTY_WORD);
  setFlag(FLAG_NOUN2,EMPTY_WORD);
  setFlag(FLAG_ADJECT2,EMPTY_WORD);
  setFlag(FLAG_PRONOUN,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_VERB,EMPTY_WORD);
  setFlag(FLAG_NOUN1,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_ADVERB,EMPTY_WORD);
}


function initializeFlags()
{
  flags = [];
  for (var  i=0;i<FLAG_COUNT;i++) flags.push(0);
  setFlag(FLAG_MAXOBJECTS_CARRIED,4);
  setFlag(FLAG_PARSER_SETTINGS,9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
  setFlag(FLAG_MAXWEIGHT_CARRIED,10);
  initializeLSWords();
  setFlag(FLAG_OBJECT_LIST_FORMAT,64); // List objects in a single sentence (comma separated)
  setFlag(FLAG_OBJECTS_CARRIED_COUNT,carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars()
{
    num_objects = last_object_number + 1;
    transcript = '';
    timeout_progress = 0;
    previous_noun = EMPTY_WORD;
    previous_verb = EMPTY_WORD;
    previous_adject = EMPTY_WORD;
    player_order_buffer = '';
    last_player_orders = [];
    last_player_orders_pointer = 0;
    graphicsON = true;
    soundsON = true;
    interruptDisabled = false;
    unblock_process = null;
    done_flag = false;
    describe_location_flag =false;
    in_response = false;
    success = false;
    doall_flag = false;
    entry_for_doall = '';
}

function initializeSound()
{
    sfxstopall();
}




function initialize()
{
    preloadsfx();
    initializeInternalVars();
    initializeSound();
    initializeFlags();
    initializeObjects();
    initializeConnections();
}



// Main loops

function descriptionLoop()
{
    do
    {
        describe_location_flag = false;
        if (!getFlag(FLAG_MODE)) clearTextWindow();
        if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here());
        h_description_init();
        playLocationMusic(loc_here());
        if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
        ACCminus(FLAG_AUTODEC2,1);
        if (isDarkHere()) ACCminus(FLAG_AUTODEC3,1);
        if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC4,1);
        callProcess(PROCESS_DESCRIPTION);
        h_description_post();
        if (describe_location_flag) continue; // descriptionLoop() again without nesting
        describe_location_flag = false;
        callProcess(PROCESS_TURN);
        if (describe_location_flag) continue;
        describe_location_flag = false;
        focusInput();
        break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
    } while (true);

}

function orderEnteredLoop(player_order)
{
    previous_verb = EMPTY_WORD;
    setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
    if (player_order == '') {writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };
    player_order = h_playerOrder(player_order); //hook
    copyOrderToTextWindow(player_order);
    toParserBuffer(player_order);
    do
    {
        describe_location_flag = false;
        ACCminus(FLAG_AUTODEC5,1);
        ACCminus(FLAG_AUTODEC6,1);
        ACCminus(FLAG_AUTODEC7,1);
        ACCminus(FLAG_AUTODEC8,1);
        if (isDarkHere()) ACCminus(FLAG_AUTODEC9,1);
        if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC10,1);

        if (describe_location_flag)
        {
            descriptionLoop();
            return;
        };

        if (getLogicSentence())
        {
            incTurns();
            done_flag = false;
            callProcess(PROCESS_RESPONSE); // Response table
            if (describe_location_flag)
            {
                descriptionLoop();
                return;
            };
            if (!done_flag)
            {
                if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION)))
                {
                    descriptionLoop();
                    return;
                } else if (getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) {writeSysMessage(SYSMESS_WRONGDIRECTION);ACCnewline();} else {writeSysMessage(SYSMESS_CANTDOTHAT);ACCnewline();};

            }
        } else
        {
            h_invalidOrder(player_order);
            if (parser_word_found) {writeSysMessage(SYSMESS_IDONTUNDERSTAND);   ACCnewline() }
                              else {writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };
        }
        callProcess(PROCESS_TURN);
    } while (player_order_buffer !='');
    previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
    focusInput();
}


function restart()
{
    location.reload();
}


function hideBlock()
{
    clearInputWindow();
    $('.block_layer').hide('slow');
    enableInterrupt();
    $('.input').show();
    focusInput();
}

function hideAnykeyLayer()
{
    $('.anykey_layer').hide();
    $('.input').show();
    focusInput();
}

function showAnykeyLayer()
{
    $('.anykey_layer').show();
    $('.input').hide();
}

//called when the block layer is closed
function closeBlock()
{
    if (!inBlock) return;
    inBlock = false;
    hideBlock();
    var proToCall = unblock_process;
    unblock_process = null;
    callProcess(proToCall);
    if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder()
{
    var prompt_msg = getFlag(FLAG_PROMPT);
    if (!prompt_msg)
    {
        var random = Math.floor((Math.random()*100));
        if (random<30) prompt_msg = SYSMESS_PROMPT0; else
        if ((random>=30) && (random<60)) prompt_msg = SYSMESS_PROMPT1; else
        if ((random>=60) && (random<90)) prompt_msg = SYSMESS_PROMPT2; else
        if (random>=90) prompt_msg = SYSMESS_PROMPT3;
    }
    $('.prompt').attr('placeholder', $('<div>'+getSysMessageText(prompt_msg).replace(/(?:<br>)*$/,'').replace( /<br>/g, ', ' )+'</div>').text());
}


function divTextScrollUp()
{
    var currentPos = $('.text').scrollTop();
    if (currentPos>=DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop(0);
}

function divTextScrollDown()
{
    var currentPos = $('.text').scrollTop();
    if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop($('.text')[0].scrollHeight);
}

// Autocomplete functions

function predictiveText(currentText)
{
    if (currentText == '') return currentText;
    var wordToComplete;
    var words = currentText.split(' ');
    if (autocompleteStep!=0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length-1];
    words[words.length-1] = completedWord(wordToComplete);
    return words.join(' ');
}


function initAutoComplete()
{
    for (var j=0;j<vocabulary.length;j++)
        if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
            if (vocabulary[j][VOCABULARY_WORD].length >= 3)
                autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence)
{
    var words = sentence.split(' ');
    for (var i=0;i<words.length;i++)
    {
        var finalWord = '';
        for (var j=0;j<words[i].length;j++)
        {
            var c = words[i][j].toLowerCase();
            if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
            else break;
        }

        if (finalWord.length>=3)
        {
            var index = autocomplete.indexOf(finalWord);
            if (index!=-1) autocomplete.splice(index,1);
            autocomplete.push(finalWord);
        }
    }
}

function completedWord(word)
{
    if (word=='') return '';
   autocompleteBaseWord  =word;
   var foundCount = 0;
   for (var i = autocomplete.length-1;i>=0; i--)
   {
      if (autocomplete[i].length > word.length)
         if (autocomplete[i].indexOf(word)==0)
            {
                foundCount++;
                if (foundCount>autocompleteStep)
                {
                    autocompleteStep++;
                    return autocomplete[i];
                }
            }
   }
   return word;
}


// Exacution starts here, called by the html file on document.ready()
function start()
{
    h_init(); //hook
    $('.graphics').addClass('half_graphics');
    $('.text').addClass('half_text');
    if (isBadIE()) alert(STR_BADIE)
    loadPronounSufixes();
    setInputPlaceHolder();
    initAutoComplete();

    // Assign keypress action for input box (detect enter key press)
    $('.prompt').keypress(function(e) {
        if (e.which == 13)
        {
            setInputPlaceHolder();
            player_order = $('.prompt').val();
            if (player_order.charAt(0) == '#')
            {
                addToTranscript(player_order + STR_NEWLINE);
                clearInputWindow();
            }
            else
            if (player_order!='')
                    orderEnteredLoop(player_order);
        }
    });

    // Assign arrow up key press to recover last order
    $('.prompt').keyup( function(e) {
        if (e.which  == 38) $('.prompt').val(get_prev_player_order());
        if (e.which  == 40) $('.prompt').val(get_next_player_order());
    });


    // Assign tab keydown to complete word
    $('.prompt').keydown( function(e) {
        if (e.which == 9)
            {
                $('.prompt').val(predictiveText($('.prompt').val()));
                e.preventDefault();
            } else
            {
                autocompleteStep = 0;
                autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
            }
    });

    //Detect resize to change flag 12
     $(window).resize(function () {
        setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened
        clearPictureAt();
        return;
     });


     // assign any click on block layer --> close it
     $(document).click( function(e) {

    // if waiting for END response
    if (inEND)
    {
        restart();
        return;
    }

        if (inBlock)
        {
            closeBlock();
            e.preventDefault();
            return;
        }

        if (inAnykey)  // return for ANYKEY, accepts mouse click
        {
            inAnykey = false;
            hideAnykeyLayer();
            waitKeyCallback();
            e.preventDefault();
            return;
        }

     });

     //Make tap act as click
    document.addEventListener('touchstart', function(e) {$(document).click(); }, false);


    $(document).keydown(function(e) {

        if (!h_keydown(e)) return; // hook

        // if waiting for END response
        if (inEND)
        {
            var endYESresponse = getSysMessageText(SYSMESS_YES);
            var endNOresponse = getSysMessageText(SYSMESS_NO);
            if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
            if (!endNOresponse.length) endNOresponse = 'N';
            var endYESresponseCode = endYESresponse.charCodeAt(0);
            var endNOresponseCode = endNOresponse.charCodeAt(0);

            switch ( e.keyCode )
            {
                case endYESresponseCode:
                case 13: // return
                case 32: // space
                    location.reload();
                    break;
                case endNOresponseCode:
                    inEND = false;
                    sfxstopall();
                    $('body').hide('slow');
                    break;
            }
            return;
        }


        // if waiting for QUIT response
        if (inQUIT)
        {
            var endYESresponse = getSysMessageText(SYSMESS_YES);
            var endNOresponse = getSysMessageText(SYSMESS_NO);
            if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
            if (!endNOresponse.length) endNOresponse = 'N';
            var endYESresponseCode = endYESresponse.charCodeAt(0);
            var endNOresponseCode = endNOresponse.charCodeAt(0);

            switch ( e.keyCode )
            {
                case endYESresponseCode:
                case 13: // return
                case 32: // space
                    inQUIT=false;
                    e.preventDefault();
                    waitKeyCallback();
                    return;
                case endNOresponseCode:
                    inQUIT=false;
                    waitkey_callback_function.pop();
                    hideAnykeyLayer();
                    e.preventDefault();
                    break;
            }
        }

        // ignore uninteresting keys
        switch ( e.keyCode )
        {
            case 9:  // tab   \ keys used during
            case 13: // enter / keyboard navigation
            case 16: // shift
            case 17: // ctrl
            case 18: // alt
            case 20: // caps lock
            case 91: // left Windows key
            case 92: // left Windows key
            case 93: // left Windows key
            case 225: // right alt
                // do not focus the input - the user was probably doing something else
                // (e.g. alt-tab'ing to another window)
                return;
        }


        if (inGetkey)  // return for getkey
        {
            setFlag(getkey_return_flag, e.keyCode);
            getkey_return_flag = null;
            inGetkey = false;
            hideAnykeyLayer();
            e.preventDefault();
            waitKeyCallback();
            return;
        }

        // Scroll text window using PgUp/PgDown
        if (e.keyCode==33)  // PgUp
        {
            divTextScrollUp();
            e.preventDefault();
            return;
        }
        if (e.keyCode==34)  // PgDown
        {
            divTextScrollDown();
            return;
        }


        if (inAnykey)  // return for anykey
        {
            inAnykey = false;
            hideAnykeyLayer();
            e.preventDefault();
            waitKeyCallback();
            return;
        }

        // if keypress and block displayed, close it
        if (inBlock)
            {
                closeBlock();
                e.preventDefault();
                return;
            }


        // if ESC pressed and transcript layer visible, close it
        if ((inTranscript) &&  (e.keyCode == 27))
            {
                $('.transcript_layer').hide();
                inTranscript = false;
                e.preventDefault();
                return;
            }

    // focus the input if the user is likely to expect it
    // (but not if they're e.g. ctrl+c'ing some text)
    switch ( e.keyCode )
    {
        case 8: // backspace
        case 9: // tab
        case 13: // enter
            break;
        default:
            if ( !e.ctrlKey && !e.altKey ) focusInput();
    }

    });


    $(document).bind('wheel mousewheel',function(e)
    {
        if((e.originalEvent.wheelDelta||-e.originalEvent.deltaY) > 0) divTextScrollUp(); else divTextScrollDown();
    });


    initialize();
    descriptionLoop();
    focusInput();

    h_post();  //hook

    // Start interrupt process
    setInterval( timer, TIMER_MILLISECONDS );

}

$('document').ready(
    function ()
    {
        start();
    }
    );

// VOCABULARY

vocabulary = [];
vocabulary.push([76, ":CARP", 0]);
vocabulary.push([9, "ACRO", 0]);
vocabulary.push([11, "AIR", 1]);
vocabulary.push([54, "ALTA", 1]);
vocabulary.push([2, "AND", 5]);
vocabulary.push([9, "ASCE", 0]);
vocabulary.push([63, "ASHE", 1]);
vocabulary.push([63, "ASHES", 0]);
vocabulary.push([42, "BAIT", 1]);
vocabulary.push([90, "BALL", 1]);
vocabulary.push([41, "BASK", 1]);
vocabulary.push([90, "BEAC", 1]);
vocabulary.push([81, "BED", 1]);
vocabulary.push([81, "BEDS", 1]);
vocabulary.push([80, "BENC", 1]);
vocabulary.push([30, "BLAC", 1]);
vocabulary.push([34, "BLOW", 0]);
vocabulary.push([27, "BLUE", 1]);
vocabulary.push([9, "BOAR", 0]);
vocabulary.push([56, "BOAT", 1]);
vocabulary.push([87, "BOLT", 1]);
vocabulary.push([48, "BOOK", 1]);
vocabulary.push([249, "BOOT", 1]);
vocabulary.push([11, "BOTT", 1]);
vocabulary.push([19, "BOUL", 1]);
vocabulary.push([65, "BOX", 1]);
vocabulary.push([65, "BOXE", 1]);
vocabulary.push([13, "BUCK", 1]);
vocabulary.push([81, "BUNK", 1]);
vocabulary.push([58, "BUSH", 1]);
vocabulary.push([121, "BUTT", 1]);
vocabulary.push([89, "CABI", 1]);
vocabulary.push([49, "CABL", 1]);
vocabulary.push([41, "CAGE", 1]);
vocabulary.push([45, "CAPT", 1]);
vocabulary.push([76, "CARP", 1]);
vocabulary.push([98, "CHAI", 1]);
vocabulary.push([15, "CHAN", 0]);
vocabulary.push([12, "CHEC", 0]);
vocabulary.push([42, "CHEE", 1]);
vocabulary.push([36, "CHES", 1]);
vocabulary.push([95, "CIGA", 1]);
vocabulary.push([9, "CLIM", 0]);
vocabulary.push([114, "CLOC", 1]);
vocabulary.push([50, "CLOS", 0]);
vocabulary.push([125, "CLUE", 0]);
vocabulary.push([127, "COG", 1]);
vocabulary.push([100, "COLL", 0]);
vocabulary.push([52, "CONN", 0]);
vocabulary.push([24, "CONT", 1]);
vocabulary.push([73, "CRAN", 1]);
vocabulary.push([23, "CRYS", 1]);
vocabulary.push([10, "D", 1]);
vocabulary.push([10, "DESC", 0]);
vocabulary.push([80, "DESK", 1]);
vocabulary.push([31, "DIG", 0]);
vocabulary.push([129, "DISC", 0]);
vocabulary.push([19, "DOOR", 1]);
vocabulary.push([10, "DOWN", 0]);
vocabulary.push([50, "DRAG", 0]);
vocabulary.push([46, "DRIN", 0]);
vocabulary.push([101, "DROP", 0]);
vocabulary.push([3, "E", 0]);
vocabulary.push([3, "EAST", 0]);
vocabulary.push([46, "EAT", 0]);
vocabulary.push([93, "EGG", 1]);
vocabulary.push([21, "ENTE", 0]);
vocabulary.push([17, "EXAM", 0]);
vocabulary.push([115, "FACE", 1]);
vocabulary.push([95, "FAG", 1]);
vocabulary.push([95, "FAGS", 1]);
vocabulary.push([128, "FILL", 0]);
vocabulary.push([63, "FIRE", 0]);
vocabulary.push([55, "FIX", 0]);
vocabulary.push([57, "FOLL", 0]);
vocabulary.push([100, "G", 0]);
vocabulary.push([100, "GET", 0]);
vocabulary.push([66, "GHOS", 1]);
vocabulary.push([96, "GIVE", 0]);
vocabulary.push([89, "GLAS", 1]);
vocabulary.push([21, "GO", 0]);
vocabulary.push([53, "GODD", 1]);
vocabulary.push([100, "GRAB", 0]);
vocabulary.push([114, "GRAN", 1]);
vocabulary.push([26, "GREE", 1]);
vocabulary.push([14, "HAND", 1]);
vocabulary.push([250, "HAT", 1]);
vocabulary.push([250, "HELM", 1]);
vocabulary.push([125, "HELP", 0]);
vocabulary.push([99, "HOLE", 1]);
vocabulary.push([104, "I", 0]);
vocabulary.push([53, "IDOL", 1]);
vocabulary.push([9, "IN", 0]);
vocabulary.push([113, "INSC", 1]);
vocabulary.push([21, "INSE", 0]);
vocabulary.push([104, "INVE", 1]);
vocabulary.push([104, "INVEN", 1]);
vocabulary.push([116, "JEWE", 1]);
vocabulary.push([52, "JOIN", 0]);
vocabulary.push([74, "JUMP", 0]);
vocabulary.push([47, "JUNK", 1]);
vocabulary.push([64, "KEEP", 1]);
vocabulary.push([35, "KEY", 1]);
vocabulary.push([84, "LABE", 1]);
vocabulary.push([63, "LAMP", 1]);
vocabulary.push([63, "LANT", 1]);
vocabulary.push([32, "LAY", 0]);
vocabulary.push([49, "LEAD", 1]);
vocabulary.push([74, "LEAP", 0]);
vocabulary.push([76, "LEAV", 1]);
vocabulary.push([76, "LEAVE", 0]);
vocabulary.push([91, "LEDG", 1]);
vocabulary.push([14, "LEVE", 1]);
vocabulary.push([32, "LIE", 0]);
vocabulary.push([64, "LIGH", 1]);
vocabulary.push([108, "LOAD", 0]);
vocabulary.push([105, "LOOK", 0]);
vocabulary.push([22, "LOWE", 0]);
vocabulary.push([118, "MAGN", 1]);
vocabulary.push([66, "MAN", 1]);
vocabulary.push([48, "MANU", 1]);
vocabulary.push([65, "MATC", 1]);
vocabulary.push([86, "MATE", 1]);
vocabulary.push([81, "MATT", 1]);
vocabulary.push([55, "MEND", 0]);
vocabulary.push([23, "METE", 1]);
vocabulary.push([45, "MOLE", 1]);
vocabulary.push([33, "MUSI", 1]);
vocabulary.push([1, "N", 0]);
vocabulary.push([92, "NEST", 1]);
vocabulary.push([110, "NEW", 0]);
vocabulary.push([111, "NORM", 0]);
vocabulary.push([1, "NORT", 0]);
vocabulary.push([20, "NOTC", 1]);
vocabulary.push([72, "OFF", 1]);
vocabulary.push([78, "ON", 1]);
vocabulary.push([38, "OPEN", 0]);
vocabulary.push([10, "OUT", 0]);
vocabulary.push([11, "OXYG", 1]);
vocabulary.push([48, "PAGE", 1]);
vocabulary.push([13, "PAIL", 1]);
vocabulary.push([24, "PANE", 1]);
vocabulary.push([94, "PAPE", 1]);
vocabulary.push([55, "PATC", 0]);
vocabulary.push([44, "PAUS", 0]);
vocabulary.push([119, "PEND", 1]);
vocabulary.push([100, "PICK", 0]);
vocabulary.push([122, "PICT", 0]);
vocabulary.push([47, "PILE", 1]);
vocabulary.push([28, "PINK", 1]);
vocabulary.push([33, "PIPE", 1]);
vocabulary.push([42, "PLAN", 1]);
vocabulary.push([113, "PLAQ", 1]);
vocabulary.push([120, "PLAT", 1]);
vocabulary.push([34, "PLAY", 0]);
vocabulary.push([52, "PLUG", 0]);
vocabulary.push([74, "POLE", 1]);
vocabulary.push([19, "PORT", 1]);
vocabulary.push([29, "PRES", 0]);
vocabulary.push([37, "PRIN", 1]);
vocabulary.push([97, "PUDD", 1]);
vocabulary.push([50, "PULL", 0]);
vocabulary.push([29, "PUSH", 0]);
vocabulary.push([101, "PUT", 0]);
vocabulary.push([106, "Q", 0]);
vocabulary.push([106, "QUIT", 0]);
vocabulary.push([105, "R", 0]);
vocabulary.push([151, "RAML", 0]);
vocabulary.push([150, "RAMS", 0]);
vocabulary.push([75, "RAVI", 1]);
vocabulary.push([17, "READ", 0]);
vocabulary.push([24, "RECE", 1]);
vocabulary.push([25, "RED", 1]);
vocabulary.push([105, "REDE", 0]);
vocabulary.push([38, "RELE", 0]);
vocabulary.push([102, "REMO", 0]);
vocabulary.push([55, "REPA", 0]);
vocabulary.push([108, "REST", 0]);
vocabulary.push([75, "RIVE", 1]);
vocabulary.push([19, "ROCK", 1]);
vocabulary.push([58, "ROOT", 1]);
vocabulary.push([18, "ROPE", 1]);
vocabulary.push([47, "RUBB", 1]);
vocabulary.push([14, "RUST", 1]);
vocabulary.push([2, "S", 0]);
vocabulary.push([79, "SAND", 1]);
vocabulary.push([107, "SAVE", 0]);
vocabulary.push([124, "SCOR", 0]);
vocabulary.push([117, "SCRE", 1]);
vocabulary.push([17, "SEAR", 0]);
vocabulary.push([43, "SET", 0]);
vocabulary.push([249, "SHOE", 1]);
vocabulary.push([50, "SHUT", 0]);
vocabulary.push([113, "SIGN", 1]);
vocabulary.push([19, "SLAB", 1]);
vocabulary.push([32, "SLEE", 0]);
vocabulary.push([50, "SLID", 0]);
vocabulary.push([63, "SMOK", 1]);
vocabulary.push([2, "SOUT", 0]);
vocabulary.push([126, "SPAN", 1]);
vocabulary.push([85, "SPEC", 0]);
vocabulary.push([53, "STAT", 1]);
vocabulary.push([106, "STOP", 0]);
vocabulary.push([64, "STRI", 1]);
vocabulary.push([15, "SWAP", 0]);
vocabulary.push([77, "SWIM", 0]);
vocabulary.push([100, "T", 0]);
vocabulary.push([80, "TABL", 1]);
vocabulary.push([100, "TAKE", 0]);
vocabulary.push([59, "TAR", 1]);
vocabulary.push([2, "THEN", 5]);
vocabulary.push([47, "THIN", 1]);
vocabulary.push([60, "TIN", 1]);
vocabulary.push([65, "TOOL", 1]);
vocabulary.push([63, "TORC", 1]);
vocabulary.push([70, "TOYS", 1]);
vocabulary.push([37, "TRAC", 1]);
vocabulary.push([41, "TRAP", 1]);
vocabulary.push([58, "TREE", 1]);
vocabulary.push([19, "TUNN", 1]);
vocabulary.push([22, "TURN", 0]);
vocabulary.push([109, "TYPE", 1]);
vocabulary.push([9, "U", 1]);
vocabulary.push([39, "UNBO", 0]);
vocabulary.push([88, "UNDO", 0]);
vocabulary.push([39, "UNLO", 0]);
vocabulary.push([9, "UP", 0]);
vocabulary.push([74, "VAUL", 1]);
vocabulary.push([51, "VISO", 1]);
vocabulary.push([4, "W", 0]);
vocabulary.push([44, "WAIT", 0]);
vocabulary.push([82, "WALL", 1]);
vocabulary.push([66, "WARR", 1]);
vocabulary.push([97, "WATE", 1]);
vocabulary.push([103, "WEAR", 0]);
vocabulary.push([94, "WEIG", 1]);
vocabulary.push([16, "WELL", 1]);
vocabulary.push([4, "WEST", 0]);
vocabulary.push([22, "WIND", 0]);
vocabulary.push([83, "WITC", 1]);
vocabulary.push([123, "WORD", 0]);



// SYS MESSAGES

total_sysmessages=68;

sysmessages = [];

sysmessages[0] = "Everything is dark. You can't see.<br>";
sysmessages[1] = "{TEXTPIC|y.svg|1}ou can also see:-<br>";
sysmessages[2] = "What do you want to do now?";
sysmessages[3] = "{EXITS|@38|1000}";
sysmessages[4] = "Please enter your next order";
sysmessages[5] = "Please type in your orders";
sysmessages[6] = "Sorry, I don't understand that. Try some different words.<br>";
sysmessages[7] = "\nYou can't go in that direction.<br>";
sysmessages[8] = "\nYou can't.<br>";
sysmessages[9] = "{TEXTPIC|y.svg|1}ou have with you:-\n";
sysmessages[10] = " WORN ▫";
sysmessages[11] = "▫ Nothing at all. ▫<br>";
sysmessages[12] = "\nAre you sure you wish to quit?<br>";
sysmessages[13] = "\nWould you like another game?<br>";
sysmessages[14] = "Thank you for playing....\n\n{CLASS|center|'THE ENERGEM ENIGMA'}<br>";
sysmessages[15] = "DONE<br>";
sysmessages[16] = "{CLASS|center|Press any key to continue}<br>";
sysmessages[17] = "\nYou have used ";
sysmessages[18] = " unit";
sysmessages[19] = "s";
sysmessages[20] = " of air.<br>";
sysmessages[21] = "You have scored ";
sysmessages[22] = "%.<br>";
sysmessages[23] = "You're not wearing it.<br>";
sysmessages[24] = "You can't. Your hands are full.<br>";
sysmessages[25] = "You already have it.<br>";
sysmessages[26] = "You cannot see it here.<br>";
sysmessages[27] = "You can't carry any more.<br>";
sysmessages[28] = "You don't have it.<br>";
sysmessages[29] = "You're already wearing it.<br>";
sysmessages[30] = "Y<br>";
sysmessages[31] = "N<br>";
sysmessages[32] = "More...<br>";
sysmessages[33] = "><br>";
sysmessages[34] = "<br>";
sysmessages[35] = "Time passes...<br>";
sysmessages[36] = "";
sysmessages[37] = "";
sysmessages[38] = "";
sysmessages[39] = "";
sysmessages[40] = "\nYou can't.<br>";
sysmessages[41] = "\nYou can't.<br>";
sysmessages[42] = "You can't. Your hands are full.<br>";
sysmessages[43] = "You can't carry any more.<br>";
sysmessages[44] = "You put {OREF} into<br>";
sysmessages[45] = "{OREF} is not into<br>";
sysmessages[46] = "<br>";
sysmessages[47] = "<br>";
sysmessages[48] = "<br>";
sysmessages[49] = "You don't have it.<br>";
sysmessages[50] = "\nYou can't.<br>";
sysmessages[51] = ".<br>";
sysmessages[52] = "That is not into<br>";
sysmessages[53] = "nothing at all<br>";
sysmessages[54] = "File not found.<br>";
sysmessages[55] = "File corrupt.<br>";
sysmessages[56] = "I/O error. File not saved.<br>";
sysmessages[57] = "Directory full.<br>";
sysmessages[58] = "Please enter savegame name you used when saving the game status.";
sysmessages[59] = "Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>";
sysmessages[60] = "Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.";
sysmessages[61] = "<br>";
sysmessages[62] = "Sorry? Please try other words.<br>";
sysmessages[63] = "Here<br>";
sysmessages[64] = "you can see<br>";
sysmessages[65] = "you can see<br>";
sysmessages[66] = "inside you see<br>";
sysmessages[67] = "on top you see<br>\n\n\n\n\n\n";

// USER MESSAGES

total_messages=8;

messages = [];

messages[1000] = "Exits: ";
messages[1001] = "You can't see any exits\n";
messages[1003] = "{ACTION|nort|nort}";
messages[1004] = "{ACTION|sout|sout}";
messages[1005] = "{ACTION|east|east}";
messages[1006] = "{ACTION|west|west}";
messages[1011] = "{ACTION|up|up}";
messages[1012] = "{ACTION|out|out}\n\n\n\n\n\n";

// WRITE MESSAGES

total_writemessages=339;

writemessages = [];

writemessages[0] = "RESPONSE_START";
writemessages[1] = "RESPONSE_USER";
writemessages[2] = "\nThe warrior blocks your path.";
writemessages[3] = "\nThe boat spins round and round getting nearer to the centre of the whirlpool by the second..... All of a sudden you are thrown clear of the boat and.........";
writemessages[4] = "\nYou are caught by a sludge monster and eaten.";
writemessages[5] = "\nIt's too far to swim unaided, your arms grow tired and you sink to the bottom. You fall victim to the sludge monster.";
writemessages[6] = "\nUnfortunately the ball has a slow puncture and has deflated.";
writemessages[7] = "\nIt's too far to swim unaided, your arms grow tired and you sink to the bottom. You fall victim to the sludge monster.";
writemessages[8] = "\nYou are swept over the top of the waterfall and fall to your death.";
writemessages[9] = "\nThe bridge is raised at the opposite side of the ravine.";
writemessages[10] = "\nYou bang your nose on a slab of rock.";
writemessages[11] = "\nThe bridge is up.";
writemessages[12] = "\nThere is a time and place for everything..and this is neither.";
writemessages[13] = "\nYou climb the rope and are now able to reach the crystal.";
writemessages[14] = "\nIt's a bit too wobbly to climb.";
writemessages[15] = "\nYou climb up the rope but seeing nothing of interest, you climb back down.";
writemessages[16] = "\nYou climb up the rope but seeing nothing of interest, you climb back down.";
writemessages[17] = "\nYou get half-way up the cliff but your feet slip on the rocks and you fall to your death.";
writemessages[18] = "\nIt's locked.";
writemessages[19] = "\nIt's shut!";
writemessages[20] = "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy.";
writemessages[21] = "\nYou have already done that.";
writemessages[22] = "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy.";
writemessages[23] = "\nYou have already done that.";
writemessages[24] = "\nAs you turn the handle the door slowly rises upwards.";
writemessages[25] = "\nNothing happens.";
writemessages[26] = "\nYou cannot pass through a closed portcullis.";
writemessages[27] = "\nYou start to climb down to the village below but your feet slip on the rocks and you fall to your death.";
writemessages[28] = "\nThe lighthouse keeper snatches the crystal from your hand and disappears up the stairs.";
writemessages[29] = "\nAs you climb into the well you slip on the moss covered walls and fall to your death.";
writemessages[30] = "\nAs you change bottle's the old one disintegrates.";
writemessages[31] = "\nAs you change bottle's the old one disintegrates.";
writemessages[32] = "\nAs you change bottle's the old one disintegrates.";
writemessages[33] = "\nAs you change bottle's the old one disintegrates.";
writemessages[34] = "\nYou are now on your last bottle of oxygen.";
writemessages[35] = "\nSorry....you are not carrying any.";
writemessages[36] = "As you remove the bottle the remaining air escapes and the bottle disintegrates....you have wasted valuable air.";
writemessages[37] = "As you remove the bottle the remaining air escapes and the bottle disintegrates....you have wasted valuable air.";
writemessages[38] = "As you remove the bottle the remaining air escapes and the bottle disintegrates....you have wasted valuable air.";
writemessages[39] = "\nYou find a packet of cigarettes.";
writemessages[40] = "\nYou have already done that.";
writemessages[41] = "\nThe bucket is at the bottom of the well.";
writemessages[42] = "\nYou find a packet of cigarettes.";
writemessages[43] = "\nYou have already done that.";
writemessages[44] = "\nA rusty handle with a square end.";
writemessages[45] = "\nMost objects may only be examined when carried.";
writemessages[46] = "\nYou can see a rusty handle on the side of the well and a bucket hanging above it.";
writemessages[47] = "\nYou have already done that.";
writemessages[48] = "\nAt one end of the rope are the words 'MADE IN INDIA'.";
writemessages[49] = "\nIt is a metal door. There is a small square notch to one side.";
writemessages[50] = "\nIt is set into the east wall and appears to be some kind of door, although there is no handle or lock.";
writemessages[51] = "\nThere is a bolt at the top.";
writemessages[52] = "\nAn open portcullis.";
writemessages[53] = "\nA means of opening the door.";
writemessages[54] = "\nA small square notch.";
writemessages[55] = "\nA complicated instrument for detecting the presence of rare minerals. It has a small control panel and a plug socket on the side.";
writemessages[56] = "\nMost objects may only be examined when carried.";
writemessages[57] = "\nIt has five small buttons on it, RED, GREEN, BLUE and PINK, with the BLACK button on the side.";
writemessages[58] = "\nMost objects may only be examined when carried.";
writemessages[59] = "\nVery spacious, with 9 cogs and a pendulum (seen them somewhere else).";
writemessages[60] = "\nA small intricately carved pipe.";
writemessages[61] = "\nA small key.";
writemessages[62] = "\nInside the chest you can see a crystalometer.";
writemessages[63] = "\nRather like Samantha Fox's...... BIG.";
writemessages[64] = "\nYou have already done that.";
writemessages[65] = "\nYou can see a small key that has been trodden into the mud by an animal.";
writemessages[66] = "\nYou have already done that.";
writemessages[67] = "\nNot made by any animal.";
writemessages[68] = "\nAn old washing basket with a flapping lid.";
writemessages[69] = "\nAn edible, green leafy plant.";
writemessages[70] = "\nThe only recognizable species is a cheese plant.";
writemessages[71] = "\nYou have already done that.";
writemessages[72] = "\nAmongst all the rubbish you find a pole.";
writemessages[73] = "\nYou have already done that.";
writemessages[74] = "{CLASS|center|\nCRYSTALOMETER MANUAL\n}THIS INSTRUMENT WILL ENABLE YOU TO DETECT A CRYSTAL OF A GIVEN COLOUR, WITHIN AN AREA OF SIX LOCATIONS. IT HAS ALREADY BEEN PRE-PROGRAMED TO COLLECT FOUR COLOUR CRYSTALS IN THE FOLLOWING ORDER:-";
writemessages[75] = "\nUnfortunately the rest of the book has been burnt.";
writemessages[76] = "An electrical cable attached to the helmet, with a connector at one end.";
writemessages[77] = "\nWOW! It's got six pairs of arms. It's also wearing a helmet.";
writemessages[78] = "\nYou have already done that.";
writemessages[79] = "\nA stone bench encrusted with jewels. It has a large statue at one end.";
writemessages[80] = "\nIt is a small boat, made from reads and waterproofed with a thick layer of a black sticky substance.";
writemessages[81] = "There is a bare patch in the bottom that needs repair.";
writemessages[82] = "\nIt is a small boat, made from reads and waterproofed with a thick layer of a black sticky substance.";
writemessages[83] = "\nHidden amongst the bushes is an old boat.";
writemessages[84] = "\nYou have already done that.";
writemessages[85] = "\nFrom the roots oozes a sticky tar like substance.";
writemessages[86] = "\nFull of air.....what did you expect?";
writemessages[87] = "\nFull of water.";
writemessages[88] = "\nFull of tar.";
writemessages[89] = "\nAmongst all the ashes you find a book page.";
writemessages[90] = "\nYou have already done that.";
writemessages[91] = "\nAn electric lantern with a small switch marked on/off.";
writemessages[92] = "\nAn electric lantern with a small switch marked on/off.";
writemessages[93] = "\nYou notice that the source of light is a green crystal.";
writemessages[94] = "\nA very bright light.";
writemessages[95] = "\nNeatly stacked boxes. In one of them you find a magnet.";
writemessages[96] = "\nThere's just one screwdriver in it.";
writemessages[97] = "\nYou have already done that.";
writemessages[98] = "\nYou have already done that.";
writemessages[99] = "\nThere is a cloud of smoke above him caused by his compulsive chain smoking.";
writemessages[100] = "\nThe only toy that is not broken is a small pipe.";
writemessages[101] = "\nYou have already done that.";
writemessages[102] = "\nIt is a means of lowering and raising the bridge. There is a small square notch on the side.";
writemessages[103] = "\nA very long, flexible pole.";
writemessages[104] = "\nA fast flowing river.";
writemessages[105] = "\nA fast flowing river.";
writemessages[106] = "\nAmongst a pile of leaves you uncover a lantern.";
writemessages[107] = "\nYou have already done that.";
writemessages[108] = "\nInside one of the drawers is a pair of boots.";
writemessages[109] = "\nYou have already done that.";
writemessages[110] = "\nYou have already done that.";
writemessages[111] = "\nBehind the desk sits a hungry lighthouse keeper.";
writemessages[112] = "\nOld and dirty.";
writemessages[113] = "\nA smelly looking mattress.";
writemessages[114] = "\nVery untidy... In fact there's a tool box on it!";
writemessages[115] = "\nNot really that important.";
writemessages[116] = "\nIt has a small label wrapped around it's middle.";
writemessages[117] = "\nIt says 'CHEESE  PICKLE'..Must b e a sandwitch.";
writemessages[118] = "\nA glass cabinet, it opens for easy access to the light.";
writemessages[119] = "\nDeflated.";
writemessages[120] = "\nInflated.";
writemessages[121] = "\nYou can see a large nest.";
writemessages[122] = "\nA large untidy nest containing one egg.";
writemessages[123] = "\nA large smooth egg.";
writemessages[124] = "\nAn egg shaped blue crystal.";
writemessages[125] = "\nA large smooth egg.";
writemessages[126] = "\nA large smooth egg.";
writemessages[127] = "\nYou have already done that.";
writemessages[128] = "\nAn oval, glass paperweight.";
writemessages[129] = "\nA packet of 19 (someone's smoked one).";
writemessages[130] = "\nA murky puddle.";
writemessages[131] = "\nA length of chain.";
writemessages[132] = "\nAn easily accessible entrance.";
writemessages[133] = "\nThere is a sheet of metal that has a small plate of a similar material set snuggly in it's centre.";
writemessages[134] = "\nBehind the metal plate there is a large cog.";
writemessages[135] = "\nThere is a large cog with 17 jewels on.";
writemessages[136] = "\nThe 'EIGHTEAM' woz 'ere. Aug '87";
writemessages[137] = "\n 17 Jewels: MADE IN HONG KONG ";
writemessages[138] = "\n 17 Jewels: MADE IN HONG KONG ";
writemessages[139] = "\nIt is a very large Grandfather clock. It has a large face, and a recess on the front. There is a keyhole on the side.";
writemessages[140] = "\nYou have already done that.";
writemessages[141] = "\nA normal sort of clock face. It has a small inscription in the centre, and three small screws around the outside. It is set to 12:15.";
writemessages[142] = "\nA normal sort of clock face. It has a small inscription in the centre, and three small screws around the outside. It is set to 12:15.";
writemessages[143] = "\nVery rare jewels.";
writemessages[144] = "\n16 red and one pink.";
writemessages[145] = "\nVery rare jewels.";
writemessages[146] = "\nA small flat ended screwdriver.";
writemessages[147] = "\nIt would attract metal (like most other magnets).";
writemessages[148] = "\nIt is obviously removable, but there is no visible means of doing so.";
writemessages[149] = "\nA large cog.";
writemessages[150] = "\nA large jewelled cog.";
writemessages[151] = "\nA pair of climbing shoes.";
writemessages[152] = "\nA tin helmet with a slide down visor. There is also a cable attached to the side with a\nplug at the end.";
writemessages[153] = "\nMost objects may only be examined when carried.";
writemessages[154] = "\nYou see nothing of interest.";
writemessages[155] = "\nIt fits perfectly.";
writemessages[156] = "\nIt fits perfectly.";
writemessages[157] = "\nAs you climb into the well you slip on the moss covered walls and fall to your death.";
writemessages[158] = "\nYou cannot pass through a closed portcullis.";
writemessages[159] = "\nIt's locked.";
writemessages[160] = "\nIt's shut!";
writemessages[161] = "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy.";
writemessages[162] = "\nYou have already done that.";
writemessages[163] = "\nAs you turn the handle the door slowly rises upwards.";
writemessages[164] = "\nNothing happens.";
writemessages[165] = "\nThe door slowly falls down into place.";
writemessages[166] = "\nAs you turn the handle it puts the cogs into motion and the bridge slowly lowers into place.";
writemessages[167] = "\nThe bridge slowly raises into place.";
writemessages[168] = "\nNothing happens.";
writemessages[169] = "\nThe clock starts ticking softly.";
writemessages[170] = "\nThe clock starts ticking softly.";
writemessages[171] = "\nThe crystalometer gives a small beep as a program is selected.";
writemessages[172] = "It is in 'RED' mode.";
writemessages[173] = "\nThe crystalometer gives a small beep as a program is selected.";
writemessages[174] = "It is in 'GREEN' mode.";
writemessages[175] = "\nThe crystalometer gives a small beep as a program is selected.";
writemessages[176] = "It is in 'BLUE' mode.";
writemessages[177] = "\nThe crystalometer gives a small beep as a program is selected.";
writemessages[178] = "It is in 'PINK' mode.";
writemessages[179] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[180] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[181] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[182] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[183] = "{CLASS|center|\nNOTHING DETECTED}";
writemessages[184] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[185] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[186] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[187] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[188] = "{CLASS|center|\nCRYSTAL DETECTED}";
writemessages[189] = "{CLASS|center|\nNOTHING DETECTED}";
writemessages[190] = "{CLASS|center|\nNOTHING DETECTED}";
writemessages[191] = "{CLASS|center|\nNOTHING DETECTED}";
writemessages[192] = "\nWrong game!";
writemessages[193] = "\nA bright pulsating light scans your body, then a digitized message says:";
writemessages[194] = "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN.";
writemessages[195] = "\nA bright pulsating light scans your body, then a digitized message says:";
writemessages[196] = "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN.";
writemessages[197] = "\nA bright pulsating light scans your body, then a digitized message says:";
writemessages[198] = "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN.";
writemessages[199] = "\nA bright pulsating light scans your body, then a digitized message says:";
writemessages[200] = "TRANSPORTATION DENIED: ALL FOUR CRYSTALS NOT PRESENT: TRY AGAIN.";
writemessages[201] = "\nA bright pulsating light scans your body, then a digitized message says:";
writemessages[202] = "ALL FOUR CRYSTALS DETECTED: PREPARE FOR TRANSPORTATION.";
writemessages[203] = "\nYou uncover an old tin.";
writemessages[204] = "\nYou have already done that.";
writemessages[205] = "\nYou disturb some of the sand and it rises into the air forming itself into a witch.";
writemessages[206] = "\nYou uncover a small lever.";
writemessages[207] = "\nAs you lie down on the mattress you notice the red crystal hanging from the ceiling.";
writemessages[208] = "\nComfortable isn't it.";
writemessages[209] = "\nComfortable isn't it.";
writemessages[210] = "\nYou have already done that.";
writemessages[211] = "\nYou blow gently into the pipe, mystical Indian music fills your ears and the rope rises upwards.";
writemessages[212] = "\nThe rope is already taut.";
writemessages[213] = "\nThe air is filled with mystical Indian music.";
writemessages[214] = "\nYou have already done that.";
writemessages[215] = "\nThere is no means of opening the door here.";
writemessages[216] = "\nYou have already done that.";
writemessages[217] = "\nIt's locked.";
writemessages[218] = "\nNow the mole has been EXTRICATED it FRANTICALLY burrows through the cave in creating a new exit.";
writemessages[219] = "\nThe mole is scared, adds to the droppings, and scurries away.";
writemessages[220] = "\nYou try to unlock the chest but the absence of a key makes it impossible.";
writemessages[221] = "\nThe chest is now unlocked.";
writemessages[222] = "\nYou can't do that.....Yet.";
writemessages[223] = "\nTime passes.....";
writemessages[224] = "\nTime passes.....";
writemessages[225] = "\nTime passes.....";
writemessages[226] = "\nThe mole is tempted by the bait and wanders into the trap..... It is trapped!";
writemessages[227] = "\nTime passes.....";
writemessages[228] = "\nTime passes.....";
writemessages[229] = "\nTime passes.....";
writemessages[230] = "\nTime passes.....";
writemessages[231] = "\nTime passes.....";
writemessages[232] = "\nYUK!!";
writemessages[233] = "\nThe water is highly neurotoxic, that is to say it attacks the central nervous system, causing intense pain, profuse sweating, difficulty in breathing, violent convulsions, unconciousness and finally......DEATH!";
writemessages[234] = "\nThe water is highly neurotoxic, that is to say it attacks the central nervous system, causing intense pain, profuse sweating, difficulty in breathing, violent convulsions, unconciousness and finally......DEATH!";
writemessages[235] = "\nThere is a loud rumbling noise and the pile of sand gradually seeps away.....a large smooth slab of rock slides down from the ceiling. ";
writemessages[236] = "\nThe visor on the helmet slides down.";
writemessages[237] = "\nThe visor is already shut.";
writemessages[238] = "\nYou can't do that.....Yet.";
writemessages[239] = "\nYou can't do that.....Yet.";
writemessages[240] = "\nYou give the boat a hard tug, it slides down the river bank, hits the water with a splash......... then sinks.";
writemessages[241] = "\nYou give the boat a hard tug and it slides down the bank into the water.";
writemessages[242] = "\nIt fits perfectly.";
writemessages[243] = "\nYou can't do that.....Yet.";
writemessages[244] = "\nYou have already done that.";
writemessages[245] = "\nIt will not plug into any of the objects you are carrying.";
writemessages[246] = "\nThe pages from the book and the sticky tar make the boat water- proof.";
writemessages[247] = "\nYou haven't got everything You need to repair the boat.";
writemessages[248] = "\nYou haven't got everything You need to repair the boat.";
writemessages[249] = "\nYou can't do that.....Yet.";
writemessages[250] = "\nYou have already done that.";
writemessages[251] = "\nThe tracks disappear into some overgrown bushes.";
writemessages[252] = "\nIt's already off....stupid.";
writemessages[253] = "\nIt's already on...stupid!";
writemessages[254] = "\nWarning: SMOKING CAN CAUSE HEART DISEASE.";
writemessages[255] = "\nIt's already off....stupid.";
writemessages[256] = "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed.";
writemessages[257] = "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed.";
writemessages[258] = "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed.";
writemessages[259] = "\nThe river is much to wide for you to jump without help. You are swept against some jagged rocks and are killed.";
writemessages[260] = "\nThe ravine is much to wide and you fall to your death.";
writemessages[261] = "\nThe pole catches on the edge of the river, you sail high in the air and.........";
writemessages[262] = "\nThe pole catches on the edge of the river, you sail high in the air and.........";
writemessages[263] = "\nWheeee...";
writemessages[264] = "\nThe lamp shines brightly in the cabinet.";
writemessages[265] = "\nThe current is too strong and sweeps you against the rocks.... you are dead.";
writemessages[266] = "\nThe current is too strong and sweeps you against the rocks.... you are dead.";
writemessages[267] = "\nIt's already on...stupid!";
writemessages[268] = "\nYou have already done that.";
writemessages[269] = "\nYou have already done that.";
writemessages[270] = "\nThe lighthouse keeper takes the sandwich, and waffles it down as though he hasn't eaten for a week, he then looks around his desk for something to give you in return. He gives you a glass paperweight.";
writemessages[271] = "\nThe lighthouse keeper takes the sandwich, and waffles it down as though he hasn't eaten for a week, he then looks around his desk for something to give you in return. He gives you a glass paperweight.";
writemessages[272] = "\nGrateful, for at last getting some real cigarettes, he gives you the chain he's been smoking, and leaves.";
writemessages[273] = "\nThe ghost takes the chain, then rattles it a few times, he is so pleased that he gives a beach ball to you before disapearing.";
writemessages[274] = "\nThere is a loud crack. The bucket is sent crashing to the bottom of the well, and the handle comes away in your hand.. ...........clumsy.";
writemessages[275] = "\nIt's stuck fast.";
writemessages[276] = "\nIt's stuck fast.";
writemessages[277] = "\nYou are already carrying the crystalometer.";
writemessages[278] = "\nYou can't reach it.";
writemessages[279] = "\nYou have already done that.";
writemessages[280] = "\nThe cabinet is closed.";
writemessages[281] = "\nAnnoyed at you for stealing her egg, an eagle swoops down from the sky snatching it from your hand and replacing it in the nest.";
writemessages[282] = "\nThe Crystalometer bleep's and a recorded message is played:\n&#34;All Crystals found: Now take them to the underground trans- porter tube, and prepare for transport.&#34;";
writemessages[283] = "\nIt's stuck fast.";
writemessages[284] = "\nIt is much too large for just one mere mortal to lift.";
writemessages[285] = "\nThe mole is scared, adds to the droppings, and scurries away.";
writemessages[286] = "\nAs you attempt to remove this sacred object from it's resting place, a bolt of plasma strikes you down.....you are a bit dead.";
writemessages[287] = "\nThe boat is too heavy to carry.";
writemessages[288] = "\nYou cannot see a boat.";
writemessages[289] = "\nAnd what do you suggest you should use to collect it in.";
writemessages[290] = "\nYou have already done that.";
writemessages[291] = "\nAnnoyed at you for stealing her egg, an eagle swoops down from the sky snatching it from your hand and replacing it in the nest.";
writemessages[292] = "\nAnnoyed at you for stealing her egg, an eagle swoops down from the sky snatching it from your hand and replacing it in the nest.";
writemessages[293] = "\nYou take the paperweight in mistake for the egg.";
writemessages[294] = "\nAfter collecting all the water, you discover a trapdoor.";
writemessages[295] = "\nYou have already done that.";
writemessages[296] = "\nAs you drop the Crystalometer the cable disconnects.";
writemessages[297] = "\nThe visor slowly slides up.";
writemessages[298] = "\nAs you drop the Crystalometer the cable disconnects.";
writemessages[299] = "\nAs the tin hits the floor the tar spills out and seeps into the floor.";
writemessages[300] = "\nAs the tin hits the floor, the water seeps away.";
writemessages[301] = "\nAs the lantern hits the floor it goes out.";
writemessages[302] = "\nAs the lantern hits the floor it goes out.";
writemessages[303] = "\nThe lamp shines brightly in the cabinet.";
writemessages[304] = "\nThe paperweight settles nicely next to the egg in the nest.";
writemessages[305] = "\nYou can't do that.....Yet.";
writemessages[306] = "\nYou remove the face leaving a large hole.";
writemessages[307] = "\nYou have already done that.";
writemessages[308] = "\nAs you remove the helmet the cable disconnects.";
writemessages[309] = "\nThe visor slowly slides up.";
writemessages[310] = "\nIt's already off....stupid.";
writemessages[311] = "\nAs you remove the helmet the cable disconnects.";
writemessages[312] = "\nRed first.";
writemessages[313] = "\nGreen next.";
writemessages[314] = "\nNow the blue.";
writemessages[315] = "\nThere's only one left..Berk.";
writemessages[316] = "\nThis has all the trappings of a useful object.";
writemessages[317] = "\nThis will cause you to wait with baited breath.";
writemessages[318] = "\nYou may have thought it was a spelling error!";
writemessages[319] = "\nA sticky situation is at the root of this problem.";
writemessages[320] = "\nBe generous.";
writemessages[321] = "\nBe generous.";
writemessages[322] = "\nBe generous.";
writemessages[323] = "\nNo, you plonker, there is not a spanner in this game, it was an example.  GIVE ME STRENGTH.";
writemessages[324] = "\nIf you are in real need of help, then use HELP &#34;OBJECT&#34;.\neg. HELP SPANNER will give you a clue about the spanner.";
writemessages[325] = "\nWe're not giving too much away!";
writemessages[326] = "\nAnd what do you suggest you should use to collect it in.";
writemessages[327] = "\nYou have already done that.";
writemessages[328] = "\nAfter collecting all the water, you discover a trapdoor.";
writemessages[329] = "\nYou have already done that.";
writemessages[330] = "\nYou can't do that.....Yet.";
writemessages[331] = "RESPONSE_DEFAULT_START";
writemessages[332] = "RESPONSE_DEFAULT_END";
writemessages[333] = "PRO1";
writemessages[334] = "PRO2";
writemessages[335] = "\nYou are getting low on air......";
writemessages[336] = "\nSorry you have run out of air...";
writemessages[337] = "\nThe visor slowly slides up.";
writemessages[338] = "\nThe spirit of a brave warrior (called Stephen) appears.";

// LOCATION MESSAGES

total_location_messages=84;

locations = [];

locations[0] = "{CLASS|center|SCENARIO\n}After Professor Roberts's rescue from the planet Arg, he has been able to continue working  on his latest  project.  One problem is that during  his captivation, he was given a truth serum.  So now the  enemy know what he has been working on.\n\nFor his next  project, four rare crystals are needed.  These  are energy  emitting  gems, and  are more commonly called 'ENERGEMS'. There is only one planet, in the Glenbo  system, that  these gems are known to exist.";
locations[1] = "{TEXTPIC|y.svg|1}ou are in a pleasant clearing surrounded by various types of plant life. There are exits to the {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[2] = "{TEXTPIC|y.svg|1}ou are at the {ACTION|northern|northern} end of a long winding road. It is very muddy. Exits are to the {ACTION|NORTH|NORTH} and to the {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[3] = "{TEXTPIC|y.svg|1}ou are walking down a narrow pathway. In the mud are some strange tracks, presumably from the planet's animal life. Exits lead {ACTION|NORTH|NORTH} and SOUTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[4] = "{TEXTPIC|y.svg|1}ou are walking down a narrow {ACTION|NORTH|NORTH} - {ACTION|SOUTH|SOUTH} pathway. To the {ACTION|south|south} you can see the mountains.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[5] = "{TEXTPIC|y.svg|1}ou are in a wide sweeping valley. The only possible exits are NORTH and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[6] = "{TEXTPIC|y.svg|1}ou are on a narrow ledge. The floor is a little slippery so you must tread carefully. Exits are {ACTION|NORTH|NORTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[7] = "{TEXTPIC|y.svg|1}ou are at the bottom of a long steep mountain slope. There is little vegetation here. Exits lead {ACTION|EAST|EAST}, {ACTION|NORTH|NORTH} and {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[8] = "{TEXTPIC|y.svg|1}ou are at the end of a steep sloping path. Towards the {ACTION|west|west} you can see the mountain peak. Exits are {ACTION|SOUTH|SOUTH} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[9] = "{TEXTPIC|y.svg|1}ou are in a crossover of three passages. Choice of exits are DOWN, WEST or SOUTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[10] = "{TEXTPIC|y.svg|1}ou are at the edge of an under ground river. The floor is very sandy. Exits are NORTH or SOUTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[11] = "{TEXTPIC|y.svg|1}ou are now swimming in the underground river. You can go NORTH or EAST.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[12] = "{TEXTPIC|y.svg|1}ou are at the foot of the mountain. It is a long {ACTION|climb|climb} to the top. The only possible exits lead {ACTION|UP|UP} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[13] = "{TEXTPIC|y.svg|1}ou are now half-way up the mountain. A narrow ledge leads to the {ACTION|WEST|WEST}. Other exits leads {ACTION|EAST|EAST} or {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[14] = "{TEXTPIC|y.svg|1}ou are in what appears to be an ancient place of worship. There is a kind of altar on one wall with an inscription above it. The only exit is EAST.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[15] = "{TEXTPIC|y.svg|1}ou are in a steep sloping passage with a low ceiling. You can go either UP or DOWN.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[16] = "{TEXTPIC|y.svg|1}ou are in a low damp cave with a sandy floor. The effect of the light reflecting through the waterfall causes strange dancing shadows to fill one wall. You can see a small hole high above. The only other exit is to the {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[17] = "{TEXTPIC|y.svg|1}ou are on a mountain ledge. Looking down you can see a small settlement. You can go {ACTION|WEST|WEST} or {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[18] = "{TEXTPIC|y.svg|1}ou are on the mountain peak. Exits are {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[19] = "{TEXTPIC|y.svg|1}ou are {ACTION|in|in} an old mountain hide -away.  It was obviously used as a camp by bandits, as there are still a few of their things left lying around.  You are able to go {ACTION|UP|UP}, {ACTION|SOUTH|SOUTH} or {ACTION|EAST|EAST}.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[20] = "";
locations[21] = "{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small low cave. The only possible exit is {ACTION|UP|UP}\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[22] = "{TEXTPIC|i.svg|1}t is very narrow here. Loose rocks fall over the edge as you cautiously make your way along the ledge. Exits lead {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[23] = "{TEXTPIC|y.svg|1}ou are at the side of a river. You can see a waterfall to the {ACTION|SOUTH|SOUTH}. Other exits lead to the {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[24] = "{TEXTPIC|y.svg|1}ou are walking along the {ACTION|west|west} bank of the river. There are some strange tracks in the mud. The path leads {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[25] = "{TEXTPIC|y.svg|1}ou have entered a clearing. The remains of a small camp fire are in the middle. The only possible exit is {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[26] = "{TEXTPIC|y.svg|1}ou are floating gently down a river. The river flows from the {ACTION|SOUTH|SOUTH} to the {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[27] = "{TEXTPIC|y.svg|1}ou are at the edge of the forest. There is a forest path that leads {ACTION|WEST|WEST}. The only other exit is to the {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[28] = "{TEXTPIC|y.svg|1}ou are now a little deeper into the forest. It is darker here as the trees are blocking most of the sunlight. The ground is covered with a thick carpet of leaves. The path leads {ACTION|WEST|WEST} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[29] = "{TEXTPIC|y.svg|1}ou are on the north bank of the forest river. The only visible exit leads {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[30] = "{TEXTPIC|t.svg|1}he river is now flowing through the middle of the forest. The trees overhang from each bank to meet each other in the middle. You can go {ACTION|NORTH|NORTH} or {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[31] = "{TEXTPIC|t.svg|1}he current is much stronger here. You can see a whirlpool to the NORTH. You can also go {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[32] = "{TEXTPIC|y.svg|1}ou are on the south bank of the forest river. The only possible exit is {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[33] = "{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small clearing that has one large tree standing {ACTION|in|in} the centre. You can go {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[34] = "{TEXTPIC|y.svg|1}ou are now at the top of the tree. You can see a large hole amongst the branches. Exits are {ACTION|DOWN|DOWN} or {ACTION|IN|IN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[35] = "{TEXTPIC|y.svg|1}ou are inside the large hollow tree, it is damp and stuffy here. The only possible exits lead {ACTION|OUT|OUT} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[36] = "{TEXTPIC|t.svg|1}his is an ante-chamber and has been used as a store in the past, as there are still a few empty boxes lying around. You can go {ACTION|NORTH|NORTH} or {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[37] = "{TEXTPIC|y.svg|1}ou are in an old dormitory. Exits are {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and to the {ACTION|EAST|EAST}\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[38] = "{TEXTPIC|y.svg|1}ou are in a small cave, the walls of which are made of some sort of mineral unknown to you. Exits lead {ACTION|EAST|EAST}, {ACTION|WEST|WEST} and {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[39] = "{TEXTPIC|y.svg|1}ou are in a small round tunnel that has obviously been made by an intelligent being. The roots of the tree help to keep the walls from caving in. Exits are {ACTION|WEST|WEST} and {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[40] = "{TEXTPIC|t.svg|1}here are two passages ahead of you. One passage leads to the {ACTION|WEST|WEST}, whilst the other will take you {ACTION|NORTH|NORTH}. You are also able to go {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[41] = "{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a large earthy cave deep down {ACTION|in|in} the ground. You can go {ACTION|NORTH|NORTH} or {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[42] = "{TEXTPIC|y.svg|1}ou are in a maze of tunnels. Exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[43] = "{TEXTPIC|y.svg|1}ou are in a maze of tunnels. Exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[44] = "{TEXTPIC|y.svg|1}ou are in a maze of tunnels. Exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[45] = "{TEXTPIC|y.svg|1}ou are in an animals lair. The floor is littered with smelly droppings and discarded food. There are exits to the {ACTION|SOUTH|SOUTH} and to the {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[46] = "{TEXTPIC|y.svg|1}ou are in a slowly widening tunnel. There is a strong odour of cheese here. Exits are {ACTION|NORTH|NORTH} and {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[47] = "{TEXTPIC|y.svg|1}ou are crawling through a very recently excavated tunnel. It appears to have been dug by some strange animal. You can go {ACTION|DOWN|DOWN} or {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[48] = "{TEXTPIC|y.svg|1}ou are in a short passage that appears to be the most recently mined part of the system. Exits are {ACTION|NORTH|NORTH} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[49] = "{TEXTPIC|y.svg|1}ou are now at the end of the passage. There is a door at the far end and an exit {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[50] = "{TEXTPIC|y.svg|1}ou are in a cave almost full of boxes. There is a table in the middle of the room. The only possible exit is {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[51] = "{TEXTPIC|y.svg|1}ou are in a small crossover of three passages. Exits lead to the {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[52] = "{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small gallery. You can see a recently dug tunnel leading {ACTION|UP|UP}. There are also exits to the {ACTION|NORTH|NORTH} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[53] = "{TEXTPIC|t.svg|1}he walls here are well shored. Timbers at least a foot thick support both walls and ceiling. The only exit is {ACTION|west|west}.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[54] = "{TEXTPIC|y.svg|1}ou are in a very well worked section of the mine. You can go {ACTION|WEST|WEST} or EAST.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[55] = "{TEXTPIC|t.svg|1}here is a choice of passages here. You can go {ACTION|SOUTH|SOUTH}, {ACTION|WEST|WEST} or {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[56] = "{TEXTPIC|y.svg|1}ou are at a crossover of three paths. Tunnels lead {ACTION|SOUTH|SOUTH}, {ACTION|WEST|WEST} and {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[57] = "{TEXTPIC|y.svg|1}ou are at the edge of a small shallow pool, looking around you can see that you are in an old mine. Possible exits lead {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH}, {ACTION|EAST|EAST} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[58] = "{TEXTPIC|y.svg|1}ou are knee deep in a small pool of water. There are some pieces of the boat floating around in the water. The only possible exit leads {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[59] = "";
locations[60] = "{TEXTPIC|y.svg|1}ou are on the main road of a small village. It seems to be quite deserted. Exits are {ACTION|NORTH|NORTH}, {ACTION|SOUTH|SOUTH} and {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[61] = "{TEXTPIC|y.svg|1}ou are on a sandy shore.  The tide is out.  Possible exits lead {ACTION|WEST|WEST} and NORTH.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[62] = "{TEXTPIC|y.svg|1}ou are further along the road. There is a hut SOUTH and a path {ACTION|EAST|EAST}. The road runs from {ACTION|NORTH|NORTH} to {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[63] = "{TEXTPIC|y.svg|1}ou are on the village road. There are huts to the {ACTION|NORTH|NORTH} and {ACTION|SOUTH|SOUTH}. The road runs {ACTION|EAST|EAST}, and there is a bridge leading WEST. You can see a small crank next to the bridge.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[64] = "{TEXTPIC|y.svg|1}ou are at the bottom of the cliff face. With a little effort and some equipment it is possible to go UP. The only other exit is to the {ACTION|EAST|EAST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[65] = "{TEXTPIC|t.svg|1}his is obviously the home of a warrior as there are various weapons hanging on one wall. The only exit is NORTH.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[66] = "{TEXTPIC|t.svg|1}his appears to be a normal family home. There are lots of toys left lying over the floor, most of them broken. The only way out is {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[67] = "{TEXTPIC|t.svg|1}his is the dining hut. There is a large table with lots of plates set round it. The only exit is {ACTION|SOUTH|SOUTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[68] = "{TEXTPIC|y.svg|1}ou are in a small alcove that was once used as an office. You can go {ACTION|EAST|EAST} or {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[69] = "{TEXTPIC|y.svg|1}ou are at the bottom of the well. There is no longer any water here, but it is rather muddy. The only exit is {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[70] = "{TEXTPIC|y.svg|1}ou are in a small grove. There is a portcullis set into a sheer cliff face.  Exits are to the {ACTION|NORTH|NORTH} and DOWN.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[71] = "{TEXTPIC|y.svg|1}ou are sitting in a boat. You can climb OUT or sail {ACTION|NORTH|NORTH}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[72] = "{TEXTPIC|y.svg|1}ou are at the edge of a steep ravine. You can see a small village on the other side. There is an entrance to a bridge leading EAST and a path {ACTION|WEST|WEST}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[73] = "{TEXTPIC|y.svg|1}ou are {ACTION|in|in} a small dank cellar. There is a flight of steps on one wall. The only exit is {ACTION|UP|UP}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[74] = "";
locations[75] = "{TEXTPIC|y.svg|1}ou are on a sandy shore.  The tide is in.  Possible exits lead {ACTION|WEST|WEST} and NORTH.\n\n{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[76] = "{TEXTPIC|y.svg|1}ou are standing on a small island. A lighthouse towers above your head, it's light flashes far out to sea. There is a sandy shore SOUTH and a door leading {ACTION|IN|IN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[77] = "{TEXTPIC|y.svg|1}ou are on the ground floor of the lighthouse. There is a table and two chairs {ACTION|in|in} the middle of the room. A staircase winds UPwards and a door leads OUT.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[78] = "{TEXTPIC|y.svg|1}ou are on the second floor of the lighthouse. You can see a set of bunk beds. The stairs lead {ACTION|UP|UP} and {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[79] = "{TEXTPIC|y.svg|1}ou are in the light room. A glass cabinet takes up most of the room. The light shines brightly in the cabinet, it's beam reflected by a revolving mirror. Stairs lead {ACTION|DOWN|DOWN}.\n\n{CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}";
locations[80] = "{CLASS|center| ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫\n\n\n C O N G R A T U L A T I O N S\n\n\n ▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫\n\n}You have successfully  completed the  'ENERGEM ENIGMA'.  You are now  being  transported  back to the professor's laboratory. When you arrive you  will be sent for de-briefing.   The  four  energy crystals are  to be  used in the professor's  latest  top  secret invention.  Will it  be you  who is sent to try it out?";
locations[81] = "CONT-\n\nUnfortunately,  the  last  agent sent to collect them  never came back!  You  are now going to the planet EDAM in order to continue the search.  All  the  equipment has been  left  scattered around the planet.  All  you  will have with you is your air bottles. Do not waste air  by swapping  them unnecessarily.";
locations[82] = "\nExtra commands include:-\n\nRAMSAVE\tRAMLOAD\nNEW TYPEFACE\tNORMAL TYPEFACE\nCHECK AIR\tSWAP BOTTLES\nSCORE\tHELP\nPICTURES\tWORDS\n\n We hope that you will have many hours of enjoyment playing this game.  If you have any comments or criticisms, then please write to:                             {CLASS|center|The Eighteam            \n C/O Precision Games   \n 2 Fern Hill         \n Langdon Hills     \n Basildon        \n Essex.}";
locations[83] = "{CLASS|center|THE 'ENERGEM ENIGMA'}\n Was written  and designed by the Eighteam  Produced by PrecisionG ames:  Many  thanks  to Gilsoft for The Quill, The Illustrator The Press.  A special  thanks to Phill Wade  for  sorting out our U.D.G problem.\n The Eighteam:\n\n {CLASS|center|Peter, Caff, David,  Sean\n} Additional material: Richard.\n {CLASS|center|▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫▫◻□◻▫}\n\n\n\n\n";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, 27, 2, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[2] = [ -1, 1, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[4] = [ -1, 3, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[5] = [ -1, -1, -1, -1, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, 7, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[7] = [ -1, 8, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, 7, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[10] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, 8, -1, -1, -1, -1, -1, 13, -1, -1, -1, -1, -1, -1 ];
connections[13] = [ -1, -1, -1, 17, 19, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[16] = [ -1, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[17] = [ -1, -1, -1, -1, 13, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1 ];
connections[18] = [ -1, -1, -1, 17, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[19] = [ -1, -1, 70, 13, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1 ];
connections[20] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 70, -1, -1, -1, -1, -1, -1 ];
connections[22] = [ -1, -1, -1, 23, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[23] = [ -1, -1, 16, 24, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[24] = [ -1, -1, -1, 25, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[25] = [ -1, -1, -1, -1, 24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[26] = [ -1, 30, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[27] = [ -1, -1, 1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[28] = [ -1, -1, -1, 27, 29, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[29] = [ -1, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[30] = [ -1, 31, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[31] = [ -1, -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[32] = [ -1, -1, -1, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[33] = [ -1, -1, -1, 32, -1, -1, -1, -1, -1, 34, -1, -1, -1, -1, -1, -1 ];
connections[34] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 35, 33, -1, -1, -1, -1, -1 ];
connections[35] = [ -1, -1, -1, 39, -1, -1, -1, -1, -1, -1, 34, -1, -1, -1, -1, -1 ];
connections[36] = [ -1, 38, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[37] = [ -1, 36, 40, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[38] = [ -1, -1, 36, 40, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[39] = [ -1, -1, -1, -1, 35, -1, -1, -1, -1, -1, 41, -1, -1, -1, -1, -1 ];
connections[40] = [ -1, 37, 41, -1, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[41] = [ -1, 40, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1, -1, -1, -1 ];
connections[42] = [ -1, 42, 43, 45, 42, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[43] = [ -1, 43, 44, 43, 43, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[44] = [ -1, 43, 43, 42, 43, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[45] = [ -1, 42, 46, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[46] = [ -1, 45, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[47] = [ -1, 46, -1, -1, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, -1, -1 ];
connections[48] = [ -1, 51, -1, 49, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[49] = [ -1, -1, -1, -1, 48, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[50] = [ -1, -1, 49, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[51] = [ -1, 57, 48, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[52] = [ -1, 55, -1, 56, -1, -1, -1, -1, -1, 47, -1, -1, -1, -1, -1, -1 ];
connections[53] = [ -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[54] = [ -1, -1, -1, -1, 55, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[55] = [ -1, -1, 52, 54, 56, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[56] = [ -1, -1, 57, 55, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[57] = [ -1, 56, 51, 58, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[58] = [ -1, -1, -1, -1, 57, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[59] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[60] = [ -1, 44, 62, -1, 64, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[61] = [ -1, -1, -1, -1, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[62] = [ -1, 60, -1, 61, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[63] = [ -1, 67, 66, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[64] = [ -1, -1, -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[65] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[66] = [ -1, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[67] = [ -1, -1, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[68] = [ -1, -1, -1, 57, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[69] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 4, -1, -1, -1, -1, -1, -1 ];
connections[70] = [ -1, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[71] = [ -1, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[72] = [ -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[73] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1 ];
connections[74] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[75] = [ -1, -1, -1, -1, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[76] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 77, -1, -1, -1, -1, -1, -1 ];
connections[77] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 78, -1, -1, -1, -1, -1, -1 ];
connections[78] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 79, 77, -1, -1, -1, -1, -1 ];
connections[79] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 78, -1, -1, -1, -1, -1 ];
connections[80] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[81] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[82] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[83] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, 27, 2, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[2] = [ -1, 1, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[4] = [ -1, 3, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[5] = [ -1, -1, -1, -1, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, 7, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[7] = [ -1, 8, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, 7, -1, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[10] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, 8, -1, -1, -1, -1, -1, 13, -1, -1, -1, -1, -1, -1 ];
connections_start[13] = [ -1, -1, -1, 17, 19, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[16] = [ -1, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[17] = [ -1, -1, -1, -1, 13, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1 ];
connections_start[18] = [ -1, -1, -1, 17, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[19] = [ -1, -1, 70, 13, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1 ];
connections_start[20] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 70, -1, -1, -1, -1, -1, -1 ];
connections_start[22] = [ -1, -1, -1, 23, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[23] = [ -1, -1, 16, 24, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[24] = [ -1, -1, -1, 25, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[25] = [ -1, -1, -1, -1, 24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[26] = [ -1, 30, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[27] = [ -1, -1, 1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[28] = [ -1, -1, -1, 27, 29, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[29] = [ -1, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[30] = [ -1, 31, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[31] = [ -1, -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[32] = [ -1, -1, -1, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[33] = [ -1, -1, -1, 32, -1, -1, -1, -1, -1, 34, -1, -1, -1, -1, -1, -1 ];
connections_start[34] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 35, 33, -1, -1, -1, -1, -1 ];
connections_start[35] = [ -1, -1, -1, 39, -1, -1, -1, -1, -1, -1, 34, -1, -1, -1, -1, -1 ];
connections_start[36] = [ -1, 38, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[37] = [ -1, 36, 40, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[38] = [ -1, -1, 36, 40, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[39] = [ -1, -1, -1, -1, 35, -1, -1, -1, -1, -1, 41, -1, -1, -1, -1, -1 ];
connections_start[40] = [ -1, 37, 41, -1, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[41] = [ -1, 40, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1, -1, -1, -1 ];
connections_start[42] = [ -1, 42, 43, 45, 42, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[43] = [ -1, 43, 44, 43, 43, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[44] = [ -1, 43, 43, 42, 43, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[45] = [ -1, 42, 46, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[46] = [ -1, 45, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[47] = [ -1, 46, -1, -1, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, -1, -1 ];
connections_start[48] = [ -1, 51, -1, 49, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[49] = [ -1, -1, -1, -1, 48, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[50] = [ -1, -1, 49, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[51] = [ -1, 57, 48, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[52] = [ -1, 55, -1, 56, -1, -1, -1, -1, -1, 47, -1, -1, -1, -1, -1, -1 ];
connections_start[53] = [ -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[54] = [ -1, -1, -1, -1, 55, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[55] = [ -1, -1, 52, 54, 56, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[56] = [ -1, -1, 57, 55, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[57] = [ -1, 56, 51, 58, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[58] = [ -1, -1, -1, -1, 57, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[59] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[60] = [ -1, 44, 62, -1, 64, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[61] = [ -1, -1, -1, -1, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[62] = [ -1, 60, -1, 61, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[63] = [ -1, 67, 66, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[64] = [ -1, -1, -1, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[65] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[66] = [ -1, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[67] = [ -1, -1, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[68] = [ -1, -1, -1, 57, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[69] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 4, -1, -1, -1, -1, -1, -1 ];
connections_start[70] = [ -1, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[71] = [ -1, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[72] = [ -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[73] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1 ];
connections_start[74] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[75] = [ -1, -1, -1, -1, 62, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[76] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 77, -1, -1, -1, -1, -1, -1 ];
connections_start[77] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 78, -1, -1, -1, -1, -1, -1 ];
connections_start[78] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 79, 77, -1, -1, -1, -1, -1 ];
connections_start[79] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 78, -1, -1, -1, -1, -1 ];
connections_start[80] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[81] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[82] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[83] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];
resources.push([RESOURCE_TYPE_IMG, 21, "dat/location21.svg"]);
resources.push([RESOURCE_TYPE_IMG, 27, "dat/location27.svg"]);
resources.push([RESOURCE_TYPE_IMG, 28, "dat/location28.svg"]);
resources.push([RESOURCE_TYPE_IMG, 30, "dat/location30.svg"]);
resources.push([RESOURCE_TYPE_IMG, 37, "dat/location37.svg"]);
resources.push([RESOURCE_TYPE_IMG, 4, "dat/location4.svg"]);
resources.push([RESOURCE_TYPE_IMG, 40, "dat/location40.svg"]);
resources.push([RESOURCE_TYPE_IMG, 42, "dat/location42.svg"]);
resources.push([RESOURCE_TYPE_IMG, 43, "dat/location43.svg"]);
resources.push([RESOURCE_TYPE_IMG, 44, "dat/location44.svg"]);
resources.push([RESOURCE_TYPE_IMG, 51, "dat/location51.svg"]);
resources.push([RESOURCE_TYPE_IMG, 52, "dat/location52.svg"]);
resources.push([RESOURCE_TYPE_IMG, 55, "dat/location55.svg"]);
resources.push([RESOURCE_TYPE_IMG, 56, "dat/location56.svg"]);
resources.push([RESOURCE_TYPE_IMG, 61, "dat/location61.svg"]);
resources.push([RESOURCE_TYPE_IMG, 75, "dat/location75.svg"]);
resources.push([RESOURCE_TYPE_IMG, 8, "dat/location8.svg"]);
resources.push([RESOURCE_TYPE_IMG, 9, "dat/location9.svg"]);


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "▫ A Lantern ▫";
objectsNoun[0] = 255;
objectsAdjective[0] = 255;
objectsLocation[0] = 252;
objectsLocation_start[0] = 252;
objectsWeight[0] = 1;
objectsWeight_start[0] = 1;
objectsAttrLO[0] = 1;
objectsAttrLO_start[0] = 1;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "▫ 4 oxygen bottles ▫";
objectsNoun[1] = 255;
objectsAdjective[1] = 255;
objectsLocation[1] = 254;
objectsLocation_start[1] = 254;
objectsWeight[1] = 1;
objectsWeight_start[1] = 1;
objectsAttrLO[1] = 0;
objectsAttrLO_start[1] = 0;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

objects[2] = "▫ 3 Oxygen Bottles ▫";
objectsNoun[2] = 255;
objectsAdjective[2] = 255;
objectsLocation[2] = 252;
objectsLocation_start[2] = 252;
objectsWeight[2] = 1;
objectsWeight_start[2] = 1;
objectsAttrLO[2] = 0;
objectsAttrLO_start[2] = 0;
objectsAttrHI[2] = 0;
objectsAttrHI_start[2] = 0;

objects[3] = "▫ 2 Oxygen Bottles ▫";
objectsNoun[3] = 255;
objectsAdjective[3] = 255;
objectsLocation[3] = 252;
objectsLocation_start[3] = 252;
objectsWeight[3] = 1;
objectsWeight_start[3] = 1;
objectsAttrLO[3] = 0;
objectsAttrLO_start[3] = 0;
objectsAttrHI[3] = 0;
objectsAttrHI_start[3] = 0;

objects[4] = "▫ 1 Oxygen Bottle ▫";
objectsNoun[4] = 255;
objectsAdjective[4] = 255;
objectsLocation[4] = 252;
objectsLocation_start[4] = 252;
objectsWeight[4] = 1;
objectsWeight_start[4] = 1;
objectsAttrLO[4] = 0;
objectsAttrLO_start[4] = 0;
objectsAttrHI[4] = 0;
objectsAttrHI_start[4] = 0;

objects[5] = "▫ A Crystalometer ▫";
objectsNoun[5] = 255;
objectsAdjective[5] = 255;
objectsLocation[5] = 252;
objectsLocation_start[5] = 252;
objectsWeight[5] = 1;
objectsWeight_start[5] = 1;
objectsAttrLO[5] = 0;
objectsAttrLO_start[5] = 0;
objectsAttrHI[5] = 0;
objectsAttrHI_start[5] = 0;

objects[6] = "▫ A Rusty Handle ▫";
objectsNoun[6] = 14;
objectsAdjective[6] = 255;
objectsLocation[6] = 252;
objectsLocation_start[6] = 252;
objectsWeight[6] = 1;
objectsWeight_start[6] = 1;
objectsAttrLO[6] = 0;
objectsAttrLO_start[6] = 0;
objectsAttrHI[6] = 0;
objectsAttrHI_start[6] = 0;

objects[7] = "▫ A Length of Rope ▫";
objectsNoun[7] = 18;
objectsAdjective[7] = 255;
objectsLocation[7] = 252;
objectsLocation_start[7] = 252;
objectsWeight[7] = 1;
objectsWeight_start[7] = 1;
objectsAttrLO[7] = 0;
objectsAttrLO_start[7] = 0;
objectsAttrHI[7] = 0;
objectsAttrHI_start[7] = 0;

objects[8] = "▫ An Open Portcullis ▫";
objectsNoun[8] = 255;
objectsAdjective[8] = 255;
objectsLocation[8] = 252;
objectsLocation_start[8] = 252;
objectsWeight[8] = 1;
objectsWeight_start[8] = 1;
objectsAttrLO[8] = 0;
objectsAttrLO_start[8] = 0;
objectsAttrHI[8] = 0;
objectsAttrHI_start[8] = 0;

objects[9] = "▫ A Red Crystal ▫";
objectsNoun[9] = 25;
objectsAdjective[9] = 255;
objectsLocation[9] = 252;
objectsLocation_start[9] = 252;
objectsWeight[9] = 1;
objectsWeight_start[9] = 1;
objectsAttrLO[9] = 0;
objectsAttrLO_start[9] = 0;
objectsAttrHI[9] = 0;
objectsAttrHI_start[9] = 0;

objects[10] = "▫ A Green Crystal ▫";
objectsNoun[10] = 26;
objectsAdjective[10] = 255;
objectsLocation[10] = 252;
objectsLocation_start[10] = 252;
objectsWeight[10] = 1;
objectsWeight_start[10] = 1;
objectsAttrLO[10] = 0;
objectsAttrLO_start[10] = 0;
objectsAttrHI[10] = 0;
objectsAttrHI_start[10] = 0;

objects[11] = "▫ A Blue Crystal ▫";
objectsNoun[11] = 27;
objectsAdjective[11] = 255;
objectsLocation[11] = 252;
objectsLocation_start[11] = 252;
objectsWeight[11] = 1;
objectsWeight_start[11] = 1;
objectsAttrLO[11] = 0;
objectsAttrLO_start[11] = 0;
objectsAttrHI[11] = 0;
objectsAttrHI_start[11] = 0;

objects[12] = "▫ A Pink Crystal ▫";
objectsNoun[12] = 28;
objectsAdjective[12] = 255;
objectsLocation[12] = 252;
objectsLocation_start[12] = 252;
objectsWeight[12] = 1;
objectsWeight_start[12] = 1;
objectsAttrLO[12] = 0;
objectsAttrLO_start[12] = 0;
objectsAttrHI[12] = 0;
objectsAttrHI_start[12] = 0;

objects[13] = "▫ A Pipe ▫";
objectsNoun[13] = 33;
objectsAdjective[13] = 255;
objectsLocation[13] = 252;
objectsLocation_start[13] = 252;
objectsWeight[13] = 1;
objectsWeight_start[13] = 1;
objectsAttrLO[13] = 0;
objectsAttrLO_start[13] = 0;
objectsAttrHI[13] = 0;
objectsAttrHI_start[13] = 0;

objects[14] = "▫ A Handle in the Wall ▫";
objectsNoun[14] = 255;
objectsAdjective[14] = 255;
objectsLocation[14] = 252;
objectsLocation_start[14] = 252;
objectsWeight[14] = 1;
objectsWeight_start[14] = 1;
objectsAttrLO[14] = 0;
objectsAttrLO_start[14] = 0;
objectsAttrHI[14] = 0;
objectsAttrHI_start[14] = 0;

objects[15] = "▫ A Small Key ▫";
objectsNoun[15] = 35;
objectsAdjective[15] = 255;
objectsLocation[15] = 252;
objectsLocation_start[15] = 252;
objectsWeight[15] = 1;
objectsWeight_start[15] = 1;
objectsAttrLO[15] = 0;
objectsAttrLO_start[15] = 0;
objectsAttrHI[15] = 0;
objectsAttrHI_start[15] = 0;

objects[16] = "▫ A Mole ▫";
objectsNoun[16] = 255;
objectsAdjective[16] = 255;
objectsLocation[16] = 252;
objectsLocation_start[16] = 252;
objectsWeight[16] = 1;
objectsWeight_start[16] = 1;
objectsAttrLO[16] = 0;
objectsAttrLO_start[16] = 0;
objectsAttrHI[16] = 0;
objectsAttrHI_start[16] = 0;

objects[17] = "▫ A Basket ▫";
objectsNoun[17] = 41;
objectsAdjective[17] = 255;
objectsLocation[17] = 53;
objectsLocation_start[17] = 53;
objectsWeight[17] = 1;
objectsWeight_start[17] = 1;
objectsAttrLO[17] = 0;
objectsAttrLO_start[17] = 0;
objectsAttrHI[17] = 0;
objectsAttrHI_start[17] = 0;

objects[18] = "▫ A Cheese Plant ▫";
objectsNoun[18] = 42;
objectsAdjective[18] = 255;
objectsLocation[18] = 252;
objectsLocation_start[18] = 252;
objectsWeight[18] = 1;
objectsWeight_start[18] = 1;
objectsAttrLO[18] = 0;
objectsAttrLO_start[18] = 0;
objectsAttrHI[18] = 0;
objectsAttrHI_start[18] = 0;

objects[19] = "▫ A Mole Trap ▫";
objectsNoun[19] = 255;
objectsAdjective[19] = 255;
objectsLocation[19] = 252;
objectsLocation_start[19] = 252;
objectsWeight[19] = 1;
objectsWeight_start[19] = 1;
objectsAttrLO[19] = 0;
objectsAttrLO_start[19] = 0;
objectsAttrHI[19] = 0;
objectsAttrHI_start[19] = 0;

objects[20] = "▫ A Captive Mole ▫";
objectsNoun[20] = 45;
objectsAdjective[20] = 255;
objectsLocation[20] = 252;
objectsLocation_start[20] = 252;
objectsWeight[20] = 1;
objectsWeight_start[20] = 1;
objectsAttrLO[20] = 0;
objectsAttrLO_start[20] = 0;
objectsAttrHI[20] = 0;
objectsAttrHI_start[20] = 0;

objects[21] = "▫ A Book Page ▫";
objectsNoun[21] = 48;
objectsAdjective[21] = 255;
objectsLocation[21] = 252;
objectsLocation_start[21] = 252;
objectsWeight[21] = 1;
objectsWeight_start[21] = 1;
objectsAttrLO[21] = 0;
objectsAttrLO_start[21] = 0;
objectsAttrHI[21] = 0;
objectsAttrHI_start[21] = 0;

objects[22] = "▫ A Helmet ▫";
objectsNoun[22] = 255;
objectsAdjective[22] = 255;
objectsLocation[22] = 252;
objectsLocation_start[22] = 252;
objectsWeight[22] = 1;
objectsWeight_start[22] = 1;
objectsAttrLO[22] = 0;
objectsAttrLO_start[22] = 0;
objectsAttrHI[22] = 0;
objectsAttrHI_start[22] = 0;

objects[23] = "▫ A Handle in the Crank ▫";
objectsNoun[23] = 255;
objectsAdjective[23] = 255;
objectsLocation[23] = 252;
objectsLocation_start[23] = 252;
objectsWeight[23] = 1;
objectsWeight_start[23] = 1;
objectsAttrLO[23] = 0;
objectsAttrLO_start[23] = 0;
objectsAttrHI[23] = 0;
objectsAttrHI_start[23] = 0;

objects[24] = "▫ A Boat ▫";
objectsNoun[24] = 255;
objectsAdjective[24] = 255;
objectsLocation[24] = 252;
objectsLocation_start[24] = 252;
objectsWeight[24] = 1;
objectsWeight_start[24] = 1;
objectsAttrLO[24] = 0;
objectsAttrLO_start[24] = 0;
objectsAttrHI[24] = 0;
objectsAttrHI_start[24] = 0;

objects[25] = "▫ An Empty Tin ▫";
objectsNoun[25] = 60;
objectsAdjective[25] = 255;
objectsLocation[25] = 252;
objectsLocation_start[25] = 252;
objectsWeight[25] = 1;
objectsWeight_start[25] = 1;
objectsAttrLO[25] = 0;
objectsAttrLO_start[25] = 0;
objectsAttrHI[25] = 0;
objectsAttrHI_start[25] = 0;

objects[26] = "▫ A Tin of Tar ▫";
objectsNoun[26] = 255;
objectsAdjective[26] = 255;
objectsLocation[26] = 252;
objectsLocation_start[26] = 252;
objectsWeight[26] = 1;
objectsWeight_start[26] = 1;
objectsAttrLO[26] = 0;
objectsAttrLO_start[26] = 0;
objectsAttrHI[26] = 0;
objectsAttrHI_start[26] = 0;

objects[27] = "▫ A Lantern ▫";
objectsNoun[27] = 63;
objectsAdjective[27] = 255;
objectsLocation[27] = 252;
objectsLocation_start[27] = 252;
objectsWeight[27] = 1;
objectsWeight_start[27] = 1;
objectsAttrLO[27] = 0;
objectsAttrLO_start[27] = 0;
objectsAttrHI[27] = 0;
objectsAttrHI_start[27] = 0;

objects[28] = "▫ A Pair of Climbing Boots ▫";
objectsNoun[28] = 249;
objectsAdjective[28] = 255;
objectsLocation[28] = 252;
objectsLocation_start[28] = 252;
objectsWeight[28] = 1;
objectsWeight_start[28] = 1;
objectsAttrLO[28] = 2;
objectsAttrLO_start[28] = 2;
objectsAttrHI[28] = 0;
objectsAttrHI_start[28] = 0;

objects[29] = "▫ A Boulder ▫";
objectsNoun[29] = 255;
objectsAdjective[29] = 255;
objectsLocation[29] = 252;
objectsLocation_start[29] = 252;
objectsWeight[29] = 1;
objectsWeight_start[29] = 1;
objectsAttrLO[29] = 0;
objectsAttrLO_start[29] = 0;
objectsAttrHI[29] = 0;
objectsAttrHI_start[29] = 0;

objects[30] = "▫ A Lantern ▫";
objectsNoun[30] = 255;
objectsAdjective[30] = 255;
objectsLocation[30] = 252;
objectsLocation_start[30] = 252;
objectsWeight[30] = 1;
objectsWeight_start[30] = 1;
objectsAttrLO[30] = 0;
objectsAttrLO_start[30] = 0;
objectsAttrHI[30] = 0;
objectsAttrHI_start[30] = 0;

objects[31] = "▫ A Ghostly Warrior ▫";
objectsNoun[31] = 255;
objectsAdjective[31] = 255;
objectsLocation[31] = 252;
objectsLocation_start[31] = 252;
objectsWeight[31] = 1;
objectsWeight_start[31] = 1;
objectsAttrLO[31] = 0;
objectsAttrLO_start[31] = 0;
objectsAttrHI[31] = 0;
objectsAttrHI_start[31] = 0;

objects[32] = "▫ A Dead Warrior ▫";
objectsNoun[32] = 255;
objectsAdjective[32] = 255;
objectsLocation[32] = 252;
objectsLocation_start[32] = 252;
objectsWeight[32] = 1;
objectsWeight_start[32] = 1;
objectsAttrLO[32] = 0;
objectsAttrLO_start[32] = 0;
objectsAttrHI[32] = 0;
objectsAttrHI_start[32] = 0;

objects[33] = "▫ A Large Sword ▫";
objectsNoun[33] = 255;
objectsAdjective[33] = 255;
objectsLocation[33] = 252;
objectsLocation_start[33] = 252;
objectsWeight[33] = 1;
objectsWeight_start[33] = 1;
objectsAttrLO[33] = 0;
objectsAttrLO_start[33] = 0;
objectsAttrHI[33] = 0;
objectsAttrHI_start[33] = 0;

objects[34] = "▫ A Pole ▫";
objectsNoun[34] = 74;
objectsAdjective[34] = 255;
objectsLocation[34] = 252;
objectsLocation_start[34] = 252;
objectsWeight[34] = 1;
objectsWeight_start[34] = 1;
objectsAttrLO[34] = 0;
objectsAttrLO_start[34] = 0;
objectsAttrHI[34] = 0;
objectsAttrHI_start[34] = 0;

objects[35] = "▫ A Witch ▫";
objectsNoun[35] = 83;
objectsAdjective[35] = 255;
objectsLocation[35] = 252;
objectsLocation_start[35] = 252;
objectsWeight[35] = 1;
objectsWeight_start[35] = 1;
objectsAttrLO[35] = 0;
objectsAttrLO_start[35] = 0;
objectsAttrHI[35] = 0;
objectsAttrHI_start[35] = 0;

objects[36] = "▫ A Pile of Sand ▫";
objectsNoun[36] = 255;
objectsAdjective[36] = 255;
objectsLocation[36] = 73;
objectsLocation_start[36] = 73;
objectsWeight[36] = 1;
objectsWeight_start[36] = 1;
objectsAttrLO[36] = 0;
objectsAttrLO_start[36] = 0;
objectsAttrHI[36] = 0;
objectsAttrHI_start[36] = 0;

objects[37] = "▫ A Large, Smooth Slab of Rock ▫";
objectsNoun[37] = 255;
objectsAdjective[37] = 255;
objectsLocation[37] = 54;
objectsLocation_start[37] = 54;
objectsWeight[37] = 1;
objectsWeight_start[37] = 1;
objectsAttrLO[37] = 0;
objectsAttrLO_start[37] = 0;
objectsAttrHI[37] = 0;
objectsAttrHI_start[37] = 0;

objects[38] = "▫ A Small Lever ▫";
objectsNoun[38] = 255;
objectsAdjective[38] = 255;
objectsLocation[38] = 252;
objectsLocation_start[38] = 252;
objectsWeight[38] = 1;
objectsWeight_start[38] = 1;
objectsAttrLO[38] = 0;
objectsAttrLO_start[38] = 0;
objectsAttrHI[38] = 0;
objectsAttrHI_start[38] = 0;

objects[39] = "";
objectsNoun[39] = 255;
objectsAdjective[39] = 255;
objectsLocation[39] = 252;
objectsLocation_start[39] = 252;
objectsWeight[39] = 1;
objectsWeight_start[39] = 1;
objectsAttrLO[39] = 0;
objectsAttrLO_start[39] = 0;
objectsAttrHI[39] = 0;
objectsAttrHI_start[39] = 0;

objects[40] = "▫ A Caved in Exit ▫";
objectsNoun[40] = 255;
objectsAdjective[40] = 255;
objectsLocation[40] = 44;
objectsLocation_start[40] = 44;
objectsWeight[40] = 1;
objectsWeight_start[40] = 1;
objectsAttrLO[40] = 0;
objectsAttrLO_start[40] = 0;
objectsAttrHI[40] = 0;
objectsAttrHI_start[40] = 0;

objects[41] = "▫ An Excavated Tunnel ▫";
objectsNoun[41] = 255;
objectsAdjective[41] = 255;
objectsLocation[41] = 252;
objectsLocation_start[41] = 252;
objectsWeight[41] = 1;
objectsWeight_start[41] = 1;
objectsAttrLO[41] = 0;
objectsAttrLO_start[41] = 0;
objectsAttrHI[41] = 0;
objectsAttrHI_start[41] = 0;

objects[42] = "▫ A Beach Ball ▫";
objectsNoun[42] = 90;
objectsAdjective[42] = 255;
objectsLocation[42] = 252;
objectsLocation_start[42] = 252;
objectsWeight[42] = 1;
objectsWeight_start[42] = 1;
objectsAttrLO[42] = 0;
objectsAttrLO_start[42] = 0;
objectsAttrHI[42] = 0;
objectsAttrHI_start[42] = 0;

objects[43] = "▫ A Paperweight ▫";
objectsNoun[43] = 94;
objectsAdjective[43] = 255;
objectsLocation[43] = 252;
objectsLocation_start[43] = 252;
objectsWeight[43] = 1;
objectsWeight_start[43] = 1;
objectsAttrLO[43] = 0;
objectsAttrLO_start[43] = 0;
objectsAttrHI[43] = 0;
objectsAttrHI_start[43] = 0;

objects[44] = "▫ A packet of cigarettes ▫";
objectsNoun[44] = 95;
objectsAdjective[44] = 255;
objectsLocation[44] = 252;
objectsLocation_start[44] = 252;
objectsWeight[44] = 1;
objectsWeight_start[44] = 1;
objectsAttrLO[44] = 0;
objectsAttrLO_start[44] = 0;
objectsAttrHI[44] = 0;
objectsAttrHI_start[44] = 0;

objects[45] = "▫ A length of chain ▫";
objectsNoun[45] = 98;
objectsAdjective[45] = 255;
objectsLocation[45] = 252;
objectsLocation_start[45] = 252;
objectsWeight[45] = 1;
objectsWeight_start[45] = 1;
objectsAttrLO[45] = 0;
objectsAttrLO_start[45] = 0;
objectsAttrHI[45] = 0;
objectsAttrHI_start[45] = 0;

objects[46] = "▫ A man ▫";
objectsNoun[46] = 255;
objectsAdjective[46] = 255;
objectsLocation[46] = 50;
objectsLocation_start[46] = 50;
objectsWeight[46] = 1;
objectsWeight_start[46] = 1;
objectsAttrLO[46] = 0;
objectsAttrLO_start[46] = 0;
objectsAttrHI[46] = 0;
objectsAttrHI_start[46] = 0;

objects[47] = "▫ A puddle ▫";
objectsNoun[47] = 255;
objectsAdjective[47] = 255;
objectsLocation[47] = 54;
objectsLocation_start[47] = 54;
objectsWeight[47] = 1;
objectsWeight_start[47] = 1;
objectsAttrLO[47] = 0;
objectsAttrLO_start[47] = 0;
objectsAttrHI[47] = 0;
objectsAttrHI_start[47] = 0;

objects[48] = "▫ A trapdoor ▫";
objectsNoun[48] = 255;
objectsAdjective[48] = 255;
objectsLocation[48] = 252;
objectsLocation_start[48] = 252;
objectsWeight[48] = 1;
objectsWeight_start[48] = 1;
objectsAttrLO[48] = 0;
objectsAttrLO_start[48] = 0;
objectsAttrHI[48] = 0;
objectsAttrHI_start[48] = 0;

objects[49] = "▫ An entrance down ▫";
objectsNoun[49] = 255;
objectsAdjective[49] = 255;
objectsLocation[49] = 252;
objectsLocation_start[49] = 252;
objectsWeight[49] = 1;
objectsWeight_start[49] = 1;
objectsAttrLO[49] = 0;
objectsAttrLO_start[49] = 0;
objectsAttrHI[49] = 0;
objectsAttrHI_start[49] = 0;

objects[50] = "▫ A tin of water ▫";
objectsNoun[50] = 255;
objectsAdjective[50] = 255;
objectsLocation[50] = 252;
objectsLocation_start[50] = 252;
objectsWeight[50] = 1;
objectsWeight_start[50] = 1;
objectsAttrLO[50] = 0;
objectsAttrLO_start[50] = 0;
objectsAttrHI[50] = 0;
objectsAttrHI_start[50] = 0;

objects[51] = "▫ A screwdriver ▫";
objectsNoun[51] = 117;
objectsAdjective[51] = 255;
objectsLocation[51] = 252;
objectsLocation_start[51] = 252;
objectsWeight[51] = 1;
objectsWeight_start[51] = 1;
objectsAttrLO[51] = 0;
objectsAttrLO_start[51] = 0;
objectsAttrHI[51] = 0;
objectsAttrHI_start[51] = 0;

objects[52] = "▫ a small magnet ▫";
objectsNoun[52] = 118;
objectsAdjective[52] = 255;
objectsLocation[52] = 252;
objectsLocation_start[52] = 252;
objectsWeight[52] = 1;
objectsWeight_start[52] = 1;
objectsAttrLO[52] = 0;
objectsAttrLO_start[52] = 0;
objectsAttrHI[52] = 0;
objectsAttrHI_start[52] = 0;

objects[53] = "▫ A Grandfather clock ▫";
objectsNoun[53] = 255;
objectsAdjective[53] = 255;
objectsLocation[53] = 57;
objectsLocation_start[53] = 57;
objectsWeight[53] = 1;
objectsWeight_start[53] = 1;
objectsAttrLO[53] = 0;
objectsAttrLO_start[53] = 0;
objectsAttrHI[53] = 0;
objectsAttrHI_start[53] = 0;

objects[54] = "▫ a clock face ▫";
objectsNoun[54] = 115;
objectsAdjective[54] = 255;
objectsLocation[54] = 252;
objectsLocation_start[54] = 252;
objectsWeight[54] = 1;
objectsWeight_start[54] = 1;
objectsAttrLO[54] = 0;
objectsAttrLO_start[54] = 0;
objectsAttrHI[54] = 0;
objectsAttrHI_start[54] = 0;

objects[55] = "▫ a plate stuck to a magnet ▫";
objectsNoun[55] = 255;
objectsAdjective[55] = 255;
objectsLocation[55] = 252;
objectsLocation_start[55] = 252;
objectsWeight[55] = 1;
objectsWeight_start[55] = 1;
objectsAttrLO[55] = 0;
objectsAttrLO_start[55] = 0;
objectsAttrHI[55] = 0;
objectsAttrHI_start[55] = 0;

objects[56] = "▫ A key in the clock ▫";
objectsNoun[56] = 255;
objectsAdjective[56] = 255;
objectsLocation[56] = 252;
objectsLocation_start[56] = 252;
objectsWeight[56] = 1;
objectsWeight_start[56] = 1;
objectsAttrLO[56] = 0;
objectsAttrLO_start[56] = 0;
objectsAttrHI[56] = 0;
objectsAttrHI_start[56] = 0;

objects[57] = "▫ A button on the wall ▫";
objectsNoun[57] = 255;
objectsAdjective[57] = 255;
objectsLocation[57] = 69;
objectsLocation_start[57] = 69;
objectsWeight[57] = 1;
objectsWeight_start[57] = 1;
objectsAttrLO[57] = 0;
objectsAttrLO_start[57] = 0;
objectsAttrHI[57] = 0;
objectsAttrHI_start[57] = 0;

objects[58] = "▫ A rusty bucket ▫\n\n\n\n\n\n";
objectsNoun[58] = 13;
objectsAdjective[58] = 255;
objectsLocation[58] = 252;
objectsLocation_start[58] = 252;
objectsWeight[58] = 1;
objectsWeight_start[58] = 1;
objectsAttrLO[58] = 0;
objectsAttrLO_start[58] = 0;
objectsAttrHI[58] = 0;
objectsAttrHI_start[58] = 0;

last_object_number =  58; 
carried_objects = 1;
total_object_messages=59;

